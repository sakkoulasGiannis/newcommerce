<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id');
            $table->integer('erp_id')->nullable();
            $table->string('title')->required();
            $table->string("name")->required()->index();
            $table->boolean('active')->default(true);
            $table->boolean('hidden')->default(true);
            $table->text("description")->nullable();
            $table->string('meta_title')->nullable();
            $table->text("meta_description")->nullable();
            $table->integer("order")->default(0);
            $table->boolean('dynamic')->default(0);
            $table->boolean('exclude_skroutz')->default(0);
            $table->boolean('exclude_bestprice')->default(0);

            $table->json('query')->nullable();

            $table->timestamps();
        });

        Schema::create("product_category", function (Blueprint $table) {
            $table->foreignId('product_id')->constrained()->onDelete('cascade');
            $table->integer("category_id")->unsigned()->index();
            $table->foreign("category_id")->references("id")->on('categories')->onDelete("cascade");
            $table->primary(['product_id', 'category_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_category');
        Schema::dropIfExists('categories');
    }
}
