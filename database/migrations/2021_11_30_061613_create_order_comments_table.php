<?php
	
	use Illuminate\Database\Migrations\Migration;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Support\Facades\Schema;
	
	class CreateOrderCommentsTable extends Migration
	{
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up()
		{
			Schema::create('order_comments', function (Blueprint $table) {
				$table->id();
				$table->integer('order_id')->unsigned();
				$table->string('user_role', 150)->default('user');
				$table->string('user_name', 150)->default('user');
				$table->mediumText('comment')->nullable();
				$table->timestamps();
			});
		}
		
		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down()
		{
			Schema::dropIfExists('order_comments');
		}
	}
