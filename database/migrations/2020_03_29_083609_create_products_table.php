<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();

            $table->string('title')->required();
            $table->string('name')->unique()->index();
            $table->text('description')->nullable();
            $table->text('small_description')->nullable();
            $table->string('sku')->unique()->index();
            $table->string('product_type')->default('simple');
            $table->float('price');
            $table->float('sale_price')->nullable();
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->integer('rule_id')->nullable();
            $table->integer('sort')->default(0);
            $table->integer('group')->unsigned()->index();
            $table->boolean('is_featured')->nullable();
            $table->boolean('is_active')->default(0);
            $table->boolean('has_calculator')->default(0);
            $table->integer('tax_class_id');
            $table->integer('weight')->nullable();
            $table->integer('length')->nullable();
            $table->integer('width')->nullable();
            $table->integer('height')->nullable();

            $table->json('schema_markup')->nullable();
            $table->dateTime('new_from')->nullable();
            $table->dateTime('new_to')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
