<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('redirects', function (Blueprint $table) {
            $table->string('sku')->nullable()->after('product_id');
            $table->string('type')->nullable()->after('sku'); // category or product
            $table->integer('item_id')->nullable()->after('product_id');
            $table->dropForeign(['product_id']);
            $table->dropColumn('product_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('redirects', function (Blueprint $table) {
            $table->dropColumn('sku');
            $table->dropColumn('type');
            $table->dropColumn('item_id');
            $table->integer('product_id')->nullable();
        });
    }
};
