<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToOrderDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_details', function (Blueprint $table) {
            $table->text('billing_email')->after('sub_total')->nullable();
            $table->text('shipping_email')->after('billing_mobile')->nullable();
            $table->text('invoice_status')->after('invoice_id')->nullable();
            $table->text('courier_name')->after('shipping_id')->nullable();
            $table->text('payment_name')->after('payment_id')->nullable();
            $table->text('afm')->after('shipping_lastname')->nullable();
            $table->text('doy')->after('afm')->nullable();
            $table->text('company')->after('doy')->nullable();
            $table->text('activity')->after('company')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_details', function (Blueprint $table) {
            $table->dropColumn('billing_email');
            $table->dropColumn('shipping_email');
            $table->dropColumn('invoice_status');
            $table->dropColumn('courier_name');
            $table->dropColumn('payment_name');
            $table->dropColumn('afm');
            $table->dropColumn('doy');
            $table->dropColumn('company');
            $table->dropColumn('activity');
        });
    }
}
