<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('attribute_terms', function (Blueprint $table) {
            $table->id();
            $table->foreignId('attribute_id')->constrained()->onDelete('cascade');
            $table->integer('erp_id')->nullable();
            $table->string('title');
            $table->string('name');
//            $table->string('type')->default('text');
            $table->integer('term_order')->default(0);
//            $table->boolean('active')->default(0);
            $table->string('color')->nullable();
            $table->string('image')->nullable();
            $table->unique(['attribute_id', 'name']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('attribute_terms');
    }
};
