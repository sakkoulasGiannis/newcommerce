<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeToStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stocks', function (Blueprint $table) {
            $table->string('model_type')->after('barcode')->required();
            $table->integer('model_id')->after('model_type')->required(); // the id of the selected field
            $table->string('model_title')->after('model_id')->required(); // the id of the selected field
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stocks', function (Blueprint $table) {
            $table->dropColumn('model_type');
            $table->dropColumn('model_id');
            $table->dropColumn('model_title');
        });
    }
}
