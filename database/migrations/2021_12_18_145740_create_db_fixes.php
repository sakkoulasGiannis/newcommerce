<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDbFixes extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::table('order_vouchers', function (Blueprint $table) {
			$table->integer('order_detail_id')->nullable()->after('id');
		});

		Schema::table('order_details', function (Blueprint $table) {
			$table->integer('shipping_cost')->nullable()->after('sub_total');
		});
		Schema::table('order_details', function (Blueprint $table) {
			$table->integer('payment_cost')->nullable()->after('sub_total');
		});


		Schema::table('couriers', function (Blueprint $table) {
			$table->integer('payment_cost')->nullable()->after('price');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

		if (Schema::hasColumn('order_vouchers', 'order_detail_id')) {
			Schema::table('order_vouchers', function (Blueprint $table) {
				$table->dropColumn('order_detail_id');
			});
		}
		if (Schema::hasColumn('order_details', 'shipping_cost')) {
			Schema::table('order_details', function (Blueprint $table) {
				$table->dropColumn('shipping_cost');
			});
		}
		if (Schema::hasColumn('order_details', 'payment_cost')) {
			Schema::table('order_details', function (Blueprint $table) {
				$table->dropColumn('payment_cost');
			});
		}
		if (Schema::hasColumn('couriers', 'payment_cost')) {
			Schema::table('couriers', function (Blueprint $table) {
				$table->dropColumn('payment_cost');
			});
		}
	}
}
