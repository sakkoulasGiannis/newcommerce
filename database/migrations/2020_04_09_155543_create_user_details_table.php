<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()->onDelete('cascade');
            $table->integer('language_id')->default(1);
            $table->integer('billing_country_id');
            $table->string('billing_region')->required();
            $table->string('billing_city')->required();
            $table->string('billing_street')->required();
            $table->string('billing_zip')->required();
            $table->string('billing_street_number')->required();
            $table->string('billing_phone')->required();
            $table->string('billing_mobile')->required();
            $table->integer('shipping_country_id');
            $table->string('shipping_region')->required();
            $table->string('shipping_city')->required();
            $table->string('shipping_street')->required();
            $table->string('shipping_zip')->required();
            $table->string('shipping_street_number')->required();
            $table->string('shipping_phone')->required();
            $table->string('shipping_mobile')->required();
            $table->string('shipping_name')->required();
            $table->string('shipping_lastname')->required();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }
}
