<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('countries', function (Blueprint $table) {
			$table->id();
			$table->string('title')->required();
			$table->string('iso')->unique();
			$table->boolean('is_active')->default(0);
			$table->string('currency')->nullable();
			$table->string('currency_title')->nullable();
			$table->json('payment_rules')->nullable();
            $table->foreignId('continent_id')->constrained();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('countries');
	}
}
