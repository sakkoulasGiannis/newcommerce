<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('carts', function (Blueprint $table) {
            $table->decimal('shipping_extra', 10, 2)->default(0)->after('voucher_id');
            $table->decimal('payment_extra', 10, 2)->default(0)->after('voucher_id');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('carts', function (Blueprint $table) {
            $table->dropColumn('shipping_extra');
            $table->dropColumn('payment_extra');
        });
    }
};
