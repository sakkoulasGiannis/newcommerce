<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fields', function (Blueprint $table) {
            $table->id();
            $table->string('title')->required();
            $table->string('name')->required();
            $table->string('type')->default('text');
            $table->string('erp_id')->nullable();
            $table->string('model_type')->required();  // add new field model
            $table->integer('positions')->default(0); // all postions
            $table->string('position', 100)->default('product');
            $table->boolean('can_select_multiple')->default(0);  // add new field model
            $table->boolean('is_read_only')->default(0);  // add new field model
            $table->boolean('is_enabled')->default(1);  // add new field model
            $table->boolean('has_images')->default(0);  // add new field model
            $table->boolean('has_colors')->default(0);  // add new field model
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fields');
    }
}
