<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMegaMenuToCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->boolean('mega_menu_is_active')->default(0)->after('meta_description');
            $table->boolean('dynamic_query_is_active')->default(0)->after('mega_menu_is_active');
            $table->text('mega_menu_content')->after('dynamic_query_is_active')->nullable();
            $table->text('dynamic_query_content')->after('mega_menu_content')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('mega_menu_is_active');
            $table->dropColumn('dynamic_query_is_active');
            $table->dropColumn('mega_menu_content');
            $table->dropColumn('dynamic_query_content');

        });
    }
}
