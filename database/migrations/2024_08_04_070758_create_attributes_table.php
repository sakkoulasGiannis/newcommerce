<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('attributes', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->integer('erp_id')->nullable();
            $table->string('name')->unique();
            $table->string('order_by')->default('text');
            $table->string('type')->default('text');
            $table->integer('order')->default(0);
            $table->boolean('is_variable')->default(0);  // add new field model
            $table->boolean('visible_on_product_page')->default(1);
            $table->boolean('visible_on_product_listing')->default(1);
            $table->boolean('used_for_variations')->default(0);
            $table->boolean('can_select_multiple')->default(0);  // add new field model
            $table->boolean('is_read_only')->default(0);  // add new field model
            $table->boolean('is_enabled')->default(1);  // add new field model
            $table->boolean('has_images')->default(0);  // add new field model
            $table->boolean('has_colors')->default(0);  // add new field model
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('attributes');
    }
};
