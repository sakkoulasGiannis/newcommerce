<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vouchers', function (Blueprint $table) {
            $table->bigIncrements('id');
            // The voucher code
            $table->string('code')->nullable();
            // The human readable voucher code name
            $table->string('name')->unique()->required()->index();
            // The description of the voucher - Not necessary
            $table->text('description')->nullable();
            // The number of uses currently
            $table->integer('uses')->default(0);
            // The max uses this voucher has
            $table->integer('max_uses')->default(1);
            // How many times a user can use this voucher.
            $table->integer('max_uses_user')->default(1);
            // The type can be: voucher, discount, sale. What ever you want.
            $table->tinyInteger('type')->unsigned();
            // The amount to discount by (in pennies) in this example.
            $table->integer('discount_amount')->nullable();
            // Whether or not the voucher is a percentage or a fixed price.
            $table->boolean('is_fixed')->default(true);
            $table->json("query")->nullable();

            // When the voucher begins
            $table->softDeletes();
            $table->timestamp('starts_at')->useCurrent();
            // When the voucher ends
            $table->timestamp('expires_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vouchers');
    }
}
