<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountiesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('counties', function (Blueprint $table) {
			$table->id();
			$table->string('title')->required();
			$table->foreignId('country_id')->constrained()->onDelete('cascade');
			$table->boolean('is_active')->default(0);
            $table->integer('order')->default(0);
			$table->timestamps();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('counties');
	}
}
