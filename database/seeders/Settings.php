<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class Settings extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $settings = [
            [
                'title' => 'Company Logo',
                'key' => 'logo',
                'value' => 'images/logo.png',
                'group' => 'company',
            ], [
                'title' => 'AlphaBank Settings',
                'key' => 'alphabank_credit_card',
                'value' => json_encode([
                    "alphabank_active" => false,
                    "alphabank_test_env" => true,
                    "alphabank_mid" => null,
                    "alphabank_secret" => null,
                    "alphabank_pre_authorize" => false,
                    "alphabank_max_installments" => 0,
                    "alphabank_installments" => null
                ]),
                'group' => 'payments',
            ], [
                'title' => 'Shipping And Payment  rule',
                'key' => 'shipping_payment_rule',
                "value" => '[{"countries":{"ids":[1],"counties":[{"ids":[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51],"cart_rules":[{"operator":"or","rules":[{"cart":{"operator":"or","min_price":"0","max_price":null,"method":">"}},{"shippings":{"ids":[1,2,4],"payments":[{"ids":3,"extra_price":"2","county_id":null}]}},{"shippings":{"ids":[],"payments":[]}}]}]}]}}]',
                'group' => 'shipping_payment',
            ]
        ];

        foreach ($settings as $setting) {
            \App\Models\Setting::create($setting);
        }

    }
}
