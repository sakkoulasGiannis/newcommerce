<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run() {

//        $faker = Faker::create();
		//        for ($a = 1; $a <= 500; $a++) {
		//            DB::table('products')->insert([
		//                'title' => json_encode(['el'=>$faker->realText(30), 'en'=>$faker->realText(30)]),
		//                'product_type' => 'Dynamic',
		//                'name' => $faker->slug . rand(11110, 100000010),
		//                'sku' => $faker->realText(10) . rand(111110, 1000111100),
		//                'is_active' => 1,
		//                'tax_class_id' => 1,
		//                'price' => $faker->randomNumber(2),
		//                'description' => $faker->paragraph(),
		//                'group'=>1
		//            ]);
		//        }
		$this->call([
			Roles::class,
			Permissions::class,
//			Categories::class,
//			Products::class,
			Admins::class,
			Languages::class,
			Countries::class,
//            Fields::class,
            Payments::class,
            Couriers::class,
            Taxes::class,
            Warehouses::class,
		]);
		// \App\Models\User::factory(10)->create();
	}
}
