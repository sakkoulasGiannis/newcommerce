<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use App\Models\Field;
use App\Models\StockVariation;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Schema;
use Faker\Factory as Faker;


class Eshoes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        //demo products 

        for ($a = 1; $a <= 2; $a++) {
        $faker = Faker::create();
        
            $sku = $faker->realText(10) . rand(111110, 1000111100);
            DB::table('products')->insert([
                'title' => json_encode(['el'=>$faker->realText(30), 'en'=>$faker->realText(30)]),
                'product_type' => 'Dynamic',
                'name' => $faker->slug . rand(11110, 100000010),
                'sku' => $sku,
                'is_active' => 1,
                'tax_class_id' => 1,
                'price' => $faker->numberBetween(10, 100),
                'description' => json_encode(['el'=>$faker->paragraph(), 'en'=>$faker->paragraph()]),
                'group'=>1
            ]);

            $product = Product::where('sku', $sku)->first();
//variables 
            $attributes  = [
  "attribute_id"=> 1,
  "attributes"=> [
    "brand"=> $faker->realText(15),
    "color"=> $faker->realText(15),
    "season"=> $faker->realText(15),
    "type"=> $faker->realText(15),
    "heelheight"=> rand(1, 100),
    "material"=> $faker->realText(15)
]
];
 

            foreach($attributes['attributes'] as $key => $a){
                $model = "\\App\\Fields\\Field".ucfirst($key);
                if($value = $model::where('name', str_slug($a, '-'))->first() ){
                    $product->$key()->sync([$value->id]);
                }else{
                    $value = $model::create(['title' => $a, 'name'=>str_slug($a, '-')]);
                    $product->$key()->sync([$value->id]);
                }
            }

// stock 
            $createdStock = $product->stock()->updateOrCreate(
                    ['sku' => $sku, 'product_id'=> $product->id],
                    [
                        'sku'=>$sku,
                        'ean'=> $faker->realText(15),
                        'barcode'=> $faker->realText(15),
                        'price'=> null,
                        'quantity'=> 2
                    ]
                );


                    $stock = [
                        "variations"=> [    
                              "name"=> "size",
                              "value"=> "one size"
                          ]
                      ];

                    foreach ($stock as  $vkey => $variation) {
                        
                        $model = "\\App\\Fields\\Field".ucfirst($variation['name']);
                        $fieldValue = $model::where('name', str_slug($variation['value']))->first();
                        if($fieldValue == null){
                            $fieldValue = $model::create(['title' => $variation['value'], 'name'=>str_slug($variation['value'], '-')]);
                        }


                        $field = Field::where('name', $variation['name'])->first();
                        StockVariation::create([
                            'stock_id'=>$createdStock->id,
                            'field_id'=>$field->id,
                            'model_type' => "\App\Fields\Field".ucFirst($variation['name']),
                            'model_id'=> $fieldValue->id
                        ]);

                    }






        }

    }
}
