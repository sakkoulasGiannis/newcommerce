<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Permissions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            'name' => 'Manage Users',
            'guard_name' => 'web',
            'created_at' => Carbon::now(),
        ]);

        DB::table('permissions')->insert([
            'name' => 'Create Fields',
            'guard_name' => 'web',
            'created_at' => Carbon::now(),
        ]);

        DB::table('permissions')->insert([
            'name' => 'Delete products',
            'guard_name' => 'web',
            'created_at' => Carbon::now(),
        ]);

        DB::table('permissions')->insert([
            'name' => 'View products',
            'guard_name' => 'web',
            'created_at' => Carbon::now(),
        ]);

        DB::table('permissions')->insert([
            'name' => 'View Orders',
            'guard_name' => 'web',
            'created_at' => Carbon::now(),
        ]);
    }
}
