<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Languages extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->insert([
            'title' => 'el',
            'name' => 'el',
            'created_at' => Carbon::now(),
        ]);

        DB::table('languages')->insert([
            'title' => 'en',
            'name' => 'en',
            'created_at' => Carbon::now(),
        ]);
    }
}
