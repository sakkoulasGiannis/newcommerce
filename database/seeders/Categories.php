<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Categories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        
            $categories = [
[1, 0,  32, json_encode(['el'=> 'Γυναικεία', 'en'=>'Γυναικεία' ]),'gynaikeia-papoutsia',  1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[2, 1,  33, json_encode(['el'=> 'Γόβες', 'en'=> 'Γόβες']),    'gobes',    1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[3, 1,  34, json_encode(['el'=> 'Πέδιλα', 'en'=> 'Πέδιλα']),  'pedila',   1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[4, 1,  36, json_encode(['el'=> 'Μπαλαρίνες', 'en'=> 'Μπαλαρίνες']),  'mpalarines',   1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[5, 1,  37, json_encode(['el'=> 'Casual', 'en'=> 'Casual']),  'casual',   1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[6, 1,  38, json_encode(['el'=> 'Σαγιονάρες', 'en'=> 'Σαγιονάρες']),  'sagionares',   1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[7, 1,  181,    json_encode(['el'=> 'Πλατφόρμες', 'en'=> 'Πλατφόρμες']),  'platformes',   1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[8, 1,  40, json_encode(['el'=> 'Μπότες', 'en'=> 'Μπότες']),  'mpotes',   1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[9, 1,  41, json_encode(['el'=> 'Μποτάκια', 'en'=> 'Μποτάκια']),  'mpotakia', 1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[10,1,  55,json_encode(['el'=> 'Νυφικά', 'en'=> 'Νυφικά']),   'nyfika',   1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[11,    1,  56,json_encode(['el'=> 'Μοκασίνια', 'en'=> 'Μοκασίνια']), 'gynaikeia-mokasinia',  1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[12,    1,  57,json_encode(['el'=> 'Παντόφλες', 'en'=> 'Παντόφλες']), 'gynaikeies-pantofles', 1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[13,    1,  93,json_encode(['el'=> 'Σανδάλια', 'en'=> 'Σανδάλια']),   'sandalia', 1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[14,    1,  100,json_encode(['el'=> 'Αθλητικά', 'en'=> 'Αθλητικά']),  'athlitika',    1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[15,    1,  103,    json_encode(['el'=> 'Γαλότσες', 'en'=> 'Γαλότσες']),  'galotses', 1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[16,    1,  182,    json_encode(['el'=> 'Εσπαντρίγιες', 'en'=> 'Εσπαντρίγιες']),  'espantrigies', 1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[17,    1,  161,    json_encode(['el'=> 'Sneakers - Αθλητικά' ,'en' => 'Sneakers Αθλητικά' ]),'sneakers', 1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[18,    1,  198,    json_encode(['el'=> 'Oxfords - Slip On' , 'en'=> 'Oxfords - OnSlip'  ])   ,    'oxfords-slip-on',  1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[19,    1,  241,    json_encode(['el'=> 'Mules', 'en'=> 'Mules']),    'mules-241',    1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[20,    0,  42, json_encode(['el'=> 'Ανδρικά', 'en'=> 'Ανδρικά']),    'andrika-papoutsia',    1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[21,    20, 43, json_encode(['el'=> 'Μποτάκια', 'en'=> 'Μποτάκια']),  'mpotakia', 1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[22,    20, 44, json_encode(['el'=> 'Μπότες', 'en'=> 'Μπότες']),  'mpotes',   1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[23,    20, 45, json_encode(['el'=> 'Μοκασίνια - Loafers', 'en'=>'Μοκασίν Loafers']) ,    'mokasinia',    1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[24,    20, 46, json_encode(['el'=> 'Σανδάλια', 'en'=> 'Σανδάλια']),  'sandalia', 1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],

[25,    20, 47, json_encode(['el'=> 'Casual', 'en'=> 'Casual']),  'casual', 1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[26,    20, 48, json_encode(['el'=> 'Αθλητικά', 'en'=> 'Αθλητικά']), 'sandalia-up-to-60',    1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[148,   0,  202,    json_encode(['el'=> 'Luxury', 'en'=> 'Luxury']),  'luxury',   1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[149,   148,    203,    json_encode(['el'=> 'Γυναικεία Παπούτσια' , 'en'=> 'Γυναικεία Παπούτσια']) ,    'luxury-gynaikeia-papoutsia',   1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[150,   149,    216,    json_encode(['el'=> 'Πέδιλα', 'en'=> 'Πέδιλα']),  'pedila-216',   1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[151,   149,    217,    json_encode(['el'=> 'Σανδάλια', 'en'=> 'Σανδάλια']),  'sandalia-217', 1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[152,   149,    218,    json_encode(['el'=> 'Σαγιονάρες', 'en'=> 'Σαγιονάρες']),  'sagionarew-218',   1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[153,   149,    219,    json_encode(['el'=> 'Εσπαντρίγιες', 'en'=> 'Εσπαντρίγιες']),  'espantrigiew-219', 1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[154,   149,    220,    json_encode(['el'=> 'Sneakers', 'en'=> 'Sneakers']),  'sneakers-220', 1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[155,   149,    221,    json_encode(['el'=> 'Γόβες', 'en'=> 'Γόβες']),    'gobew-221',    1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[156,   149,    222,    json_encode(['el'=> 'Oxford - Slip On', 'en'=> 'Oxford -  OnSlip' ]) ,  'oxford-slip-on-222',   1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[157,   149,    223,    json_encode(['el'=> 'Μπότες - Μποτάκια' , 'en' => 'Μπότες - Μποτάκια']) ,    'mpotew-mpotakia-223',  1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[158,   149,    235,    json_encode(['el'=> 'Πλατφόρμες', 'en'=> 'Πλατφόρμες']),  'platformew-235',   1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[159,   149,    236,    json_encode(['el'=> 'Μπαλαρίνες', 'en'=> 'Μπαλαρίνες']),  'mpalarinew-236',   1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[160,   148,    211,    json_encode(['el'=> 'Ανδρικά Παπούτσια' , 'en'=>'Ανδρικά Παπούτσια' ]) , 'luxury-andrika-papoutsia', 1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[161,   160,    225,    json_encode(['el'=> 'Sneakers', 'en'=> 'Sneakers']),  'sneakers-225', 1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[162,   160,    226,    json_encode(['el'=> 'Μοκασίνια - Loafers' , 'en'=> 'Μοκασίν Loafers' ]) ,'mokasinia-loafers-226',    1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[163,   160,    227,    json_encode(['el'=> 'Σαγιονάρες - Παντόφλες' , 'en' =>'Σαγιονάρ Παντόφλες']) ,  'sagionarew-227',   1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[164,   160,    228,    json_encode(['el'=> 'Μποτάκια', 'en'=> 'Μποτάκια']),  'mpotakia-228', 1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[165,   148,    212,    json_encode(['el'=> 'Τσάντες - Πορτοφόλια' , 'en'=>'Τσάντ Πορτοφόλια']) ,  'luxury-tsantes-portofolia',    1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[166,   165,    229,    json_encode(['el'=> 'Σακίδια Πλάτης - BackPack', 'en'=>'Σακίδια Πλάτ BackPack']) ,    'sakidia-plathw-backpack-229',  1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[167,   165,    230,    json_encode(['el'=> 'Τσάντες ώμου' , 'en'=> 'Τσάντες ώμου']) , 'tsantew-vmoy-230', 1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[168,   165,    231,    json_encode(['el'=> 'Τσάντες Χειρός' , 'en'=> 'Τσάντες Χειρός']) , 'tsantew-xeirow-231',   1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[169,   165,    232,    json_encode(['el'=> 'Φάκελοι - Clutch' , 'en'=> 'Φάκελοι - Clutch']) ,  'fakeloi-clutch-232',   1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[170,   165,    233,    json_encode(['el'=> 'Πορτοφόλια', 'en'=> 'Πορτοφόλια']),  'portofolia-233',   1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[171,   165,    238,    json_encode(['el'=> 'Μπρελόκ', 'en'=> 'Μπρελόκ']),    'mprelok-238',  1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[172,   165,    243,    json_encode(['el'=> 'Ζώνες', 'en'=> 'Ζώνες']),    'zvnew-243',    1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47'],
[173,   165,    257,    json_encode(['el'=> 'Μπρελόκ', 'en'=> 'Μπρελόκ']),    'mprelok-257',  1,  1,  NULL,   NULL,   NULL,   0,  0,  NULL,   NULL,   0,  0,  NULL,   '2021-01-28 15:00:47',  '2021-01-28 15:00:47']

];



       
        foreach ($categories as $cat) {
            $title = json_decode($cat[3], true);
          
            DB::table('categories')->insert([
                "id"=>$cat[0],
                "parent_id"  => $cat[1],
             "erp_id" => $cat[2],
             "title" => json_encode(['el'=>$title['en'], 'en'=> $title['en']]),
             "name" => $cat[4],
             "active" => $cat[5],
             "hidden" => $cat[6],
             "description" => $cat[7],
             "meta_title" => $cat[8],
             "meta_description" => $cat[9],
             "mega_menu_is_active" => $cat[10],
             "dynamic_query_is_active" => $cat[11],
             "mega_menu_content" => $cat[12],
             "dynamic_query_content" => $cat[13],
             "order" => $cat[14],
             "dynamic" => $cat[15],
             "query" => $cat[16],
             "created_at" => $cat[17],
                
            ]);
 
        }


    }
}