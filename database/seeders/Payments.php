<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class Payments extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $payments = [
            ['title'=> 'Πληρωμή στο κατάστημα', 'name'=>'pay_in_store', 'description'=> 'Πληρωμή στο κατάστημα', 'active'=> 1],
            ['title'=> 'Πληρωμή με κάρτα Alphabank', 'name'=>'alphabank_credit_card', 'description'=> 'Πληρωμή με πιστωτική κάρτα στο περιβάλλον alphabank', 'active'=> 1],
            ['title'=> 'Πληρωμή με Αντικαταβολή', 'name'=>'cod', 'description'=> 'Cash on delivery', 'active'=> 1],
            ['title'=> 'Κατάθεση σε τράπεζα', 'name'=>'bank_transfer', 'description'=> 'Κατάθεση σε τράπεζα', 'active'=> 1],
            ['title'=> 'Πληρωμή με PayPal', 'name'=>'paypal', 'description'=> 'Πληρωμή με PayPal', 'active'=> 1],
            ['title'=> 'Πληρωμή με Viva Wallet', 'name'=>'viva_wallet', 'description'=> 'Πληρωμή με Viva Wallet', 'active'=> 0],
            ['title'=> 'Πληρωμή με Stripe', 'name'=>'stripe', 'description'=> 'Πληρωμή με Stripe', 'active'=> 0],

        ];

        foreach ($payments as $payment) {
            \App\Models\Payment::create($payment);
        }

    }
}
