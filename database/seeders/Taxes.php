<?php

namespace Database\Seeders;

use App\Models\Tax;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class Taxes extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Tax::make()->create([
            'country_code' => 'GR',
            'county_code' => '*',
            'zip_codes' => '*',
            'tax_rate' => '24',
            'tax_name' => 'ΦΠΑ',
        ]);
    }
}
