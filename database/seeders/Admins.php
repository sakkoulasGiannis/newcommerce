<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
class Admins extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = DB::table('users')->insert([
            'name' => "Gianni",
            'lastname' => "Sakkoulas",
            'mobile' => "6936780044",
            'phone' => "",
            'email' => "sakkoulas@gmail.com",
            "password" => Hash::make("sakkoulas3@!"),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        User::first()->assignRole('admin');

    }
}
