<?php

namespace Database\Seeders;

use App\Models\Continent;
use App\Models\Country;
use App\Models\County;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class Countries extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

        //// read this https://api.covid19api.com/countries
//        $apiUrl = "https://api.covid19api.com/countries";
//        $ch = curl_init();
//
//        curl_setopt($ch, CURLOPT_URL, $apiUrl);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//
//        $response = curl_exec($ch);
//        if(curl_errno($ch)) {
//            echo 'Error:' . curl_error($ch);
//        }
//        curl_close($ch);
//
//
//        $countries = json_decode($response, true);
//
//        if(!empty($countries)) {
//            echo "<ul>";
//            foreach($countries as $country) {
//                echo "<li>" . htmlspecialchars($country['Country']) . " (" . htmlspecialchars($country['Slug']) . ") - ISO: " . htmlspecialchars($country['ISO2']) . "</li>";
//            }
//            echo "</ul>";
//        } else {
//            echo "Δεν βρέθηκαν χώρες.";
//        }

        Continent::create([
            'title' => 'Europe',
            'iso' => 'EU',
            'is_active' => 1,
            'created_at' => Carbon::now(),
        ]);


        Country::create([
            'title' => 'Greece',
            'iso' => 'GR',
            'is_active' => 1,
            'currency' => '€',
            'currency_title' => 'euro',
            'created_at' => Carbon::now(),
            'continent_id' => 1,
        ]);

		$country = Country::first()->id;
		$counties = [
            'Αιτωλίας',
            'Αργολίδας',
            'Αρκαδίας',
            'Άρτας',
            'Αττικής',
            'Αχαΐας',
            'Βοιωτίας',
            'Γρεβενών',
            'Δράμας',
            'Δωδεκανήσου',
            'Έβρου',
            'Εύβοιας',
            'Ευρυτανίας',
            'Ζακύνθου',
            'Ηλείας',
            'Ημαθείας',
            'Ηρακλείου',
            'Θεσπρωτίας',
            'Θεσσαλονίκης',
            'Ιωαννίνων',
            'Καβάλας',
            'Καρδίτσας',
            'Καστοριάς',
            'Κέρκυρας',
            'Κεφαλληνίας',
            'Κιλκίς',
            'Κοζάνης',
            'Κορινθίας',
            'Κυκλάδων',
            'Λακωνίας',
            'Λάρισας',
            'Λασιθίου',
            'Λέσβου',
            'Λευκάδας',
            'Μαγνησίας',
            'Μεσσηνίας',
            'Ξάνθης',
            'Πέλλας',
            'Πιερίας',
            'Πρέβεζας',
            'Ρεθύμνου',
            'Ροδόπης',
            'Σάμου',
            'Σερρών',
            'Τρικάλων',
            'Φθιώτιδας',
            'Φλώρινας',
            'Φωκίδας',
            'Χαλκιδικής',
            'Χανίων',
            'Χίου',
		];
		foreach ($counties as $c) {
            County::create([
                'title' => $c,
                'country_id' => $country,
                'is_active' => 1,
                'created_at' => Carbon::now(),
            ]);

		}

	}
}
