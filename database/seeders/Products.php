<?php

namespace Database\Seeders;

use App\Scopes\ActiveProducts;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\Product;
use App\Models\Category;
class Products extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {




        $faker = Faker::create();
        for ($a = 1; $a <= 10; $a++) {
            $p = DB::table('products')->insert([
                'title' => json_encode(['el' => $faker->realText(30), 'en' => $faker->realText(30)]),
                'product_type' => 'Simple',
                'name' => $faker->slug . rand(11110, 100000010),
                'sku' => $faker->realText(10) . rand(111110, 1000111100),
                'product_is_active' => 1,
                'tax_class_id' => 1,
                'price' => $faker->randomNumber(2),
                'description' => json_encode(['el' => $faker->paragraph(), 'en' => $faker->paragraph()]),
                'small_description' => json_encode(['el' => $faker->paragraph(), 'en' => $faker->paragraph()]),
                'meta_title' => json_encode(['el' => $faker->realText(30), 'en' => $faker->realText(30)]),
                'meta_description' => json_encode(['el' => $faker->realText(30), 'en' => $faker->realText(30)]),
                'group' => 1
            ]);


        }


        $products = Product::withoutGlobalScope(ActiveProducts::class)->get();
       if(count(Category::all()) > 0) {
           foreach ($products as $product) {
               $product->categories()->attach(Category::all()->random()->id);
           }
       }


    }
}
