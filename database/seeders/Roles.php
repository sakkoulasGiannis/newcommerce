<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Roles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'Admin',
            'guard_name' => 'web',
            'created_at' => Carbon::now(),
        ]);

        DB::table('roles')->insert([
            'name' => 'User',
            'guard_name' => 'web',
            'created_at' => Carbon::now(),
        ]);


    }
}
