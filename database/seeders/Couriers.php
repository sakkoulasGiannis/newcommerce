<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class Couriers extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $couriers = [
            ['title'=> 'Γενική Ταχυδρομική', 'name'=>'geniki', 'description'=> 'Γενική Ταχυδρομική'],
            ['title'=> 'ACS Courier', 'name'=>'acs', 'description'=> 'ACS Courier'],
            ['title'=> 'Speedex', 'name'=>'speedex', 'description'=> 'Speedex'],
            ['title'=> 'ΕΛΤΑ Courier', 'name'=>'elta', 'description'=> 'ΕΛΤΑ Courier'],
            ['title'=> 'Παραλαβή απο το κατάστημα', 'name'=>'storePickUp', 'description'=> 'Παραλαβή απο το κατάστημα'],
        ];

        foreach ($couriers as $courier) {
            \App\Models\Courier::create($courier);
        }
    }
}
