<?php

namespace Database\Seeders;

use App\Models\Warehouse;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class Warehouses extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Warehouse::make([
            'name' => 'Main Warehouse',
            'address' => 'Main Street 1',
            'city_id' => 1,
            'country_id' => 1,
            'phone' => '1234567890',
        ])->save();
    }
}
