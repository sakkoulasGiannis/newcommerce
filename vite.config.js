import { defineConfig } from 'vite';
// import laravel from 'vite-plugin-laravel';
export default defineConfig({

    build: {
        outDir: 'public/vendor/Onix',
        emptyOutDir: true,
        rollupOptions: {
            input: {
                grape: '/resources/vendor/Onix/js/OnixSetup/onixGrapeJs.js',
                app: '/resources/vendor/Onix/js/app.js',
                css: '/resources/vendor/Onix/css/app.css',
            },
        },
    }
});
