const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    mode: 'jit', // Just-in-Time mode for faster builds
    content: [
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/test/**/*.blade.php',
        './public/themes/**/*.blade.php',
        './resources/js/**/*.vue',
        'node_modules/preline/dist/*.js',


    ],
    theme: {
        extend: {
            maxWidth: {
                '8xl': '90rem',
            },
            fontFamily: {
                // sans: ['Nunito', ...defaultTheme.fontFamily.sans],
                sans: ['"Manrope"', 'sans-serif'], // Ορίζει την προεπιλεγμένη sans-serif

                luckiest: ['"Luckiest Guy"', 'cursive'],
                manrope: ['"Manrope"', 'sans-serif'],
                body: 'Manrope',

            },
            colors: {
                'es-gold': '#baa85b',
                'es-blue': '#1b315a',
            },
        },
        container: {
            center: true, // Centers the content horizontally within the container
            padding: {
                DEFAULT: '1rem', // Default padding around the content
                sm: '2rem', // Padding for small screens and up
                lg: '4rem', // Padding for large screens and up
                xl: '5rem', // Padding for extra large screens and up
            },
            screens: {
                '2xl': '1280px', // Maximum width for 2xl screens
            },
        },
    },
    variants: {
        extend: {
            textColor: ['responsive', 'hover', 'focus', 'group-hover'],
            opacity: ['disabled'],
        },
    },
    plugins: [

        require('@tailwindcss/forms'), // Tailwind CSS form plugin
        // require('@tailwindcss/typography'), // Tailwind CSS typography plugin
    ],
};
