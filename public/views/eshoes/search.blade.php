@extends('eshoes.layouts.app')
@section('content')
<div class="container mx-auto">
    <div class="grid grid-cols-12 gap-4">
        <div class="col-span-10">
            <hr>
            <div class="grid grid-cols-1 md:grid-cols-4 gap-4">
                @foreach($products as $product)
                @include('eshoes.elements.productItem', $product)
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
