@extends('eshoes.layouts.app')
@section('content')
{{-- cartItems', 'conditions', 'cartDetails' --}}
<checkout :countries="{{json_encode($countries)}}" :cartcontent="{{json_encode($cartContent)}}" :cartitems="{{json_encode($cartItems)}}" :cartdetails="{{json_encode($cartDetails)}}" :conditions="{{json_encode($conditions)}}" />
@endsection
