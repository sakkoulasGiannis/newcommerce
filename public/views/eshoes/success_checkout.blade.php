@extends('eshoes.layouts.app')
@section('content')
    <div class="container mx-auto">
        <div class="grid grid-cols-1 md:grid-cols-12 md:gap-4 mt-10">

            <div class="col-span-12">
                <span class="title"><b>ΕΥΧΑΡΙΣΤΟΥΜΕ ΓΙΑ ΤΗΝ ΠΑΡΑΓΓΕΛΙΑ ΣΑΣ!</b></span>
                <p>
                    Ο κωδικός παραγγελίας είναι: {{$order}}
                </p>
                <p>Για την επιβεβαίωση της παραγγελίας σας, θα λάβετε σχετικό e-mail.</p>
            </div>
        </div>
    </div>
@endsection
@section('page_scripts')

@endsection
