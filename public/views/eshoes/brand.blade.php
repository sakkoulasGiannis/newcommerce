@extends('eshoes.layouts.app')
@section('content')
    <div class="container mx-auto">

        <div class="grid grid-cols-1 md:grid-cols-12 md:gap-4">
            <div x-data={open:true} class="col-span-12 md:col-span-2">

                <a id="filterToggler"
                    class="cursor-pointer rounded text-blue-500 hover:bg-blue-500 focus:outline-none md:hidden">
                    <svg xmlns="https://www.w3.org/2000/svg" class="ml-4 h-6 w-6" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M3 4a1 1 0 011-1h16a1 1 0 011 1v2.586a1 1 0 01-.293.707l-6.414 6.414a1 1 0 00-.293.707V17l-4 4v-6.586a1 1 0 00-.293-.707L3.293 7.293A1 1 0 013 6.586V4z" />
                    </svg>
                </a>

                <div id="filters" class="my-2 hidden px-4 py-3 text-gray-700 md:block">
                    @include('eshoes.elements.filters')
                </div>
            </div>
            <div class="col-span-10">
                <div class="mb-2 grid grid-cols-1 p-4 md:grid-cols-12">
                    <form method="GET" class="col-span-4 md:col-span-3">
                        <select onchange="this.form.submit()" class="browser-default left" name="sort">


                            <option {{ !request()->exists('sort') && request()->get('sort') == '' ? 'selected' : null }}
                                value="">Νέα Προϊόντα</option>
                            <option
                                {{ request()->exists('sort') && request()->get('sort') == 'price_asc' ? 'selected' : null }}
                                value="price_asc">Τιμή Αύξουσα</option>
                            <option
                                {{ request()->exists('sort') && request()->get('sort') == 'price_desc' ? 'selected' : null }}
                                value="price_desc">Τιμή Φθίνουσα </option>
                            <option
                                {{ request()->exists('sort') && request()->get('sort') == 'discount_asc' ? 'selected' : null }}
                                value="discount_asc">Ποσοστό Έκτπωσης Αύξουσα</option>
                            <option
                                {{ request()->exists('sort') && request()->get('sort') == 'discount_desc' ? 'selected' : null }}
                                value="discount_desc">Ποσοστό Έκπτωσης Φθίνουσα </option>
                            <option
                                {{ request()->exists('sort') && request()->get('sort') == 'season_asc' ? 'selected' : null }}
                                value="season_asc">Νέες Παραλαβές Αύξουσα </option>
                            <option
                                {{ request()->exists('sort') && request()->get('sort') == 'season_desc' ? 'selected' : null }}
                                value="season_desc">Νέες Παραλαβές Φθίνουσα </option>
                        </select>
                    </form>
                    <form method="GET" class="col-span-12 md:col-span-1" onChange='submit();' name="pagination">
                        <select class="browser-default" onChange='submit();' name="pagination">
                            <option value="32"
                                {{ session()->get('paginate') && session()->get('paginate') == 32 ? 'selected' : null }}>
                                32
                            </option>
                            <option value="64"
                                {{ session()->get('paginate') && session()->get('paginate') == 64 ? 'selected' : null }}>
                                64
                            </option>
                            <option value="96"
                                {{ session()->get('paginate') && session()->get('paginate') == 96 ? 'selected' : null }}>
                                96
                            </option>
                            <option value="128"
                                {{ session()->get('paginate') && session()->get('paginate') == 128 ? 'selected' : null }}>
                                128
                            </option>
                        </select>
                    </form>
                    <div class="pt-2 md:col-span-8">
                        <div class="flex justify-end">
                            {{ $products->links('eshoes.pagination') }}
                        </div>
                    </div>
                </div>
                <hr>
                <div class="grid grid-cols-1 gap-4 md:grid-cols-4">
                    @foreach ($products as $product)
                        <div class="relative p-4 text-center transition duration-200 ease-in-out hover:shadow-xl">
                            @if ($product->rule)
                                <div
                                    class="absolut<e right-0 flex h-10 w-10 items-center justify-center rounded-full bg-green-500 text-xs text-white">
                                    -{{ $product->rule->discount_value }}%</div>
                            @endif
                            <a href="/{{ $product->name }}">
                                <img src="{{ $product->getFirstMediaUrl() }}" alt="">
                            </a>

                            @if ($product->brand->first())
                                <span
                                    class="brand text-center text-gray-600">{{ $product->brand->first()->title }}</span>
                                <br />
                            @endif
                            <span class="brand right-0 mt-2 text-xs text-gray-600">ΚΩΔ: {{ $product->sku }}</span> <br />
                            <span class="price text-xl font-bold text-gray-900">{{ $product->price }}€</span> <br />
                            <a href="/{{ $product->name }}"
                                class="mt-2 mb-4 border border-gray-500 px-4 py-2 text-xs hover:bg-blue-900 hover:text-white hover:shadow-lg">
                                ΔΕΙΤΕ ΤΟ

                            </a>
                        </div>
                    @endforeach
                </div>
                <div class="center pt-4 text-center">
                    {{ $products->links('eshoes.pagination') }}

                </div>
            </div>


        </div>
    </div>
@endsection
