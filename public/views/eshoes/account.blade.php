@extends('eshoes.layouts.app')
@section('content')
    <div class="container mx-auto">
        <div class="grid grid-cols-6 gap-4 mt-4">
            @include('eshoes.account.userMenu')
            <div class="md:col-span-4 col-span-12 p-2 md:p-20">
                <p>Γεια σας {{$user->name}} {{$user->lastname}} </p>
                <p>Από τον πίνακα ελέγχου του λογαριασμού σας μπορείτε να δείτε τις πρόσφατες παραγγελίες σας, να
                    διαχειριστείτε τις διευθύνσεις αποστολής και χρέωσης και να επεξεργασθείτε το συνθηματικό σας και
                    τις λεπτομέρειες του λογαριασμού σας.</p>
            </div>
        </div>
    </div>
@endsection
