@extends('eshoes.layouts.app')
@section('content')
<div class="container mx-auto">
    <div class="grid grid-cols-12 gap-4">
        <div class="col-start-5 col-span-4 mt-8">
            <h1 class="text-2xl">Νέα Εγγραφή</h1>
            <form method="POST" action="{{ route('register.custom') }}">
                @csrf


                <div class="mt-4">
                    <label class="block mb-2 text-indigo-500" for="username">Όνομα</label>
                    <input type="text"   id="email" class="w-full p-2 mb-6 text-indigo-700 border-b-2 border-indigo-500 outline-none focus:bg-gray-300" name="name" value="{{old('name')}}" required  >
                    @if ($errors->has('name'))
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                    @endif
                </div>

                <div class="mt-4">
                    <label class="block mb-2 text-indigo-500" for="username">Επίθετο</label>
                    <input type="text"   id="email" class="w-full p-2 mb-6 text-indigo-700 border-b-2 border-indigo-500 outline-none focus:bg-gray-300" name="lastname" value="{{old('lastname')}}" required  >
                    @if ($errors->has('lastname'))
                    <span class="text-danger">{{ $errors->first('lastname') }}</span>
                    @endif
                </div>

                <div class="mt-4">
                    <label class="block mb-2 text-indigo-500" for="username">Τηλέφωνο</label>
                    <input type="text"   id="email" class="w-full p-2 mb-6 text-indigo-700 border-b-2 border-indigo-500 outline-none focus:bg-gray-300" name="mobile" value="{{old('mobile')}}" required  >
                    @if ($errors->has('mobile'))
                    <span class="text-danger">{{ $errors->first('mobile') }}</span>
                    @endif
                </div>


                <div class="mt-4">
                    <label class="block mb-2 text-indigo-500" for="username">Username</label>
                    <input type="text" placeholder="Email" id="email" class="w-full p-2 mb-6 text-indigo-700 border-b-2 border-indigo-500 outline-none focus:bg-gray-300" name="email" required autofocus>
                    @if ($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                    @endif  
                </div>
                <div class="mb-4">
                    <label class="block mb-2 text-indigo-500" for="password">Password</label>
                    <input type="password" placeholder="Password" id="password" class="w-full p-2 mb-6 text-indigo-700 border-b-2 border-indigo-500 outline-none focus:bg-gray-300" name="password" required>
                    @if ($errors->has('password'))
                    <span class="text-danger">{{ $errors->first('password') }}</span>
                    @endif
                </div>



<div class="mb-4">
                        <label class="block mb-2 text-indigo-500" for="password">Επιβεβαίωση Κωδικού</label>

                                 <input id="password-confirm" type="password" class="w-full p-2 mb-6 text-indigo-700 border-b-2 border-indigo-500 outline-none focus:bg-gray-300" name="password_confirmation" required>
                            </div>

                <div>
                    <button type="submits" class="w-full bg-indigo-700 hover:bg-pink-700 text-white font-bold py-2 px-4 mb-6 rounded">ΕΓΓΡΑΦΗ</button>
                     
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
