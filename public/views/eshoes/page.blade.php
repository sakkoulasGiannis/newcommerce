@extends('eshoes.layouts.app')
@section('content')
    <div class="container mx-auto p-4">
        <div class="grid grid-cols-12 gap-4">
            <div class="col-span-12">
                <h1 class="font-bold text-2xl text-es-blue">{{ $page->title }}</h1>

            </div>
            <div class="col-span-12">
                {!! $page->body !!}
            </div>
        </div>
    </div>
@endsection
