@extends('eshoes.layouts.app')
@section('content')
<div class="border-t border-gray-200 text-center pt-8">
    <h1 class="text-9xl font-bold text-purple-400">Ουπς! </h1>
    <p class="text-2xl pb-8 px-12 font-medium">ΔΕΝ ΒΡΕΘΗΚΑΝ ΠΡΟΪΟΝΤΑ ΣΤΟ ΚΑΛΑΘΙ ΣΑΣ.</p>
    <a href="/" class="bg-gradient-to-r from-purple-400 to-blue-500 hover:from-pink-500 hover:to-orange-500 text-white font-semibold px-6 py-3 rounded-md mr-6">
    Επιστροφή στην Αρχική σελίδα
    </a>
    <a href="/new-products" class="bg-gradient-to-r from-red-400 to-red-500 hover:from-red-500 hover:to-red-500 text-white font-semibold px-6 py-3 rounded-md">
        Επιστροφή στις νέες παραλαβές
    </a>
    </div>
@endsection
