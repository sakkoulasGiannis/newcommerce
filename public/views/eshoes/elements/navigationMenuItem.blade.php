<li class="menu-item p-0 m-0 h-5" style="list-style: none;">
    <a class="level2 text-es-blue text-sm p-0 m-0"
        href="{{ $url }}"><span>{{ $title }}</span></a>
</li>