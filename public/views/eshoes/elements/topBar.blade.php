<div class="hidden md:block container mx-auto bg-white ">
    <div class="grid grid-cols-3 gap-2   ">
        <div class=" mt-4 justify-self-start">

            <search-simple></search-simple>

            {{-- <input class="w-96 appearance-none border rounded w-full py-4 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline text-sm" id="username" type="text" placeholder="Αναζήτηση ... "> --}}
        </div>
        <div class="justify-self-center mt-1">
            <a href="/">
                <img src="/views/eshoes/assets/img/e-shoes-logo-neo-fin.png" alt="e-shoes" width="230">
            </a>
        </div>
        <div class="justify-self-end mt-4">
            <span class="inline-grid grid-cols-3 gap-4">
                <span class="clickHart" >
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                         stroke="currentColor" class="w-8 h-8 text-es-blue hover:text-es-gold">
                        <path stroke-linecap="round" stroke-linejoin="round"
                              d="M21 8.25c0-2.485-2.099-4.5-4.688-4.5-1.935 0-3.597 1.126-4.312 2.733-.715-1.607-2.377-2.733-4.313-2.733C5.1 3.75 3 5.765 3 8.25c0 7.22 9 12 9 12s9-4.78 9-12z"/>
                      </svg>
                </span>

                <span>
                    @auth

                        <a href="/account">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                 stroke-width="1.5" stroke="currentColor"
                                 class="w-8 h-8 text-es-blue hover:text-es-gold">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                      d="M17.982 18.725A7.488 7.488 0 0012 15.75a7.488 7.488 0 00-5.982 2.975m11.963 0a9 9 0 10-11.963 0m11.963 0A8.966 8.966 0 0112 21a8.966 8.966 0 01-5.982-2.275M15 9.75a3 3 0 11-6 0 3 3 0 016 0z"/>
                            </svg>
                        </a>

                    @endauth
                    @guest
                        <a href="/login">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                         stroke="currentColor" class="w-8 h-8 text-es-blue hover:text-es-gold">
                        <path stroke-linecap="round" stroke-linejoin="round"
                              d="M17.982 18.725A7.488 7.488 0 0012 15.75a7.488 7.488 0 00-5.982 2.975m11.963 0a9 9 0 10-11.963 0m11.963 0A8.966 8.966 0 0112 21a8.966 8.966 0 01-5.982-2.275M15 9.75a3 3 0 11-6 0 3 3 0 016 0z"/>
                      </svg>

                      </a>
                    @endguest
                </span>
                <div>
                    @php
                    //@todo: fix cart
//                        $cartContent = \Cart::getContent();
                    @endphp

                    <div class="cartbar-button-open block lg:inline-block align-middle text-black hover:text-gray-700" style=" z-index: 20000;">
                      <a href="#" role="button" class="relative flex cartbar-open">
{{--                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"--}}
{{--                             stroke="currentColor"--}}
{{--                             class="w-8  h-8 text-es-blue hover:text-es-gold  {{ $cartContent->count() > 0 ? 'text-es-gold' : null }}">--}}
{{--                                <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 10.5V6a3.75 3.75 0 10-7.5 0v4.5m11.356-1.993l1.263 12c.07.665-.45 1.243-1.119 1.243H4.25a1.125 1.125 0 01-1.12-1.243l1.264-12A1.125 1.125 0 015.513 7.5h12.974c.576 0 1.059.435 1.119 1.007zM8.625 10.5a.375.375 0 11-.75 0 .375.375 0 01.75 0zm7.5 0a.375.375 0 11-.75 0 .375.375 0 01.75 0z"/>--}}
{{--                        </svg>--}}
{{--                          //@todo: fix cart--}}
{{--                           @if ($cartContent->count() > 0)--}}
{{--                              <span class="absolute bg-es-blue right-0 rounded-full text-center w-4 h-4 text-xs text-white">{{ $cartContent->count() }}</span>--}}
{{--                          @endif--}}
                      </a>
                    </div>



                </div>
              </span>
        </div>
    </div>
    {{--  algolia search results  --}}
    {{--    <div class="grid grid-1">--}}
    {{--        <search-results></search-results>--}}
    {{--    </div>--}}
</div>
