<div class="container mx-auto">
    <div class="mb-4 grid gap-4 sm:grid-cols-4 md:grid-cols-12">
        <div class="sm:col-span-5 md:col-span-5">
            <figure class="effect-honey">
                <a href="/gynaikeia-papoutsia">
                    <img src="{{ URL::asset('/views/eshoes/assets/img/gynaikeia-631x600.jpg') }}" alt="img04"/>
                </a>

            </figure>
            <div>

                <a href="/gynaikeia-papoutsia"><h2>ΓΥΝΑΙΚΕΊΑ <span>ΠΑΠΟΥΤΣΙΑ</span> <i> > </i></h2></a>
            </div>
        </div>
        <div class="sm:col-span-4 md:col-span-4">
            <figure class="effect-honey">
                <a href="/accessories">
                    <img src="{{ URL::asset('/views/eshoes/assets/img/tsantes-501x292_1.jpg') }}" alt="img04"/>
                </a>

            </figure>
            <div>

                <a href="/accessories"> <h2>ΤΣΑΝΤΕΣ <span>ΑΞΕΣΟΥΑΡ</span> <i> > </i></h2></a>
            </div>
            <figure class="effect-honey">
                <a href="/paidika-papoutsia">
                    <img src="{{ URL::asset('/views/eshoes/assets/img/paidika-501x292_1.jpg') }}" alt="img04"/>
                </a>

            </figure>
            <div>

                <a href="/paidika-papoutsia"> <h2>ΠΑΙΔΙΚΑ <span>ΠΑΠΟΥΤΣΙΑ</span> <i> > </i></h2></a>
            </div>
        </div>
        <div class="sm:col-span-3 md:col-span-3">
            <figure class="effect-honey">
                <a href="/andrika-papoutsia">
                    <img src="{{ URL::asset('/views/eshoes/assets/img/andrika-372x600.jpg') }}" alt="img04"/>
                </a>

            </figure>
            <div>

                <a href="/andrika-papoutsia">   <h2>ΑΝΔΡΙΚΑ <span>ΠΑΠΟΥΤΣΙΑ</span> <i> > </i></h2></a>
            </div>
        </div>
    </div>

    <h2 class="border-b-2 p-2 pl-4 font-bold text-es-blue text-lg mt-8 ">ΝΕΑ ΓΥΝΑΙΚΕΙΑ</h2>
    <div class="grid grid-cols-2 md:grid-cols-6 ">

{{--        @foreach ($latestWhoman as $product)--}}
{{--            <div>--}}
{{--                @include('eshoes.elements.productItem', $product)--}}
{{--            </div>--}}
{{--        @endforeach--}}
    </div>

    <h2 class="border-b-2 p-2 pl-4 font-bold text-es-blue text-lg mt-8 ">ΝΕΑ ΑΝΔΡΙΚΑ</h2>
    <div class="grid grid-cols-2 md:grid-cols-6 ">

{{--        @foreach ($latestMen as $product)--}}
{{--            <div>--}}
{{--                @include('eshoes.elements.productItem', $product)--}}
{{--            </div>--}}
{{--        @endforeach--}}
    </div>




    <div class="grid gap-4 sm:grid-cols-4 md:grid-cols-12 mt-8">
        <div class="sm:col-span-6 md:col-span-6">
            <img class="" src="{{ URL::asset('/views/eshoes/assets/img/rouxa-800x800_2.jpg') }}" alt="">
        </div>
        <div class="sm:col-span-6 md:col-span-6">
            <img src=" {{ URL::asset('/views/eshoes/assets/img/rouxa-800x800.jpg') }}" alt="">
        </div>
    </div>
</div>
