<div class="sizesblock group-hover:bg-red absolute hidden text-center">
	<span class="text-es-gold ml-1 text-sm">Νούμερα:</span>

	@foreach ($product->stock as $key => $s)
		<a href="{{ $product->name }}?size={{ $s->model_title }}">
			<span class="text-es-gold pl-1 text-xs">{{ $s->model_title }}</span> </a>
	@endforeach

	{{--		@foreach ($product->variations() as $key => $s)--}}
	{{--			<a href="{{ $product->name }}?size={{ $s }}">--}}
	{{--					<span class="text-es-gold pl-1 text-xs">{{ $s }}--}}
	{{--						@if (isset($product->variations()[$key + 1]))--}}
	{{--							&#8226;--}}
	{{--						@endif--}}
	{{--					</span> </a>--}}
	{{--		@endforeach--}}


</div>