    @if (!\Cookie::has('policy'))
        <div class="alert alert-dismissible cookiealert show" role="alert">
            <div class="cookiealert-container container">
                <div class="row">
                    <div class="col-md-9">
                        🍪 <b>@lang('footer.cookies') </b>
                    </div>
                    <div class="col-md-3 text-center">
                        {!! Form::open(['route' => 'policy.accept', 'method' => 'post']) !!}
                        <button type="submit" class="btn btn-danger acceptcookies"> @lang('cookies.Decline') </button>
                        <button type="submit" class="btn btn-primary acceptcookies"> @lang('global.Accept') </button>
                        <br />
                        <a style="color:white;" href="#" class="white" data-toggle="modal"
                            data-target="#gdprModal">
                            @lang('footer.cookies more')
                        </a>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-lg" id="gdprModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">@lang('cookies.Regulations')</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(['route' => 'policy.accept', 'method' => 'post']) !!}
                        <div class="form-check">
                            {{ Form::checkbox('acceptance_1', 1, true, [
                                'id' => 'acceptance_1',
                                'class' => 'form-check-input',
                                'disabled' => 'disabled',
                            ]) }}
                            {{ Form::label('acceptance_1', Lang::get('cookies.Necessary'), ['class' => 'form-check-label']) }}
                            <p>
                                @lang('cookies.NecessaryDescription')
                            </p>
                        </div>
                        <div class="form-check">
                            {{                             Form::checkbox('acceptance_2', 1, true, ['id' => 'acceptance_2', 'class' => 'form-check-input']) }}
                            {{                             Form::label('acceptance_2', Lang::get('cookies.experience'), ['class' => 'form-check-label']) }}
                            <p>
                                @lang('cookies.experienceDescription')
                            </p>
                        </div>
                        <div class="form-check">
                            {{                             Form::checkbox('acceptance_3', 1, true, ['id' => 'acceptance_3', 'class' => 'form-check-input']) }}
                            {{                             Form::label('acceptance_3', Lang::get('cookies.communication'), ['class' => 'form-check-label']) }}
                            <p>
                                @lang('cookies.communicationDescription')
                            </p>
                        </div>
                        <div class="form-check">
                            {{                             Form::checkbox('acceptance_4', 1, true, ['id' => 'acceptance_4', 'class' => 'form-check-input']) }}
                            {{                             Form::label('acceptance_4', Lang::get('cookies.advertising'), ['class' => 'form-check-label']) }}
                            <p>
                                @lang('cookies.advertisingDescription')
                            </p>
                        </div>
                        <div class="row">
                            <div class="col-12 text-center">
                                {{-- {{Form::submit(Lang::get('cookies.Decline'), ['class'=> 'btn btn-danger btn-lg'])}} --}}
                                <input value="{{ Lang::get('cookies.Decline') }}" type="submit"
                                    class="btn btn-danger acceptcookies" />
                                {{ Form::submit(Lang::get('cookies.accept'), ['class' => 'btn btn-primary btn-lg']) }}
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    @endif
