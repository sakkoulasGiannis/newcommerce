<table>
    <thead>
        <tr>
            <th>ΟΝΟΜΑ ΠΑΡΑΛΗΠΤΗ</th>
            <th>ΠΕΡΙΟΧΗ</th>
            <th>ΟΔΟΣ</th>
            <th>ΑΡΙΘΜΟΣ</th>
            <th>ΟΡΟΦΟΣ</th>
            <th>ΤΚ</th>
            <th>EMAIL ΠΑΡΑΛΗΠΤΗ</th>
            <th>ΤΗΛΕΦΩΝΟ</th>
            <th>ΚΙΝΗΤΟ</th>
            <th>ΥΠΟΚΑΤΑΣΤΗΜΑ</th>
            <th>ΠΑΡΑΤΗΡΗΣΕΙΣ</th>
            <th>ΧΡΕΩΣΗ</th>
            <th>ΤΕΜΑΧΙΑ</th>
            <th>ΒΑΡΟΣ</th>
            <th>ΑΝΤΙΚΑΤΑΒΟΛΗ</th>
            <th>ΤΡΟΠΟΣ ΠΛΗΡΩΜΗΣ</th>
            <th>ΑΣΦΑΛΕΙΑ</th>
            <th>ΚΕΝΤΡΟ ΚΟΣΤΟΥΣ</th>
            <th>ΣΧΕΤΙΚΟ1</th>
            <th>ΣΧΕΤΙΚΟ2</th>
            <th>ΩΡΑ ΠΑΡΑΔΟΣΗΣ</th>
            <th>ΠΡΟΙΟΝΤΑ</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($orders as $o)
            <tr>
                <td>{{ $o->detail->shipping_name }} {{ $o->detail->shipping_lastname }}</td>
                <td>{{ $o->detail->shipping_city }}</td>
                <td>{{ $o->detail->shipping_street }}</td>
                <td>{{ $o->detail->shipping_street_number }}</td>
                <td> </td>
                <td>{{ $o->detail->shipping_zip }}</td>
                <td>{{ $o->detail->billing_email }}</td>
                <td>{{ $o->detail->billing_phone }}</td>
                <td>{{ $o->detail->billing_mobile }}</td>
                <td>{{ $o->detail->comments()->pluck('comment') }}</td>
                <td>ACS </td>
                <td>1</td>
                <td>0.2</td>
                <td>{{ $o->detail->total }}</td>
                <td>{{ $o->detail->payment_id == 1 ? 'Μ' : null }}</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        @endforeach
    </tbody>
</table>
