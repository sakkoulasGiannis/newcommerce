<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Commerce') }}</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
     <link href="https://fonts.googleapis.com/css?family=Material+Icons" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js?').time()}}" defer></script>

    <!-- Fonts -->
    <link href="{{ asset('css/app.css?'). time() }}" rel="stylesheet">

    <!-- Styles -->
 </head>
<body>

    <div id="app">


            @yield('content')

    </div>

@yield('page_scripts')
</body>
</html>
