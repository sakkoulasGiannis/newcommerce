@extends('default.layouts.app')
@section('content')
sdfsdf
    <div class="ps-page--default">
        <div class="container">
            <div class="ps-page__content">
                <div class="ps-tab-root">
                    <form class="ps-form--auth">
                        <ul class="ps-tab-list">
                            <li class="active"><a href="#tab-1">Είσοδος</a></li>
                            <li><a href="#tab-2">Εγγραφή</a></li>
                        </ul>
                    </form>
                        <div class="ps-tabs">
                            <div class="ps-tab active" id="tab-1">
                                <form class="ps-form--auth" method="POST" action="{{ route('login') }}">
                                    @csrf
                                <div class="form-group">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="email">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="password">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <div class="ps-checkbox">
                                        <input class="form-control form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <label for="remember">Να με θυμάσαι!</label>

                                    </div>
                                </div>
                                <div class="form-group submit">
                                    <button class="ps-btn ps-btn--fullwidth ps-btn--black">ΕΙΣΟΔΟΣ</button>
                                </div>
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        Υπενθύμηση Κωδικού
                                    </a>
                                @endif
                                </form>
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>
@endsection
