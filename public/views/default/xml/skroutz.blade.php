<mywebstore>
    <created_at>{{date('Y-m-d h:m')}}</created_at>
    <products>
        @foreach ($products as $product)

            <product>
                <UniqueID><![CDATA[{{($product->old_id != null)?$product->old_id:$product->id}}]]></UniqueID>
                <name><![CDATA[ {{$product->name}} {{$product->sku}} ]]></name>
                <link>
                <![CDATA[ {{$domain.$product->name}} ]]></link>
                @foreach($product->getMedia("product") as $var => $image)
                    @if($var == 0)
                        <image><![CDATA[{{$image->getFullUrl()}}]]></image>
                    @else
                        <additionalimage><![CDATA[{{$image->getFullUrl()}}]]></additionalimage>
                    @endif
                @endforeach

                <category><![CDATA[ {{ $product->flatCategories->pluck('title.el')->implode(' > ') }} ]]></category>
                <price_with_vat><![CDATA[ {{$product->price}} ]]></price_with_vat>
                <mpn><![CDATA[ {{$product->sku}} ]]></mpn>
                <description><![CDATA[ {{strip_tags($product->description)}} ]]></description>
                <instock><![CDATA[ Y ]]></instock>
                <availability><![CDATA[ Σε απόθεμα ]]></availability>
                <manufacturer><![CDATA[{{($product->brand && $product->brand->first() )?$product->brand->first()->title:''}}]]></manufacturer>

                @php
                    $sizes = array();
                    foreach($product->stock->where('quantity', '>', 0) as $a) {
                     $sizes[] = str_replace(",",".",$a->model_title);

                    }
                @endphp
                <variations>

                    @foreach($product->stock->where('quantity', '>', 0) as $a)
                        <variation>
                            <variationid>{{$a->id}}</variationid>
                            <link>
                            <![CDATA[{{$domain.$product->name}}?s={{$a->model_title}}]]></link>
                            <availability>Παράδοση 1 έως 3 ημέρες</availability>
                            <manufacturersku>{{$a->Sku}}</manufacturersku>
                             <price_with_vat>{{$product->price}}</price_with_vat>
                            <size>{{$a->model_title}}</size>
                            <quantity>{{$a->quantity}}</quantity>
                        </variation>
                    @endforeach
                </variations>
                <size><![CDATA[ {{implode(',',$sizes)}} ]]></size>
                <color><![CDATA[ {{$product->color->first()->title}} ]]></color>
            </product>

        @endforeach

    </products>
</mywebstore>

