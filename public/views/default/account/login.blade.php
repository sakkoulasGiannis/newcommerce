@extends('default.layouts.app')
@section('content')
    <div class="container mx-auto">
        <div class="grid grid-cols-12 gap-4">
            <div class="md:col-start-5 md:col-span-4 col-span-12 p-4 mt-8">
                <h1 class="text-2xl">ΕΙΣΟΔΟΣ</h1>
                <form method="POST" action="{{ route('login.user') }}">
                    @csrf
                    <div class="mt-4">
                        <label class="block mb-2 text-indigo-500" for="username">Username</label>
                        <input type="text" placeholder="Email" id="email"
                               class="w-full p-2 mb-6 text-indigo-700 border-b-2 border-indigo-500 outline-none focus:bg-gray-300"
                               name="email" required autofocus>
                        @if ($errors->has('email'))
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                    <div class="mb-4">
                        <label class="block mb-2 text-indigo-500" for="password">Password</label>
                        <input type="password" placeholder="Password" id="password"
                               class="w-full p-2 mb-6 text-indigo-700 border-b-2 border-indigo-500 outline-none focus:bg-gray-300"
                               name="password" required>
                        @if ($errors->has('password'))
                            <span class="text-danger">{{ $errors->first('password') }}</span>
                        @endif
                    </div>
                    <div>
                        <button type="submits"
                                class="w-full bg-indigo-700 hover:bg-pink-700 text-white font-bold py-2 px-4 mb-6 rounded">
                            ΕΙΣΟΔΟΣ
                        </button>
                        ΕΓΓΡΑΦΗ
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
