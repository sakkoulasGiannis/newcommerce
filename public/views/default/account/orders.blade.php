@extends('eshoes.layouts.app')
@section('content')
	<div class="container mx-auto">
		<div class="grid grid-cols-6 gap-4 mt-4">
			@include('eshoes.account.userMenu')
			<table class="w-full">
				<tr>
					<th>Αρ</th>
					<th>Shipping Id</th>
					<th>Ημ/νια Δημιουργίας</th>
				</tr>
				@foreach($orders as $o)
					<tr class="w-full">
						<td>{{$o->order_num}}</td>
						<td>{{$o->shipping_id}}</td>
						<td>{{$o->created_at}}</td>
					</tr>
				@endforeach
			</table>
		</div>
	</div>
@endsection
