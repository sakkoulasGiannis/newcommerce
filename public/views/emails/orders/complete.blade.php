@component('mail::message')

# Παραγγελία \#{{$order->order_num}}



# Η ΠΑΡΑΓΓΕΛΙΑ ΣΑΣ ΕΣΤΑΛΕΙ


<div>
  <p>
  Γεια σας, {{$order->detail->billing_name}} {{$order->detail->billing_lastname}}, <br/>
 Η παραγγελία σας με αριθμό {{$order->order_num}} βρίσκεται σε κατάσταση "ολοκληρωμένη ".

@if($order->tracking_number != null)
Ο αριθμός αποστολής του δέματος σας είναι:<b>{{$order->tracking_number}}.</b> <br/>
Εχετε επιλέξει την μέθοδο αποστολής: <b> {{$order->detail->courier_name}}</b>.<br/>

Για να παρακολουθήσετε την εξέλιξη της αποστολής της παραγγελίας σας πατήστε τον παρακάτω σύνδεσμο:
{{-- geniki --}}
@if($order->detail->shipping_id == 2)
 <a href="https://www.taxydromiki.com/track/{{$order->tracking_number}}"> https://www.taxydromiki.com/track/{{$order->tracking_number}}</a>
@endif
{{-- acs --}}
@if($order->detail->shipping_id == 3)
<a href="https://www.acscourier.net">https://www.acscourier.net</a>  και κάντε αναζήτηση
              <br/> τον αριθμό της αποστολής σας: <b>{{$order->tracking_number}}</b>
@endif
{{-- elta --}}
@if($order->detail->shipping_id == 4)
<a href='https://elta.gr/track?code={{$order->tracking_number}}'>https://elta.gr/track?code={{$order->tracking_number}}</a>
@endif
{{-- svuum --}}
@if($order->detail->shipping_id == 5)
  <a href="https://svuum.gr/exypiretisi-pelaton/tracking-orders/"> https://svuum.gr/exypiretisi-pelaton/tracking-orders/ </a>  και κάντε αναζήτηση <br/> τον αριθμό της αποστολής σας: <b>{{$order->tracking_number}}</b>
@endif


@endif





<br/>
<p>Αν έχετε οποιαδήποτε απορία, μη διστάσετε να επικοινωνήσετε μαζί μας στο {{ config('app.email') }} ή μέσω τηλεφώνου στο 2810 228 000.</p>

 </div>






{{--@component('mail::button', ['url' => ''])--}}
{{--    ΔΕΙΤΕ ΤΗΝ ΠΟΡΕΙΑ ΤΗΣ ΠΑΡΑΓΓΕΛΙΑΣ--}}
{{--@endcomponent--}}


<small>
  {{-- Για οποιαδήποτε απορία, παρακαλούμε οπως επικοινωνήσετε μαζί μας στο
  {{config('settings.email')}} ή μέσω τηλεφώνου στο {{config('settings.phone')}} --}}
  <br /> Δευτέρα & Τετάρτη, 9πμ – 4μμ, <br />Τρίτη, Πέμπτη, Παρασκευή 9πμ – 8μμ <br />Σάββατο 10:00 - 14:00.

</small>

<br>
<br>

Ευχαριστούμε,<br>
{{ config('app.name') }}
@endcomponent
