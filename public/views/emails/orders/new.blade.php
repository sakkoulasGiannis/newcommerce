@component('mail::message')

# Νέα Παραγγελία \#{{$order->order_num}}


<div style="font-size: 14px; margin-bottom: 10px;">
    <strong>Ημ/νια Παραγγελίας</strong> {{$order->detail->created_at}}
</div>


<div style="font-size: 12px;">
  Γεια σας, {{$order->detail->billing_name}} {{$order->detail->billing_lastname}}
  <p style="font-size: 12px;">Ευχαριστούμε που επιλέξατε το {{ config('app.name') }} για τις αγορές σας!
  Μολις πραγματοποιηθεί η αποστολή της παραγγελία σας, θα λάβετε ενημερωτικό email
  με τον σχετικό σύνδεσμο για την παρακολουθηση και την εξέλιξή της.</p>

 <p style="font-size: 12px;"> Ακολουθεί η επιβεβαίωση της παραγγελίας σας:</p>
</div>

# Στοιχεία Παραγγελίας
<div class="custom table">
<table >
<tr>
  <td style="width:50%">
    <p style="font-size: 12px;">
      <strong>Πληροφορίες Χρέωσης:</strong><br>
      {{$order->detail->billing_street}} <br />
      T.K. {{$order->detail->billing_zip}} <br>
      {{$order->detail->billing_city}} {{$order->detail->billing_region}} <br />
      {{$order->detail->billing_mobile}}
      <strong>Email </strong> <br/>{{$order->detail->billing_email}}
    </p>

  </td>
  <td style="width:50%">
    <p style="font-size: 12px;">
      <strong>Πληροφορίες αποστολής :</strong><br>
      {{$order->detail->shipping_street}} <br />
      T.K. {{$order->detail->shipping_zip}} <br>
      {{$order->detail->shipping_city}} {{$order->detail->shipping_region}} <br />
      {{$order->detail->shipping_mobile}}
      <strong>Email </strong> <br/> {{$order->detail->shipping_email}}
    </p>

  </td>

</tr>
</table>


<table class="borderd">
<tr>
    <td style="width:50%">
    <p style="font-size: 12px;">
      <strong>Μέθοδος Πληρωμής: </strong><br>
      {{$order->detail->payment_name}}
    </p>
  </td>
  <td style="width:50%">
    <p style="font-size: 12px;">
      <strong>Τρόπος Αποστολής:</strong> <br />
      {{$order->detail->courier_name}}
    </p>
  </td>
</tr>
</table>

</div>

# Προϊόντα Παραγγελίας
<div class="custom table">
  <table class="borderd">
    <thead>
      <tr>
        <th>ΦΩΤΟΓΡΑΦΙΑ</th>
        <th>ΠΡΟΪΟΝ</th>
        <th class="center">ΤΜΧ</th>
        <th class="center">ΤΙΜΗ</th>
        <th class="center">ΣΥΝΟΛΟ</th>
      </tr>
    </thead>
    <tbody >
      @foreach($order->detail->products as $product)
      <tr>
        <td>
          @php
          $img = \Spatie\MediaLibrary\MediaCollections\Models\Media::find($product->id)->first()->original_url;
          @endphp
          <img src="{{$img}}" widt="150" style="width: 100px;"/>
        </td>
        <td>{{$product->title}} <br/>
        <small><b>sku:</b> {{$product->sku}}</small>
        </td>
        <td class="center">{{$product->quantity}}</td>
        <td class="center">{{$product->price}}€</td>
        <td class="right">{{$product->price * $product->quantity}}€</td>
      </tr>
      @endforeach
    </tbody>
  </table>

  <br/>


<div class="custom table">
  <table class="borderd">
    <tbody>
      @if($order->detail->vouchers)
      @foreach($order->detail->vouchers as $voucher)
        <tr>
          <td>Κουπόνι Αρ: {{$voucher->code}} </td>
          <td class="center">{{$voucher->discount_amount}} % </td>
        </tr>
      @endforeach
    @endif
    <tr>
      <td><b>Κόστος Μεταφορικών</b></td>
      <td >
        <b>{{number_format($order->detail->shipping_cost, 2)}}€</b>
      </td>
    </tr>
      <tr>
          <td><b>Κώστος Πληρωμής</b></td>
          <td >
              <b>{{number_format($order->detail->payment_cost, 2)}}€</b>
          </td>
      </tr>
    <tr>
      <td><b>Σύνολο</b></td>
      <td class="center"><b>{{number_format($order->detail->total + $order->detail->shipping_cost, 2)}}€</b> </td>
    </tr>
    </tbody>
  </table>
</div>


</div>
<div style="clear: right;">  </div>
<br/>



{{--@component('mail::button', ['url' => ''])--}}
{{--    ΔΕΙΤΕ ΤΗΝ ΠΟΡΕΙΑ ΤΗΣ ΠΑΡΑΓΓΕΛΙΑΣ--}}
{{--@endcomponent--}}


<small>
  Για οποιαδήποτε απορία, παρακαλούμε οπως επικοινωνήσετε μαζί μας στο
  {{config('settings.email')}} ή μέσω τηλεφώνου στο {{config('settings.phone')}}
  <br /> Δευτέρα & Τετάρτη, 9πμ – 4μμ, <br />Τρίτη, Πέμπτη, Παρασκευή 9πμ – 8μμ <br />Σάββατο 10:00 - 14:00.

</small>

Ευχαριστούμε,<br>
{{ config('app.name') }}
@endcomponent
