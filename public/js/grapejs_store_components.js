grapesjs.plugins.add('save-template', (editor) => {
    // Προσθέτουμε ένα κουμπί στη γραμμή εργαλείων
    editor.Panels.addButton('options', {
        id: 'save-components',
        className: 'fa fa-save',
        command: 'save-template',

        attributes: { title: 'Save Components' }
    });

    // Ορισμός της εντολής που θα εκτελείται όταν πατηθεί το κουμπί
    editor.Commands.add('save-template', {
        run(editor, sender) {
            sender && sender.set('active', 0); // Απενεργοποίηση του κουμπιού μετά το πάτημα

            // Απόκτηση των components ως JSON string
            const componentsJson = JSON.stringify(editor.getComponents());

            // Αποστολή του JSON σε έναν διακομιστή
            fetch('/save-template', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ components: componentsJson }),
            })
                .then(response => response.json())
                .then(data => {
                    console.log('Components saved successfully:', data);
                })
                .catch((error) => {
                    console.error('Error saving components:', error);
                });
        }
    });
});
grapesjs.plugins.add('save-selected-component-plugin', (editor) => {
    // Προσθήκη κουμπιού στο toolbar των components
    editor.on('component:toggled', (model) => {
        if (model) {
            const toolbar = model.get('toolbar') || [];

            // Έλεγχος αν το κουμπί αποθήκευσης υπάρχει ήδη στο toolbar
            if (!toolbar.some(btn => btn.command === 'save-selected-component')) {
                toolbar.push({
                    labels: 'Save',
                    attributes: { class: 'fa fa-save', title: 'Save Component' },
                    command: 'save-selected-component',
                });
                model.set('toolbar', toolbar);
            }
        }
    });

    // Ορισμός της εντολής που θα αποθηκεύει το επιλεγμένο component
    editor.Commands.add('save-selected-component', {
        run(editor, sender) {
            const selectedComponent = editor.getSelected(); // Απόκτηση του επιλεγμένου component

            if (!selectedComponent) {
                alert('No component selected!');
                return;
            }

            // Δημιουργία του modal για την επιλογή κατηγορίας
            const modal = editor.Modal;
            const content = document.createElement('div');
            const select = document.createElement('select');

            // Προκαθορισμένες κατηγορίες
            const categories = ['basic', 'header', 'footer', 'heros', 'promo', 'product_item', 'headers', 'footers', 'promos', 'other'];

            categories.forEach(category => {
                const option = document.createElement('option');
                option.value = category;
                option.textContent = category.charAt(0).toUpperCase() + category.slice(1);
                select.appendChild(option);
            });

            // Προσθήκη του select στο modal
            content.innerHTML = '<p>Select a category to save the component:</p>';
            content.appendChild(select);
            const titleInput = document.createElement('input');
            titleInput.type = 'text';
            titleInput.placeholder = 'Enter title...';
            titleInput.style.marginBottom = '10px';

            content.appendChild(titleInput);


            // Προσθήκη κουμπιού "Save" στο modal
            const saveButton = document.createElement('button');
            saveButton.textContent = 'Save';
            saveButton.style.marginTop = '10px';
            saveButton.style.marginLeft = '10px';
            saveButton.className = 'gjs-btn-prim';
            saveButton.onclick = () => {
                const selectedCategory = select.value;
                const title = titleInput.value.trim();
                if (!title) {
                    alert('Please enter a title.');
                    return;
                }
                // Μετατροπή του component σε JSON string
                const componentJson = JSON.stringify(selectedComponent.toJSON());

                // Αποστολή του JSON σε έναν διακομιστή μαζί με την κατηγορία
                fetch('/manage/save-component', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({ component: componentJson, category: selectedCategory, title: title }),
                })
                    .then(response => response.json())
                    .then(data => {
                        console.log('Component saved successfully in category:', selectedCategory, 'with title:', title, data);
                        modal.close(); // Κλείσιμο του modal μετά την αποθήκευση
                    })
                    .catch((error) => {
                        console.error('Error saving component:', error);
                    });
            };

            content.appendChild(saveButton);
            modal.setTitle('Save Component');
            modal.setContent(content);
            modal.open();
        }
    });
});


grapesjs.plugins.add('download-component-plugin', (editor) => {
    // Προσθήκη κουμπιού στο toolbar των components
    editor.on('component:toggled', (model) => {
        if (model) {
            const toolbar = model.get('toolbar') || [];

            // Έλεγχος αν το κουμπί λήψης υπάρχει ήδη στο toolbar
            if (!toolbar.some(btn => btn.command === 'download-selected-component')) {
                toolbar.push({
                    attributes: { class: 'fa fa-download', title: 'Download Component' },
                    command: 'download-selected-component',
                });
                model.set('toolbar', toolbar);
            }
        }
    });

    // Ορισμός της εντολής που θα κατεβάζει το επιλεγμένο component
    editor.Commands.add('download-selected-component', {
        run(editor, sender) {
            const selectedComponent = editor.getSelected(); // Απόκτηση του επιλεγμένου component

            if (!selectedComponent) {
                alert('No component selected!');
                return;
            }

            // Μετατροπή του component σε JSON string
            const componentJson = JSON.stringify(selectedComponent.toJSON(), null, 2);

            // Δημιουργία και κατέβασμα του αρχείου JSON
            const blob = new Blob([componentJson], { type: 'application/json' });
            const url = URL.createObjectURL(blob);
            const a = document.createElement('a');
            a.href = url;
            a.download = 'component.json';
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
            URL.revokeObjectURL(url);
        }
    });
});


grapesjs.plugins.add('download-component-plugin', (editor) => {
    // Προσθήκη κουμπιού στο toolbar των components
    editor.on('component:toggled', (model) => {
        if (model) {
            const toolbar = model.get('toolbar') || [];

            // Έλεγχος αν το κουμπί λήψης υπάρχει ήδη στο toolbar
            if (!toolbar.some(btn => btn.command === 'download-selected-component')) {
                toolbar.push({
                    attributes: { class: 'fa fa-download', title: 'Download Component' },
                    command: 'download-selected-component',
                });
                model.set('toolbar', toolbar);
            }
        }
    });

    // Ορισμός της εντολής που θα κατεβάζει το επιλεγμένο component
    editor.Commands.add('download-selected-component', {
        run(editor, sender) {
            const selectedComponent = editor.getSelected(); // Απόκτηση του επιλεγμένου component

            if (!selectedComponent) {
                alert('No component selected!');
                return;
            }

            // Μετατροπή του component σε JSON string
            const componentJson = JSON.stringify(selectedComponent.toJSON(), null, 2);

            // Δημιουργία και κατέβασμα του αρχείου JSON
            const blob = new Blob([componentJson], { type: 'application/json' });
            const url = URL.createObjectURL(blob);
            const a = document.createElement('a');
            a.href = url;
            a.download = 'component.json';
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
            URL.revokeObjectURL(url);
        }
    });
});
