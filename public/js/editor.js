setTimeout(function () {
    console.log('Script initialized');

//     // Function to close all mega-menus
//     function closeAllMegaMenus() {
//         document.querySelectorAll('.dropdown-content').forEach(menu => {
//             menu.style.display = 'none';
//         });
//     }
//
//     // Function to toggle the mega-menu
//     function toggleMegaMenu(event) {
//         event.preventDefault();
//
//         const span = event.currentTarget;
//         let megaMenu = span.nextElementSibling;
//
//         // If the next sibling is not a mega-menu, find the closest .nav-item and get the .mega-menu
//         if (!megaMenu || !megaMenu.classList.contains('dropdown-content')) {
//             const navItem = span.closest('.dropdown');
//             if (navItem) {
//                 megaMenu = navItem.querySelector('.dropdown-content');
//             }
//         }
//
//         // Check if the megaMenu is found and is of the right class
//         if (!megaMenu || !megaMenu.classList.contains('dropdown-content')) {
//             console.log('No corresponding dropdown-content found');
//             return;
//         }
//
//         // Get the current display state of the mega-menu
//         const isCurrentlyVisible = megaMenu.style.display === 'block' || megaMenu.style.display === '';
//
//         // Close all other mega-menus
//         closeAllMegaMenus();
//
//         // Toggle the display of the selected mega-menu
//         megaMenu.style.display = isCurrentlyVisible ? 'none' : 'block';
//         console.log('Toggled mega-menu visibility:', megaMenu.style.display);
//     }
//
//     // Add event listener to each span.open
//     const openItems = document.querySelectorAll('.open');
//
//     openItems.forEach(item => {
//         console.log('clicked span')
//         item.addEventListener('click', toggleMegaMenu);
//
//     });
//
//     // Ensure that clicking outside of the mega-menu closes it
//     document.addEventListener('click', (event) => {
//         console.log('click event')
//         if (!event.target.closest('.dropdown') && !event.target.closest('.dropdown-content') && !event.target.closest('.open')) {
//             console.log('Clicked outside, closing all mega-menus');
//             closeAllMegaMenus();
//         }
//     });
//
// //    toggle menu for edit
//     document.addEventListener('DOMContentLoaded', function() {
//         // Βρίσκουμε όλα τα στοιχεία που έχουν την κλάση 'desktop-item'
//         const desktopItems = document.querySelectorAll('.desktop-item');
//
//
//         desktopItems.forEach(item => {
//             item.addEventListener('click', function(event) {
//                 event.preventDefault(); // Αποτρέπουμε την προεπιλεγμένη ενέργεια του συνδέσμου
//
//                 // Βρίσκουμε το κοντινότερο mega-box
//                 let megaBox = this.nextElementSibling;
//
//                 while (megaBox && !megaBox.classList.contains('mega-box')) {
//                     megaBox = megaBox.nextElementSibling;
//                 }
//
//                 if (megaBox) {
//                     // Αφαιρούμε την κλάση 'open-mega' από όλα τα mega-box
//                     document.querySelectorAll('.mega-box').forEach(box => {
//                         if (box !== megaBox) {
//                             box.classList.remove('open-mega');
//                         }
//                     });
//
//                     // Toggle την κλάση 'open-mega' στο mega-box που βρήκαμε
//                     megaBox.classList.toggle('open-mega');
//                 }
//             });
//         });
//
//         // Κλείσιμο του mega-menu όταν κάνετε κλικ έξω από αυτό
//         document.addEventListener('click', function(event) {
//             if (!event.target.closest('.mega-box') && !event.target.closest('.desktop-item')) {
//                 document.querySelectorAll('.mega-box.open-mega').forEach(box => {
//                     box.classList.remove('open-mega');
//                 });
//             }
//         });
//     });

    // pure css nav code
//     const toggleButton = document.querySelector('.menu-toggle');
//     const closeButton = document.querySelector('.menu-close');
//     const navMenu = document.getElementById('nav-menu');
//     const dropdownToggles = document.querySelectorAll('.dropdown-toggle');
//
//     // Toggle the nav menu
//     toggleButton.addEventListener('click', () => {
//         navMenu.classList.toggle('open');
//     });
//
//     // Close the nav menu
//     closeButton.addEventListener('click', () => {
//         navMenu.classList.remove('open');
//     });
//
//     // Toggle the mega menus on click
//     dropdownToggles.forEach(toggle => {
//         toggle.addEventListener('click', (e) => {
//             e.preventDefault(); // Αποφυγή της προεπιλεγμένης ενέργειας του συνδέσμου
//
//             const megaMenu = toggle.nextElementSibling;
//
//             // Κλείσε όλα τα άλλα ανοιχτά μενού
//             document.querySelectorAll('.mega-menu.open').forEach(menu => {
//                 if (menu !== megaMenu) {
//                     menu.classList.remove('open');
//                 }
//             });
//
//             // Εναλλαγή της προβολής του επιλεγμένου μενού
//             megaMenu.classList.toggle('open');
//         });
//     });
//
// //     close mobile menu on click out
//
//     // Close the nav menu if clicking outside of it
//     document.addEventListener('click', (event) => {
//         if (!navMenu.contains(event.target) && !toggleButton.contains(event.target)) {
//             navMenu.classList.remove('open');
//         }
//     });
//
//     // toggle mobile menu ον gestures
//     let startX = 0;
//     let endX = 0;
//
//     // Αναγνωριστικό Gesture για touchstart
//     document.addEventListener('touchstart', (event) => {
//         startX = event.touches[0].clientX;
//     });
//
//     // Αναγνωριστικό Gesture για touchmove
//     document.addEventListener('touchmove', (event) => {
//         endX = event.touches[0].clientX;
//     });
//
//     // Αναγνωριστικό Gesture για touchend
//     document.addEventListener('touchend', () => {
//         const swipeDistance = endX - startX;
//
//         // Αν το swipe είναι από αριστερά προς τα δεξιά (swipe δεξιά)
//         if (swipeDistance > 50) {
//             // Άνοιγμα μενού εάν το swipe είναι δεξιά
//             navMenu.classList.add('open');
//         }
//
//         // Αν το swipe είναι από δεξιά προς τα αριστερά (swipe αριστερά)
//         if (swipeDistance < -50) {
//             // Κλείσιμο μενού εάν το swipe είναι αριστερά
//             navMenu.classList.remove('open');
//         }
//     });
    document.addEventListener('DOMContentLoaded', () => {
        const toggleButton = document.querySelector('.menu-toggle');
        const closeButton = document.querySelector('.menu-close');
        const navMenu = document.getElementById('nav-menu');
        const dropdownToggles = document.querySelectorAll('.dropdown-toggle');

        // Ελέγχουμε αν τα στοιχεία υπάρχουν πριν προσθέσουμε τους event listeners
        if (toggleButton && navMenu) {
            // Toggle το μενού πλοήγησης
            toggleButton.addEventListener('click', () => {
                navMenu.classList.toggle('open');
            });
        }

        if (closeButton && navMenu) {
            // Κλείσιμο του μενού πλοήγησης
            closeButton.addEventListener('click', () => {
                navMenu.classList.remove('open');
            });
        }

        if (dropdownToggles.length > 0) {
            // Εναλλαγή των mega menus με κλικ
            dropdownToggles.forEach(toggle => {
                toggle.addEventListener('click', (e) => {
                    e.preventDefault(); // Αποφυγή της προεπιλεγμένης ενέργειας του συνδέσμου

                    const megaMenu = toggle.nextElementSibling;

                    // Κλείσιμο όλων των άλλων ανοιχτών μενού
                    document.querySelectorAll('.mega-menu.open').forEach(menu => {
                        if (menu !== megaMenu) {
                            menu.classList.remove('open');
                        }
                    });

                    // Εναλλαγή της προβολής του επιλεγμένου μενού
                    if (megaMenu) {
                        megaMenu.classList.toggle('open');
                    }
                });
            });
        }

        // Κλείσιμο του μενού πλοήγησης αν γίνει κλικ εκτός αυτού
        document.addEventListener('click', (event) => {
            if (navMenu && toggleButton && !navMenu.contains(event.target) && !toggleButton.contains(event.target)) {
                navMenu.classList.remove('open');
            }
        });

        // Επεξεργασία χειρονομιών για την εναλλαγή του μενού πλοήγησης
        let startX = 0;
        let endX = 0;

        // Αναγνωριστικό Gesture για touchstart
        document.addEventListener('touchstart', (event) => {
            startX = event.touches[0].clientX;
        });

        // Αναγνωριστικό Gesture για touchmove
        document.addEventListener('touchmove', (event) => {
            endX = event.touches[0].clientX;
        });

        // Αναγνωριστικό Gesture για touchend
        document.addEventListener('touchend', () => {
            const swipeDistance = endX - startX;

            // Άνοιγμα μενού αν το swipe είναι δεξιά
            if (navMenu && swipeDistance > 50) {
                navMenu.classList.add('open');
            }

            // Κλείσιμο μενού αν το swipe είναι αριστερά
            if (navMenu && swipeDistance < -50) {
                navMenu.classList.remove('open');
            }
        });
    });

}, (1500));
