const globalSettingsPlugin = (editor, opts = {}) => {
    // Default Options
    const defaultOptions = {
        fonts: ['Arial', 'Helvetica', 'sans-serif'],
        googleFonts: '',
        primaryColor: '#3498db',
        secondaryColor: '#2ecc71',
        textColor: '#333333',
        backgroundColor: '#ffffff',
        containerWidth: '1200px',
    };


    // Συγχώνευση των default options με αυτές που παρέχονται από τον χρήστη
    const config = { ...defaultOptions, ...opts };

    // Προσθήκη global CSS variables και container width
    const addGlobalStyles = () => {
        const styleTag = document.createElement('style');
        styleTag.innerHTML = `
      :root {
        --primary-color: ${config.primaryColor};
        --secondary-color: ${config.secondaryColor};
        --text-color: ${config.textColor};
        --background-color: ${config.backgroundColor};
        --container-width: ${config.containerWidth};
        --font-family: ${config.fonts.join(', ')};
      }
      .container {
        max-width: var(--container-width);
        margin: 0 auto;
      }
    `;
        document.head.appendChild(styleTag);


        // Προσθήκη Google Fonts αν έχουν οριστεί
        if (config.googleFonts) {
            const linkTag = document.createElement('link');
            linkTag.rel = 'stylesheet';
            linkTag.href = `https://fonts.googleapis.com/css2?family=${config.googleFonts.replace(' ', '+')}&display=swap`;
            document.head.appendChild(linkTag);

            // Ενημέρωση της γραμματοσειράς για χρήση του Google Font
            document.documentElement.style.setProperty('--font-family', `'${config.googleFonts}', ${config.fonts.join(', ')}`);
        }
    };

    // Κλήση της συνάρτησης για να προσθέσει τα styles
    addGlobalStyles();

    // Προσθήκη του κουμπιού στο GrapeJS options panel
    const panel = editor.Panels.getPanel('options');

    if (!panel.get('buttons').find(btn => btn.id === 'open-global-settings')) {
        editor.Panels.addButton('options', {
            id: 'open-global-settings',
            className: 'fa fa-cog',
            command: 'open-global-settings',
            attributes: { title: 'Global Settings' },
        });
    }

    // Προσθήκη του command για άνοιγμα του global settings panel
    editor.Commands.add('open-global-settings', {
        run: function(editor, sender) {
            sender && sender.set('active', 0);

            const modal = editor.Modal;
            const content = `
        <div style="padding: 10px;">
          <h3>Global Settings</h3>
          <div style="margin-bottom: 10px;">
            <label>Primary Color:</label><br>
            <input type="color" id="primaryColorInput" value="${config.primaryColor}">
          </div>
          <div style="margin-bottom: 10px;">
            <label>Secondary Color:</label><br>
            <input type="color" id="secondaryColorInput" value="${config.secondaryColor}">
          </div>
          <div style="margin-bottom: 10px;">
            <label>Text Color:</label><br>
            <input type="color" id="textColorInput" value="${config.textColor}">
          </div>
          <div style="margin-bottom: 10px;">
            <label>Background Color:</label><br>
            <input type="color" id="backgroundColorInput" value="${config.backgroundColor}">
          </div>
          <div style="margin-bottom: 10px;">
            <label>Fonts:</label><br>
            <input type="text" id="fontsInput" value="${config.fonts.join(', ')}" style="width: 100%;">
          </div>
          <div style="margin-bottom: 10px;">
            <label>Google Font (π.χ. Roboto):</label><br>
            <input type="text" id="googleFontsInput" value="${config.googleFonts}" style="width: 100%;">
          </div>
          <div style="margin-bottom: 10px;">
            <label>Container Width:</label><br>
            <input type="text" id="containerWidthInput" value="${config.containerWidth}" style="width: 100%;">
          </div>
          <button id="saveGlobalSettings" style="margin-top: 10px;">Save</button>
        </div>
      `;

            modal.setTitle('Global Settings');
            modal.setContent(content);
            modal.open();

            document.getElementById('saveGlobalSettings').onclick = function() {
                // Λήψη των τιμών από το modal
                config.primaryColor = document.getElementById('primaryColorInput').value;
                config.secondaryColor = document.getElementById('secondaryColorInput').value;
                config.textColor = document.getElementById('textColorInput').value;
                config.backgroundColor = document.getElementById('backgroundColorInput').value;
                config.fonts = document.getElementById('fontsInput').value.split(',').map(f => f.trim());
                config.googleFonts = document.getElementById('googleFontsInput').value.trim();
                config.containerWidth = document.getElementById('containerWidthInput').value.trim();

                // Εφαρμογή των αλλαγών
                addGlobalStyles();
                modal.close();
            };
        }
    });
};

// Εγκατάσταση του plugin στο GrapeJS
grapesjs.plugins.add('global-settings-plugin', globalSettingsPlugin);
