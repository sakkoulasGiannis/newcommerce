grapesjs.plugins.add('heading-plugin', (editor, opts = {}) => {
    const defaultOpts = {
        // Μπορείς να προσθέσεις προεπιλεγμένες επιλογές εδώ
        // Για παράδειγμα, προεπιλεγμένες τιμές ή ιδιότητες
    };

    const options = {...defaultOpts, ...opts};

    // Προσθήκη του κουμπιού για headings στο panel
    editor.Panels.addButton('options', [{
        id: 'add-heading',
        active: true, // Το κουμπί είναι ενεργό από πριν
        label: 'Add Heading',
        className: 'fa fa-header', // Χρησιμοποιεί εικονίδιο header
        command: 'add-heading',
        category: 'Basic',
        attributes: { title: 'Add Heading' },
    }]);

    // Προσθήκη εντολών για την προσθήκη διαφορετικών headings
    editor.Commands.add('add-heading', {
        run(editor, sender) {
            const types = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'];
            const options = types.map(type => ({
                text: type.toUpperCase(),
                value: type,
            }));

            editor.runCommand('open-heading-modal', { options });
        }
    });

    // Δημιουργία εντολής για το modal
    editor.Commands.add('open-heading-modal', {
        run(editor, { options }) {
            const modal = editor.Modal;
            const select = document.createElement('select');
            select.innerHTML = options.map(option => `<option value="${option.value}">${option.text}</option>`).join('');

            const form = document.createElement('div');
            form.innerHTML = `<label>Select Heading:</label>`;
            form.appendChild(select);

            modal.setTitle('Add Heading');
            modal.setContent(form);
            modal.open();

            modal.once('open', () => {
                modal.getButtons()[0].set('label', 'Add');
                modal.getButtons()[0].on('click', () => {
                    const selectedValue = select.value;
                    editor.addComponents(`<${selectedValue}>${selectedValue} Text</${selectedValue}>`);
                    modal.close();
                });
            });
        }
    });
});
