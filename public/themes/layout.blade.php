
<!DOCTYPE html>
<html lang="el">

<head>

    <meta charset="UTF-8">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    {{--    <link href="{{ asset('views/chronoro/dist/styles.css') }}" rel="stylesheet">--}}
    <script src="https://cdn.jsdelivr.net/npm/alpinejs@3.x.x/dist/cdn.min.js" defer></script>
{{--        <script src="https://cdn.tailwindcss.com"></script>--}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tailwindcss@latest/dist/tailwind.min.css">

</head>
<body>



@include(config('app.theme_folder') . '.header')



 {{-- </footer> --}}

</body>

</html>
