@extends('default.layout')
@section('content')
    <div class="max-w-8xl mx-auto   md:mt-0 pb-16   sm:pb-24">

        <div class="grid grid-cols-12 md:grid-cols-12 md:gap-4">
            <div class="col-span-12 pb-4 ">

                @include(\Config::get('view.theme').'.breadcrumbs', ['breadcrumbs' => $breadcrumbs, 'current' => null])
                {{--                <button id="filterToggleButton" class="p-2 bg-blue-500 text-white rounded-md mt-12">Toggle Filters</button>--}}
            </div>

            <div id="filters" x-data={open:true}
                 class="col-span-12 bg-white md:col-span-3 border-r fixed h-full left-0 p-4 transform transition-transform z-10 sm:static sm:-translate-x-full sm:block sm:max-w-xs md:block md:translate-x-0 top-0">
                {{-- <filters :products="{{ json_encode($allProducts) }}" :category="{{ $category }}" /> --}}
                @include(\Config::get('view.theme').'.filters')
            </div>

            <div class="col-span-12 md:col-span-9">

                <div class="text-right">
                    <button id="filterToggleButton"
                            class="cursor-pointer  text-white  md:hidden fixed bottom-20 bg-[#c09578] right-4 z-10 p-2">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="h-6 w-6 inline">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M6 13.5V3.75m0 9.75a1.5 1.5 0 0 1 0 3m0-3a1.5 1.5 0 0 0 0 3m0 3.75V16.5m12-3V3.75m0 9.75a1.5 1.5 0 0 1 0 3m0-3a1.5 1.5 0 0 0 0 3m0 3.75V16.5m-6-9V3.75m0 3.75a1.5 1.5 0 0 1 0 3m0-3a1.5 1.5 0 0 0 0 3m0 9.75V10.5" />
                        </svg>
                        <span class="hide-on-scroll">Φίλτρα </span>
                    </button>
                </div>

                <div class=" flex  justify-end  pb-4 gap-4">
                    <div class="">
                        <form method="GET">
                            <select onchange="this.form.submit()"
                                    class=" w-full rounded-md border-0 py-1.5 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                    name="sort">


                                <option
                                    {{ !request()->exists('sort') && request()->get('sort') == '' ? 'selected' : null }}
                                    value="">Νέα Προϊόντα
                                </option>
                                <option
                                    {{ request()->exists('sort') && request()->get('sort') == 'price_asc' ? 'selected' : null }}
                                    value="price_asc">Τιμή Αύξουσα
                                </option>
                                <option
                                    {{ request()->exists('sort') && request()->get('sort') == 'price_desc' ? 'selected' : null }}
                                    value="price_desc">Τιμή Φθίνουσα
                                </option>
                                <option
                                    {{ request()->exists('sort') && request()->get('sort') == 'discount_asc' ? 'selected' : null }}
                                    value="discount_asc">Ποσοστό Έκτπωσης Αύξουσα
                                </option>
                                <option
                                    {{ request()->exists('sort') && request()->get('sort') == 'discount_desc' ? 'selected' : null }}
                                    value="discount_desc">Ποσοστό Έκπτωσης Φθίνουσα
                                </option>
                                <option
                                    {{ request()->exists('sort') && request()->get('sort') == 'season_asc' ? 'selected' : null }}
                                    value="season_asc">Νέες Παραλαβές Αύξουσα
                                </option>
                                <option
                                    {{ request()->exists('sort') && request()->get('sort') == 'season_desc' ? 'selected' : null }}
                                    value="season_desc">Νέες Παραλαβές Φθίνουσα
                                </option>
                            </select>
                        </form>
                    </div>
                    <form method="GET">
                        <select
                            class=" w-full rounded-md border-0 py-1.5 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6"
                            onChange='submit();' name="pagination">
                            <option value="32"
                                {{ session()->get('paginate') && session()->get('paginate') == 32 ? 'selected' : null }}>
                                32
                            </option>
                            <option value="64"
                                {{ session()->get('paginate') && session()->get('paginate') == 64 ? 'selected' : null }}>
                                64
                            </option>
                            <option value="96"
                                {{ session()->get('paginate') && session()->get('paginate') == 96 ? 'selected' : null }}>
                                96
                            </option>
                            <option value="128"
                                {{ session()->get('paginate') && session()->get('paginate') == 128 ? 'selected' : null }}>
                                128
                            </option>
                        </select>
                    </form>

                    <div class="flex justify-end">
                        {{--@include(\Config::get('view.theme').'.pagination', ['data' => $products])--}}
                    </div>

                </div>

                <hr>
                <div class="grid grid-cols-2 gap-6 md:grid-cols-3 mt-4">

                    @foreach ($products as $product)
                        @include(\Config::get('view.theme').'.product_card', $product)
                    @endforeach
                </div>
                <div class="center pt-4 text-center">
                                        {{ $products->links(config('view.theme').'.pagination') }}
                </div>

            </div>
        </div>
    </div>
@endsection
@section('page_scripts')
    <script>
        document.addEventListener("DOMContentLoaded", function () {
            const filters = document.getElementById('filters');
            const filterToggleButton = document.getElementById('filterToggleButton');
            const toggleClose = document.getElementById('toggle-mobile-menu');

            // Λειτουργία για το άνοιγμα/κλείσιμο του μενού με κλικ στο κουμπί
            function handleToggleMenu() {
                filters.classList.toggle('hidden');
            }

            filterToggleButton.addEventListener('click', handleToggleMenu);
            toggleClose.addEventListener('click', handleToggleMenu);

            // Κλείσιμο του μενού όταν κάνεις κλικ έξω από αυτό
            document.addEventListener('click', function (event) {
                const isClickInside = filters.contains(event.target) || filterToggleButton.contains(event.target);
                if (!isClickInside && !filters.classList.contains('hidden')) {
                    filters.classList.add('hidden');
                }
            });

            // Gestures για άνοιγμα/κλείσιμο του μενού
            let startX;

            document.addEventListener('touchstart', function (event) {
                startX = event.touches[0].clientX;
            });

            document.addEventListener('touchmove', function (event) {
                if (!startX) return;

                let currentX = event.touches[0].clientX;
                let diffX = startX - currentX;

                // Αν η διαφορά είναι αρνητική (swipe από αριστερά προς δεξιά)
                if (diffX < -50 && filters.classList.contains('hidden')) {
                    filters.classList.remove('hidden');
                }

                // Αν η διαφορά είναι θετική (swipe από δεξιά προς αριστερά)
                if (diffX > 50 && !filters.classList.contains('hidden')) {
                    filters.classList.add('hidden');
                }
            });

            document.addEventListener('touchend', function () {
                startX = null;  // Επαναφορά της αρχικής τιμής για το επόμενο gesture
            });

            // Λογική για την εφαρμογή της class 'hidden' ανάλογα με το μέγεθος της οθόνης
            function handleResize() {
                if (window.innerWidth < 640) {
                    filters.classList.add('hidden'); // Κρύβει το μενού σε mobile
                } else {
                    filters.classList.remove('hidden'); // Εμφανίζει το μενού σε desktop
                }
            }

            // Αρχική κλήση της handleResize για να ελέγξει το μέγεθος της οθόνης
            handleResize();

            // Προσθήκη event listener για την αλλαγή του μεγέθους της οθόνης
            window.addEventListener('resize', handleResize);


            window.addEventListener('scroll', function() {
                var buttonText = document.querySelector('.hide-on-scroll');
                if (window.scrollY > 50) {
                    buttonText.style.display = 'none';
                } else {
                    buttonText.style.display = 'inline';
                }
            });

        });


    </script>
    <style>
        @media (max-width: 640px) {
            #filters {
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                width: 80%;
                /*transform: translateX(-100%);*/
            }
        }

    </style>
@endsection
