     <main class="mx-auto mt-6 bg-white ">


            <aside>
                <h2 class="sr-only">ΦΙΛΤΡΑ</h2>

                <div class="">

                    <div class="ml-3 absolute -right-10 h-7 items-center md:hidden" id="">
                        <button id="toggle-mobile-menu" type="button" class="relative rounded-md bg-white text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">
                            <span class="absolute -inset-2.5"></span>
                            <span class="sr-only">Close panel</span>
                            <svg class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
                            </svg>
                        </button>
                    </div>
                    <form ref="form" method="GET" action="{{ url()->current() }}" class="  divide-y divide-gray-200">
                        <div>
                            <fieldset>
                                <legend class="block text-sm font-medium text-[#c09578]">Εκπτωτικά Προϊόντα</legend>
                                <div class="space-y-3 pt-2">
                                    <div class="switch mb-2">
                                        <label class="flex items-center">
                                            <input onChange='submit();' type="checkbox" name="discounted" value="true" {{ request()->has('discounted') && request()->get('discounted') == 'true' ? 'checked' : null }} class="checkbox-orange h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500" />
                                            <span class="ml-2 text-sm text-gray-600">Εκπτωτικά Προϊόντα</span>
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                        </div>

                        <input type="hidden" name="query" value="true">
                        @foreach ($attributes as $attribute)
                            <div class="pt-2">
                                <fieldset>
                                    <legend class="block text-sm font-medium text-[#c09578]">{{ $attribute['title'] }}</legend>
                                    @if ($attribute['terms'] && count($attribute['terms']) > 8)
                                        <input class="border-1 w-full border-gray-400 p-2 text-sm" type="text" id="search{{ $attribute['name'] }}" onkeyup="searchFilterValues('{{ $attribute['name'] }}')" placeholder="Αναζήτηση  {{ $attribute['title'] }}"  title="Αναζήτηση">
                                    @endif
                                    <div class="filter-block mb-4 max-h-48 overflow-auto scroll-smooth p-2 hover:scroll-auto space-y-3 pt-2" id="filterValuesContainer{{ $attribute['name'] }}">
                                        @if($attribute['terms'])
                                            @foreach ($attribute['terms'] as $value)
                                                @if ($value['title'] != '')
                                                    <label class="flex items-center" id="field{{ $attribute['name'] }}">
                                                        <input onChange='submit();' type="checkbox"  name="{{ $attribute['name'] }}[]" value="{{ $value['name'] }}"
                                                               {{(is_array(request()->get($attribute['name'])) && in_array($value['name'], request()->get($attribute['name']))) ? 'checked' : ''}} class="checkbox-orange h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500" />
                                                        <span class="text-es-blue ml-2 text-sm text-gray-600">{{ $value['title'][$local] }}</span>
                                                    </label>
                                                @else
                                                    <label class="flex items-center" id="field{{ $attribute['name'] }}">
                                                        <input onChange='submit();' type="checkbox"  name="{{ $attribute['name'] }}"  value="{{ $value['name'] }}"
                                                               {{ request()->has($attribute['name']) && $value['name'] == request()->get($attribute['name']) ? 'checked ' : null }} class="checkbox-orange h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500" />
                                                        <span class="text-es-blue ml-2 text-sm text-gray-600">{{ $value['title'][$local] }}</span>
                                                    </label>
                                                @endif
                                            @endforeach
                                        @endif
                                    </div>
                                </fieldset>
                            </div>
                        @endforeach
                        <button class="bg-es-blue px-2 py-2 text-white">Αναζήτηση</button>
                    </form>
                </div>
            </aside>

    </main>

