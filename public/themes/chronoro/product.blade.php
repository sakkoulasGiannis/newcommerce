@extends('default.layout', ['product' => $product])
@section('content')

    <div class="bg-white">
        <div class="pb-6 pt-6 ">
            @include(\Config::get('view.theme').'.breadcrumbs', ['breadcrumbs' => $breadcrumbs, 'current' => $product->title])
            <div class="mx-auto mt-8 max-w-2xl px-4 sm:px-6 lg:max-w-8xl lg:px-8">
                <div class="lg:grid lg:auto-rows-min lg:grid-cols-12 lg:gap-x-8">
                    @include(\Config::get('view.theme').'.product_gallery', ['product' => $product])
                    <div class="lg:col-span-5 lg:col-start-8">
                        <div>
                            <h1 class="text-xl font-medium text-gray-900">{{$product->title}}</h1>
                            <span class="text-gray-500">sku: {{$product->sku}}</span>
                            <p class="text-2xl italic font-black  text-[#c09578] productPrice first-color priceColor mt-4">{{$product->price}}€
                                @if(isset($product->originalPrice))
                                    <span class="ml-2 text-3xl font-medium italic text-gray-700 line-through">{{$product->originalPrice}}€</span>
                                @endif
                            </p>
                        </div>


                        @livewire('new-item-to-cart-info')

                        @hook('beforeAddToCartBtn')


                        @if(count($stock) > 1)
                            <div id="variableSelect">
                                <livewire:add-variable-to-cart :stock="$stock" :product="$product" />

                            </div>

                        @else

                            @foreach($stock as $s)
                                <livewire:add-to-cart :product="$product" :productId="$product->id" :stock="$s"/>
                            @endforeach

                        @endif

                        @hook('afterAddToCartBtn', ['product' => $product])

                        <div class="mt-4">

                            {!! $product->small_description !!}
                        </div>

                        @include(\Config::get('view.theme').'.promo_product')


                    </div>
                    <!-- Image gallery -->
                    <div class="mt-8 lg:col-span-5">

                        <!-- Color picker -->


                        <div class="grid grid-cols-2 gap-4 md:grid-cols-3 mt-4">
                            {{--                            @foreach($product->crossSellProducts as $crossSell)--}}
                            {{--                                @dd(\App\Models\Product::find($crossSell->cross_sell_product_id))--}}
                            {{--                                @include(\Config::get('view.theme').'.product_card', ['product' => $crossSell])--}}
                            {{--                            @endforeach--}}
                        </div>

                        {{--                            <button type="submit" class="mt-8 flex w-full items-center justify-center rounded-md border border-transparent bg-indigo-600 px-8 py-3 text-base font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">Add to cart</button>--}}

                        <!-- Product details -->

                        <div class="mt-10">
                            @if($product->description != '')
                                <h2 class="text-sm font-medium text-gray-900">Περιγραφή</h2>

                                <div class="prose prose-sm mt-4 text-gray-500">
                                    {{$product->description}}
                                </div>
                        </div>
                        @endif

                        @include(\Config::get('view.theme').'.product_attributes', ['product' => $product])

                    </div>
                    <!-- Policies -->
                </div>
            </div>
        </div>
    </div>

    @livewire('query-products',
    ['category_id' => $product->categories()->first()->id,
    'total_items' => 8,
    'sort' => 'created_at',
    'sortDirection' => 'Desc',
    'totalColumns'=>4,
    'title'=>['el'=>'Σχετικά προϊόντα'],
    'priceBetween'=>[$product->price - ($product->price * 0.10), $product->price + ($product->price * 0.10)]])


@endsection
@section('page_scripts')
    @livewireScripts

    <script src="https://cdnjs.cloudflare.com/ajax/libs/venobox/2.1.8/venobox.min.js"
            integrity="sha512-LvcjoBF1sjBfiAJpi1Vt5pJXcT7A+0BK6nvwYkp0PwL3zNswVsRi3GURZXlRN8o6E9p0pJUJi5vsp6LSqVBzhw=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/venobox/2.1.8/venobox.min.css"
          integrity="sha512-GypU0XXzM1fTasrwwQdNWacbV5aRsbJCjDdxfRyiYwDMvFkAI1LMny6mDbrM19kKGMCA+dQpilQgRZoPjhsGfg=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <script>



        // end crossell products


        new VenoBox({
            selector: '.product-image',
            numeration: true,

            infinigall: true,
            share: false,

        });

        document.getElementById('addToCartButton').addEventListener('click', function (e) {
            const button = e.currentTarget;
            const rect = button.getBoundingClientRect();
            const heart = document.createElement('div');
            heart.classList.add('heart');
            heart.style.left = rect.left + rect.width / 2 + 'px';
            heart.style.top = rect.top + 'px';

            document.body.appendChild(heart);

            setTimeout(function () {
                heart.remove();
            }, 1000);
        });

    </script>
    <style>
        .heart {
            position: absolute;
            width: 20px;
            height: 20px;
            background-color: red;
            transform: rotate(45deg);
            animation: fly 1s ease-out;
        }

        .heart::before, .heart::after {
            content: '';
            position: absolute;
            width: 20px;
            height: 20px;
            background-color: red;
            border-radius: 50%;
        }

        .heart::before {
            top: -10px;
            left: 0;
        }

        .heart::after {
            left: 10px;
            top: 0;
        }

        @keyframes fly {
            0% {
                opacity: 1;
                transform: scale(0.5) translateY(0);
            }
            100% {
                opacity: 0;
                transform: scale(1.5) translateY(-200px);
            }
        }
    </style>
@endsection
