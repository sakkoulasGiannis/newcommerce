@extends('default.layout')
@section('content')
    <section class="bg-white py-8 antialiased dark:bg-gray-900 md:py-16">
        <div class="mx-auto max-w-2xl px-4 2xl:px-0">
            <h2 class="text-xl font-semibold text-gray-900 dark:text-white sm:text-2xl mb-2">Η Παραγγελίας σας ολοκληρώθηκε!</h2>
            <p class="text-gray-500 dark:text-gray-400 mb-6 md:mb-8">Αρ. Παραγγελίας: <a href="#" class="font-medium text-gray-900 dark:text-white hover:underline">#{{$order->order_num}}</a>Θα σας ειδοποιήσουμε με email μόλις αποσταλεί η παραγγελία σας.</p>
            <div class="space-y-4 sm:space-y-2 rounded-lg border border-gray-100 bg-gray-50 p-6 dark:border-gray-700 dark:bg-gray-800 mb-6 md:mb-8">
                <dl class="sm:flex items-center justify-between gap-4">
                    <dt class="font-normal mb-1 sm:mb-0 text-gray-500 dark:text-gray-400">Ημ/νια</dt>
                    <dd class="font-medium text-gray-900 dark:text-white sm:text-end">{{$order->created_at}}</dd>
                </dl>
                <dl class="sm:flex items-center justify-between gap-4">
                    <dt class="font-normal mb-1 sm:mb-0 text-gray-500 dark:text-gray-400">Τρόπος Πληρωμής</dt>
                    <dd class="font-medium text-gray-900 dark:text-white sm:text-end">{{$order->detail->payment_name}}</dd>
                </dl>
                <dl class="sm:flex items-center justify-between gap-4">
                    <dt class="font-normal mb-1 sm:mb-0 text-gray-500 dark:text-gray-400">Τρόπος Αποστολής</dt>
                    <dd class="font-medium text-gray-900 dark:text-white sm:text-end">{{$order->detail->courier_name}}</dd>
                </dl>
                <dl class="sm:flex items-center justify-between gap-4">
                    <dt class="font-normal mb-1 sm:mb-0 text-gray-500 dark:text-gray-400">Όνομα</dt>
                    <dd class="font-medium text-gray-900 dark:text-white sm:text-end">{{$order->detail->billing_name}} {{$order->detail->billing_lastname}}, {{$order->detail->country->title}}</dd>
                </dl>
                <dl class="sm:flex items-center justify-between gap-4">
                    <dt class="font-normal mb-1 sm:mb-0 text-gray-500 dark:text-gray-400">Δ/νση</dt>
                    <dd class="font-medium text-gray-900 dark:text-white sm:text-end">{{$order->detail->billing_city}}, {{$order->detail->billing_street}}</dd>
                </dl>
                <dl class="sm:flex items-center justify-between gap-4">
                    <dt class="font-normal mb-1 sm:mb-0 text-gray-500 dark:text-gray-400">Τηλέφωνο</dt>
                    <dd class="font-medium text-gray-900 dark:text-white sm:text-end">{{$order->detail->billing_mobile}}</dd>
                </dl>
            </div>
            <div class="flex items-center space-x-4">
                <p  class="text-white bg-primary-700 hover:bg-primary-800 focus:ring-4 focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 dark:bg-primary-600 dark:hover:bg-primary-700 focus:outline-none dark:focus:ring-primary-800">Ευχαριστούμε για την παραγγελία</p>
{{--                <a href="#" class="py-2.5 px-5 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-primary-700 focus:z-10 focus:ring-4 focus:ring-gray-100 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700">Return to shopping</a>--}}
            </div>
        </div>
    </section>

@endsection
@section('page_scripts')

@endsection
