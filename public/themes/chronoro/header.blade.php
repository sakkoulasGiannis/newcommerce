<div class="bg-white">

{{--    @include(\Config::get('view.theme').'.mobile_menu')--}}
    <header class="relative">
        <nav aria-label="Top">
            <!-- Top navigation -->
            @include(config('view.theme') . '.top_bar')

            <!-- Secondary navigation -->
            <div class="bg-white mt-4 md:mt-0">
                <div class="mx-auto max-w-8xl px-4 sm:px-6">
                    <div class="flex flex-wrap   items-center justify-between border-b pb-6">
                        <!-- Logo (lg+) -->
                        <div class="w-2/4 md:w-1/3 order-1 md:order-2 lg:items-center justify-center">
                            <a href="{{$local}}" class="flex justify-center" >
                                <span class="sr-only">Chronoro</span>
                                @if(isset($settings['logo']))
                                    <img src="{{ $settings['logo']['value'] }}" alt="{{ $settings['logo']['title'] }}"
                                         class=" w-84 ">
                                @else
                                    <h1>Chronoro.gr</h1>
                                @endif
                            </a>
                        </div>
                        <div id="searchForm" class="w-4/5 md:w-1/3 order-2 md:order-1">
                            @livewire('search-products')
                        </div>
                        <div class="w-2/4 md:w-1/3 order-1 md:order-3">

                            <div class="flex items-center   justify-end">
                                <a href="/whishlist" class="group text-sm font-medium text-gray-700 hover:text-gray-800 lg:block">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="h-6 w-6 flex-shrink-0 text-gray-400 hover:text-red-600  ">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M21 8.25c0-2.485-2.099-4.5-4.688-4.5-1.935 0-3.597 1.126-4.312 2.733-.715-1.607-2.377-2.733-4.313-2.733C5.1 3.75 3 5.765 3 8.25c0 7.22 9 12 9 12s9-4.78 9-12Z" />
                                    </svg>
                                </a>
                                <a href="/login"
                                   title="login"
                                   class="lg:ml-8 ml-4 group text-sm font-medium text-gray-700 hover:text-gray-800 lg:block">

                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="h-6 w-6 flex-shrink-0 text-gray-400 group-hover:text-gray-500">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M17.982 18.725A7.488 7.488 0 0 0 12 15.75a7.488 7.488 0 0 0-5.982 2.975m11.963 0a9 9 0 1 0-11.963 0m11.963 0A8.966 8.966 0 0 1 12 21a8.966 8.966 0 0 1-5.982-2.275M15 9.75a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z" />
                                    </svg>
                                </a>

                                <!-- Cart -->

                                <div class="ml-4 flow-root lg:ml-8">
                                    <a href="/cart" class="group -m-2 flex items-center p-2">
                                        <svg class="h-6 w-6 flex-shrink-0 text-gray-400 group-hover:text-gray-500"
                                             fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                             stroke="currentColor" aria-hidden="true">
                                            <path stroke-linecap="round" stroke-linejoin="round"
                                                  d="M15.75 10.5V6a3.75 3.75 0 10-7.5 0v4.5m11.356-1.993l1.263 12c.07.665-.45 1.243-1.119 1.243H4.25a1.125 1.125 0 01-1.12-1.243l1.264-12A1.125 1.125 0 015.513 7.5h12.974c.576 0 1.059.435 1.119 1.007zM8.625 10.5a.375.375 0 11-.75 0 .375.375 0 01.75 0zm7.5 0a.375.375 0 11-.75 0 .375.375 0 01.75 0z"/>
                                        </svg>
                                        <span
                                            class="ml-2 text-sm font-medium text-gray-700 group-hover:text-gray-800">
                                                @livewire('total-cart-items')
                                            </span>
                                        <span class="sr-only">items in cart, view bag</span>
                                    </a>
                                </div>

                            </div>
                        </div>

                        <button id="toggle_mobile_menu" type="button" class="flex justify-end items-center  md:hidden order-4 w-1/5 -ml-2 rounded-md bg-white p-2 text-gray-400">
                            <span class="sr-only">Open menu</span>
                            <svg  class="h-10 w-10" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                  stroke="currentColor" aria-hidden="true">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                      d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"/>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
            <div class="bg-white  h-10 md:h-24 lg:block">
                <div class="mx-auto max-w-8xl px-4 sm:px-6">
                    <div class="flex h-16 items-center md:justify-center ">


                        @include(\Config::get('view.theme').'.desktop_menu')


                        <!-- Mobile menu and search (lg-) -->

                    </div>
                </div>
            </div>

        </nav>
    </header>
</div>
<script>
    document.addEventListener('click', function(event) {
        var searchInput = document.getElementById('search-input');
        var searchResults = document.getElementById('searchResults');
        //
        // if (!searchInput.contains(event.target) && !searchResults.contains(event.target)) {
        //     searchResults.style.display = 'none';
        // }
    });

    document.getElementById('search-input').addEventListener('click', function() {
        var searchResults = document.getElementById('searchResults');
        searchResults.style.display = 'block';
    });
</script>
