<div class="carousel-wrapper">
    <div class="carousel-container">
        <!-- Carousel Items -->
        <div class="carousel-item">
            <img src="https://www.chronoro.gr/wp-content/uploads/2024/07/HAM_OPENHEART_2000x760_PAPADAKIS.jpg" alt="Image 1" class="w-full h-auto">
        </div>
        <div class="carousel-item">
            <img src="https://www.chronoro.gr/wp-content/uploads/2023/12/orisds-min.png" alt="Image 2" class="w-full h-auto">
        </div>

    </div>
    <button class="carousel-button prev">&lt;</button>
    <button class="carousel-button next">&gt;</button>
</div>
<script>
    document.addEventListener('DOMContentLoaded', () => {
        const container = document.querySelector('.carousel-container');
        const items = document.querySelectorAll('.carousel-item');
        const totalItems = items.length;
        let currentIndex = 0;

        document.querySelector('.carousel-button.next').addEventListener('click', () => {
            currentIndex++;
            if (currentIndex >= totalItems / 2) {
                currentIndex = 0;
                container.style.transition = 'none'; // Disable transition for instant snap
                container.style.transform = 'translateX(0)';
                setTimeout(() => {
                    container.style.transition = 'transform 0.5s ease';
                }, 0);
            } else {
                container.style.transform = `translateX(-${100 * currentIndex}%)`;
            }
        });

        document.querySelector('.carousel-button.prev').addEventListener('click', () => {
            currentIndex--;
            if (currentIndex < 0) {
                currentIndex = totalItems / 2 - 1;
                container.style.transition = 'none'; // Disable transition for instant snap
                container.style.transform = `translateX(-${100 * currentIndex}%)`;
                setTimeout(() => {
                    container.style.transition = 'transform 0.5s ease';
                }, 0);
            } else {
                container.style.transform = `translateX(-${100 * currentIndex}%)`;
            }
        });
    });
</script>
