@extends('default.layout')
@section('content')

    <div class="bg-gray-50">
        <div class="mx-auto max-w-2xl px-4 pb-24 pt-16 sm:px-6 lg:max-w-8xl lg:px-8">
            <h2 class="sr-only">Ολοκλήρωση αγοράς</h2>



                 @livewire('checkout-form', ['cart' => $cart, 'countries' => $countries])

                <!-- Order summary -->


        </div>
    </div>

@endsection

@section('page_scripts')
    <script>


        document.addEventListener('DOMContentLoaded', function () {
            const toggleInvoice = document.getElementById('toggleInvoice');
            const invoiceContent = document.getElementById('invoiceContent');

            toggleInvoice.addEventListener('click', function () {
                // Εναλλαγή εμφάνισης
                if (invoiceContent.style.display === 'none') {
                    invoiceContent.style.display = 'grid';  // Αντί για block, χρησιμοποιούμε grid λόγω της διάταξης.
                } else {
                    invoiceContent.style.display = 'none';
                }
            });
        });


    </script>
@endsection
