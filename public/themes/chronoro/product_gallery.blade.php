<div class="mt-8 lg:col-span-7 lg:col-start-1 lg:row-span-3 lg:row-start-1 lg:mt-0">
    <div id="product-gallery" class="grid grid-cols-1 lg:grid-cols-2 {{(count($product->getMedia('product'))> 1)?'lg:grid-rows-3':null}}  lg:gap-8">
        @foreach($product->getMedia('product') as $m)
            @if($loop->first)
                <a href="{{$m->getUrl()}}" data-gall="gallery" class="product-image rounded-lg lg:col-span-2 lg:row-span-2">
                    <img src="{{$m->getUrl()}}" alt="{{$m->name}}" class="">
                </a>
            @else
                <a href="{{$m->getUrl()}}" data-gall="gallery" class="product-image hidden rounded-lg lg:block">
                    <img src="{{$m->getUrl()}}" alt="{{$m->name}}." class="hidden rounded-lg lg:block">
                </a>
            @endif
        @endforeach
    </div>
</div>
