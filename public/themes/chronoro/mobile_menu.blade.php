<div id="mobile_menu" class="relative z-40 lg:hidden  hidden transition-opacity ease-linear duration-300 " role="dialog" aria-modal="true">

    <div class="fixed inset-0 bg-black bg-opacity-25" aria-hidden="true"></div>

    {{-- mobile menu --}}
    <div class="fixed inset-0 z-40 flex">

        <div class="relative flex w-full max-w-xs flex-col overflow-y-auto bg-white pb-12 shadow-xl">
            <div class="flex px-4 pb-2 pt-5">
                <button id="mobile_menu_close_button" type="button" class="-m-2 inline-flex items-center justify-center rounded-md p-2 text-gray-400">
                    <span class="sr-only">Close menu</span>
                    <svg class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
            </div>

            @foreach($menu as $m)
                @include(\Config::get('view.theme').'.menu_item', ['item' => $m, 'parentName' => ''])
            @endforeach
        </div>
    </div>
</div>
