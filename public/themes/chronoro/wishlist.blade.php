@extends('default.layout')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Wishlist</h1>
                <div class="row">
                    @foreach($products as $product)
                        <div class="col-md-4">
                            <div class="card">
                                <img class="card-img-top" src="{{ $product->image }}" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $product->name }}</h5>
                                    <p class="card-text">{{ $product->description }}</p>
                                    <a href="{{ route('product.show', $product->id) }}" class="btn btn-primary">View</a>


                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
