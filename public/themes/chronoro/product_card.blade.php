<div class="group relative hover:shadow-lg transition-all ease-in-out delay-150 p-2 rounded-md">
    <div class="aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-md bg-gray-200 lg:aspect-none  lg:h-80">
        <a href="{{$product->name}}">
            <img src="{{$product->thumb}}" alt="" class="h-full w-full object-cover object-center lg:h-full lg:w-full">
        </a>
    </div>
    <div class="  flex justify-between">
        <div>
            <h3 class="text-sm text-gray-700">
                <a href="{{$product->name}}">
                    {{$product->title}}
                </a>
            </h3>
            <p class="mt-1 text-sm text-gray-500"></p>
        </div>
    </div>
    <p class="text-md font-medium text-gray-900 text-right">
        <span class="font-bold text-[#c09578] ">{{$product->price}}€</span>

        @if(isset($product->originalPrice))
            <span class="ml-2 text-md font-medium  text-gray-700 line-through">{{$product->originalPrice}}€</span>
        @endif
    </p>
</div>
