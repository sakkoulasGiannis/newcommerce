<div id="menu-wrapper" class="  md:justify-center gap-6 align-center sidebar md:flex hidden">
    <div id="close-menu" class="md:hidden cursor-pointer absolute right-4 z-10">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="size-6">
            <path stroke-linecap="round" stroke-linejoin="round" d="M6 18 18 6M6 6l12 12" />
        </svg>
    </div>
    @foreach($menu as $m)
        @include(\Config::get('view.theme').'.menu_item', ['item' => $m, 'parentName' => ''])
    @endforeach
</div>
{{--<div id="menu-overlay" class="fixed inset-0"></div>--}}

<style>
    @media only screen and (max-width: 768px) {
        .sidebar {

            flex-direction: column; /* Stack items vertically */
            position: fixed; /* Make it a sidebar */
            top: 0;
            left: 0; /* You can change this to right: 0 for right sidebar */
            width: 80%; /* Adjust the width of the sidebar */
            height: 100vh; /* Full height sidebar */
            background-color: #ffffff; /* Example background color */
            padding: 10px; /* Padding inside the sidebar */
            box-shadow: 2px 0 5px rgba(0, 0, 0, 0.1); /* Optional shadow */
            z-index: 1000; /* Sidebar stays on top */
        }
        .mega-menu {

            box-shadow: none!important;
             display: inline-block!important;
        }
        #menu-wrapper{
            overflow-y: scroll;
        }

        .sidebar {transform: translateX(-120%);transition: all 0.2s ease;  }

        .sidebar.open {
             transform: translateX(0);
        }
    }

</style>
<script>
    const sidebar = document.getElementById('menu-wrapper');
    const openMenuButton = document.getElementById('toggle_mobile_menu');
    const closeMenuButton = document.getElementById('close-menu');
    const overlay = document.getElementById('menu-overlay');

    // Λειτουργία για το άνοιγμα του μενού
    function openMenu() {
        sidebar.classList.add('open');
        sidebar.classList.remove('hidden');
        // overlay.classList.add('active'); // Προσθέτουμε το overlay
    }

    // Λειτουργία για το κλείσιμο του μενού
    function closeMenu() {
        sidebar.classList.remove('open');
        sidebar.classList.add('hidden');
        // overlay.classList.remove('active'); // Αφαιρούμε το overlay
    }

    // Κουμπί για άνοιγμα μενού
    openMenuButton.addEventListener('click', openMenu);

    // Κουμπί για κλείσιμο μενού
    closeMenuButton.addEventListener('click', closeMenu);

    // Κλείσιμο του μενού αν πατηθεί έξω από αυτό (στο overlay)
    // overlay.addEventListener('click', closeMenu);

    // Gestures για άνοιγμα/κλείσιμο μενού
    let touchStartX = 0;
    let touchEndX = 0;

    function handleGesture() {
        // Αν σπρώχνω από αριστερά προς τα δεξιά (άνοιγμα)
        if (touchEndX > touchStartX + 50) {
            openMenu();
        }
        // Αν σπρώχνω από δεξιά προς τα αριστερά (κλείσιμο)
        if (touchEndX < touchStartX - 50) {
            closeMenu();
        }
    }

    // Καταγραφή της αρχικής θέσης του swipe
    document.addEventListener('touchstart', (e) => {
        touchStartX = e.changedTouches[0].screenX;
    });

    // Καταγραφή της τελικής θέσης του swipe
    document.addEventListener('touchend', (e) => {
        touchEndX = e.changedTouches[0].screenX;
        handleGesture();
    });

</script>
