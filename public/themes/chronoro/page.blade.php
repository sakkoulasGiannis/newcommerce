@extends('default.layout')
@section('content')

<div class="bg-white mt-8 mb-8">

    <main class="isolate">
         <div class="mx-auto -mt-12 max-w-7xl px-6 sm:mt-0 lg:px-8 xl:-mt-8">
            <div class="mx-auto max-w-2xl lg:mx-0 lg:max-w-none">
                <h1 class="text-pretty text-2xl font-semibold tracking-tight text-gray-900 sm:text-5xl">{{$page->title}}</h1>
                <div class="mt-6 flex   gap-x-8 gap-y-20 lg:flex-row">
                    <div class="lg:w-full   lg:flex-auto">
                        <p class="text-base text-gray-600">{!! $page->body !!}</p>
                     </div>

                </div>
            </div>
        </div>

    </main>


</div>

@endsection
