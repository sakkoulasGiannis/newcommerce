@extends('default.layout')
@section('content')
{{--    @include(\Config::get('view.theme').'.slider')--}}


{{--<section aria-labelledby="collections-heading" class="bg-gray-100">--}}

    <div class="container mx-auto max-w-8xl px-4  sm:px-6 mb-8">
        <div class="grid grid-cols-1 md:grid-cols-2 gap-4">
            <div>
                <img src="https://dummyimage.com/700x560/dddddd/fff" alt="Desk with leather desk pad, walnut desk organizer, wireless keyboard and mouse, and porcelain mug." >
            </div>
            <div class="grid grid-cols-1 gap-4">
                <div>
                    <img src="https://dummyimage.com/700x272/dddddd/fff" alt="Desk with leather desk pad, walnut desk organizer, wireless keyboard and mouse, and porcelain mug." >
                </div>
                <div>
                    <img src="https://dummyimage.com/700x272/dddddd/fff" alt="Desk with dleather desk pad, walnut desk organizer, wireless keyboard and mouse, and porcelain mug." >
                </div>
            </div>
        </div>
    </div>

    <div class="mx-auto max-w-7xl px-4 sm:px-6 lg:px-6">
        <div class="mx-auto max-w-2xl py-8 sm:py-24 lg:max-w-none lg:py-8 mb-12">
            <div class=" space-y-12 grid lg:grid-cols-4 lg:gap-x-6 lg:space-y-0">
                <div class="group relative">
                    <div class="relative h-80 w-full   rounded-lg bg-white sm:aspect-h-1 sm:aspect-w-2 lg:aspect-h-1 lg:aspect-w-1 group-hover:opacity-75 sm:h-64">
                        <img src="https://www.chronoro.gr/wp-content/uploads/2023/12/monopetra.png" alt="Desk with leather desk pad, walnut desk organizer, wireless keyboard and mouse, and porcelain mug." >
                    </div>
                    <h3 class=" text-sm text-gray-500">
                        <a href="#">
                            <span class="absolute inset-0"></span>
                            ΜΟΝΟΠΕΤΡΑ
                        </a>
                    </h3>
                    {{--                    <p class="text-base font-semibold text-gray-900">Work from home accessories</p>--}}
                </div>
                <div class="group relative">
                    <div class="relative h-80 w-full  rounded-lg bg-white sm:aspect-h-1 sm:aspect-w-2 lg:aspect-h-1 lg:aspect-w-1 group-hover:opacity-75 sm:h-64">
                        <img src="https://www.chronoro.gr/wp-content/uploads/2023/12/beres.png" alt="Desk with leather desk pad, walnut desk organizer, wireless keyboard and mouse, and porcelain mug." >
                    </div>
                    <h3 class="mt-6 text-sm text-gray-500">
                        <a href="#">
                            <span class="absolute inset-0"></span>
                            ΒΕΡΕΣ
                        </a>
                    </h3>
                    {{--                    <p class="text-base font-semibold text-gray-900">Work from home accessories</p>--}}
                </div>
                <div class="group relative">
                    <div class="relative h-80 w-full  rounded-lg bg-white sm:aspect-h-1 sm:aspect-w-2 lg:aspect-h-1 lg:aspect-w-1 group-hover:opacity-75 sm:h-64">
                        <img src="https://www.chronoro.gr/wp-content/uploads/2023/12/stauroi.png" alt="Wood table with porcelain mug, leather journal, brass pen, leather key ring, and a houseplant." >
                    </div>
                    <h3 class="mt-6 text-sm text-gray-500">
                        <a href="#">
                            <span class="absolute inset-0"></span>
                            ΣΤΑΥΡΟΙ
                        </a>
                    </h3>
                    {{--                    <p class="text-base font-semibold text-gray-900">Journals and note-taking</p>--}}
                </div>
                <div class="group relative">
                    <div class="relative h-80 w-full  rounded-lg bg-white sm:aspect-h-1 sm:aspect-w-2 lg:aspect-h-1 lg:aspect-w-1 group-hover:opacity-75 sm:h-64">
                        <img src="https://www.chronoro.gr/wp-content/uploads/2023/12/kostantinata.png" alt="Collection of four insulated travel bottles on wooden shelf." >
                    </div>
                    <h3 class="mt-6 text-sm text-gray-500">
                        <a href="#">
                            <span class="absolute inset-0"></span>
                            ΚΟΣΜΗΜΑΤΑ
                        </a>
                    </h3>
                    {{--                    <p class="text-base font-semibold text-gray-900">Daily commute essentials</p>--}}
                </div>
            </div>
        </div>
    </div>
{{--</section>--}}

<div class="text-center">
    <img src="https://dummyimage.com/1920x560/dddddd/fff" class="w-full" alt="">

</div>

    @livewire('query-products', ['category_id' => 1, 'total_items' => 4, 'sort' => 'created_at', 'sortDirection' => 'Desc', 'totalColumns'=>4, 'moreLink'=>null, 'title'=>['el'=>'Νέα Προϊόντα']])



    <div class="container mx-auto max-w-8xl px-4  sm:px-6 mb-8">
        <div class="flex flex-row justify-center gap-3 items-center py-12">
            <a class="w-1/2" href=""><img src="https://dummyimage.com/692/dddddd/fff" alt=""></a>
            <a class="w-1/2" href=""><img src="https://dummyimage.com/692/dddddd/fff" alt=""></a>
            <a class="w-1/2" href=""><img src="https://dummyimage.com/692/dddddd/fff" alt=""></a>
            <a class="w-1/2" href=""><img src="https://dummyimage.com/692/dddddd/fff" alt=""></a>
        </div>
    </div>




@livewire('query-products', ['category_id' => 2, 'total_items' => 4, 'sort' => 'created_at', 'sortDirection' => 'Desc', 'totalColumns'=>4, 'moreLink'=>null, 'title'=>['el'=>'Νέα Προϊόντα']])

{{--end product list--}}



@endsection
@section('page_scripts')
<style>
    .carousel-wrapper {
        position: relative;
        overflow: hidden;
        width: 100%;
    }

    .carousel-container {
        display: flex;
        transition: transform 0.5s ease;
    }

    .carousel-item {
        flex: 0 0 100%;
        max-width: 100%;
    }

    .carousel-button {
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
        background: rgba(0, 0, 0, 0.5);
        color: #fff;
        border: none;
        padding: 0.5rem;
        cursor: pointer;
    }

    .carousel-button.prev {
        left: 1rem;
    }

    .carousel-button.next {
        right: 1rem;
    }
</style>
@endsection
