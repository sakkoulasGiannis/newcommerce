@extends('default.layout')
@section('content')
    @include(\Config::get('view.theme').'.slider')

{{--product list--}}

        @livewire('query-products', ['category_id' => 1, 'total_items' => 8, 'sort' => 'created_at', 'sortDirection' => 'Desc', 'totalColumns'=>4, 'moreLink'=>null, 'title'=>['el'=>'Νέα Προϊόντα']])



{{--end product list--}}

{{--general information--}}
<div class="bg-gray-50 mt-4">
    <div class="mx-auto max-w-2xl px-4 py-24 sm:px-6 sm:py-32 lg:max-w-7xl lg:px-8">
        <div class="grid grid-cols-1 gap-y-12 sm:grid-cols-2 sm:gap-x-6 lg:grid-cols-4 lg:gap-x-8">
            <div>
                <img src="https://tailwindui.com/img/ecommerce/icons/icon-delivery-light.svg" alt="" class="h-24 w-auto">
                <h3 class="mt-6 text-sm font-medium text-gray-900">Free Shipping</h3>
                <p class="mt-2 text-sm text-gray-500">It&#039;s not actually free we just price it into the products. Someone&#039;s paying for it, and it&#039;s not us.</p>
            </div>
            <div>
                <img src="https://tailwindui.com/img/ecommerce/icons/icon-chat-light.svg" alt="" class="h-24 w-auto">
                <h3 class="mt-6 text-sm font-medium text-gray-900">24/7 Customer Support</h3>
                <p class="mt-2 text-sm text-gray-500">Our AI chat widget is powered by a naive series of if/else statements. Guaranteed to irritate.</p>
            </div>
            <div>
                <img src="https://tailwindui.com/img/ecommerce/icons/icon-fast-checkout-light.svg" alt="" class="h-24 w-auto">
                <h3 class="mt-6 text-sm font-medium text-gray-900">Fast Shopping Cart</h3>
                <p class="mt-2 text-sm text-gray-500">Look how fast that cart is going. What does this mean for the actual experience? I don&#039;t know.</p>
            </div>
            <div>
                <img src="https://tailwindui.com/img/ecommerce/icons/icon-gift-card-light.svg" alt="" class="h-24 w-auto">
                <h3 class="mt-6 text-sm font-medium text-gray-900">Gift Cards</h3>
                <p class="mt-2 text-sm text-gray-500">Buy them for your friends, especially if they don&#039;t like our store. Free money for us, it&#039;s great.</p>
            </div>
        </div>
    </div>
</div>



@endsection
@section('page_scripts')
<style>
    .carousel-wrapper {
        position: relative;
        overflow: hidden;
        width: 100%;
    }

    .carousel-container {
        display: flex;
        transition: transform 0.5s ease;
    }

    .carousel-item {
        flex: 0 0 100%;
        max-width: 100%;
    }

    .carousel-button {
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
        background: rgba(0, 0, 0, 0.5);
        color: #fff;
        border: none;
        padding: 0.5rem;
        cursor: pointer;
    }

    .carousel-button.prev {
        left: 1rem;
    }

    .carousel-button.next {
        right: 1rem;
    }
</style>
@endsection
