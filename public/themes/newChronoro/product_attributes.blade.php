<ul role="list" class="divide-y divide-gray-100">

    @foreach($product->attributes as $attribute)
        <li class="relative flex justify-between gap-x-6 px-4 py-5 hover:bg-gray-50 sm:px-6 lg:px-8">
            <div class="flex min-w-0 gap-x-4">
                <div class="min-w-0 flex-auto">
                    <p class="text-sm font-semibold leading-6 text-gray-900">
                        <span class="absolute inset-x-0 -top-px bottom-0"></span>
                        {{ $attribute->title }}

                    </p>

                </div>
            </div>
            <div class="flex shrink-0 items-center gap-x-4">
                <div class="hidden sm:flex sm:flex-col sm:items-end">
                    @foreach($product->attributeTerms->where('attribute_id', $attribute->id) as $term)
                        <p>{{ $term->title }}</p>
                    @endforeach
                </div>
            </div>
        </li>
    @endforeach
</ul>
