@extends('default.layout')
@section('content')

    <div class="bg-gray-50">
        <div class="mx-auto max-w-2xl px-4 pb-24 pt-16 sm:px-6 lg:max-w-7xl lg:px-8">
            <h2 class="sr-only">Ολοκλήρωση αγοράς</h2>



                 @livewire('checkout-form', ['cart' => $cart, 'countries' => $countries])

                <!-- Order summary -->


        </div>
    </div>

@endsection
