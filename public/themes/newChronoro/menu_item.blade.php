<div class="nav-list {{(isset($item['mega_menu_is_active']) && $item['mega_menu_is_active'])?'flex':'relative '}}">
    @if(isset($item['type']) && $item['type'] == 'image')
        @if(!isset($item['hideTitle']) || isset($item['hideTitle'])  && !$item['hideTitle'])
            <a href="/{{ isset($item['href']) ? $item['href'] : ($parentName ? $parentName.'/'.$item['name'] : '#') }}"
               class="menu-link relative z-10 -mb-px flex items-center border-b-2 border-transparent pt-px text-sm font-medium text-gray-700 transition-colors duration-200 ease-out hover:text-gray-800"
               aria-expanded="false">
                <img src="{{$item['icon']}}" alt="{{$item['name']}}" class="h-full">
            </a>
        @else
            <img src="{{$item['icon']}}" alt="{{$item['name']}}" class="h-full">
        @endif

        <img src="{{$item['icon']}}" alt="{{$item['name']}}" class="h-full">
    @endif
    @if(!isset($item['hideTitle']) || isset($item['hideTitle'])  && !$item['hideTitle'])
        <a href="/{{ isset($item['href']) ? $item['href'] : ($parentName ? $parentName.'/'.$item['name'] : $item['name']) }}"
           class="menu-link relative z-10 -mb-px flex items-center border-b-2 border-transparent pt-px text-sm font-medium text-gray-700 transition-colors duration-200 ease-out hover:text-gray-800"
           aria-expanded="false"> {{$item['title'][$local]}}
        </a>
    @endif
    @if(count($item['children']) > 0)
        @if($item['mega_menu_is_active'])
            <div class="nav-item   mega-menu">
                <div class="absolute inset-0 top-1/2 bg-white shadow" aria-hidden="true"></div>
                <div class="relative bg-white">
                    <div class="mx-auto max-w-7xl px-8">
                        <div
                            class="grid {{($item['mega_menu_is_active'])?'grid-cols-'.count($item['children']): 'grid-cols-1'}} items-start gap-x-8 gap-y-10 pb-12 pt-10">
                            @foreach($item['children'] as $child)
                                <div>
                                    @if(isset($child['type']) && $child['type'] == 'image')
                                        <img src="{{$child['icon']}}" alt="" class="mb-4">
                                    @endif
                                    @if(!isset($child['hideΤitle']) || isset($child['hideTitle'])  && !$child['hideTitle'])

                                        <a href="/{{ isset($child['href']) ? $child['href'] : ($parentName ? $parentName.'/'.$item['name'].'/'.$child['name'] : $item['name'].'/'.$child['name']) }}"
                                           class="font-medium text-gray-900">{{$child['title'][$local]}}</a>
                                    @endif
                                    @if(count($child['children']) > 0)
                                        <ul role="list" class="mt-6 space-y-6 sm:mt-4 sm:space-y-4">
                                            @foreach($child['children'] as $subChild)
                                                @include(\Config::get('view.theme').'.menu_item', ['item' => $subChild, 'parentName' => $parentName ? $parentName.'/'.$item['name'].'/'.$child['name'] : $item['name'].'/'.$child['name']])
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="absolute p-2  z-10  mt-5 flex w-screen max-w-max -translate-x-1/2  left-1/2 mega-menu ">
                <div
                    class="w-screen max-w-md flex-auto overflow-hidden  rounded-md bg-white text-sm leading-6 shadow-lg ring-1 ring-gray-900/5">
                    @foreach($item['children'] as $child)
                        <div class="group relative flex gap-x-6  p-2 hover:bg-gray-50 border-b border-gray-200">
                            @if(isset($child['type']) && $child['type'] == 'image')
                                <img src="{{$child['icon']}}" alt="" class="mb-4">
                            @endif
                            @if(!isset($child['hideΤitle']) || isset($child['hideTitle'])  && !$child['hideTitle'])

                                <a href="/{{ isset($child['href']) ? $child['href'] : ($parentName ? $parentName.'/'.$item['name'].'/'.$child['name'] : $item['name'].'/'.$child['name']) }}"
                                   class="font-medium text-gray-900">{{$child['title'][$local]}}</a>
                            @endif
                            @if(count($child['children']) > 0)
                                <ul role="list" class="mt-6 space-y-6 sm:mt-4 sm:space-y-4">
                                    @foreach($child['children'] as $subChild)
                                        @include(\Config::get('view.theme').'.menu_item', ['item' => $subChild, 'parentName' => $parentName ? $parentName.'/'.$item['name'].'/'.$child['name'] : $item['name'].'/'.$child['name']])
                                    @endforeach
                                </ul>
                            @endif

                        </div>
                    @endforeach
                </div>

            </div>

        @endif
    @endif
</div>
