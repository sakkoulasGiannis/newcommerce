<!-- Secondary navigation -->
<div class="bg-white">
    <div class="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
        <div class="border-b border-gray-200">
            <div class="flex h-16 items-center justify-between">
                <div class="hidden h-full lg:flex">
                    <!-- Mega menus -->

                    <div class="ml-8">
                        <div class="flex h-full justify-left space-x-8">
                            @foreach($menu as $m)

                                @include(\Config::get('view.theme').'.menu_item', ['item' => $m, 'parentName' => ''])

                            @endforeach
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
