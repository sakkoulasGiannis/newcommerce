<div class="mt-6">
    <main class="mx-auto">


            <aside>
                <h2 class="sr-only">Filters</h2>

                <!-- Mobile filter dialog toggle, controls the 'mobileFilterDialogOpen' state. -->
                <button type="button" class="inline-flex items-center lg:hidden">
                    <span class="text-sm font-medium text-gray-700">Filters</span>
                    <svg class="ml-1 h-5 w-5 flex-shrink-0 text-gray-400" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                        <path d="M10.75 4.75a.75.75 0 00-1.5 0v4.5h-4.5a.75.75 0 000 1.5h4.5v4.5a.75.75 0 001.5 0v-4.5h4.5a.75.75 0 000-1.5h-4.5v-4.5z" />
                    </svg>
                </button>

                <div class="hidden lg:block">
                    <form ref="form" method="GET" action="{{ url()->current() }}" class="  divide-y divide-gray-200">
                        <div>
                            <fieldset>
                                <legend class="block text-sm font-medium text-gray-900">Εκπτωτικά Προϊόντα</legend>
                                <div class="space-y-3 pt-6">
                                    <div class="switch mb-2">
                                        <label class="flex items-center">
                                            <input onChange='submit();' type="checkbox" name="discounted" value="true" {{ request()->has('discounted') && request()->get('discounted') == 'true' ? 'checked' : null }} class="checkbox-orange h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500" />
                                            <span class="ml-2 text-sm text-gray-600">Εκπτωτικά Προϊόντα</span>
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                        </div>

                        <input type="hidden" name="query" value="true">
                        @foreach ($attributes as $attribute)
                            <div class="pt-2">
                                <fieldset>
                                    <legend class="block text-sm font-medium text-gray-900">{{ $attribute['title'] }}</legend>
                                    @if ($attribute['terms'] && count($attribute['terms']) > 8)
                                        <input class="border-1 w-full border-gray-400 p-2 text-sm" type="text" id="search{{ $attribute['name'] }}" onkeyup="searchFilterValues('{{ $attribute['name'] }}')" placeholder="Αναζήτηση  {{ $attribute['title'] }}"  title="Αναζήτηση">
                                    @endif
                                    <div class="filter-block mb-4 max-h-48 overflow-auto scroll-smooth p-2 hover:scroll-auto space-y-3 pt-2" id="filterValuesContainer{{ $attribute['name'] }}">
                                        @if($attribute['terms'])
                                            @foreach ($attribute['terms'] as $value)
                                                @if ($value['title'] != '')
                                                    <label class="flex items-center" id="field{{ $attribute['name'] }}">
                                                        <input onChange='submit();' type="checkbox"  name="{{ $attribute['name'] }}[]" value="{{ $value['name'] }}"
                                                               {{(is_array(request()->get($attribute['name'])) && in_array($value['name'], request()->get($attribute['name']))) ? 'checked' : ''}} class="checkbox-orange h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500" />
                                                        <span class="text-es-blue ml-2 text-sm text-gray-600">{{ $value['title'][$local] }}</span>
                                                    </label>
                                                @else
                                                    <label class="flex items-center" id="field{{ $attribute['name'] }}">
                                                        <input onChange='submit();' type="checkbox"  name="{{ $attribute['name'] }}"  value="{{ $value['name'] }}"
                                                               {{ request()->has($attribute['name']) && $value['name'] == request()->get($attribute['name']) ? 'checked ' : null }} class="checkbox-orange h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500" />
                                                        <span class="text-es-blue ml-2 text-sm text-gray-600">{{ $value['title'][$local] }}</span>
                                                    </label>
                                                @endif
                                            @endforeach
                                        @endif
                                    </div>
                                </fieldset>
                            </div>
                        @endforeach
                        <button class="bg-es-blue px-2 py-2 text-white">Αναζήτηση</button>
                    </form>
                </div>
            </aside>

    </main>

</div>

