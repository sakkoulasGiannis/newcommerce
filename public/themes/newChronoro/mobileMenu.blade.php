<div class="fixed top-0 z-50 flex w-full justify-between bg-white text-gray-50 shadow-lg md:hidden">

    <button class="mobile-menu-button p-4 focus:bg-gray-700 focus:outline-none">
        <svg xmlns="https://www.w3.org/2000/svg" class="text-es-blue h-5 w-5" fill="none" viewBox="0 0 24 24"
            stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
        </svg>

    </button>
    <a href="/" class="block w-full p-4"><img src="/views/eshoes/assets/img/logo.png" alt=""></a>

    <a href="/account" class="mt-3 p-4 focus:bg-gray-700 focus:outline-none">
        <svg xmlns="http://www.w3.org/2000/svg" class="text-es-blue h-6 w-6" fill="none" viewBox="0 0 24 24"
            stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
        </svg>
    </a>

    <button class="mobile-search-button p-4 focus:bg-gray-400 focus:outline-none">
        <svg xmlns="https://www.w3.org/2000/svg" class="text-es-blue h-6 w-6" fill="none" viewBox="0 0 24 24"
            stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
        </svg>
    </button>
    <button class="p-4 focus:bg-gray-700 focus:outline-none">
        <svg xmlns="https://www.w3.org/2000/svg" class="text-es-blue h-6 w-6" fill="none" viewBox="0 0 24 24"
            stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                d="M16 11V7a4 4 0 00-8 0v4M5 9h14l1 12H4L5 9z" />
        </svg>
    </button>
</div>
<div class="sidebar text-blue fixed inset-y-0 left-0 z-10 mt-8 hidden h-full w-full -translate-x-full transform overflow-y-auto border-r bg-white shadow-lg transition duration-200 ease-in-out"
    {{-- class="hidden sidebar z-10 bg-white shadow-lg border-r text-blue w-5/6 absolute inset-y-0 left-0 transform -translate-x-full transition duration-200 ease-in-out mt-16" --}}>
    <span class="float-left p-5">ΚΑΤΗΓΟΡΙΕΣ</span>
    <button class="mobile-menu-button-close float-right p-4 focus:bg-gray-700 focus:outline-none">
        <svg xmlns="https://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24"
            stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
        </svg>
    </button>
    <ul class="mt-16 border-b-2 bg-white shadow-lg">
        <li class="toggleable border-t-2 hover:text-white">
            <input type="checkbox" value="selected" id="new-products" class="toggle-input hidden">
            <label for="new-products" class="text-es-blue block cursor-pointer py-6 px-4 text-sm font-bold lg:p-6">ΝΕΕΣ
                ΠΑΡΑΛΑΒΕΣ</label>
            <div role="toggle" class="mobile-menu text-es-blue sm:mb-0">
                <div class="container mx-auto flex w-full flex-wrap justify-between">
                    <ul class="w-full bg-white">
                        @if(isset($allCategories))
                        @foreach ($allCategories as $cat)
                            @if ($cat->id == 650)
                                @foreach ($cat->children as $c)
                                    <li class="menu-item border" style="list-style: none;">
                                        <a class="text-gray-600 level2 level2 block w-full p-6 hover:bg-gray-200 hover:text-white"
                                            href="/{{ $cat->name }}/{{ $c->name }}">
                                            <span>{{ $c->title }}</span>
                                        </a>
                                    </li>
                                @endforeach
                            @endif
                        @endforeach
                        @endif
                    </ul>


                </div>
            </div>
        </li>
        <li class="toggleable border-t-2 hover:text-white">
            <input type="checkbox" value="selected" id="toggle-one" class="toggle-input hidden">
            <label for="toggle-one"
                class="text-es-blue block cursor-pointer py-6 px-4 text-sm font-bold lg:p-6">ΓΥΝΑΙΚΕΙΑ</label>
            <div role="toggle" class="mobile-menu text-es-blue sm:mb-0">
                <div class="container mx-auto flex w-full flex-wrap justify-between">
                    <ul class="w-full bg-white">
                        @if(isset($menu))

                        @foreach ($menu as $cat)
                            @if ($cat->id == 592)
                                @foreach ($cat->children as $c)
                                    <li class="menu-item border" style="list-style: none;">
                                        <a class="text-gray-600 level2 level2 block w-full p-6 hover:bg-gray-200 hover:text-white"
                                            href="/{{ $cat->name }}/{{ $c->name }}">
                                            <span>{{ $c->title }}</span>
                                        </a>
                                    </li>
                                @endforeach
                            @endif
                        @endforeach
                        @endif
                    </ul>

                </div>
            </div>
        </li>

        <li class="toggleable border-t-2 hover:text-white">
            <input type="checkbox" value="selected" id="toggle-two" class="toggle-input hidden">
            <label for="toggle-two"
                class="text-es-blue block cursor-pointer py-6 px-4 text-sm font-bold lg:p-6">ΑΝΔΡΙΚΑ</label>
            <div role="toggle" class="mobile-menu text-es-blue sm:mb-0">
                <div class="container mx-auto flex w-full flex-wrap justify-between">
                    <ul class="w-full bg-white">
                        @if(isset($allCategories))
                        @foreach ($allCategories as $cat)
                            @if ($cat->id == 613)
                                @foreach ($cat->children as $c)
                                    <li class="menu-item border" style="list-style: none;">
                                        <a class="level2 block w-full p-6 hover:bg-gray-200 hover:text-white"
                                            href="/{{ $cat->name }}/{{ $c->name }}">
                                            <span>{{ $c->title }}</span></a>
                                    </li>
                                @endforeach
                            @endif
                        @endforeach
                        @endif
                    </ul>

                </div>
            </div>
        </li>
        <li class="toggleable border-t-2 hover:text-white">
            <input type="checkbox" value="selected" id="toggle-three" class="toggle-input hidden">
            <label for="toggle-three"
                class="text-es-blue block cursor-pointer py-6 px-4 text-sm font-bold lg:p-6">ΠΑΙΔΙΚΑ</label>
            <div role="toggle" class="mobile-menu text-es-blue sm:mb-0">
                <div class="container mx-auto flex w-full flex-wrap justify-between">
                    <ul class="w-full bg-white">
                        @if(isset($allCategories))

                        @foreach ($allCategories as $cat)
                            @if ($cat->id == 625)
                                @foreach ($cat->children as $c)
                                    <li class="menu-item border" style="list-style: none;">
                                        <a class="level2 block w-full p-6 hover:bg-gray-200 hover:text-white"
                                            href="/{{ $cat->name }}/{{ $c->name }}">
                                            <span>{{ $c->title }}</span></a>
                                    </li>
                                @endforeach
                            @endif
                        @endforeach
                        @endif
                    </ul>

                </div>
            </div>
        </li>
        <li class="toggleable border-t-2 hover:text-white">
            <input type="checkbox" value="selected" id="toggle-four" class="toggle-input hidden">
            <label for="toggle-four"
                class="text-es-blue block cursor-pointer py-6 px-4 text-sm font-bold lg:p-6">ΑΞΕΣΟΥΑΡ</label>
            <div role="toggle" class="mobile-menu text-es-blue sm:mb-0">
                <div class="container mx-auto flex w-full flex-wrap justify-between">
                    <ul class="w-full bg-white">
                        @if(isset($allCategories))

                        @foreach ($allCategories as $cat)
                            @if ($cat->id == 749)
                                @foreach ($cat->children as $c)
                                    <li class="menu-item border" style="list-style: none;">
                                        <a class="level2 block w-full p-6 hover:bg-gray-200 hover:text-white"
                                            href="/{{ $cat->name }}/{{ $c->name }}">
                                            <span>{{ $c->title }}</span>
                                        </a>
                                    </li>
                                @endforeach
                            @endif
                        @endforeach
                        @endif
                    </ul>

                </div>
            </div>
        </li>
        <li class="toggleable border-t-2 hover:text-white">
            <input type="checkbox" value="selected" id="toggle-five" class="toggle-input hidden">
            <label for="toggle-five"
                class="text-es-blue block cursor-pointer py-6 px-4 text-sm font-bold lg:p-6">TAMARIS</label>
            <div role="toggle" class="mobile-menu text-es-blue sm:mb-0">
                <div class="container mx-auto flex w-full flex-wrap justify-between">
                    <ul class="w-full bg-white">
                        @if(isset($allCategories))

                        @foreach ($allCategories as $cat)
                            @if ($cat->id == 625)
                                @foreach ($cat->children as $c)
                                    <li class="menu-item border" style="list-style: none;">
                                        <a class="level2 block w-full p-6 hover:bg-gray-200 hover:text-white"
                                            href="/{{ $cat->name }}/{{ $c->name }}">
                                            <span>{{ $c->title }}</span></a>
                                    </li>
                                @endforeach
                            @endif
                        @endforeach
                        @endif
                    </ul>

                </div>
            </div>
        </li>
        <li class="toggleable border-t-2 hover:text-white">
            <input type="checkbox" value="selected" id="prosfores" class="toggle-input hidden">
            <label for="prosfores"
                class="text-es-blue block cursor-pointer py-6 px-4 text-sm font-bold lg:p-6">ΠΡΟΣΦΟΡΕΣ</label>
            <div role="toggle" class="mobile-menu text-es-blue sm:mb-0">
                <div class="container mx-auto flex w-full flex-wrap justify-between">
                    <ul class="w-full bg-white">
                        @if(isset($allCategories))

                        @foreach ($allCategories as $cat)
                            @if ($cat->id == 592)
                                @foreach ($cat->children as $c)
                                    <li class="menu-item border" style="list-style: none;">
                                        <a class="level2 block w-full p-6 hover:bg-gray-200 hover:text-white"
                                            href="/prosforew-62/{{ $cat->name }}/{{ $c->name }}">
                                            <span>{{ $c->title }}</span>
                                        </a>
                                    </li>
                                @endforeach
                            @endif
                        @endforeach
                        @endif
                    </ul>

                </div>
            </div>
        </li>
        <li class="border-t-2 hover:text-white">
            <a href="/brands"><label
                    class="text-es-blue block cursor-pointer py-6 px-4 text-sm font-bold lg:p-6">BRANDS</label></a>
        </li>

    </ul>
</div>
