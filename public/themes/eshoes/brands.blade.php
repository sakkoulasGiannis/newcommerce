@extends('eshoes.layouts.app')
@section('content')
    <div class="container mx-auto">
        <div class="grid grid-cols-1 md:grid-cols-12 md:gap-4">

            @foreach ($brands as $key => $brand)
                @if (count($brand) > 0 && $key != '')
                    <div class="p-2">

                        <h2 class="mt-4 border-b-2 border-gray-400 font-bold">{{ $key }}</h2>

                        @foreach ($brand as $b)
                            <div class="panel pt-2">
                                <a href="./brands/{{ $b->name }}">{{ $b->title }}</a>
                            </div>
                        @endforeach
                    </div>
                @endif
            @endforeach
        </div>
    </div>
@endsection
