<!DOCTYPE html>
<html lang="el">

<head>
    <!-- moosend -->
    <script>
        //load TrackerJS
        // !function (t, n, e, o, a) {
        //     function d(t) {
        //         var n = ~~(Date.now() / 3e5), o = document.createElement(e);
        //         o.async = !0, o.src = t + "?ts=" + n;
        //         var a = document.getElementsByTagName(e)[0];
        //         a.parentNode.insertBefore(o, a)
        //     }
        //
        //     t.MooTrackerObject = a, t[a] = t[a] || function () {
        //         return t[a].q ? void t[a].q.push(arguments) : void (t[a].q = [arguments])
        //     }, window.attachEvent ? window.attachEvent("onload", d.bind(this, o)) : window.addEventListener("load", d.bind(this, o), !1)
        // }(window, document, "script", "//cdn.stat-track.com/statics/moosend-tracking.min.js", "mootrack");
        // //tracker has to be initialized otherwise it will generate warnings and wont sendtracking events
        // mootrack('init', '0a3d5f625bc04d20bef8df293c08bd5c');
    </script>
    <!-- end moosend -->
    {{--    <script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="cf71daff-ccab-42c2-a171-ed23f7da5cc2" data-blockingmode="auto" type="text/javascript"></script>--}}
    <!-- Google Tag Manager -->
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-K63M6ZC');
    </script>
    <!-- End Google Tag Manager -->
    <meta charset="UTF-8">
    <!--Import Google Icon Font-->
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.css"/>
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css"/>
    <script src="https://kit.fontawesome.com/6a68f08925.js" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('views/eshoes/assets/css/custom.css?v='.time()) }}" rel="stylesheet">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;500;900&display=swap" rel="stylesheet">
    <link crossorigin href="https://QOXUJDLYXT-dsn.algolia.net" rel="preconnect"/>
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.4/js/lightbox.min.js"
            integrity="sha512-Ixzuzfxv1EqafeQlTCufWfaC6ful6WFqIz4G+dWvK0beHw0NVJwvCKSgafpy5gwNqKmgUfIBraVwkKI+Cz0SEQ=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.4/css/lightbox.min.css"
          integrity="sha512-ZKX+BvQihRJPA8CROKBhDNvoc2aDMOdAlcm7TUQY+35XYtrd3yh95QOOhsPDQY9QnKE0Wqag9y38OIgEvb88cA=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Google Tag Manager -->
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NN9WMHB');
    </script>
    <!-- End Google Tag Manager -->
</head>

<body>


<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K63M6ZC" height="0" width="0"
            style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NN9WMHB" height="0" width="0"
            style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="frontapp">
    @role('Admin')
    <div class="flex bg-blue-500 px-2 gap-4">
        <div class="flex-1"><a class="text-white text-xs" href="/manage">ΔΙΑΧΕΙΡΙΣΗ</a></div>
        @if(isset($product) && $product->id != null)

            <div class="flex-1">
                <a class="text-white text-xs" href=" @yield('editLink')/edit">ΕΠΕΞΕΡΓΑΣΙΑ</a>
            </div>
        @endif

    </div>


    @endrole
    <div class="md-sticky-top z-100 bg-white" style="z-index:1000;">
        @include('eshoes.elements.topBar')

        @include('eshoes.elements.navigation')
    </div>

    @include('eshoes.elements.mobileMenu')
    {{-- @include('eshoes.elements.cartcontent') --}}
    @include('eshoes.elements.cartMenu')

    @yield('content')
</div>
<script src="{{ asset('js/app.js') }}"></script>
{{-- </footer> --}}
<footer class="footer-1 mt-20 bg-es-blue py-8 sm:py-12">
    <div class="container mx-auto px-4">
        <div class="sm:-mx-4 sm:flex sm:flex-wrap md:py-4">
            <div class="px-4 sm:w-full md:w-2/12 xl:w-2/12 mt-8 px-4 sm:mt-0 text-center">
                <img src="/views/eshoes/assets/img/footer-bottom-logo.png" class="mb-8" alt="παπούτσια λογότυπο">

                <div class="text-center">
                    <h5 class="mb-4 text-md text-white  sm:text-center  mt-8">Συνδεθείτε μαζί μας!</h5>
                </div>
                <div class="flex sm:justify-center xl:justify-center">
                    <a href="https://www.facebook.com/e.shoes.online" target="_blank"
                       class="h-10 w-10 rounded-full  py-1 text-center text-white">
                        <i class="fab fa-facebook fa-2x"></i> </a>

                    <a href="https://www.instagram.com/eshoes.gr/" target="_blank"
                       class="ml-2 h-8 w-8 rounded-full  py-1 text-center text-white">
                        <i class="fab fa-instagram fa-2x"></i> </a>
                </div>
            </div>
            <div class="px-4 sm:w-full md:w-2/12 xl:w-2/12 mt-8 px-4 sm:mt-0">
                <h5 class="mb-6 text-xl text-white font-normal">Πληροφορίες</h5>
                <ul class="footer-links list-none">
                    <li class="mb-2">
                        <a href="/content/about-us"
                           class="border-b font-light border-solid border-transparent text-white">Προφίλ
                            Εταιρίας</a>
                    </li>
                    <li class="mb-2">
                        <a href="/content/secure-data"
                           class="border-b font-light border-solid border-transparent text-white">Ασφάλειας
                            Συναλλαγών</a>
                    </li>
                    <li class="mb-2">
                        <a href="/login"
                           class="border-b font-light border-solid border-transparent text-white">Ο
                            Λογαριασμός μου</a>
                    </li>
                    <li class="mb-2">
                        <a href="#"
                           class="border-b font-light border-solid border-transparent text-white">Επικοινωνήστε
                            μαζί μας</a>
                    </li>
                    <li class="mb-2">
                        <a href="content/prostasia-dedomenon"
                           class="border-b font-light border-solid border-transparent text-white">Προστασία
                            Δεδομένων</a>
                    </li>
                    <li class="mb-2">
                        <a href="/sitemap.xml"
                           class="border-b font-light border-solid border-transparent text-white">Χάρτης
                            Ιστοχώρου</a>
                    </li>
                </ul>
            </div>
            <div class="px-4 sm:w-full md:w-2/12 xl:w-2/12 mt-8 px-4 sm:mt-0">
                <h5 class="mb-6 text-xl text-white font-normal">Παραγγελίες</h5>
                <ul class="footer-links list-none">
                    <li class="mb-2">
                        <a href="/content/payment-ways"
                           class="border-b font-light border-solid border-transparent text-white">Τρόποι
                            Πληρωμής</a>
                    </li>
                    <li class="mb-2">
                        <a href="/content/send-items"
                           class="border-b font-light border-solid border-transparent text-white">Αποστολή
                            Προϊόντων</a>
                    </li>
                    <li class="mb-2">
                        <a href="/content/return-policy"
                           class="border-b font-light border-solid border-transparent text-white">Αλλαγές
                            / Επιστροφές</a>
                    </li>
                    <li class="mb-2">
                        <a href="/content/faqs"
                           class="border-b font-light border-solid border-transparent text-white">Συχνές
                            Ερωτήσεις</a>
                    </li>
                    <li class="mb-2">
                        <a href="/content/oroi-hrisis"
                           class="border-b font-light border-solid border-transparent text-white">Όροι
                            Χρήσης</a>
                    </li>
                </ul>
            </div>

            <div class="px-4 sm:w-full md:w-3/12 xl:w-3/12 mt-8 px-4 sm:mt-0">
                <div class="flex">
                    <div class="w-1/4">
                        <img src="/views/eshoes/assets/img/footer-icon-dorean.png" class="mb-8  px-2" alt="">
                    </div>
                    <div class="w-3/4">
                        <span class=" text-2xl font-normal text-white">ΔΩΡΕΑΝ</span> <br/>
                        <span class="text-white mt-4  font-light	">Αποστολή & Επιστροφή</span>
                    </div>
                </div>

                <div class="flex mt-8">
                    <div class="w-1/4">
                        <img src="/views/eshoes/assets/img/footer-icon-payment.png" class="mb-8  px-2" alt="">
                    </div>
                    <div class="w-3/4">
                        <span class=" text-2xl font-normal text-white">ΠΛΗΡΩΜΗ</span> <br/>
                        <span class="text-white mt-4 font-light	 ">Με αντικαταβολή ή κάρτα</span>
                    </div>
                </div>
            </div>

            <div class="px-4 sm:w-full md:w-3/12 xl:w-3/12 mt-8 px-4 sm:mt-0">
                <div class="text-left md:text-right">
                    <span class="text-white text-1xl block font-light">ΤΗΛ. ΠΑΡΑΓΓΕΛΙΩΝ</span>
                    <span class="text-white text-3xl">2810 228 000</span>
                </div>

                <div class="mt-8">
                    <img src="/views/eshoes/assets/img/footer-greca.png" class="mb-8  px-2" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="container container mx-auto border-t ">

        <div class="text-center">
            <img src="/views/eshoes/assets/img/footer-banks-sliced_14.png" class="inline-block" alt="banks-sliced_14"/>
            <img src="/views/eshoes/assets/img/footer-banks-sliced_13.png" class="inline-block" alt="banks-sliced_13"/>
            <img src="/views/eshoes/assets/img/footer-banks-sliced_09.png" class="inline-block" alt="banks-sliced_09"/>
            <img src="/views/eshoes/assets/img/footer-banks-sliced_10.png" class="inline-block" alt="banks-sliced_10"/>
            <img src="/views/eshoes/assets/img/footer-banks-sliced_12.png" class="inline-block" alt="banks-sliced_12"/>
            <img src="/views/eshoes/assets/img/footer-banks-sliced_11.png" class="inline-block" alt="banks-sliced_11"/>
            <img src="/views/eshoes/assets/img/footer-banks-sliced_08.png" class="inline-block" alt="banks-sliced_08"/>
            <img src="/views/eshoes/assets/img/footer-banks-sliced_07.png" class="inline-block" alt="banks-sliced_07"/>
            <img src="/views/eshoes/assets/img/footer-banks-sliced_06.png" class="inline-block" alt="banks-sliced_06"/>
            <img src="/views/eshoes/assets/img/footer-banks-sliced_05.png" class="inline-block" alt="banks-sliced_05"/>
            <img src="/views/eshoes/assets/img/footer-banks-sliced_04.png" class="inline-block" alt="banks-sliced_04"/>
            <img src="/views/eshoes/assets/img/footer-banks-sliced_03.png" class="inline-block" alt="banks-sliced_03"/>
            <img src="/views/eshoes/assets/img/footer-banks-sliced_02.png" class="inline-block" alt="banks-sliced_02"/>
            <img src="/views/eshoes/assets/img/footer-banks-sliced_01.png" class="inline-block" alt="banks-sliced_01"/>
        </div>


    </div>


    {{--    <script id="CookieDeclaration" src="https://consent.cookiebot.com/cf71daff-ccab-42c2-a171-ed23f7da5cc2/cd.js" type="text/javascript" async></script>--}}

</footer>


<script type="text/javascript">
    {{--function toggleModal(modalID) {--}}
    {{--    document.getElementById(modalID).classList.toggle("hidden");--}}
    {{--    document.getElementById(modalID + "-backdrop").classList.toggle("hidden");--}}
    {{--    document.getElementById(modalID).classList.toggle("flex");--}}
    {{--    document.getElementById(modalID + "-backdrop").classList.toggle("flex");--}}
    {{--}--}}

    {{--@if (!\Cookie::has('policy'))--}}
    {{--document.getElementById(modalID).classList.toggle("hidden");--}}
    {{--@endif--}}
</script>
<!--JavaScript at end of body for optimized loading-->
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script> --}}
{{-- <script src="{{asset('/views/eshoes/assets/js/nouislider.js')}}"></script> --}}
{{-- <script src="{{asset('/views/eshoes/assets/js/typingcarousel.js')}}"></script> --}}
{{-- <script src="{{asset('/views/eshoes/assets/js/custom.js')}}"></script> --}}
<script>
    $('.clickHart').click(function () {
        $(this).toggleClass('text-red-500');
    });
    const btnMenu = document.querySelector('.mobile-menu-button');
    const btnCart = document.querySelector('.cartbar-button-open');
    const btnClose = document.querySelector('.mobile-menu-button-close');
    const btnCartClose = document.querySelector('.cartbar-button-close');
    const btnCartClose2 = document.querySelector('.cartbar-button-close-link');
    const sidebar = document.querySelector('.sidebar');
    const cardSidebar = document.querySelector('.cartbar');

    const mobileSearchButton = document.querySelector('.mobile-search-button');
    const searchbox = document.querySelector('.searchbox');

    btnMenu.addEventListener('click', () => {
        sidebar.classList.toggle("-translate-x-full");
        sidebar.classList.toggle("hidden");
    })
    btnClose.addEventListener('click', () => {
        sidebar.classList.toggle("-translate-x-full");
        sidebar.classList.toggle("hidden");
    })

    function opencartnav() {
        cardSidebar.classList.remove("hidden");
    }

    btnCart.addEventListener('click', () => {
        cardSidebar.classList.toggle("hidden");
    })
    btnCartClose.addEventListener('click', () => {
        cardSidebar.classList.toggle("hidden");
    })
    btnCartClose2.addEventListener('click', () => {

        cardSidebar.classList.toggle("hidden");
    })

    mobileSearchButton.addEventListener('click', () => {
        searchbox.classList.toggle("hidden");
    })

    // toggle filters
    $('#filterToggler').click(function () {
        $("#filters").toggleClass("hidden");
    });

    function scrollTop() {
        window.scrollTo(0, 0);
    };


    //     lazy loading scripts
    document.addEventListener("DOMContentLoaded", function () {
        var lazyloadImages;

        if ("IntersectionObserver" in window) {
            lazyloadImages = document.querySelectorAll(".lazy");
            var imageObserver = new IntersectionObserver(function (entries, observer) {
                entries.forEach(function (entry) {
                    if (entry.isIntersecting) {
                        var image = entry.target;
                        image.src = image.dataset.src;
                        image.classList.remove("lazy");
                        image.classList.remove('animate-pulse')
                        imageObserver.unobserve(image);
                    }
                });
            });

            lazyloadImages.forEach(function (image) {
                imageObserver.observe(image);
            });
        } else {
            var lazyloadThrottleTimeout;
            lazyloadImages = document.querySelectorAll(".lazy");

            function lazyload() {
                if (lazyloadThrottleTimeout) {
                    clearTimeout(lazyloadThrottleTimeout);
                }

                lazyloadThrottleTimeout = setTimeout(function () {
                    var scrollTop = window.pageYOffset;
                    lazyloadImages.forEach(function (img) {
                        if (img.offsetTop < (window.innerHeight + scrollTop)) {
                            img.src = img.dataset.src;
                            img.classList.remove('lazy');
                        }
                    });
                    if (lazyloadImages.length == 0) {
                        document.removeEventListener("scroll", lazyload);
                        window.removeEventListener("resize", lazyload);
                        window.removeEventListener("orientationChange", lazyload);
                    }
                }, 20);
            }

            document.addEventListener("scroll", lazyload);
            window.addEventListener("resize", lazyload);
            window.addEventListener("orientationChange", lazyload);
        }
    })


    // filter filter values


    var searchFilterValues = function (field) {

        var input, filter, ul, li, a, i, txtValue;
        input = document.getElementById("search" + field);
        filter = input.value.toUpperCase();
        ul = document.getElementById("filterValuesContainer" + field);
        li = ul.getElementsByTagName("label");
        for (i = 0; i < li.length; i++) {
            a = li[i].getElementsByTagName("span")[0];
            txtValue = a.textContent || a.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
        }
    }
</script>
@yield('page_scripts')
</body>

</html>
