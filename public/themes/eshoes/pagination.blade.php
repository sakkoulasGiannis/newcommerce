@if ($paginator->hasPages())
    <div class="flex flex-col items-center">
        <div class="flex text-gray-700">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <div class="mr-1 flex h-8 w-8 cursor-pointer items-center justify-center rounded-full bg-gray-200">
                    <a href="#!">
                        <svg xmlns="https://www.w3.org/2000/svg" width="100%" height="100%" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                            stroke-linejoin="round" class="feather feather-chevron-left h-4 w-4">
                            <polyline points="15 18 9 12 15 6"></polyline>
                        </svg>
                    </a>
                </div>
            @else
                <div class="mr-1 flex h-8 w-8 cursor-pointer items-center justify-center rounded-full bg-gray-200">
                    <a href="{{ $paginator->previousPageUrl() }}">
                        <svg xmlns="https://www.w3.org/2000/svg" width="100%" height="100%" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                            stroke-linejoin="round" class="feather feather-chevron-left h-4 w-4">
                            <polyline points="15 18 9 12 15 6"></polyline>
                        </svg>
                    </a>
                </div>
            @endif
            <div class="flex h-8 rounded-full font-medium">

                {{-- Pagination Elements --}}
                @foreach ($elements as $element)
                    {{-- "Three Dots" Separator --}}
                    @if (is_string($element))
                        {{-- <li class="waves-effect disabled">{{ $element }}</li> --}}
                    @endif

                    {{-- Array Of Links --}}
                    @if (is_array($element))
                        @foreach ($element as $page => $url)
                            @if ($page == $paginator->currentPage())
                                <div
                                    class="bg-[#c09578] hidden w-8 items-center justify-center rounded-full leading-5 text-white transition duration-150 ease-in md:flex">
                                    {{ $page }}
                                </div>
                            @else
                                <div class="hidden md:flex">
                                    <a class="tx-sm w-8 cursor-pointer items-center justify-center rounded-full leading-5 transition duration-150 ease-in hover:bg-blue-900 hover:text-white md:flex"
                                        href="{{ $url }}">{{ $page }}</a>
                                </div>
                            @endif
                        @endforeach
                    @endif
                @endforeach
            </div>
            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <a href="{{ $paginator->nextPageUrl() }}" rel="next">
                    <div class="ml-1 flex h-8 w-8 cursor-pointer items-center justify-center rounded-full bg-gray-200">
                        <svg xmlns="https://www.w3.org/2000/svg" width="100%" height="100%" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                            stroke-linejoin="round" class="feather feather-chevron-right h-4 w-4">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>
            @else
                <li class="disabled">></li>
            @endif
        </div>
    </div>

@endif
