@extends('eshoes.layouts.app')
@section('content')
    <div class="container mx-auto">
        <div class="mt-8">
            @if (Session::has('message'))
                <p class="text-white bg-red-400 px-4 py-4">{{ Session::get('message') }}</p>
            @endif
            <h1 class="small">Καλάθι Αγορών</h1>
            <div class="section">
                <div class="flex flex-col">
                    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                            <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                                <table class="min-w-full divide-y divide-gray-200">
                                    <thead class="bg-gray-50">
                                    <tr>
                                        <th scope="col"
                                            class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            ΦΩΤΟΓΡΑΦΙΑ
                                        </th>
                                        <th scope="col"
                                            class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            ΤΙΤΛΟΣ
                                        </th>
                                        <th scope="col"
                                            class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            ΤΙΜΗ
                                        </th>
                                        <th scope="col"
                                            class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            ΤΜΧ
                                        </th>
                                        <th scope="col"
                                            class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            ΤΕΛΙΚΗ
                                        </th>
                                        <th scope="col" class="relative px-6 py-3">
                                            <span class="sr-only">ΑΦΑΙΡΕΣΗ</span>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody class="bg-white divide-y divide-gray-200">
                                    @foreach ($items as $val => $item)
                                        <tr>
                                            <td class="px-6 py-4 whitespace-nowrap">
                                                <div class="flex items-center">
                                                    <div class="flex-shrink-0 h-10 w-10">
                                                        <img class="h-10 w-10 rounded-full"
                                                             src="{{ $item->attributes->img_url }}" alt="">
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap">
                                                <div class="text-sm text-gray-900">{{ $item->name }}</div>

                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap">

                                                {{ $item->price }}€

                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap">
                                                {{ $item->quantity }}
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap">
                                                {{ $item->getPriceSum() }}€
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                                {!! Form::open(['route' => ['cart.deleteItem', $item->id], 'method' =>
                                                'post']) !!}
                                                <button class="unstyled trunsparent pointer">
                                                    <svg xmlns="https://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                                                         viewBox="0 0 24 24" stroke="currentColor">
                                                        <path stroke-linecap="round" stroke-linejoin="round"
                                                              stroke-width="2"
                                                              d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"/>
                                                    </svg>
                                                </button>
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>

                                    @endforeach




                                    @foreach ($conditions as $condition)
                                        {{-- @foreach ($condition as $c) --}}
                                        <tr>

                                            <td class="px-6 py-4">{{ $condition->getType() }}</td>
                                            <td class="px-6 py-4">{{ $condition->getName() }}</td>
                                            <td class="px-6 py-4"></td>
                                            <td class="px-6 py-4"></td>
                                            <td class="px-6 py-4">{{ $condition->getValue() }}</td>
                                        </tr>
                                        {{-- @endforeach --}}
                                    @endforeach


                                    <tr>
                                        <td class=" px-8 py-4"></td>
                                        <td class="px-6 py-4 whitespace-nowrap">ΤΕΛΙΚΟ ΣΥΝΟΛΟ</td>
                                        <td class=" px-8 py-4"></td>
                                        <td class=" px-8 py-4"></td>
                                        <td class="px-6 py-4 whitespace-nowrap"><b>{{ $cartDetails['total'] }}€</b>
                                        </td>
                                        <td class="  px-8 py-4"></td>

                                    </tr>
                                    <!-- More people... -->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid grid-cols-1 md:grid-cols-2">
            <form action="/check_for_voucher" method="post">
                @csrf
                <input type="text" value="" name="code" placeholder="Κωδικός κουπονιού"
                       class=" mt-4 lg:w-1/5 px-4 py-2 text-lg border border-gray-300   lg:text-lg focus:outline-none focus:ring-1 focus:ring-blue-600">
                <input type="hidden" value="true" name="redirect">
                <input type="submit" class="border shadow-md bg-green-500 text-white py-3 px-4" value="ΚΟΥΠΟΝΙ">

            </form>


            @if (count($items) > 0)
                <div class="section">
                    <div class="col s12 mt-6   text-right">
                        <a class="border shadow-md bg-es-blue text-white py-3 px-4" href="{{ route('cart.checkout') }}">ΟΛΟΚΛΗΡΩΣΗ
                        </a>
                    </div>
                </div>
            @endif
        </div>


    </div>
@endsection
