@extends('eshoes.layouts.app')
@section('content')
    <div class="mt-10 md:mt-4">
        @include('eshoes.elements.fslider')
    </div>
    @include('eshoes.elements.homeBanners')



    <div class="container mx-auto mt-8 ">

            <h2 class="pl-border-b-2 p-2 pl-4 font-bold text-es-blue text-lg mt-8">ΝΕΑ ΠΑΙΔΙΚΑ</h2>
            <div class="grid grid-cols-2 md:grid-cols-6 ">
{{--                @foreach ($latestChild as $product)--}}
{{--                    <div>--}}
{{--                        @include('eshoes.elements.productItem', $product)--}}
{{--                    </div>--}}
{{--                @endforeach--}}
            </div>

    </div>

@endsection
@section('page_scripts')
    <script>
        var swiper = new Swiper(".slider", {
            spaceBetween: 30,
            centeredSlides: true,
            autoplay: {
                delay: 3500,
                disableOnInteraction: false,
            },
            pagination: {
                el: ".swiper-pagination",
                // type: "progressbar",
            },
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
        });
    </script>
    <style>
        .swiper-slide {
            text-align: center;
            font-size: 18px;
            background: #fff;

            /* Center slide text vertically */
            display: -webkit-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            -webkit-justify-content: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            -webkit-align-items: center;
            align-items: center;
        }

        .swiper-slide img {
            display: block;
            width: 100%;
            height: 100%;
            object-fit: cover;
        }

        .grid figure figcaption {
            padding: 2em;
            color: #fff;
            text-transform: uppercase;
            font-size: 1.25em;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden;
        }

        .grid figure figcaption::before,
        .grid figure figcaption::after {
            pointer-events: none;
        }

        .grid figure figcaption,
        .grid figure figcaption > a {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }

        /* Anchor will cover the whole item by default */
        /* For some effects it will show as a button */
        .grid figure figcaption > a {
            z-index: 1000;
            text-indent: 200%;
            white-space: nowrap;
            font-size: 0;
            opacity: 0;
        }

        .grid figure h2 {
            word-spacing: -0.15em;
            font-weight: 300;
        }

        .grid figure h2 span {
            font-weight: 800;
        }

        .grid figure h2,
        .grid figure p {
            margin: 0;
        }

        .grid figure p {
            letter-spacing: 1px;
            font-size: 68.5%;
        }

        /*---------------*/
        /***** Honey *****/
        /*---------------*/
        .grid figure {
            position: relative;
            float: left;
            overflow: hidden;
            margin: 10px 1%;


            background: #3085a3;
            text-align: center;
            cursor: pointer;
        }

        .grid figure img {
            position: relative;
            display: block;
            min-height: 100%;
            max-width: 100%;
            opacity: 0.8;
        }

        figure.effect-honey {
            /*background: #4a3753;*/
        }

        figure.effect-honey img {
            opacity: 0.9;
            -webkit-transition: opacity 0.35s;
            transition: opacity 0.35s;
        }

        figure.effect-honey:hover img {
            opacity: 0.5;
        }

        figure.effect-honey figcaption::before {
            position: absolute;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 10px;
            background: #fff;
            content: '';
            -webkit-transform: translate3d(0, 10px, 0);
            transform: translate3d(0, 10px, 0);
        }

        figure.effect-honey h2 {
            position: absolute;
            bottom: 0;
            left: 0;
            padding: 1em 1.5em;
            width: 100%;
            text-align: left;
            -webkit-transform: translate3d(0, -30px, 0);
            transform: translate3d(0, -30px, 0);
        }

        figure.effect-honey h2 i {
            font-style: normal;
            opacity: 0;
            -webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
            transition: opacity 0.35s, transform 0.35s;
            -webkit-transform: translate3d(0, -30px, 0);
            transform: translate3d(0, -30px, 0);
        }

        figure.effect-honey figcaption::before,
        figure.effect-honey h2 {
            -webkit-transition: -webkit-transform 0.35s;
            transition: transform 0.35s;
        }

        figure.effect-honey:hover figcaption::before,
        figure.effect-honey:hover h2,
        figure.effect-honey:hover h2 i {
            opacity: 1;
            -webkit-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0);
        }

    </style>
@endsection
