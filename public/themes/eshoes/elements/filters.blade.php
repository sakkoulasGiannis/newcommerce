<div class="mt-6">
    <form ref="form" method="GET" action="{{ url()->current() }}">
        <div class="switch mb-2">

            <label class="flex flex items-center">
                <input onChange='submit();' type="checkbox" name="discounted" value="true"
                    {{ request()->has('discounted') && request()->get('discounted') == 'true' ? 'checked' : null }}
                    class="checkbox-orange" />
                <span class="ml-2 text-sm">Εκπτωτικά Προϊόντα</span>
            </label>

        </div>
        <input type="hidden" name="query" value="true">

        @foreach ($fields as $field)
            <span class="text-es-blue text-lg">{{ $field->title }}</span>
            @if ($field->values && count($field->values) > 8)
                <input class="border-1 w-full border-gray-400 p-2 text-sm" type="text" id="search{{ $field->name }}"
                    onkeyup="searchFilterValues('{{ $field->name }}')" placeholder="Αναζήτηση  {{ $field->title }}"
                    title="Type in a name">
            @endif

            <div class="filter-block mb-4 max-h-48 overflow-auto scroll-smooth p-2 hover:scroll-auto"
                id="filterValuesContainer{{ $field->name }}">
@if($field->values)
                @foreach ($field->values as $value)
                    @if ($value->title != '')

                        <label class="flex flex items-center" id="field{{ $field->name }}">
{{--                            @if($field->can_select_multiple)--}}

                            <input onChange='submit();' type="checkbox"  name="{{ $field->name }}[]"
                                value="{{ $value->name }}"
                                   {{(is_array(request()->get($field->name)) && in_array($value->name, request()->get($field->name))) ? 'checked' : ''}}
{{--                                {{ request()->has($field->name) && $value->name == request()->get($field->name) ? 'checked ' : null }}--}}
                                class="checkbox-orange" />

                            @else
                                <input onChange='submit();' type="checkbox"  name="{{ $field->name }}"
                                value="{{ $value->name }}"
                                {{ request()->has($field->name) && $value->name == request()->get($field->name) ? 'checked ' : null }}
                                class="checkbox-orange" />

                            @endif

                            <span class="text-es-blue ml-2 text-sm">{{ $value->title }}</span>
                            {{-- <span>{{$value->title}}</span> --}}
                        </label>
{{--                    @endif--}}
                @endforeach
    @endif;
            </div>

        @endforeach
        <button class="bg-es-blue px-2 py-2 text-white">Αναζήτηση</button>
    </form>

</div>
