<div class="productItem group relative p-4 text-center transition duration-200 ease-in-out border border-white hover:border-gray-200">
	@if ($product->price && $product->originalPrice)
		<div class="absolute right-0 flex h-10 w-10 items-center justify-center rounded-full bg-es-gold text-xs text-white">
			 {{ number_format((($product->originalPrice - $product->price ) / $product->originalPrice) * 100, 0)}}%
		</div>
	@elseif ($product->rule)
		<div class="absolute right-0 flex h-10 w-10 items-center justify-center rounded-full bg-green-500 text-xs text-white">
			-{{ $product->rule->discount_value }}%
		</div>
	@endif

	@if ($product->getFirstMediaUrl('default', 'thumb'))
		<a href="{{ $product->name }}">

			<img class="lazy mb-6  animate-pulse" loading="lazy"  src="/views/eshoes/assets/img/loadingshoe.png"
				 data-src="{{ $product->getFirstMediaUrl('default', 'thumb') }}" alt=""> </a>
	@else
		<a href="{{ $product->name }}">
			<img class="mb-6" loading="lazy"  src="https://demofree.sirv.com/nope-not-here.jpg">
		</a>
	@endif
		{{-- @include('eshoes.elements.productItemSize') --}}


	@if ($product->brand && $product->brand->first())
		<span class="brand text-center text-gray-600">{{ $product->brand->first()->title }}</span> <br/>
	@endif
	<span class="brand right-0 mt-2 text-xs text-gray-600">ΚΩΔ: {{ $product->sku }}</span> <br/>
	@if ($product->price && $product->originalPrice)
		<del class="price text-gray-600"> {{ $product->originalPrice }}€</del>
		<span class="price ml-2 text-lg text-red-500"> <b>{{ $product->price }}€</b></span>
	@else
		<span class="price text-es-gold">{{ $product->price }}€</span>
	@endif
	{{-- <a href="{{$product->name}}" --}}
	{{-- class="hover:bg-blue-900 hover:text-white hover:shadow-lg border border-gray-500 px-4 py-2 mt-2 text-xs mb-4  "> --}}
	{{-- ΔΕΙΤΕ ΤΟ --}}
	{{--  --}}{{-- <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor"> --}}
	{{-- <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 11V7a4 4 0 00-8 0v4M5 9h14l1 12H4L5 9z" /> --}}
	{{-- </svg> --}}
	{{-- </a> --}}
</div>
