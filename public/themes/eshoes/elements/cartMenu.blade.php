<div class="cartbar text-blue fixed inset-y-0 right-0 z-50 hidden h-full w-96 transform overflow-y-auto border-r bg-white shadow-xl transition duration-200 ease-in-out"
		{{-- class="hidden sidebar z-10 bg-white shadow-lg border-r text-blue w-5/6 absolute inset-y-0 left-0 transform -translate-x-full transition duration-200 ease-in-out mt-16" --}} style="z-index: 20000;">

	<div class="h-16 w-full bg-es-blue">
		<button class="cartbar-button-close float-right p-4 focus:bg-gray-700 focus:outline-none" >
			<svg xmlns="https://www.w3.org/2000/svg" class="h-6 w-6 text-white" fill="none" viewBox="0 0 24 24"
				 stroke="currentColor">
				<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"/>
			</svg>
		</button>
		<span class="float-left p-5 text-white">ΚΑΛΑΘΙ ΑΓΟΡΩΝ</span>
	</div>


	<div class="mt-8">
		<div class="mb-2 flex flex-col space-y-4">

{{--			<table>--}}
{{--				@foreach (\Cart::getContent() as $cart)--}}

{{--					<tr>--}}
{{--						<td>--}}
{{--							<a href="{{$cart->attributes->product_url}}">--}}
{{--								<img src="{{ $cart->attributes->img_url }}" alt="image" class="w-28"> </a>--}}
{{--						</td>--}}
{{--						<td class="pl-2">--}}
{{--							<a href="{{$cart->attributes->product_url}}">--}}
{{--								<h2 class="text-es-blue text-xl font-bold">{{ $cart->name }}</h2>--}}
{{--								<small>{{ $cart->attributes->sku }}</small> </a>--}}
{{--							<p class="text-es-gold text-lg">x {{ $cart->price }} € {{ $cart->quantity }}</p>--}}
{{--						</td>--}}
{{--						<td>{!! Form::open(['route' => ['cart.deleteItem', $cart->id], 'method' => 'post']) !!}--}}
{{--							<button class="unstyled trunsparent pointer">--}}
{{--								<svg xmlns="https://www.w3.org/2000/svg" class="h-4 w-4" fill="none"--}}
{{--									 viewBox="0 0 24 24" stroke="currentColor">--}}
{{--									<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"--}}
{{--										  d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"/>--}}
{{--								</svg>--}}
{{--							</button>--}}
{{--							{!! Form::close() !!}--}}
{{--						</td>--}}
{{--					</tr>--}}
{{--				@endforeach--}}
{{--			</table>--}}
		</div>
	</div>
{{--	@if (\Cart::getContent()->count() > 0)--}}
{{--		<div class="m-4 mt-16 mb-8">--}}
{{--			<span class="text-lg"><b>Σύνολο:</b> {{ \Cart::getTotal()  }} €</span>--}}

{{--		</div>--}}
{{--	@endif--}}
{{--	<div class="m-4   w-full text-center">--}}


{{--		@if (\Cart::getContent()->count() > 0)--}}
{{--			<a class="bg-es-blue mb-12 px-4 py-2 text-center text-white" href="/cart">ΟΛΟΚΛΗΡΩΣΗ ΠΑΡΑΓΓΕΛΙΑΣ</a> <br/>--}}
{{--			<br/>--}}
{{--		@else--}}
{{--			<span class="block"> Δεν έχετε προϊόντα στο καλάθι</span>--}}
{{--		@endif--}}
{{--		<a class="cartbar-button-close-link text-es-blue cursor-pointer text-sm   "> κλείσιμο παραθύρου </a>--}}
{{--	</div>--}}
</div>
