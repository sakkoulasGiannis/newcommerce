<ul>
@if(isset($allCategories))
    @foreach ($allCategories as $cat)
        @if ($cat->id == $parentid)
            @foreach ($cat->children as $c)
                <li class="menu-item p-0 m-0 h-5" style="list-style: none;">
                    <a class="level2 text-es-blue text-sm p-0 m-0"
                        href="{{ isset($append) ? $append : null }}/{{ $cat->name }}/{{ $c->name }}"><span>{{ $c->title }}</span></a>
                </li>
            @endforeach
        @endif
    @endforeach
@endif
</ul>
