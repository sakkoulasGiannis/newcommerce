<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>Emporiki shop demo</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">


    <script type="text/javascript">

        function send_tran() {

            var frm = document.getElementById('shopform1');
            frm.style.visibility="hidden";
            frm.submit();

        }
    </script>
</head>
<body onload="send_tran();">

<?php
if (isset($_GET['act']) && $_GET['act'] == "send") {

$form_data = "";
$form_data_array = array();

$form_mid = $_POST['mid'];									$form_data_array[1] = $form_mid;					//Req
$form_lang = $_POST['lang'];								$form_data_array[2] = $form_lang;					//Opt
$form_device_cate = ""; /*$_POST[''];*/						$form_data_array[3] = $form_device_cate;			//Opt
$form_order_id = $_POST['orderid'];							$form_data_array[4] = $form_order_id;				//Req
$form_order_desc = $_POST['orderDesc'];						$form_data_array[5] = $form_order_desc;				//Opt
$form_order_amount = $_POST['orderAmount'];					$form_data_array[6] = $form_order_amount;			//Req
$form_currency = $_POST['currency'];						$form_data_array[7] = $form_currency;				//Req
$form_email = $_POST['payerEmail'];							$form_data_array[8] = $form_email;					//Req
$form_phone = $_POST['payerPhone'];							$form_data_array[9] = $form_phone;					//Opt
$form_bill_country = $_POST['billCountry'];					$form_data_array[10] = $form_bill_country;			//Opt
$form_bill_state = $_POST['billState'];						$form_data_array[11] = $form_bill_state;			//Opt
$form_bill_zip = $_POST['billZip'];							$form_data_array[12] = $form_bill_zip;				//Opt
$form_bill_city = $_POST['billCity'];						$form_data_array[13] = $form_bill_city;				//Opt
$form_bill_addr = $_POST['billAddress'];					$form_data_array[14] = $form_bill_addr;				//Opt
$form_weight = $_POST['weight'];							$form_data_array[15] = $form_weight;				//Opt
$form_dimension = $_POST['dimensions'];						$form_data_array[16] = $form_dimension;				//Opt
$form_ship_counrty = $_POST['shipCountry'];					$form_data_array[17] = $form_ship_counrty;			//Opt
$form_ship_state = $_POST['shipState'];						$form_data_array[18] = $form_ship_state;			//Opt
$form_ship_zip = $_POST['shipZip'];							$form_data_array[19] = $form_ship_zip;				//Opt
$form_ship_city = $_POST['shipCity'];						$form_data_array[20] = $form_ship_city;				//Opt
$form_ship_addr = $_POST['shipAddress'];					$form_data_array[21] = $form_ship_addr;				//Opt
$form_add_fraud_score = $_POST['addFraudScore'];			$form_data_array[22] = $form_add_fraud_score;		//Opt
$form_max_pay_retries = $_POST['maxPayRetries'];			$form_data_array[23] = $form_max_pay_retries;		//Opt
$form_reject3dsU = $_POST['reject3dsU'];					$form_data_array[24] = $form_reject3dsU;			//Opt
$form_pay_method = $_POST['payMethod'];						$form_data_array[25] = $form_pay_method;			//Opt
$form_trytpe = $_POST['trType'];							$form_data_array[26] = $form_trytpe;				//Opt
$form_ext_install_offset = $_POST['extInstallmentoffset'];	$form_data_array[27] = $form_ext_install_offset;	//Opt
$form_ext_install_period = $_POST['extInstallmentperiod'];	$form_data_array[28] = $form_ext_install_period;	//Opt
$form_ext_reccuring_freq = $_POST['extRecurringfrequency'];	$form_data_array[29] = $form_ext_reccuring_freq;	//Opt
$form_ext_reccuring_enddate = $_POST['extRecurringenddate'];$form_data_array[30] = $form_ext_reccuring_enddate;	//Opt
$form_block_score = $_POST['blockScore'];					$form_data_array[31] = $form_block_score;			//Opt
$form_cssurl = $_POST['cssUrl'];							$form_data_array[32] = $form_cssurl;				//Opt
$form_confirm_url = $_POST['confirmUrl'];					$form_data_array[33] = $form_confirm_url;			//Req
$form_cancel_url = $_POST['cancelUrl'];						$form_data_array[34] = $form_cancel_url;			//Req
$form_var1 = $_POST['var1'];								$form_data_array[35] = $form_var1;
$form_var2 = $_POST['var2'];								$form_data_array[36] = $form_var2;
$form_var3 = $_POST['var3'];								$form_data_array[37] = $form_var3;
$form_var4 = $_POST['var4'];								$form_data_array[38] = $form_var4;
$form_var5 = $_POST['var5'];								$form_data_array[39] = $form_var5;
$form_secret = "XXXXXXX";									$form_data_array[40] = $form_secret;				//Req


$form_data = implode("", $form_data_array);

$digest = base64_encode(sha1($form_data,true));

$send_it_2 = "https://alpha.test.modirum.com/vpos/shophandlermpi";

?>
<form id="shopform1" name="demo" method="POST" action="<?php echo $send_it_2 ?>" accept-charset="UTF-8" >

    <input type="hidden" name="mid" value="<?php  echo $form_mid ?>"/>
    <input type="hidden" name="lang" value="<?php echo $form_lang ?>"/>
    <input type="hidden" name="deviceCategory" value="<?php  echo $form_device_cate ?>"/>
    <input type="hidden" name="orderid" value="<?php  echo $form_order_id ?>"/>
    <input type="hidden" name="orderDesc" value="<?php  echo $form_order_desc ?>"/>
    <input type="hidden" name="orderAmount" value="<?php  echo $form_order_amount ?>"/>
    <input type="hidden" name="currency" value="<?php  echo $form_currency ?>"/>
    <input type="hidden" name="payerEmail" value="<?php  echo $form_email ?>"/>
    <input type="hidden" name="payerPhone" value="<?php  echo $form_phone ?>"/>
    <input type="hidden" name="billCountry" value="<?php  echo $form_bill_country ?>"/>
    <input type="hidden" name="billState" value="<?php  echo $form_bill_state ?>"/>
    <input type="hidden" name="billZip" value="<?php  echo $form_bill_zip ?>"/>
    <input type="hidden" name="billCity" value="<?php  echo $form_bill_city ?>"/>
    <input type="hidden" name="billAddress" value="<?php  echo $form_bill_addr ?>"/>
    <input type="hidden" name="weight" value="<?php  echo $form_weight ?>"/>
    <input type="hidden" name="dimensions" value="<?php  echo $form_dimension ?>"/>
    <input type="hidden" name="shipCountry" value="<?php  echo $form_ship_counrty ?>"/>
    <input type="hidden" name="shipState" value="<?php  echo $form_ship_state ?>"/>
    <input type="hidden" name="shipZip" value="<?php  echo $form_ship_zip ?>"/>
    <input type="hidden" name="shipCity" value="<?php  echo $form_ship_city ?>"/>
    <input type="hidden" name="shipAddress" value="<?php  echo $form_ship_addr ?>"/>
    <input type="hidden" name="addFraudScore" value="<?php  echo $form_add_fraud_score ?>"/>
    <input type="hidden" name="maxPayRetries" value="<?php  echo $form_max_pay_retries ?>"/>
    <input type="hidden" name="reject3dsU" value="<?php  echo $form_reject3dsU ?>"/>
    <input type="hidden" name="payMethod" value="<?php  echo $form_pay_method ?>"/>
    <input type="hidden" name="trType" value="<?php  echo $form_trytpe ?>"/>
    <input type="hidden" name="extInstallmentoffset" value="<?php  echo $form_ext_install_offset ?>"/>
    <input type="hidden" name="extInstallmentperiod" value="<?php  echo $form_ext_install_period ?>"/>
    <input type="hidden" name="extRecurringfrequency" value="<?php  echo $form_ext_reccuring_freq ?>"/>
    <input type="hidden" name="extRecurringenddate" value="<?php  echo $form_ext_reccuring_enddate ?>"/>
    <input type="hidden" name="blockScore" value="<?php  echo $form_block_score ?>"/>
    <input type="hidden" name="cssUrl" value="<?php  echo $form_cssurl ?>"/>
    <input type="hidden" name="confirmUrl" value="<?php  echo $form_confirm_url ?>"/>
    <input type="hidden" name="cancelUrl" value="<?php  echo $form_cancel_url ?>"/>
    <input type="hidden" name="var1" value="<?php  echo $form_var1 ?>"/>
    <input type="hidden" name="var2" value="<?php  echo $form_var2 ?>"/>
    <input type="hidden" name="var3" value="<?php  echo $form_var3 ?>"/>
    <input type="hidden" name="var4" value="<?php  echo $form_var4 ?>"/>
    <input type="hidden" name="var5" value="<?php  echo $form_var5 ?>"/>
    <input type="hidden" name="digest" value="<?php  echo $digest ?>"/>
</form>

</body>
</html>

<?php
}

?>
