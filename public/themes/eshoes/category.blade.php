@extends('eshoes.layouts.app')
@section('content')
    <div class="container mx-auto mt-16 md:mt-0">
        <div class="grid grid-cols-12 md:grid-cols-12 md:gap-4">
            <div x-data={open:true} class="col-span-12 md:col-span-2">

                <a id="filterToggler"
                    class="cursor-pointer rounded text-blue-500 hover:bg-blue-500 focus:outline-none md:hidden">
                    <svg xmlns="https://www.w3.org/2000/svg" class="ml-4 h-6 w-6" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M3 4a1 1 0 011-1h16a1 1 0 011 1v2.586a1 1 0 01-.293.707l-6.414 6.414a1 1 0 00-.293.707V17l-4 4v-6.586a1 1 0 00-.293-.707L3.293 7.293A1 1 0 013 6.586V4z" />
                    </svg>
                </a>

                <div id="filters" class="my-2 hidden px-4 py-3 text-gray-700 md:block">
                    {{-- <filters :products="{{ json_encode($allProducts) }}" :category="{{ $category }}" /> --}}
                    @include('eshoes.elements.filters')
                </div>
            </div>

            <div class="col-span-12 md:col-span-10">
                <div class="mb-2 grid grid-cols-12 p-4 gap-4">
                    <div class="col-span-8 md:col-span-3">
                        <form method="GET">
                            <select onchange="this.form.submit()" class="w-auto" name="sort">


                                <option
                                    {{ !request()->exists('sort') && request()->get('sort') == '' ? 'selected' : null }}
                                    value="">Νέα Προϊόντα</option>
                                <option
                                    {{ request()->exists('sort') && request()->get('sort') == 'price_asc' ? 'selected' : null }}
                                    value="price_asc">Τιμή Αύξουσα</option>
                                <option
                                    {{ request()->exists('sort') && request()->get('sort') == 'price_desc' ? 'selected' : null }}
                                    value="price_desc">Τιμή Φθίνουσα </option>
                                <option
                                    {{ request()->exists('sort') && request()->get('sort') == 'discount_asc' ? 'selected' : null }}
                                    value="discount_asc">Ποσοστό Έκτπωσης Αύξουσα</option>
                                <option
                                    {{ request()->exists('sort') && request()->get('sort') == 'discount_desc' ? 'selected' : null }}
                                    value="discount_desc">Ποσοστό Έκπτωσης Φθίνουσα </option>
                                <option
                                    {{ request()->exists('sort') && request()->get('sort') == 'season_asc' ? 'selected' : null }}
                                    value="season_asc">Νέες Παραλαβές Αύξουσα </option>
                                <option
                                    {{ request()->exists('sort') && request()->get('sort') == 'season_desc' ? 'selected' : null }}
                                    value="season_desc">Νέες Παραλαβές Φθίνουσα </option>
                            </select>
                        </form>
                    </div>
                    <form method="GET" class="col-span-6 md:col-span-1">
                        <select class="browser-default" onChange='submit();' name="pagination">
                            <option value="32"
                                {{ session()->get('paginate') && session()->get('paginate') == 32 ? 'selected' : null }}>
                                32
                            </option>
                            <option value="64"
                                {{ session()->get('paginate') && session()->get('paginate') == 64 ? 'selected' : null }}>
                                64
                            </option>
                            <option value="96"
                                {{ session()->get('paginate') && session()->get('paginate') == 96 ? 'selected' : null }}>
                                96
                            </option>
                            <option value="128"
                                {{ session()->get('paginate') && session()->get('paginate') == 128 ? 'selected' : null }}>
                                128
                            </option>
                        </select>
                    </form>
                    <div class="col-span-12 pt-2 md:col-span-8">
                        <div class="flex justify-end">
                            {{ $products->links('eshoes.pagination') }}
                        </div>
                    </div>
                </div>

                <hr>
                <div class="grid grid-cols-2 gap-4 md:grid-cols-4 mt-4">
                    @foreach ($products as $product)
                        @include('eshoes.elements.productItem', $product)
                    @endforeach
                </div>
                <div class="center pt-4 text-center">
                    {{ $products->links('eshoes.pagination') }}

                </div>

            </div>
        </div>
    </div>
@endsection
@section('page_scripts')

@endsection
