<div class="md:col-span-1 col-span-12">
    <h1 class="p-2">Ο ΛΟΓΑΡΙΑΣΜΟΣ ΜΟΥ</h1>
    <ul class="p-2 mt-4">
        <li class="border p-2 border-gray-300"><a href="/account">ΠΙΝΑΚΑΣ ΕΛΕΓΧΟΥ</a></li>
        <li class="border p-2 border-gray-300"><a href="/account/orders">ΠΑΡΑΓΓΕΛΙΕΣ</a></li>
        <li class="border p-2 border-gray-300">ΣΤΟΙΧΕΙΑ ΛΟΓΑΡΙΑΣΜΟΥ</li>


        <li class="border p-2 border-gray-300">
            <form method="POST" action="{{ route('logout') }}">
                @csrf
                <button>ΑΠΟΣΥΝΔΕΣΗ</button>
            </form>
        </li>

        @role('Admin')
        <li class="border p-2 border-gray-300"><a href="/manage">ΔΙΑΧΕΙΡΙΣΗ</a></li>
        @endrole
    </ul>
</div>