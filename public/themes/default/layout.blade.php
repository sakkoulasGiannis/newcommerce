<!DOCTYPE html>
<html lang="el">

<head>
    <!-- moosend -->
    <script>
        //load TrackerJS
        // !function (t, n, e, o, a) {
        //     function d(t) {
        //         var n = ~~(Date.now() / 3e5), o = document.createElement(e);
        //         o.async = !0, o.src = t + "?ts=" + n;
        //         var a = document.getElementsByTagName(e)[0];
        //         a.parentNode.insertBefore(o, a)
        //     }
        //
        //     t.MooTrackerObject = a, t[a] = t[a] || function () {
        //         return t[a].q ? void t[a].q.push(arguments) : void (t[a].q = [arguments])
        //     }, window.attachEvent ? window.attachEvent("onload", d.bind(this, o)) : window.addEventListener("load", d.bind(this, o), !1)
        // }(window, document, "script", "//cdn.stat-track.com/statics/moosend-tracking.min.js", "mootrack");
        // //tracker has to be initialized otherwise it will generate warnings and wont sendtracking events
        // mootrack('init', '0a3d5f625bc04d20bef8df293c08bd5c');
    </script>
    <!-- end moosend -->
    {{--    <script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="cf71daff-ccab-42c2-a171-ed23f7da5cc2" data-blockingmode="auto" type="text/javascript"></script>--}}
    <!-- Google Tag Manager -->
    <script>
        // (function (w, d, s, l, i) {
        //     w[l] = w[l] || [];
        //     w[l].push({
        //         'gtm.start': new Date().getTime(),
        //         event: 'gtm.js'
        //     });
        //     var f = d.getElementsByTagName(s)[0],
        //         j = d.createElement(s),
        //         dl = l != 'dataLayer' ? '&l=' + l : '';
        //     j.async = true;
        //     j.src =
        //         'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        //     f.parentNode.insertBefore(j, f);
        // })(window, document, 'script', 'dataLayer', 'GTM-K63M6ZC');
    </script>
    <!-- End Google Tag Manager -->
    <meta charset="UTF-8">
    <!--Import Google Icon Font-->
    {{--    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.css"/>--}}
    {{--    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css"/>--}}
    {{--    <script src="https://kit.fontawesome.com/6a68f08925.js" crossorigin="anonymous"></script>--}}
    {{--    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>--}}
    {{--    <script src="https://code.jquery.com/jquery-3.6.0.min.js"--}}
    {{--            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>--}}
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    {{--    <link href="{{ asset('views/eshoes/assets/css/custom.css?v='.time()) }}" rel="stylesheet">--}}
    {{--    <link href="{{ asset('chronoro/assets/css/custom.css?v='.time()) }}" rel="stylesheet">--}}

    {{--    <link rel="preconnect" href="https://fonts.googleapis.com">--}}
    {{--    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>--}}
    {{--    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;500;900&display=swap" rel="stylesheet">--}}
    {{--    <link crossorigin href="https://QOXUJDLYXT-dsn.algolia.net" rel="preconnect"/>--}}
    {{--    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>--}}
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.4/js/lightbox.min.js"--}}
    {{--            integrity="sha512-Ixzuzfxv1EqafeQlTCufWfaC6ful6WFqIz4G+dWvK0beHw0NVJwvCKSgafpy5gwNqKmgUfIBraVwkKI+Cz0SEQ=="--}}
    {{--            crossorigin="anonymous" referrerpolicy="no-referrer"></script>--}}
    {{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.4/css/lightbox.min.css"--}}
    {{--          integrity="sha512-ZKX+BvQihRJPA8CROKBhDNvoc2aDMOdAlcm7TUQY+35XYtrd3yh95QOOhsPDQY9QnKE0Wqag9y38OIgEvb88cA=="--}}
    {{--          crossorigin="anonymous" referrerpolicy="no-referrer"/>--}}
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">


    @if(isset($product) )
        <script type="application/ld+json">
            {
              "@context": "https://schema.org",
              "@type": "Product",
              "name": "{{$product->name}}",
              "image": "{{$product->thub}}",
              "description": "{{$product->description}}",
              "sku": "{{$product->sku}}",

{{--              "brand": {--}}
{{--                "@type": "Brand",--}}
{{--                "name": "{{(isset($product->brand)) ? $product->brand->name : ''}}"--}}
{{--              },--}}
              "offers": {
                "@type": "Offer",
                "url": "{{url()->current()}}",
                "priceCurrency": "EUR",
                "price": "{{$product->price}}",
                "itemCondition": "https://schema.org/NewCondition",
                "availability": "https://schema.org/InStock",
                "seller": {
                  "@type": "Organization",
                  "name": "Example Store"
                }
              },

            }
        </script>
    @endif



    <!-- Google Tag Manager -->
    {{--    <script>--}}
    {{--        (function (w, d, s, l, i) {--}}
    {{--            w[l] = w[l] || [];--}}
    {{--            w[l].push({--}}
    {{--                'gtm.start': new Date().getTime(),--}}
    {{--                event: 'gtm.js'--}}
    {{--            });--}}
    {{--            var f = d.getElementsByTagName(s)[0],--}}
    {{--                j = d.createElement(s),--}}
    {{--                dl = l != 'dataLayer' ? '&l=' + l : '';--}}
    {{--            j.async = true;--}}
    {{--            j.src =--}}
    {{--                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;--}}
    {{--            f.parentNode.insertBefore(j, f);--}}
    {{--        })(window, document, 'script', 'dataLayer', 'GTM-NN9WMHB');--}}
    {{--    </script>--}}
    <!-- End Google Tag Manager -->
    {{--    <link href="{{ asset('views/chronoro/dist/styles.css') }}" rel="stylesheet">--}}
{{--    <script src="https://cdn.jsdelivr.net/npm/alpinejs@3.x.x/dist/cdn.min.js" defer></script>--}}
    {{--    <script src="https://cdn.tailwindcss.com"></script>--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{--    <link rel="stylesheet" href="/css/editor.css">--}}
    <script src="/js/main.js"></script>
    {{--    <script src="https://cdn.tailwindcss.com"></script>--}}
    {{--    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/purecss@3.0.0/build/pure-min.css" integrity="sha384-X38yfunGUhNzHpBaEBsWLO+A0HDYOQi8ufWDkZ0k9e0eXz/tH3II7uKZ9msv++Ls" crossorigin="anonymous">--}}
</head>
<body>
@role('Admin!')
<div class="flex bg-black  gap-4">
    <div class="flex-1 px-4"><a class="text-white text-xs" href="/manage">ΔΙΑΧΕΙΡΙΣΗ</a></div>
    @if(isset($product) )
        <div class="flex-1 px-4">
            <a class="text-white text-xs" href="/manage/products/{{$product->id}}/edit">ΕΠΕΞΕΡΓΑΣΙΑ ΠΡΟΪΟΝΤΟΣ</a>
        </div>
    @endif
</div>
@endrole

@include(config('app.theme_folder') . '.header')

<!-- Google Tag Manager (noscript) -->
{{--<noscript>--}}
{{--    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K63M6ZC" height="0" width="0"--}}
{{--            style="display:none;visibility:hidden"></iframe>--}}
{{--</noscript>--}}
<!-- End Google Tag Manager (noscript) -->
<!-- Google Tag Manager (noscript) -->
{{--<noscript>--}}
{{--    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NN9WMHB" height="0" width="0"--}}
{{--            style="display:none;visibility:hidden"></iframe>--}}
{{--</noscript>--}}
<!-- End Google Tag Manager (noscript) -->
<div id="frontapp">

    <div class="md-sticky-top z-100 bg-white" style="z-index:1000;">
        {{--        @include('eshoes.elements.topBar')--}}

        {{--        @include('eshoes.elements.navigation')--}}
    </div>

    {{--    @include('eshoes.elements.mobileMenu')--}}
    {{-- @include('eshoes.elements.cartcontent') --}}
    {{--    @include('eshoes.elements.cartMenu')--}}

    @if (session('error'))
        <div class="max-w-8xl mx-auto border-l-4 border-yellow-400 bg-yellow-200 p-4 my-4">
            <div class="flex">
                <div class="shrink-0">
                    <svg class="size-5 text-yellow-400" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true" data-slot="icon">
                        <path fill-rule="evenodd" d="M8.485 2.495c.673-1.167 2.357-1.167 3.03 0l6.28 10.875c.673 1.167-.17 2.625-1.516 2.625H3.72c-1.347 0-2.189-1.458-1.515-2.625L8.485 2.495ZM10 5a.75.75 0 0 1 .75.75v3.5a.75.75 0 0 1-1.5 0v-3.5A.75.75 0 0 1 10 5Zm0 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2Z" clip-rule="evenodd" />
                    </svg>
                </div>
                <div class="ml-3">
                    <p class="text-sm text-yellow-700">
                        {{ session('error') }}

                    </p>
                </div>
            </div>
        </div>


    @endif

    @yield('content')

</div>
{{--@include(config('app.theme_folder') . '.footer')--}}
@include(config('app.theme_folder') . '.footer')

<script src="{{ asset('js/app.js') }}"></script>
{{-- </footer> --}}


<!--JavaScript at end of body for optimized loading-->
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script> --}}
{{-- <script src="{{asset('/views/eshoes/assets/js/nouislider.js')}}"></script> --}}
<script>


    // filter filter values

    //
    // var searchFilterValues = function (field) {
    //
    //     var input, filter, ul, li, a, i, txtValue;
    //     input = document.getElementById("search" + field);
    //     filter = input.value.toUpperCase();
    //     ul = document.getElementById("filterValuesContainer" + field);
    //     li = ul.getElementsByTagName("label");
    //     for (i = 0; i < li.length; i++) {
    //         a = li[i].getElementsByTagName("span")[0];
    //         txtValue = a.textContent || a.innerText;
    //         if (txtValue.toUpperCase().indexOf(filter) > -1) {
    //             li[i].style.display = "";
    //         } else {
    //             li[i].style.display = "none";
    //         }
    //     }
    // }
    // document.addEventListener('DOMContentLoaded', function () {
    //     var toggleButton = document.getElementById('toggle_mobile_menu');
    //     var closeButton = document.getElementById('mobile_menu_close_button');
    //     var menu = document.getElementById('mobile_menu');
    //     console.log('toggleButton')
    //     console.log(toggleButton)
    //     if (toggleButton && closeButton && menu) {
    //         toggleButton.addEventListener('click', function () {
    //             console.log('Toggle button clicked');
    //             menu.classList.toggle('hidden');
    //         });
    //         closeButton.addEventListener('click', function () {
    //             console.log('Close button clicked');
    //             menu.classList.toggle('hidden');
    //         });
    //     } else {
    //         console.log('One or more elements not found');
    //     }
    // });

    var searchFilterValues = function (field) {

        var input, filter, ul, li, a, i, txtValue;
        input = document.getElementById("search" + field);
        filter = input.value.toUpperCase();
        ul = document.getElementById("filterValuesContainer" + field);
        li = ul.getElementsByTagName("label");
        for (i = 0; i < li.length; i++) {
            a = li[i].getElementsByTagName("span")[0];
            txtValue = a.textContent || a.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
        }
    }
</script>
@yield('page_scripts')
<style>
    /*.mega-menu {*/
    /*    position: absolute;*/
    /*    display: none;*/
    /*}*/

    /*.hoverable:hover .mega-menu {*/
    /*    display: block;*/
    /*}*/
    .mega-menu {
        display: none;
        z-index: 1000;
        box-shadow: 0px 7px 9px 0px #5a5a5a42;
    }

    .mega-menu.show {
        opacity: 0.97;
        display: block;
    }

</style>
 <script>

    {{-- toglle Menu --}}
    document.querySelectorAll('.menu-link').forEach(link => {
        let showTimeout, hideTimeout;
        const megaMenu = link.nextElementSibling;

        link.addEventListener('mouseenter', function () {
            clearTimeout(hideTimeout); // Clear any pending hide timeout
            showTimeout = setTimeout(() => {
                if (megaMenu && megaMenu.classList.contains('mega-menu')) {
                    megaMenu.classList.add('show');
                }
            }, 200);
        });

        link.addEventListener('mouseleave', function () {
            clearTimeout(showTimeout); // Clear any pending show timeout
            hideTimeout = setTimeout(() => {
                if (megaMenu && megaMenu.classList.contains('mega-menu')) {
                    megaMenu.classList.remove('show');
                }
            }, 200);
        });

        if (megaMenu) {
            megaMenu.addEventListener('mouseenter', function () {
                clearTimeout(hideTimeout); // Clear any pending hide timeout
            });

            megaMenu.addEventListener('mouseleave', function () {
                hideTimeout = setTimeout(() => {
                    megaMenu.classList.remove('show');
                }, 200);
            });
        }
    });

</script>
</body>
</html>
