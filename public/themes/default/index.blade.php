
<!DOCTYPE html>
<html lang="el">

<head>
    <!-- moosend -->
    <script>
        //load TrackerJS
        // !function (t, n, e, o, a) {
        //     function d(t) {
        //         var n = ~~(Date.now() / 3e5), o = document.createElement(e);
        //         o.async = !0, o.src = t + "?ts=" + n;
        //         var a = document.getElementsByTagName(e)[0];
        //         a.parentNode.insertBefore(o, a)
        //     }
        //
        //     t.MooTrackerObject = a, t[a] = t[a] || function () {
        //         return t[a].q ? void t[a].q.push(arguments) : void (t[a].q = [arguments])
        //     }, window.attachEvent ? window.attachEvent("onload", d.bind(this, o)) : window.addEventListener("load", d.bind(this, o), !1)
        // }(window, document, "script", "//cdn.stat-track.com/statics/moosend-tracking.min.js", "mootrack");
        // //tracker has to be initialized otherwise it will generate warnings and wont sendtracking events
        // mootrack('init', '0a3d5f625bc04d20bef8df293c08bd5c');
    </script>
    <!-- end moosend -->
    {{--    <script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="cf71daff-ccab-42c2-a171-ed23f7da5cc2" data-blockingmode="auto" type="text/javascript"></script>--}}
    <!-- Google Tag Manager -->
    <script>
        // (function (w, d, s, l, i) {
        //     w[l] = w[l] || [];
        //     w[l].push({
        //         'gtm.start': new Date().getTime(),
        //         event: 'gtm.js'
        //     });
        //     var f = d.getElementsByTagName(s)[0],
        //         j = d.createElement(s),
        //         dl = l != 'dataLayer' ? '&l=' + l : '';
        //     j.async = true;
        //     j.src =
        //         'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        //     f.parentNode.insertBefore(j, f);
        // })(window, document, 'script', 'dataLayer', 'GTM-K63M6ZC');
    </script>
    <!-- End Google Tag Manager -->
    <meta charset="UTF-8">
    <!--Import Google Icon Font-->
    {{--    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.css"/>--}}
    {{--    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css"/>--}}
    {{--    <script src="https://kit.fontawesome.com/6a68f08925.js" crossorigin="anonymous"></script>--}}
    {{--    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>--}}
    {{--    <script src="https://code.jquery.com/jquery-3.6.0.min.js"--}}
    {{--            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>--}}
{{--        <link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
    {{--    <link href="{{ asset('views/eshoes/assets/css/custom.css?v='.time()) }}" rel="stylesheet">--}}

    {{--    <link rel="preconnect" href="https://fonts.googleapis.com">--}}
    {{--    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>--}}
    {{--    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;500;900&display=swap" rel="stylesheet">--}}
    {{--    <link crossorigin href="https://QOXUJDLYXT-dsn.algolia.net" rel="preconnect"/>--}}
    {{--    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>--}}
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.4/js/lightbox.min.js"--}}
    {{--            integrity="sha512-Ixzuzfxv1EqafeQlTCufWfaC6ful6WFqIz4G+dWvK0beHw0NVJwvCKSgafpy5gwNqKmgUfIBraVwkKI+Cz0SEQ=="--}}
    {{--            crossorigin="anonymous" referrerpolicy="no-referrer"></script>--}}
    {{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.4/css/lightbox.min.css"--}}
    {{--          integrity="sha512-ZKX+BvQihRJPA8CROKBhDNvoc2aDMOdAlcm7TUQY+35XYtrd3yh95QOOhsPDQY9QnKE0Wqag9y38OIgEvb88cA=="--}}
    {{--          crossorigin="anonymous" referrerpolicy="no-referrer"/>--}}
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Google Tag Manager -->
    {{--    <script>--}}
    {{--        (function (w, d, s, l, i) {--}}
    {{--            w[l] = w[l] || [];--}}
    {{--            w[l].push({--}}
    {{--                'gtm.start': new Date().getTime(),--}}
    {{--                event: 'gtm.js'--}}
    {{--            });--}}
    {{--            var f = d.getElementsByTagName(s)[0],--}}
    {{--                j = d.createElement(s),--}}
    {{--                dl = l != 'dataLayer' ? '&l=' + l : '';--}}
    {{--            j.async = true;--}}
    {{--            j.src =--}}
    {{--                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;--}}
    {{--            f.parentNode.insertBefore(j, f);--}}
    {{--        })(window, document, 'script', 'dataLayer', 'GTM-NN9WMHB');--}}
    {{--    </script>--}}
    <!-- End Google Tag Manager -->
{{--    <link href="{{ asset('views/chronoro/dist/styles.css') }}" rel="stylesheet">--}}
{{--    <script src="https://cdn.jsdelivr.net/npm/alpinejs@3.x.x/dist/cdn.min.js" defer></script>--}}

</head>

<body>
<header class="bg-white shadow">
    <div class="mx-auto max-w-7xl px-2 sm:px-4 lg:divide-y lg:divide-gray-200 lg:px-8">
        <div class="relative flex h-16 justify-between">
            <div class="relative z-10 flex px-2 lg:px-0">
                <div class="flex flex-shrink-0 items-center">
                    <a href="/"> <img class="h-8 w-auto" src="https://www.chronoro.gr/wp-content/uploads/2023/03/sitelogo.png" alt="Κόσμημα Παπαδάκης ηράκλειο κρήτης"></a>
                </div>
            </div>
            <div class="relative z-0 flex flex-1 items-center justify-center px-2 sm:absolute sm:inset-0">
                <div class="w-full sm:max-w-xs">
                    <label for="search" class="sr-only">Search</label>
                    <div class="relative">
                        <div class="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                            <svg class="h-5 w-5 text-gray-400" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                <path fill-rule="evenodd" d="M9 3.5a5.5 5.5 0 100 11 5.5 5.5 0 000-11zM2 9a7 7 0 1112.452 4.391l3.328 3.329a.75.75 0 11-1.06 1.06l-3.329-3.328A7 7 0 012 9z" clip-rule="evenodd" />
                            </svg>
                        </div>
                        <input id="search" name="search" class="block w-full rounded-md border-0 bg-white py-1.5 pl-10 pr-3 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" placeholder="Search" type="search">
                    </div>
                </div>
            </div>
            <div class="relative z-10 flex items-center lg:hidden">
                <!-- Mobile menu button -->
                <button type="button" class="relative inline-flex items-center justify-center rounded-md p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500" aria-controls="mobile-menu" aria-expanded="false">
                    <span class="absolute -inset-0.5"></span>
                    <span class="sr-only">Open menu</span>
                    <!--
                      Icon when menu is closed.

                      Menu open: "hidden", Menu closed: "block"
                    -->
                    <svg class="block h-6 w-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
                    </svg>
                    <!--
                      Icon when menu is open.

                      Menu open: "block", Menu closed: "hidden"
                    -->
                    <svg class="hidden h-6 w-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
            </div>
            <div class="hidden lg:relative lg:z-10 lg:ml-4 lg:flex lg:items-center">
                <button type="button" class="relative flex-shrink-0 rounded-full bg-white p-1 text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">
                    <span class="absolute -inset-1.5"></span>
                    <span class="sr-only">View notifications</span>
                    <svg class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M14.857 17.082a23.848 23.848 0 005.454-1.31A8.967 8.967 0 0118 9.75v-.7V9A6 6 0 006 9v.75a8.967 8.967 0 01-2.312 6.022c1.733.64 3.56 1.085 5.455 1.31m5.714 0a24.255 24.255 0 01-5.714 0m5.714 0a3 3 0 11-5.714 0" />
                    </svg>
                </button>

                <!-- Profile dropdown -->
                <div class="relative ml-4 flex-shrink-0">
                    <div>
                        <button type="button" class="relative flex rounded-full bg-white focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2" id="user-menu-button" aria-expanded="false" aria-haspopup="true">
                            <span class="absolute -inset-1.5"></span>
                            <span class="sr-only">Open user menu</span>
                            <img class="h-8 w-8 rounded-full" src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="">
                        </button>
                    </div>

                    <!--
                      Dropdown menu, show/hide based on menu state.

                      Entering: "transition ease-out duration-100"
                        From: "transform opacity-0 scale-95"
                        To: "transform opacity-100 scale-100"
                      Leaving: "transition ease-in duration-75"
                        From: "transform opacity-100 scale-100"
                        To: "transform opacity-0 scale-95"
                    -->
                    {{--                    <div class="absolute right-0 z-10 mt-2 w-48 origin-top-right rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none" role="menu" aria-orientation="vertical" aria-labelledby="user-menu-button" tabindex="-1">--}}
                    {{--                        <!-- Active: "bg-gray-100", Not Active: "" -->--}}
                    {{--                        <a href="#" class="block px-4 py-2 text-sm text-gray-700" role="menuitem" tabindex="-1" id="user-menu-item-0">Your Profile</a>--}}
                    {{--                        <a href="#" class="block px-4 py-2 text-sm text-gray-700" role="menuitem" tabindex="-1" id="user-menu-item-1">Settings</a>--}}
                    {{--                        <a href="#" class="block px-4 py-2 text-sm text-gray-700" role="menuitem" tabindex="-1" id="user-menu-item-2">Sign out</a>--}}
                    {{--                    </div>--}}
                </div>
            </div>
        </div>

        <div class="bg-white">
            <!--
              Mobile menu

              Off-canvas menu for mobile, show/hide based on off-canvas menu state.
            -->
            <div class="relative z-40 lg:hidden" role="dialog" aria-modal="true">
                <!--
                  Off-canvas menu backdrop, show/hide based on off-canvas menu state.

                  Entering: "transition-opacity ease-linear duration-300"
                    From: "opacity-0"
                    To: "opacity-100"
                  Leaving: "transition-opacity ease-linear duration-300"
                    From: "opacity-100"
                    To: "opacity-0"
                -->
                <div class="fixed inset-0 bg-black bg-opacity-25" aria-hidden="true"></div>

                <div class="fixed inset-0 z-40 flex">
                    <!--
                      Off-canvas menu, show/hide based on off-canvas menu state.

                      Entering: "transition ease-in-out duration-300 transform"
                        From: "-translate-x-full"
                        To: "translate-x-0"
                      Leaving: "transition ease-in-out duration-300 transform"
                        From: "translate-x-0"
                        To: "-translate-x-full"
                    -->
                    <div class="relative flex w-full max-w-xs flex-col overflow-y-auto bg-white pb-12 shadow-xl">
                        <div class="flex px-4 pb-2 pt-5">
                            <button type="button" class="-m-2 inline-flex items-center justify-center rounded-md p-2 text-gray-400">
                                <span class="sr-only">Close menu</span>
                                <svg class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
                                </svg>
                            </button>
                        </div>

                        <!-- Links -->
                        <div class="mt-2">
                            <div class="border-b border-gray-200">
                                <div class="-mb-px flex space-x-8 px-4" aria-orientation="horizontal" role="tablist">
                                    <!-- Selected: "border-indigo-600 text-indigo-600", Not Selected: "border-transparent text-gray-900" -->
                                    <button id="tabs-1-tab-1" class="flex-1 whitespace-nowrap border-b-2 border-transparent px-1 py-4 text-base font-medium text-gray-900" aria-controls="tabs-1-panel-1" role="tab" type="button">Women</button>
                                    <!-- Selected: "border-indigo-600 text-indigo-600", Not Selected: "border-transparent text-gray-900" -->
                                    <button id="tabs-1-tab-2" class="flex-1 whitespace-nowrap border-b-2 border-transparent px-1 py-4 text-base font-medium text-gray-900" aria-controls="tabs-1-panel-2" role="tab" type="button">Men</button>
                                </div>
                            </div>

                            <!-- 'Women' tab panel, show/hide based on tab state. -->
                            <div id="tabs-1-panel-1" class="space-y-12 px-4 pb-6 pt-10" aria-labelledby="tabs-1-tab-1" role="tabpanel" tabindex="0">
                                <div class="grid grid-cols-1 items-start gap-x-6 gap-y-10">
                                    <div class="grid grid-cols-1 gap-x-6 gap-y-10">
                                        <div>
                                            <p id="mobile-featured-heading-0" class="font-medium text-gray-900">Featured</p>
                                            <ul role="list" aria-labelledby="mobile-featured-heading-0" class="mt-6 space-y-6">
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Sleep</a>
                                                </li>
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Swimwear</a>
                                                </li>
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Underwear</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div>
                                            <p id="mobile-categories-heading" class="font-medium text-gray-900">Categories</p>
                                            <ul role="list" aria-labelledby="mobile-categories-heading" class="mt-6 space-y-6">
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Basic Tees</a>
                                                </li>
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Artwork Tees</a>
                                                </li>
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Bottoms</a>
                                                </li>
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Underwear</a>
                                                </li>
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Accessories</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-1 gap-x-6 gap-y-10">
                                        <div>
                                            <p id="mobile-collection-heading" class="font-medium text-gray-900">Collection</p>
                                            <ul role="list" aria-labelledby="mobile-collection-heading" class="mt-6 space-y-6">
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Everything</a>
                                                </li>
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Core</a>
                                                </li>
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">New Arrivals</a>
                                                </li>
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Sale</a>
                                                </li>
                                            </ul>
                                        </div>

                                        <div>
                                            <p id="mobile-brand-heading" class="font-medium text-gray-900">Brands</p>
                                            <ul role="list" aria-labelledby="mobile-brand-heading" class="mt-6 space-y-6">
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Full Nelson</a>
                                                </li>
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">My Way</a>
                                                </li>
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Re-Arranged</a>
                                                </li>
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Counterfeit</a>
                                                </li>
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Significant Other</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- 'Men' tab panel, show/hide based on tab state. -->
                            <div id="tabs-1-panel-2" class="space-y-12 px-4 pb-6 pt-10" aria-labelledby="tabs-1-tab-2" role="tabpanel" tabindex="0">
                                <div class="grid grid-cols-1 items-start gap-x-6 gap-y-10">
                                    <div class="grid grid-cols-1 gap-x-6 gap-y-10">
                                        <div>
                                            <p id="mobile-featured-heading-1" class="font-medium text-gray-900">Featured</p>
                                            <ul role="list" aria-labelledby="mobile-featured-heading-1" class="mt-6 space-y-6">
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Casual</a>
                                                </li>
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Boxers</a>
                                                </li>
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Outdoor</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div>
                                            <p id="mobile-categories-heading" class="font-medium text-gray-900">Categories</p>
                                            <ul role="list" aria-labelledby="mobile-categories-heading" class="mt-6 space-y-6">
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Artwork Tees</a>
                                                </li>
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Pants</a>
                                                </li>
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Accessories</a>
                                                </li>
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Boxers</a>
                                                </li>
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Basic Tees</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-1 gap-x-6 gap-y-10">
                                        <div>
                                            <p id="mobile-collection-heading" class="font-medium text-gray-900">Collection</p>
                                            <ul role="list" aria-labelledby="mobile-collection-heading" class="mt-6 space-y-6">
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Everything</a>
                                                </li>
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Core</a>
                                                </li>
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">New Arrivals</a>
                                                </li>
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Sale</a>
                                                </li>
                                            </ul>
                                        </div>

                                        <div>
                                            <p id="mobile-brand-heading" class="font-medium text-gray-900">Brands</p>
                                            <ul role="list" aria-labelledby="mobile-brand-heading" class="mt-6 space-y-6">
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Significant Other</a>
                                                </li>
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">My Way</a>
                                                </li>
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Counterfeit</a>
                                                </li>
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Re-Arranged</a>
                                                </li>
                                                <li class="flex">
                                                    <a href="#" class="text-gray-500">Full Nelson</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="space-y-6 border-t border-gray-200 px-4 py-6">
                            <div class="flow-root">
                                <a href="#" class="-m-2 block p-2 font-medium text-gray-900">Company</a>
                            </div>
                            <div class="flow-root">
                                <a href="#" class="-m-2 block p-2 font-medium text-gray-900">Stores</a>
                            </div>
                        </div>

                        <div class="space-y-6 border-t border-gray-200 px-4 py-6">
                            <div class="flow-root">
                                <a href="#" class="-m-2 block p-2 font-medium text-gray-900">Create an account</a>
                            </div>
                            <div class="flow-root">
                                <a href="#" class="-m-2 block p-2 font-medium text-gray-900">Sign in</a>
                            </div>
                        </div>

                        <div class="space-y-6 border-t border-gray-200 px-4 py-6">
                            <!-- Currency selector -->
                            <form>
                                <div class="inline-block">
                                    <label for="mobile-currency" class="sr-only">Currency</label>
                                    <div class="group relative -ml-2 rounded-md border-transparent focus-within:ring-2 focus-within:ring-white">
                                        <select id="mobile-currency" name="currency" class="flex items-center rounded-md border-transparent bg-none py-0.5 pl-2 pr-5 text-sm font-medium text-gray-700 focus:border-transparent focus:outline-none focus:ring-0 group-hover:text-gray-800">
                                            <option>CAD</option>
                                            <option>USD</option>
                                            <option>AUD</option>
                                            <option>EUR</option>
                                            <option>GBP</option>
                                        </select>
                                        <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center">
                                            <svg class="h-5 w-5 text-gray-500" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                                <path fill-rule="evenodd" d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z" clip-rule="evenodd" />
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <header class="relative">
                <nav aria-label="Top">
                    <!-- Top navigation -->


                    <!-- Secondary navigation -->
                    <div class="bg-white">
                        <div class="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
                            <div class="border-b border-gray-200">
                                <div class="flex h-16 items-center justify-between">


                                    <div class="hidden h-full lg:flex">
                                        <!-- Mega menus -->

                                        <div class="flex h-full justify-center space-x-8">
                                            @foreach($allCategories as $cat)
                                                <div class="flex" x-data="{ open: false }"  @mouseenter="open = true" @mouseleave="open = false">
                                                    <div class="relative flex">
                                                        <!-- Item active: "border-indigo-600 text-indigo-600", Item inactive: "border-transparent text-gray-700 hover:text-gray-800" -->
                                                        <a href="/{{$cat->name}}" type="button" class="relative z-10 -mb-px flex items-center border-b-2 border-transparent pt-px text-sm font-medium text-gray-700 transition-colors duration-200 ease-out hover:text-gray-800" aria-expanded="false">
                                                            {{$cat->title}}
                                                        </a>
                                                    </div>

                                                    <!--
                                                      'Women' mega menu, show/hide based on flyout menu state.

                                                      Entering: "transition ease-out duration-200"
                                                        From: "opacity-0"
                                                        To: "opacity-100"
                                                      Leaving: "transition ease-in duration-150"
                                                        From: "opacity-100"
                                                        To: "opacity-0"
                                                    -->
                                                    <div class="z-50 absolute inset-x-0 top-full text-gray-500 sm:text-sm" x-show="open" x-transition:enter="transition ease-out duration-200" x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100" x-transition:leave="transition ease-in duration-150" x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0">
                                                        <!-- Presentational element used to render the bottom shadow, if we put the shadow on the actual panel it pokes out the top, so we use this shorter element to hide the top of the shadow -->
                                                        <div class="absolute inset-0 top-1/2 bg-white shadow" aria-hidden="true"></div>

                                                        <div class="relative bg-white">
                                                            <div class="mx-auto max-w-7xl px-8">
                                                                <div class="grid grid-cols-2 items-start gap-x-8 gap-y-10 pb-12 pt-10">
                                                                    <div class="grid grid-cols-3 gap-x-8 gap-y-10">
                                                                        @foreach($cat->children as $catChild)
                                                                            <div>
                                                                                <a href="/{{$cat->name}}/{{$catChild->name}}" id="desktop-featured-heading-0" class="font-medium text-gray-900">{{$catChild->title}}</a>
                                                                                <ul role="list" aria-labelledby="desktop-featured-heading-0" class="mt-6 space-y-6 sm:mt-4 sm:space-y-4">
                                                                                    @foreach($catChild->children as $catSubChild)
                                                                                        <li class="flex">
                                                                                            <a href="/{{$cat->name}}/{{$catChild->name}}/{{$catSubChild->name}}" class="hover:text-gray-800">{{$catSubChild->title}}</a>
                                                                                        </li>
                                                                                    @endforeach

                                                                                </ul>
                                                                            </div>
                                                                        @endforeach

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach

                                        </div>

                                    </div>

                                    <!-- Mobile menu and search (lg-) -->
                                    <div class="flex flex-1 items-center lg:hidden">
                                        <!-- Mobile menu toggle, controls the 'mobileMenuOpen' state. -->
                                        <button id="toggle_mobile_menu" type="button" class="-ml-2 rounded-md bg-white p-2 text-gray-400">
                                            <span class="sr-only">Open menu</span>
                                            <svg class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                                                <path stroke-linecap="round" stroke-linejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
                                            </svg>
                                        </button>

                                        <!-- Search -->
                                        <a href="#" class="ml-2 p-2 text-gray-400 hover:text-gray-500">
                                            <span class="sr-only">Search</span>
                                            <svg class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                                                <path stroke-linecap="round" stroke-linejoin="round" d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z" />
                                            </svg>
                                        </a>
                                    </div>

                                    <!-- Logo (lg-) -->
                                    <a href="#" class="lg:hidden">
                                        <span class="sr-only">Your Company</span>
                                        <img src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=600" alt="" class="h-8 w-auto">
                                    </a>

                                    <div class="flex flex-1 items-center justify-end">
                                        <div class="flex items-center lg:ml-8">
                                            <div class="flex space-x-8">
                                                <div class="hidden lg:flex">
                                                    <a href="#" class="-m-2 p-2 text-gray-400 hover:text-gray-500">
                                                        <span class="sr-only">Search</span>
                                                        <svg class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                                                            <path stroke-linecap="round" stroke-linejoin="round" d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z" />
                                                        </svg>
                                                    </a>
                                                </div>

                                                <div class="flex">
                                                    <a href="#" class="-m-2 p-2 text-gray-400 hover:text-gray-500">
                                                        <span class="sr-only">Account</span>
                                                        <svg class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                                                            <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 6a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0zM4.501 20.118a7.5 7.5 0 0114.998 0A17.933 17.933 0 0112 21.75c-2.676 0-5.216-.584-7.499-1.632z" />
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>

                                            <span class="mx-4 h-6 w-px bg-gray-200 lg:mx-6" aria-hidden="true"></span>

                                            <div class="flow-root">
                                                <a href="#" class="group -m-2 flex items-center p-2">
                                                    <svg class="h-6 w-6 flex-shrink-0 text-gray-400 group-hover:text-gray-500" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                                                        <path stroke-linecap="round" stroke-linejoin="round" d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 00-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 00-16.536-1.84M7.5 14.25L5.106 5.272M6 20.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm12.75 0a.75.75 0 11-1.5 0 .75.75 0 011.5 0z" />
                                                    </svg>
                                                    <span class="ml-2 text-sm font-medium text-gray-700 group-hover:text-gray-800">0</span>
                                                    <span class="sr-only">items in cart, view bag</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
            </header>
        </div>

    </div>

    <!-- Mobile menu, show/hide based on menu state. -->

</header>


<!-- Google Tag Manager (noscript) -->
{{--<noscript>--}}
{{--    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K63M6ZC" height="0" width="0"--}}
{{--            style="display:none;visibility:hidden"></iframe>--}}
{{--</noscript>--}}
<!-- End Google Tag Manager (noscript) -->
<!-- Google Tag Manager (noscript) -->
{{--<noscript>--}}
{{--    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NN9WMHB" height="0" width="0"--}}
{{--            style="display:none;visibility:hidden"></iframe>--}}
{{--</noscript>--}}
<!-- End Google Tag Manager (noscript) -->
<div id="frontapp">
    @role('Admin')
    <div class="flex bg-blue-500 px-2 gap-4">
        <div class="flex-1"><a class="text-white text-xs" href="/manage">ΔΙΑΧΕΙΡΙΣΗ</a></div>
        @if(isset($product) && $product->id != null)
            <div class="flex-1">
                <a class="text-white text-xs" href=" @yield('editLink')/edit">ΕΠΕΞΕΡΓΑΣΙΑ Προϊόντος</a>
            </div>
        @endif

    </div>


    @endrole
    <div class="md-sticky-top z-100 bg-white" style="z-index:1000;">
        {{--        @include('eshoes.elements.topBar')--}}

        {{--        @include('eshoes.elements.navigation')--}}
    </div>

    {{--    @include('eshoes.elements.mobileMenu')--}}
    {{-- @include('eshoes.elements.cartcontent') --}}
    {{--    @include('eshoes.elements.cartMenu')--}}

    @yield('content')
</div>
<script src="{{ asset('js/app.js') }}"></script>
{{-- </footer> --}}


<!--JavaScript at end of body for optimized loading-->
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script> --}}
{{-- <script src="{{asset('/views/eshoes/assets/js/nouislider.js')}}"></script> --}}
<script>


    // filter filter values for search
    // var searchFilterValues = function (field) {
    //
    //     var input, filter, ul, li, a, i, txtValue;
    //     input = document.getElementById("search" + field);
    //     filter = input.value.toUpperCase();
    //     ul = document.getElementById("filterValuesContainer" + field);
    //     li = ul.getElementsByTagName("label");
    //     for (i = 0; i < li.length; i++) {
    //         a = li[i].getElementsByTagName("span")[0];
    //         txtValue = a.textContent || a.innerText;
    //         if (txtValue.toUpperCase().indexOf(filter) > -1) {
    //             li[i].style.display = "";
    //         } else {
    //             li[i].style.display = "none";
    //         }
    //     }
    // }




</script>
@yield('page_scripts')
<style>
    .mega-menu {
        position: absolute;
        display: none;
    }

    .hoverable:hover .mega-menu {
        display: block;
    }


</style>
</body>

</html>
