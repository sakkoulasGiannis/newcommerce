<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GdprController extends Controller
{
    public function aprove(Request $request)
    {
        if (\Auth::guest()) {

            \Cookie::queue('policy', 1, 1440);
            foreach ($request->except('_token') as $val => $policy) {
                  \Cookie::forget($val);

                \Cookie::queue($val, 1, 1440);
            }
        } else {
            // return $request->except('_token');
            $user = \Auth::user();
            if ($user->gdpr) {
                \Cookie::queue('policy', 1, 1440);
                foreach ($request->except('_token') as $val => $policy) {
                    \Cookie::forget($val);

                    \Cookie::queue($val, 1, 1440);
                }
            } else {
                // create gdpr
                $user->gdpr()->create($request->except('_token'));
                \Cookie::queue('policy', 1, 1440);
                foreach ($request->except('_token') as $val => $policy) {
                    \Cookie::forget($val);

                    \Cookie::queue($val, 1, 1440);
                }
            }
        }

        return back();
    }
}
