<?php

namespace App\Http\Controllers;

//region use

use App\Helpers\AppHelper;
use App\Mail\newOrder;
use App\Models\Country;
use App\Models\Field;

//use App\Models\Http\Requests\CheckoutRequest;
use App\Models\Order;
use App\Models\Payment;
use App\Models\Product;
use App\Models\Stock;
use App\Models\User;
use App\Models\Voucher;
use Carbon\Carbon;
use Darryldecode\Cart;
use Dotenv\Exception\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;



class CartController extends Controller
{
    // use NationalBankOfGreeceTrait;
    /**
     * cart page
     * @return $this
     */
    public function index()
    {


        // update the item on cart

        // delete an item on cart

        // view the cart items
        // return \Cart::getTotal();
        $conditions = \Cart::getConditions();
        $items = $this->cartItems();
        $cartDetails = $this->cartDetails();
        //        \Cart::clearCartConditions();

        return view(\Config::get('settings.theme') . '.cart')->with(compact('items', 'cartDetails', 'conditions'));
    }

    /**
     * checkout page
     * @return $this
     */
    public function checkout()
    {
        $couriers = [];
        $cartItems = $this->cartItems();
        $cartContent = \Cart::getContent();
        $cartDetails = $this->cartDetails();
        $conditions = \Cart::getConditions();
        $countries = Country::where('is_active', 1)->get();
        $payments = [];

        if (count($cartItems) < 1) {
            return view(\Config::get('settings.theme') . '.checkoutNoProducts');

            return 'no products';
        }

        return view(\Config::get('settings.theme') . '.checkout')->with(compact('countries', 'cartContent', 'cartItems', 'conditions', 'cartDetails', 'payments', 'couriers'));
    }

    public function completed(Request $request)
    {

        $order = $request->get('order_id');
        return view(\Config::get('settings.theme') . '.success_checkout', compact('order'));

        if (\Session::has('order_id')) {
            $order = \Session::get('order_id');
            $order = Order::findOrFail($order);
        } else {
            \Session::flash('flash_message', __('No order found'));
            return redirect(route('home'));
        }
    }

    /**
     * submit checkout form
     * @param CheckoutRequest $request
     * @return mixed
     */
    public function Complete(Request $request)
    {


        $validatedData = \Validator::make($request->get('checkout'), [
            'email' => 'required|email',
            'billing_city' => 'required',
            'billing_lastname' => 'required',
            'billing_mobile' => 'required',
            'billing_name' => 'required',
            'billing_street' => 'required',
            'billing_zip' => 'required',
            'selectedCountry.id' => 'required',
            'selectedCounty.id' => 'required',
            'shipping.id' => 'required'
        ]);
        if ($validatedData->fails()) {
            return $validatedData->validated();
        }
        if (\Cart::getContent()->count() <= 0) {
            abort(404);
        }

        /**
         * get cart detail
         */
        $cartDetails = $this->cartDetails();

        /**
         * set invoice id
         *
         */
        $invoice_id = Order::count() + 1;

        // check payment module
        //         $payment = $this->checkPayment($request);
        $order = [
            'order' => [
                'order_num' => $invoice_id,
                'ip' => $_SERVER['REMOTE_ADDR'],
                'slug' => substr(md5(rand()), 0, 100) . time(), // order random slug
            ],
        ];

        //        return $request->all();
        //        return $request->get('payment_method') ;
        $status_id = 'pending';

        if ($request->get('payment.name') == 'cod') {
            $shipping_id = 1;
            $payment_id = 1;
            $status_id = 1;
        }
        $order['comment'] = $request->get('checkout')['comment'];
        $order['detail'] = array(
            'status_id' => 1,
            'invoice_id' => null, //@todo check shipping
            'payment_id' => $request->get('checkout')['payment']['id'],
            'shipping_id' => $request->get('checkout')['shipping']['id'], //@todo check shipping
            'courier_name' => $request->get('checkout')['courier_name'],
            'payment_name' => $request->get('checkout')['payment_name'],
            'shipping_cost' => $request->get('checkout')['selectedShippingCost'],
            'payment_cost' => null,
            'total' => $cartDetails['total'],
            'sub_total' => $cartDetails['sub_total'],
            'billing_email' => $request->get('checkout')['email'],
            'billing_name' => $request->get('checkout')['billing_name'],
            'billing_lastname' => $request->get('checkout')['billing_lastname'],
            'billing_country_id' => $request->get('checkout')['selectedCounty']['id'],
            'billing_region' => $request->get('checkout')['selectedCounty']['title'],
            'billing_city' => $request->get('checkout')['selectedCounty']['title'],
            'billing_street' => $request->get('checkout')['billing_street'],
            'billing_zip' => $request->get('checkout')['billing_zip'],
            'billing_street_number' => 0,
            'billing_mobile' => $request->get('checkout')['billing_mobile'],
            'billing_phone' => $request->get('checkout')['billing_mobile'],
            'shipping_country_id' => (isset(request()->shipping['country_id']) ? request()->shipping['country_id'] : $request->get('checkout')['selectedCounty']['id']),
            'shipping_region' => (isset(request()->shipping['region']) ? request()->shipping['region'] : $request->get('checkout')['selectedCounty']['title']),
            'shipping_city' => (isset(request()->shipping['city']) ? request()->shipping['city'] : $request->get('checkout')['selectedCounty']['title']),
            'shipping_street' => (isset(request()->shipping['street']) ? request()->shipping['street'] : $request->get('checkout')['billing_street']),
            'shipping_zip' => (isset(request()->shipping['zip']) ? request()->shipping['zip'] : $request->get('checkout')['billing_zip']),
            'shipping_street_number' => 0,
            'shipping_phone' => (isset(request()->shipping['mobile']) ? request()->shipping['mobile'] : $request->get('checkout')['billing_mobile']),
            'shipping_mobile' => (isset(request()->shipping['mobile']) ? request()->shipping['mobile'] : $request->get('checkout')['billing_mobile']),
            'shipping_name' => (isset(request()->shipping['name']) ? request()->shipping['name'] : $request->get('checkout')['billing_name']),
            'shipping_email' => (isset(request()->shipping['email']) ? request()->shipping['email'] : $request->get('checkout')['email']),
            'shipping_lastname' => (isset(request()->shipping['lastname']) ? request()->shipping['lastname'] : $request->get('checkout')['billing_lastname']),
            'created_at' => Carbon::now(),
            'invoice_status' => $request->get('checkout')['invoice_status'],
            'afm' => $request->get('checkout')['afm'],
            'doy' => $request->get('checkout')['doy'],
            'company' => $request->get('checkout')['company'],
            'activity' => $request->get('checkout')['activity'],

        );


        $cartProducts = [];

        foreach (\Cart::getContent() as $cartProduct) {
            // if product is dynamic

            $associated = Stock::find($cartProduct->id);
            $newQuantity = $associated->quantity - $cartProduct->quantity;
            $associated->update(['quantity' => $newQuantity]);
            $product = Product::find($cartProduct->attributes->product);

            $order['products'][] = [
                'product_id' => $cartProduct->attributes->product,
                'stock_id' => $associated->id,
                'name' => $product->title,
                'sku' => $associated->sku,
                'price' => $cartProduct->price,
                'quantity' => $cartProduct->quantity,
            ];
        }

        $order['user'] = [
            // 'order_id'
            // 'shipping_address_id'
            // 'billing_address_id'
            'newsletter'=> request()->newsletter,
            'name' => request()->name,
            'lastname' => request()->lastname,
            'email' => request()->email,
            'mobile' => request()->billing_mobile,
        ];

        $order['address'] = request()->address;
        $order['shipping'] = request()->shipping;
        /*
        * create user and add create Order
        */
        // return $order;





        $order = $this->checkAuthAndCreateOrder($order);
        //        return $payment->execute($order);
        // \Cart::clear();
        if ($request->payment_method == 'ethniki') {
            return $this->nbgPayment($cartDetails['total'], date('d-m-Y s:i'), 'Some referense');
        }


        $paymentMethodId = $request->get('checkout')['payment']['id'];

        // no redirect required 
        if ($paymentMethodId !== 2) {
            Mail::to(request()->email)->send(new newOrder($order));
            return json_encode(['status' => 'success', 'redirect_url' => '/cart/checkout/complete', 'order_id' => $order['order_num']]);
        } else {
            return json_encode(['status' => 'success', 'redirect_url' => '/cart/checkout/alphabank', 'order_id' => $order['order_num']]);
        }




        //        return redirect('/cart/checkout/complete')->with('order_id', $order['order_num']);

        //        return view(\Config::get('settings.theme') .'.success_checkout', compact('order'));
    }

    public function payments(Request $payment)
    {
        $paymentSelected = Payment::find($payment->get('payment_id'));

        /**
         * find and return the path of module
         */
        $paymentType = $this->PaymentClass($paymentSelected);

        $getModule = \Module::find($paymentType);
        $aName = (string)$getModule->getName();
        $payment = "\Modules\\$aName\Entities\\$aName";

        $payment = new $payment;
        return $payment->info();
    }

    /**
     * find payment class
     * @param $paymentSelected
     * @return string
     */
    protected function PaymentClass($paymentSelected)
    {
        $paymentType = \Module::find($paymentSelected->class);
        //        $paymentType = "App\\Extensions\\" . $paymentSelected->class;

        return $paymentType;
    }

    //@todo delete method
    protected function checkPayment($request)
    {
        /**
         * get payment
         */
        $paymentSelected = Payment::find($request->payment_id);

        /**
         * find and return the path of module
         */
        $paymentType = $this->PaymentClass($paymentSelected);

        $getModule = \Module::find($paymentType);
        ($aName = (string)$getModule->getName());
        $payment = "\Modules\\$aName\Entities\\$aName";

        return $payment = new $payment;
    }

    /**
     * @param CheckoutRequest $request
     */
    protected function checkAuthAndCreateOrder($request)
    {


        /**
         * if user is signed in
         */
        if (Auth::check()) {
            // return 'user loged in ';
            $user = Auth::user();
        } elseif (request()->createAccount == 1) {
            $validatedData = request()->validate([
                'password' => 'required|confirmed',
                'email' => 'required|email',
            ]);
            $user = User::where('email', $request['user']['email'])->first();
            $user->password = Hash::make(request()->password);
            $user->subscribed = 1;
            $user->newsletter =$request['user']['newsletter'];
            $user->save();


            DB::beginTransaction();


            try {
                $user = User::where('email', $request['user']['email'])->first();

                if (!$user) {
                    $user = User::create($request['user']);
                    $user->password = Hash::make(request()->password);
                    // $user->subscribed = 1;
                    $user->save();
                    $user->address()->create($request['address']);
                    // $user = User::create(['email' => request()->email, 'password' => Hash::make(request()->password)]);
                    // $user = User::create($request['user']);
                    // $user->address()->create($request['address']);
                } elseif ($user && $user->subscribed == 0) {
                    $user->password = Hash::make(request()->password);
                    $user->subscribed = 1;
                    $user->newsletter =$request['user']['newsletter'];

                    $user->save();
                    $user->address()->create($request['address']);
                    DB::commit();
                }

                // $customer = $user->customer()->create(request()->all());
                // $customer->address()->create($request->address);
            } catch (ValidationException $e) {
                // DB::rollback();
                throw $e;
            }

            // $order = (new OrderController)->store($request);
        } else {

            $user = User::where('email', $request['user']['email'])->first();
          
        }
        if ($user) {
            $order = $user->orders()->create($request['order']);
        } else {
            $order = Order::create($request['order']);
        }

        if ($request['comment'] != null) {
            //@todo make role and name dynamic
            $order->comments()->create(['user_role' => 'user', 'user_name' => $request['detail']['billing_name'], 'comment' => $request['comment']]);
        }

        //        $request['details']['user_id'] = $user->id;

        $detail = $order->detail()->create($request['detail']);
        foreach ($request['products'] as $key => $rp) {
            $request['products'][$key]['order_id'] = $order->id;
        }

        $detail->products()->createMany($request['products']);

        foreach (\Cart::getConditions() as $condition) {
            $voucher = Voucher::where('code', $condition->getName())->first();
            $detail->vouchers()->create(
                [
                    'voucher_id' => $voucher->id,
                    'name' => $voucher->name,
                    'type' => 'voucher',
                    'value' => $voucher->discount_amount,
                    'is_fixed' => $voucher->is_fixed
                ]
            );
            $voucher->uses++;
            $voucher->save();
        }

        return $order;
    }

    /*
             * Products in Cart
    */
    public function cartItems()
    {
        $items = [];

        \Cart::getContent()->each(function ($item) use (&$items) {
            $items[] = $item;
        });
        return $items;
    }

    /*
             * Cart Details
             * total
             *
    */
    public function cartDetails()
    {
        $cartConditions = \Cart::getConditions();
        $conditions = [];
        foreach ($cartConditions as $key => $condition) {
            $conditions[$key]['target'] = $condition->getTarget(); // the target of which the condition was applied
            $conditions[$key]['name'] = $condition->getName(); // the name of the condition
            $conditions[$key]['type'] = $condition->getType(); // the type
            $conditions[$key]['value'] = $condition->getValue(); // the value of the condition
            $conditions[$key]['order'] = $condition->getOrder(); // the order of the condition
            $conditions[$key]['attributes'] = $condition->getAttributes(); // the attributes of the condition, returns an empty [] if no attributes added
        }

        return [
            'total_quantity' => \Cart::getTotalQuantity(),
            'sub_total' => \Cart::getSubTotal(),
            'total' => \Cart::getTotal(),
            'conditions' => $conditions
        ];
    }

    /*
             * add product in cart
    */
    public function add(Request $request)
    {

        //clear previews cart items
        //        \Cart::clearCartConditions();

        switch ($request->get('type')) {
            case 'simple':
                return $this->addSimple($request);
                break;
            case 'configurable':
                return $this->addConfigurable($request);
                break;
            case 'dynamic':
                return $this->addDynamic($request);
                break;
        }

        $stock = Stock::find($request->get('stock'));

        if (is_null($stock)) {
            abort(404); //@todo return custom error
        }

        $product = $stock->product;
        $saleCondition = null;

        // return $stock->variations;
        $attributes = [];

        foreach ($stock->variations as $sv) {
            $name = Field::find($sv->field_id)->name;
            $field = $sv->model_type;
            $value = $field::find($sv->model_id)->title;
            $attributes[] = [
                $name => $value,
                'img_url' => $product->getFirstMediaUrl('product'),
                'title' => $product->titld
            ];
        }
        /**
         * Assign products to cart
         */
        $product = [
            'id' => $stock->id,
            'name' => $product->title,
            'price' => $product->price,
            'quantity' => ($request->get('quantity') ? $request->get('quantity') : 1),
            'product_id' => $stock->product->id,
            'product_sku' => $product->sku,
            'attributes' =>
            // [
            $attributes,
            // 'associated_id' => $stock->id,
            // 'size' => $stock->value->title,
            // // 'color' => $associated->product->color->title,
            // 'img_url' => $product->getFirstMediaUrl('product'),
            // ],
            'conditions' => $saleCondition,
        ];

        \Cart::add($product);
        \Session::flash('message', 'Το Προϊόν Προστέθηκε στο Καλάθι σας.');

        return redirect()->back();
    }

    public function addConfigurable($request)
    {

        $stock = Stock::findOrFail($request->get('stock'));
        $product = $stock->product;

        $price = $product->price;
        $product = [
            'sku' => $stock->sku,
            'id' => $stock->id,
            'name' => $product->title,
            'price' => $price,
            'quantity' => ($request->get('quantity') ? $request->get('quantity') : 1),
            'product_id' => $product->id,
            'attributes' => [
                'img_url' => $product->getFirstMediaUrl(),
                'sku' => $stock->sku,
                'product' => $product->id,
                'product_sku' => $product->sku,
                'product_url' => $product->name,
            ],
            // 'conditions' => $saleCondition,
        ];
        \Cart::add($product);
        // show modal for cart redirect
        $request->session()->flash('modal', true);
        \Session::flash('message', 'Το Προϊόν Προστέθηκε στο Καλάθι σας.');

        return redirect()->back();
    }

    public function addSimple($request)
    {
        $product = Product::where('id', $request->get('product'))->first();
        $price = $product->price;
        $product = [
            'id' => $product->id,
            'name' => $product->name,
            'price' => $price,
            'quantity' => ($request->get('quantity') ? $request->get('quantity') : 1),
            'product_id' => $product->id,
            'attributes' => [
                'img_url' => $product->getFirstMediaUrl('product'),
            ],
            // 'conditions' => $saleCondition,
        ];

        \Cart::add($product);
        return redirect()->route('cart');
    }

    public function dynamic_price(Request $request)
    {

        $product = \App\Product::find($request->get('product'));
        $width = $request->get('width');
        $depth = $request->get('depth');

        $fields = [
            'width' => $request->get('width'),
            'depth' => $request->get('depth'),
        ];

        $price = $product->price;
        $query = json_decode($product->calculator->query, true);

        for ($a = 0; $a < count($query['children']); $a++) {
            echo '-' . $a . "\n";
            $item = $query['children'][$a]['query']['children'];

            echo '-- ' . $item[0]['query']['rule'] . "\n";
            if (isset($fields[$item[0]['query']['rule']])) {

                if ($fields[$item[0]['query']['rule']] >= $item[0]['query']['value']) {
                    echo $item[1]['query']['value'] . "\n";
                    if ($fields[$item[0]['query']['rule']] <= $item[1]['query']['value']) {
                        continue;
                    }

                    //                    echo $item[2]['query']['value']. ' ';
                    $price += $item[2]['query']['value'];
                    //                     echo $price;
                    //                    echo 'sdf '. $item[2]['query']['value'];
                }
            }
        }
        return $price;
    }

    public function addDynamic($request)
    {
        \Cart::clearCartConditions();

        $product = Product::where('id', $request->get('product'))->first();
        if (is_null($product)) {
            abort(404); //@todo return custom error
        }

        $saleCondition = null;

        $width = $request->get('width');
        $depth = $request->get('depth');

        $fields = [
            'width' => $request->get('width'),
            'depth' => $request->get('depth'),
        ];

        $price = $product->price;
        $query = json_decode($product->calculator->query, true);

        for ($a = 0; $a < count($query['children']); $a++) {
            $item = $query['children'][$a]['query']['children'];
            if (isset($fields[$item[0]['query']['rule']])) {

                if ($fields[$item[0]['query']['rule']] >= $item[0]['query']['value'] && $fields[$item[0]['query']['rule']] <= $item[1]['query']['value']) {
                    $price += $item[2]['query']['value'];
                }
            }
        }

        /**
         * Assign products to cart
         */
        $product = [
            'id' => $product->id,
            'name' => $product->name,
            'price' => $price,
            'quantity' => ($request->get('quantity') ? $request->get('quantity') : 1),
            'product_id' => $product->id,
            'attributes' => [
                'width' => $width,
                'size' => $depth,
                'img_url' => $product->getFirstMediaUrl('product'),
            ],
            // 'conditions' => $saleCondition,
        ];

        \Cart::add($product);
        return redirect()->route('cart');
    }

    public function voucher_remove($name)
    {
        \Cart::removeCartCondition($name);
        return back();
    }


    //    function add_voucher(Request $request)
    function check_for_voucher(Request $request)
    {
        \Cart::clearCartConditions();
        $productWithSpecialPriceAmmound = 0;
        $productWithDiscounts = false;
        $checkCartTotal = false;
        $productWithSpecialPrice = [];
        $VoucherAConditionChecker = [];
        //        $VoucherAConditionChecker['totalCart'] = false;
        $request->validate(['code:required']);
        $voucher = Voucher::where('code', $request->code)->first();

        if (!$voucher) {
            $request->session()->flash('message', 'Δέν βρέθηκε ο εκπτωτικός κωδικός!');
            if ($request->has('redirect')) {
                return back();
            } else {
                return ['status' => true, 'message' => 'Το κουπόνι καταχωρήθηκε'];
            }
        }
        //        if ($voucher->expires_at && $voucher->expires_at <= Carbon::now()) {
        //            $VoucherAConditionChecker['voucher_expired'] = false;
        //            $request->session()->flash('message', 'O εκπτωτικός κωδικός έχει λύξει!');
        //            return 'O εκπτωτικός κωδικός έχει λύξει';
        //        }

        if ($voucher->query) {
            $query = json_decode($voucher->query, true);
            $cartProducts = \Cart::getContent();
            foreach ($cartProducts as $p) {

                //                \Cart::clearItemConditions($p['id']);

                $product = Product::where('id', $p['attributes']['product'])->get();
                //                if ($product->special_price > 0 || $product->rule_id > 0) {
                //                    $productWithSpecialPrice[$product->id] = true;
                //                } else {
                //
                //                    $productWithSpecialPriceAmmound += $product->price;
                //                    $productWithSpecialPrice[$product->id] = false;
                //                }

                foreach ($query as $q) {


                    $validProducts = AppHelper::querymanager($q, $product);
                    if ($validProducts) {
                        $VoucherAConditionChecker[$q['label']] = true;
                    } else {
                        $VoucherAConditionChecker[$q['false']] = false;
                    }
                }

                $voucherPassQueryChecker = !in_array(false, $VoucherAConditionChecker, true);
                if (!isset($VoucherAConditionChecker['totalCart']) || $VoucherAConditionChecker['totalCart'] == false) {
                    if ($voucherPassQueryChecker) {
                        //                        $this->applyVoucher($voucher);

                        $productID = $p['id'];
                        $value = ($voucher->is_fixed == 1) ? "-" . $voucher->discount_amount : "-" . $voucher->discount_amount . '%';

                        //                        $coupon = new \Darryldecode\Cart\CartCondition(array(
                        //                            'name' => 'sadf',
                        //                            'type' => 'coupon',
                        //                            'value' => '-10',
                        //                        ));
                        //
                        //                        \Cart::addItemCondition($p['id'], $coupon);

                        $condition = new \Darryldecode\Cart\CartCondition(array(
                            'name' => $voucher->name,
                            'type' => 'coupon',
                            'target' => 'total', // this condition will be applied to cart's total when getTotal() is called.
                            'value' => $value,
                            'order' => 1 // the order of calculation of cart base conditions. The bigger the later to be applied.
                        ));
                        \Cart::condition($condition);
                        //                        return \Cart::getConditions();

                        //                        return $this->cartDetails();

                        //                        return \Cart::getConditionsByType('coupon');

                        $request->session()->flash('flash_message', 'Έγινε εφαρμογή του Eκπτωτικού κουπονιού!');
                    }
                }
            }
            if ($request->has('redirect')) {
                return back();
            } else {
                return ['status' => true, 'message' => 'Το κουπόνι καταχωρήθηκε'];
            }
        }

        return back();
    }

    public function applyVoucher(Voucher $voucher)
    {
        $condition = new \Darryldecode\Cart\CartCondition([
            'name' => $voucher->code,
            'type' => 'ΚΟΥΠΟΝΙ', // sale , promo , misk ,
            'target' => 'total', // this condition will be applied to cart's subtotal when getSubTotal() is called.
            'value' => ($voucher->is_fixed == 1) ? "-" . $voucher->discount_amount : "-" . $voucher->discount_amount . '%',
            'attributes' => [ // attributes field is optional
                'description' => $voucher->description,
            ],
        ]);
        \Cart::condition($condition);
    }

    /*
             * delete product in cart
    */
    public
    function delete($id)
    {
        \Cart::clearCartConditions();
        \Cart::remove($id);
        if (request()->ajax()) {
            return 'success';
        }
        return back();
        return response([
            'success' => true,
            'data' => $id,
            'message' => "cart item {$id} removed.",
        ], 200, []);
    }
}
