<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderCollection;
use App\Models\Order;
use App\Models\OrderDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\OrderExport;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx\Rels;

class OrderController extends Controller
{

    public function index()
    {

        return view('manage.orders.index');
    }

    public function listJson(Request $request)
    {


        $orders = Order::has('detail')->orderBy('id', 'DESC');
        if ($request->has('filters')) {
            foreach ($request->get('filters') as $f) {
                if ($f['selected'] != null) {
                    $orders->whereHas('detail', function ($q) use ($f) {
                        $q->where($f['name'], $f['selected']);
                    });
                }
            }
        }
        return new OrderCollection($orders->get());

        return $orders;


        return Order::with('detail', 'user')->orderBy('created_at', 'desc')->paginate($request->get('paginate'));
    }

    public function create()
    {

        return view('manage.orders.create');
    }

    public function filter()
    {

        return view('manage.orders.filter');
    }

    public function replicate(Request $request)
    {

        $order = $request->get('order');
        $order = Order::findOrFail($request->get('order_id'));

        $newOrderNumber = $request->get('orderNumber');
        $detail = OrderDetail::find($order->detail->id);

        $newOrder = $order->replicate()->fill([
            'order_num' => $newOrderNumber
        ]);;


        $newOrder->save();

        $newdetail = $detail->replicate()->fill([
            'order_id' => $newOrder->id,
            'created_at' => Carbon::now()
        ]);
        $newdetail->save();

        $detail->status = 'canceled';
        $detail->save();
        return $newOrder;
    }

    public function edit(Order $order)
    {

        $detail = $order->detail;
        $products = $detail->products;
        $vouchers = $detail->vouchers;
        $comments = $order->comments;

        //			$order = Order::with('detail', 'user', 'detail.products')->findOrFail($order)->toArray();
        return view('manage.orders.edit', compact('order'));
    }

    public function show(Order $order)
    {
        $order->opened = 1;
        $order->save();

        $detail = $order->detail;
        $comments = $order->comments;
        $products = $detail->products;
        //			$order = Order::with('detail', 'user', 'detail.products')->findOrFail($order)->toArray();
        return view('manage.orders.show', compact('order'));
    }

    /**
     * Προσθήκη Σχολίου διαχειρστή κατά την επεξεργασία της παραγγελίας.
     * @param Order $order
     * @param Request $request
     * @return void
     */
    public function addComment(Order $order, Request $request)
    {

        $order->comments()->create([
            'user_role' => 'admin',
            'user_name' => Auth::User()->lastname,
            'comment' => $request->get('message'),
        ]);
    }

    public function update(Order $order, Request $request)
    {

        $details = $request->get('detail');


        //			unset($details['created_at']);
        unset($details['updated_at']);

        $detail = OrderDetail::find($details['id']);

        $detail->update($details);

        $detail->products()->delete();
        $detail->products()->createMany($details['products']);

        $detail->vouchers()->delete();
        $detail->vouchers()->createMany($details['vouchers']);
        return $request->input();
    }


    public function filterQuery(Request $request)
    {
        $request = ['shipping_id'];

        $e = Order::query();
        $e = $e->with('detail', 'products', 'user')
            ->when(true, function ($query) {
                $query->whereHas('shipping_id', function ($q) {
                    $q->where('id', '=', 7);
                });
            })
            ->get();

        return $e;
    }


    public function store_new_order(Request $request)
    {

        $validatedData = \Validator::make($request->get('detail'), [
            // 'email' => 'required|email',
            'billing_city' => 'required',
            'billing_lastname' => 'required',
            'billing_mobile' => 'required',
            'billing_name' => 'required',
            'billing_street' => 'required',
            'billing_zip' => 'required',
            // 'selectedCountry.id' => 'required',
            // 'selectedCounty.id' => 'required',
            // 'shipping.id' => 'required'
        ]);
        if ($validatedData->fails()) {
            return  response()->json(['error' => $validatedData->errors()]);
            return $validatedData->withInput();
        }

        $invoice_id = Order::count() + 1;

        // return $request->input();
        $order = new Order();
        $order->order_num = $invoice_id;
        $order->user_id = $request->get('user_id');
        // $order->shipping_id = $request->get('shipping_id');
        $order->shipping_id = $request->get('shipping_id');
        $order->tracking_number = $request->get('tracking_number');
        // $order->payment_id = $request->get('payment_id');
        $order->save();
        $details = $request->get('detail');

        $detail = $order->detail()->create($details);

        $detail->products()->createMany($details['products']);
        return $order;
    }



    public function unopendOrders()
    {
        return Order::where('opened', 0)->count();
    }

    /**
     * @return void
     * export to excel
     */
    public function export(Request $request)
    {

        $filters = $request->input();

        return Excel::download(new OrderExport($filters), 'invoices.xlsx');

        return (new OrderExport($filters))->download('orders-' . today() . '.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }


    public function destroy(Order $order)
    {
        $order->delete();
    }
}
