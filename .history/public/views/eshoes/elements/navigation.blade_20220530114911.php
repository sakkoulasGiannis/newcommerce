<!--<div class="hidden bg-purple-50 md:block">-->
<div class="hidden bg-purple-50 md:block">
    <ul class="flex justify-center bg-white">
        <!--Hoverable Link-->
        <li class="hoverable hover:bg-teal-700">

            <a href="/new-products"
                class="text-es-blue relative block py-6 px-4 text-sm hover:bg-teal-700 lg:p-6 lg:text-base">ΝΕΕΣ
                ΠΑΡΑΛΑΒΕΣ</a>
            <div class="mega-menu z-50 mb-16 bg-teal-700 bg-white p-6 shadow-xl sm:mb-0">
                <div class="container mx-auto flex w-full flex-wrap justify-between">
                    {{-- <div class="w-full  mb-8 text-center"> --}}
                        {{-- <h2 class="font-bold text-es-blue text-2xl">Κάποιο μήνυμα εδώ αν θέλουμε.</h2> --}}
                        {{-- <p class="text-es-blue">our droids. They’ll have to wait outside. We don’t want them here.
                        </p> --}}
                        {{-- </div> --}}
                    <div class="mb-8 w-full text-center">
                        <a href="#" class="border p-2 hover:bg-blue-700 hover:text-gray-200">22</a>
                        <a href="#" class="border p-2 hover:bg-blue-700 hover:text-gray-200">23</a>
                        <a href="#" class="border p-2 hover:bg-blue-700 hover:text-gray-200">24</a>
                        <a href="#" class="border p-2 hover:bg-blue-700 hover:text-gray-200">25</a>
                        <a href="#" class="border p-2 hover:bg-blue-700 hover:text-gray-200">26</a>
                        <a href="#" class="border p-2 hover:bg-blue-700 hover:text-gray-200">27</a>
                        <a href="#" class="border p-2 hover:bg-blue-700 hover:text-gray-200">28</a>
                        <a href="#" class="border p-2 hover:bg-blue-700 hover:text-gray-200">29</a>
                        <a href="#" class="border p-2 hover:bg-blue-700 hover:text-gray-200">30</a>
                        <a href="#" class="border p-2 hover:bg-blue-700 hover:text-gray-200">31</a>
                        <a href="#" class="border p-2 hover:bg-blue-700 hover:text-gray-200">32</a>
                        <a href="#" class="border p-2 hover:bg-blue-700 hover:text-gray-200">33</a>
                        <a href="#" class="border p-2 hover:bg-blue-700 hover:text-gray-200">34</a>
                        <a href="#" class="border p-2 hover:bg-blue-700 hover:text-gray-200">35</a>
                        <a href="#" class="border p-2 hover:bg-blue-700 hover:text-gray-200">36</a>
                        <a href="#" class="border p-2 hover:bg-blue-700 hover:text-gray-200">37</a>
                        <a href="#" class="border p-2 hover:bg-blue-700 hover:text-gray-200">38</a>
                        <a href="#" class="border p-2 hover:bg-blue-700 hover:text-gray-200">39</a>
                        <a href="#" class="border p-2 hover:bg-blue-700 hover:text-gray-200">40</a>
                        <a href="#" class="border p-2 hover:bg-blue-700 hover:text-gray-200">41</a>
                        <a href="#" class="border p-2 hover:bg-blue-700 hover:text-gray-200">42</a>
                        <a href="#" class="border p-2 hover:bg-blue-700 hover:text-gray-200">43</a>
                        <a href="#" class="border p-2 hover:bg-blue-700 hover:text-gray-200">44</a>
                        <a href="#" class="border p-2 hover:bg-blue-700 hover:text-gray-200">46</a>
                        <a href="#" class="border p-2 hover:bg-blue-700 hover:text-gray-200">47</a>
                        <a href="#" class="border p-2 hover:bg-blue-700 hover:text-gray-200">48</a>
                    </div>

                    <ul
                        class="w-full border-b border-gray-600 px-4 pb-6 pt-6 sm:w-1/2 sm:border-r-0 lg:w-1/4 lg:border-r lg:border-b-0 lg:pt-3">
                        <div class="flex items-center">
                            {{-- <i class="fas fa-female fa-2x pr-2"></i> --}}
                            <a href="/new-products/nea-gynaikeia"
                                class="text-bold text-es-blue mb-2 text-xl font-bold">ΓΥΝΑΙΚΕΙΑ</a>
                        </div>
                        {{-- <p class=" text-sm">Thul klivian doldur thisspiasian calrissian. Garindan d8 aurra twi'lek
                            tund --}}
                            {{-- polis gen'dai sola tarpals.</p> --}}
                        @include('eshoes.elements.list', [
                        'parentid' => 651,
                        'append' => '/new-products',
                        ])

                    </ul>
                    <ul
                        class="w-full border-b border-gray-600 px-4 pb-6 pt-6 sm:w-1/2 sm:border-b-0 sm:border-r md:border-b-0 lg:w-1/4 lg:pt-3">
                        <div class="flex items-center">
                            {{-- <i class="fas fa-male fa-2x pr-2"></i> --}}
                            <a href="/new-products/nea-andrika"
                                class="text-bold text-es-blue mb-2 text-xl font-bold">ΑΝΔΡΙΚΑ</a>
                        </div>
                        {{-- <p class="text-sm">Thul klivian doldur thisspiasian calrissian. Garindan d8 aurra twi'lek
                            tund polis gen'dai sola tarpals.</p> --}}
                        <div class="flex items-center py-3">
                            @include('eshoes.elements.list', [
                            'parentid' => 672,
                            'append' => '/new-products',
                            ])


                        </div>
                    </ul>
                    <ul class="w-full border-gray-600 px-4 pb-6 pt-6 sm:w-1/2 lg:w-1/4 lg:pt-3">
                        <div class="flex items-center">
                            {{-- <i class="fas fa-child fa-2x pr-2"></i> --}}
                            <a href="/new-products/nea-paidika"
                                class="text-bold text-es-blue mb-2 text-xl font-bold">ΠΑΙΔΙΚΑ</a>
                        </div>
                        {{-- <p class="text-sm">Thul klivian doldur thisspiasian calrissian. Garindan d8 aurra twi'lek
                            tund polis gen'dai sola tarpals.</p> --}}
                        <div class="flex items-center py-3">
                            @include('eshoes.elements.list', [
                            'parentid' => 684,
                            'append' => '/new-products',
                            ])

                        </div>
                    </ul>
                    <ul class="w-full border-gray-600 px-4 pb-6 pt-6 sm:w-1/2 lg:w-1/4 lg:pt-3">
                        <div class="flex items-center">

                        </div>

                        <div class="flex items-center py-3">
                            <ul>
                                <li class="menu-item mb-4" style="list-style: none;"><a class="level2 text-es-blue mb-4"
                                        href="/">
                                        <img src="/views/eshoes/assets/img/tsantes-501x292_1.jpg"></a>
                                </li>
                                <li class="menu-item" style="list-style: none;"><a class="level2 text-es-blue" href="/">
                                        <img src="/views/eshoes/assets/img/tsantes-501x292_1.jpg"></a>
                                </li>
                            </ul>
                        </div>
                    </ul>
                </div>
            </div>
        </li>
        <li class="hoverable hover:bg-teal-700">
            <a href="/gynaikeia-papoutsia"
                class="text-es-blue relative block py-6 px-4 text-sm hover:bg-teal-700 lg:p-6 lg:text-base">ΓΥΝΑΙΚΕΙΑ</a>
            <div class="mega-menu z-50 mb-16 bg-teal-700 bg-white p-6 shadow-xl sm:mb-0">
                <div class="container mx-auto flex w-full flex-wrap justify-between">
                    {{-- <div class="w-full  mb-8 text-center"> --}}
                        {{-- <h2 class="font-bold text-2xl">Κάποιο μήνυμα εδώ αν θέλουμε.</h2> --}}
                        {{-- <p>our droids. They’ll have to wait outside. We don’t want them here.</p> --}}
                        {{-- </div> --}}
                    <ul
                        class="w-full border-b border-gray-600 px-4 pb-6 pt-6 sm:w-1/2 sm:border-r lg:w-1/4 lg:border-b-0 lg:pt-3">
                        <div class="flex items-center">
                            {{-- <i class="fas fa-female fa-2x pr-2"></i> --}}
                            <a href="/gynaikeia-papoutsia">
                                <h3 class="text-bold text-es-blue mb-2 text-base font-bold">ΓΥΝΑΙΚΕΙΑ ΠΑΠΟΥΤΣΙΑ</h3>
                            </a>
                        </div>
                        @include('eshoes.elements.list', ['parentid' => 592])
                    </ul>
                    <ul
                        class="w-full border-b border-gray-600 px-4 pb-6 pt-6 sm:w-1/2 sm:border-r-0 lg:w-1/4 lg:border-r lg:border-b-0 lg:pt-3">
                        <div class="flex items-center">
                            {{-- <i class="fas fa-female fa-2x pr-2"></i> --}}
                            <h2 class="text-bold text-es-blue mb-2 text-base font-bold">ΑΞΕΣΟΥΑΡ</h2>
                        </div>
                        {{-- <p class=" text-sm">Thul klivian doldur thisspiasian calrissian. Garindan d8 aurra twi'lek
                            tund --}}
                            {{-- polis gen'dai sola tarpals.</p> --}}
                        <div class="flex items-center py-3">

                            @include('eshoes.elements.list', ['parentid' => 749])

                        </div>
                    </ul>
                    <ul
                        class="w-full border-b border-gray-600 px-4 pb-6 pt-6 sm:w-1/2 sm:border-b-0 sm:border-r md:border-b-0 lg:w-1/4 lg:pt-3">
                        <div class="flex items-center">
                            <h3 class="text-bold text-es-blue mb-2 text-base font-bold">TOP BRANDS</h3>
                        </div>
                        {{-- <p class=" text-sm">Thul klivian doldur thisspiasian calrissian. Garindan d8 aurra twi'lek
                            tund --}}
                            {{-- polis gen'dai sola tarpals.</p> --}}
                        <div class="flex items-center py-3">
                            <ul>
                                <li class="menu-item" style="list-style: none;">
                                    <a class="level2 text-es-blue" href="/brands"><span>ΟΛΑ ΤΑ BRANDS</span></a>
                                </li>
                                <li class="menu-item" style="list-style: none;">
                                    <a class="level2 text-es-blue" href="/brands/tamaris"><span>Tamaris</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/sante"><span>Sante</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/levis"><span>Levis</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/wrangler"><span>Weangler</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/guess"><span>Guess</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/valentino"><span>Valentino</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/dknu"><span>DKNY</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/trussardi"><span>Trussardi</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/mille-luci"><span>Mille
                                            Luci</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/caprice"><span>Caprice</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/marco-tozzi"><span>Marco
                                            Tozzi</span></a>
                                </li>

                                <li><a class="level2 text-es-blue" href="/brands/xti"><span>Xti</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/ragazza"><span>Ragazza</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/crocs"><span>Crocs</span></a>
                                </li>

                            </ul>
                        </div>
                    </ul>
                    <ul class="w-full border-gray-600 px-4 pb-6 pt-6 sm:w-1/2 lg:w-1/4 lg:pt-3">
                        <div class="flex items-center">

                        </div>

                        <div class="flex items-center py-3">
                            <ul>
                                <li class="menu-item mb-4" style="list-style: none;"><a class="level2 text-es-blue mb-4"
                                        href="/">
                                        <img src="/views/eshoes/assets/img/tsantes-501x292_1.jpg"></a>
                                </li>
                                <li class="menu-item" style="list-style: none;"><a class="level2 text-es-blue" href="/">
                                        <img src="/views/eshoes/assets/img/tsantes-501x292_1.jpg"></a>
                                </li>
                            </ul>
                        </div>
                    </ul>
                </div>
            </div>
        </li>
        <li class="hoverable hover:bg-teal-700">
            <a href="/andrika-papoutsia"
                class="text-es-blue relative block py-6 px-4 text-sm hover:bg-teal-700 lg:p-6 lg:text-base">ΑΝΔΡΙΚΑ</a>
            <div class="mega-menu z-50 mb-16 bg-teal-700 bg-white p-6 shadow-xl sm:mb-0">
                <div class="container mx-auto flex w-full flex-wrap justify-between">
                    {{-- <div class="w-full  mb-8 text-center"> --}}
                        {{-- <h2 class="font-bold text-2xl">Κάποιο μήνυμα εδώ αν θέλουμε.</h2> --}}
                        {{-- <p>our droids. They’ll have to wait outside. We don’t want them here.</p> --}}
                        {{-- </div> --}}
                    <ul
                        class="w-full border-b border-gray-600 px-4 pb-6 pt-6 sm:w-1/2 sm:border-r lg:w-1/4 lg:border-b-0 lg:pt-3">
                        <div class="flex items-center">
                            {{-- <i class="fas fa-female fa-2x pr-2"></i> --}}
                            <a href="/gynaikeia-papoutsia">
                                <h3 class="text-bold text-es-blue mb-2 text-base font-bold">ΑΝΔΡΙΚΑ ΠΑΠΟΥΤΣΙΑ</h3>
                            </a>
                        </div>
                        @include('eshoes.elements.list', ['parentid' => 613])
                    </ul>
                    <ul
                        class="w-full border-b border-gray-600 px-4 pb-6 pt-6 sm:w-1/2 sm:border-r-0 lg:w-1/4 lg:border-r lg:border-b-0 lg:pt-3">
                        <div class="flex items-center">
                            {{-- <i class="fas fa-female fa-2x pr-2"></i> --}}
                            <h2 class="text-bold text-es-blue mb-2 text-base font-bold">ΑΞΕΣΟΥΑΡ</h2>
                        </div>
                        {{-- <p class=" text-sm">Thul klivian doldur thisspiasian calrissian. Garindan d8 aurra twi'lek
                            tund --}}
                            {{-- polis gen'dai sola tarpals.</p> --}}
                        <div class="flex items-center py-3">

                            Θα ενημερωθούν μόλις μου στείλει ο παντελής την κατηγορία
                        </div>
                    </ul>
                    <ul
                        class="w-full border-b border-gray-600 px-4 pb-6 pt-6 sm:w-1/2 sm:border-b-0 sm:border-r md:border-b-0 lg:w-1/4 lg:pt-3">
                        <div class="flex items-center">
                            <h3 class="text-bold text-es-blue mb-2 text-base font-bold">TOP BRANDS</h3>
                        </div>
                        {{-- <p class=" text-sm">Thul klivian doldur thisspiasian calrissian. Garindan d8 aurra twi'lek
                            tund --}}
                            {{-- polis gen'dai sola tarpals.</p> --}}
                        <div class="flex items-center py-3">
                            <ul>

                                <li class="menu-item" style="list-style: none;">
                                    <a class="level2 text-es-blue" href="/brands/tamaris"><span>Tamaris</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/sante"><span>Sante</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/levis"><span>Levis</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/wrangler"><span>Weangler</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/guess"><span>Guess</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/valentino"><span>Valentino</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/dknu"><span>DKNY</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/trussardi"><span>Trussardi</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/mille-luci"><span>Mille
                                            Luci</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/caprice"><span>Caprice</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/marco-tozzi"><span>Marco
                                            Tozzi</span></a>
                                </li>

                                <li><a class="level2 text-es-blue" href="/brands/xti"><span>Xti</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/ragazza"><span>Ragazza</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/crocs"><span>Crocs</span></a>
                                </li>

                            </ul>
                        </div>
                    </ul>
                    <ul class="w-full border-gray-600 px-4 pb-6 pt-6 sm:w-1/2 lg:w-1/4 lg:pt-3">
                        <div class="flex items-center">

                        </div>

                        <div class="flex items-center py-3">
                            <ul>
                                <li class="menu-item mb-4" style="list-style: none;"><a class="level2 text-es-blue mb-4"
                                        href="/">
                                        <img src="/views/eshoes/assets/img/tsantes-501x292_1.jpg"></a>
                                </li>
                                <li class="menu-item" style="list-style: none;"><a class="level2 text-es-blue" href="/">
                                        <img src="/views/eshoes/assets/img/tsantes-501x292_1.jpg"></a>
                                </li>
                            </ul>
                        </div>
                    </ul>
                </div>
            </div>
        </li>
        <li class="hoverable hover:bg-teal-700">
            <a href="/paidika"
                class="text-es-blue relative block py-6 px-4 text-sm hover:bg-teal-700 lg:p-6 lg:text-base">ΠΑΙΔΙ</a>
            <div class="mega-menu z-50 mb-16 bg-teal-700 bg-white p-6 shadow-xl sm:mb-0">
                <div class="container mx-auto flex w-full flex-wrap justify-between">
                    {{-- <div class="w-full  mb-8 text-center"> --}}
                        {{-- <h2 class="font-bold text-2xl">Κάποιο μήνυμα εδώ αν θέλουμε.</h2> --}}
                        {{-- <p>our droids. They’ll have to wait outside. We don’t want them here.</p> --}}
                        {{-- </div> --}}
                    <ul
                        class="w-full border-b border-gray-600 px-4 pb-6 pt-6 sm:w-1/2 sm:border-r lg:w-1/4 lg:border-b-0 lg:pt-3">
                        <div class="flex items-center">
                            <a href="/paidika/paidika-papoytsia-278">
                                <h3 class="text-es-blue text-bold mb-2 text-base font-bold">ΠΑΙΔΙΚΑ ΠΑΠΟΥΤΣΙΑ</h3>
                            </a>
                        </div>
                        @include('eshoes.elements.list', [
                        'parentid' => 637,
                        'append' => '/paidika',
                        ])
                    </ul>
                    <ul
                        class="w-full border-b border-gray-600 px-4 pb-6 pt-6 sm:w-1/2 sm:border-r-0 lg:w-1/4 lg:border-r lg:border-b-0 lg:pt-3">
                        <div class="flex items-center">

                            <h2 class="text-es-blue text-bold mb-2 text-base font-bold"><a
                                    href="/paidika/paidika-royxa-264">ΡΟΥΧΑ</a></h2>
                        </div>
                        <div class="flex items-center py-3">
                            @include('eshoes.elements.list', [
                            'parentid' => 626,
                            'append' => '/paidika',
                            ])
                        </div>
                    </ul>
                    <ul
                        class="w-full border-b border-gray-600 px-4 pb-6 pt-6 sm:w-1/2 sm:border-b-0 sm:border-r md:border-b-0 lg:w-1/4 lg:pt-3">
                        <div class="flex items-center">
                            <a class="text-es-blue text-bold mb-2 text-base font-bold">TOP BRANDS</a>
                        </div>
                        <div class="flex items-center py-3">
                            <ul>

                                <li class="menu-item" style="list-style: none;">
                                    <a class="level2 text-es-blue" href="/brands/mayoral"><span>Μayoral</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/michael-michael-kors"><span>Michael
                                            Kors</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/lelli-kelly"><span>Lelli
                                            Kelly</span></a></li>
                                <li><a class="level2 text-es-blue" href="/brands/primigi"><span>Primigi</span></a></li>
                                <li><a class="level2 text-es-blue" href="/brands/conguitos"><span>Conguitos</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/titanitos"><span>Titanitos</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/soliver"><span>S. Oliver</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/xti"><span>Xti</span></a></li>
                                <li><a class="level2 text-es-blue" href="/brands/asso"><span>Asso</span></a></li>
                                <li><a class="level2 text-es-blue" href="/brands/tuc-tuc"><span>Tuc Tuc</span></a></li>
                                <li><a class="level2 text-es-blue" href="/brands/emery"><span>Emery</span></a></li>
                                <li><a class="level2 text-es-blue" href="/brands/reflex"><span>Reflex</span></a></li>
                                <li><a class="level2 text-es-blue" href="/brands/crocks"><span>Crocs</span></a></li>
                            </ul>
                        </div>
                    </ul>
                    <ul class="w-full border-gray-600 px-4 pb-6 pt-6 sm:w-1/2 lg:w-1/4 lg:pt-3">
                        <div class="flex items-center">
                        </div>
                        <div class="flex items-center py-3">
                            <ul>
                                <li class="menu-item mb-4" style="list-style: none;">
                                    <a class="level2 text-es-blue mb-4" href="/">
                                        <img src="/views/eshoes/assets/img/tsantes-501x292_1.jpg"></a>
                                </li>
                                <li class="menu-item" style="list-style: none;">
                                    <a class="level2 text-es-blue" href="/">
                                        <img src="/views/eshoes/assets/img/tsantes-501x292_1.jpg"></a>
                                </li>
                            </ul>
                        </div>
                    </ul>
                </div>
            </div>
        </li>
        <li class="hoverable hover:bg-teal-700">
            <a href="/accessories"
                class="text-es-blue relative block py-6 px-4 text-sm hover:bg-teal-700 lg:p-6 lg:text-base">ΑΞΕΣΟΥΑΡ</a>
            <div class="mega-menu z-50 mb-16 bg-teal-700 bg-white p-6 shadow-xl sm:mb-0">
                <div class="container mx-auto flex w-full flex-wrap justify-between">
                    {{-- <div class="w-full  mb-8 text-center"> --}}
                        {{-- <h2 class="font-bold text-2xl">Κάποιο μήνυμα εδώ αν θέλουμε.</h2> --}}
                        {{-- <p>our droids. They’ll have to wait outside. We don’t want them here.</p> --}}
                        {{-- </div> --}}
                    <ul
                        class="w-full border-b border-gray-600 px-4 pb-6 pt-6 sm:w-1/2 sm:border-r lg:w-1/4 lg:border-b-0 lg:pt-3">
                        <div class="flex items-center">
                            <a href="/accessories">
                                <h3 class="text-bold text-es-blue mb-2 text-base font-bold">ΟΛΑ ΤΑ ΑΞΕΣΟΥΑΡ</h3>
                            </a>
                        </div>
                        @include('eshoes.elements.list', [
                        'parentid' => 647,
                        'append' => '',
                        ])
                    </ul>

                    <ul
                        class="w-full border-b border-gray-600 px-4 pb-6 pt-6 sm:w-1/2 sm:border-b-0 sm:border-r md:border-b-0 lg:w-1/4 lg:pt-3">
                        <div class="flex items-center">
                            <h3 class="text-bold text-es-blue mb-2 text-base font-bold">TOP BRANDS</h3>
                        </div>
                        <div class="flex items-center py-3">
                            <ul>

                                <li class="menu-item" style="list-style: none;"><a class="level2 text-es-blue"
                                        href="/brands/guess"><span>GUESS</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/valentino"><span>VALENTINO</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/dkny"><span>DKNY</span></a></li>
                                <li><a class="level2 text-es-blue" href="/brands/trussardi"><span>TRUSSARDI</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/ted-baker"><span>TED BAKER</span></a>
                                </li>
                                <li><a class="level2 text-es-blue" href="/brands/tamaris"><span>TAMARIS</span></a></li>
                                <li><a class="level2 text-es-blue" href="/brands/levis"><span>LEVIS</span></a></li>
                                <li><a class="level2 text-es-blue" href="/brands/kylie"><span>KYLIE</span></a></li>
                                <li><a class="level2 text-es-blue" href="/brands/mayoral"><span>MAYORAL</span></a></li>
                                <li><a class="level2 text-es-blue" href="/brands/crocks"><span>Crocs</span></a></li>
                            </ul>
                        </div>
                    </ul>
                    <ul class="w-full border-gray-600 px-4 pb-6 pt-6 sm:w-1/2 lg:w-1/4 lg:pt-3">
                        <div class="flex items-center">
                        </div>
                        <div class="flex items-center py-3">
                            <ul>
                                <li class="menu-item mb-4" style="list-style: none;">
                                    <a class="level2 text-es-blue mb-4" href="/">
                                        <img src="/views/eshoes/assets/img/tsantes-501x292_1.jpg"></a>
                                </li>
                                <li class="menu-item" style="list-style: none;">
                                    <a class="level2 text-es-blue" href="#">
                                        <img src="/views/eshoes/assets/img/tsantes-501x292_1.jpg"></a>
                                </li>
                            </ul>
                        </div>
                    </ul>
                </div>
            </div>
        </li>


        <li class="hover:bg-teal-700">
            <a href="/brands/tamaris" class="text-es-blue relative block py-6 px-2 text-sm lg:p-6 lg:text-base">TAMARIS
                STORE</a>
        </li>


        <li class="hoverable hover:bg-teal-700">
            <a href="/bazaar"
                class="text-es-blue relative block py-6 px-4 text-sm hover:bg-teal-700 lg:p-6 lg:text-base">ΠΡΟΣΦΟΡΕΣ</a>
            <div class="mega-menu z-50 mb-16 bg-teal-700 bg-white p-6 shadow-xl sm:mb-0">
                <div class="container mx-auto flex w-full flex-wrap justify-between">
                    {{-- <div class="w-full  mb-8 text-center"> --}}
                        {{-- <h2 class="font-bold text-2xl">Κάποιο μήνυμα εδώ αν θέλουμε.</h2> --}}
                        {{-- <p>our droids. They’ll have to wait outside. We don’t want them here.</p> --}}
                        {{-- </div> --}}
                    <ul
                        class="w-full border-b border-gray-600 px-4 pb-6 pt-6 sm:w-1/2 sm:border-r lg:w-1/4 lg:border-b-0 lg:pt-3">
                        <div class="flex items-center">
                            <a href="/bazaar/prosfores-gynaikeia">
                                <h3 class="text-bold text-es-blue mb-2 text-base font-bold">ΓΥΝΑΙΚΕΙΑ</h3>
                            </a>
                        </div>
                        @include('eshoes.elements.list', [
                        'parentid' => 702,
                        'append' => '/bazaar',
                        ])
                    </ul>
                    <ul
                        class="w-full border-b border-gray-600 px-4 pb-6 pt-6 sm:w-1/2 sm:border-r lg:w-1/4 lg:border-b-0 lg:pt-3">
                        <div class="flex items-center">
                            <a href="/bazaar/prosfores-andrika">
                                <h3 class="text-bold text-es-blue mb-2 text-base font-bold">ΑΝΔΡΙΚΑ</h3>
                            </a>
                        </div>
                        @include('eshoes.elements.list', [
                        'parentid' => 723,
                        'append' => '/bazaar',
                        ])
                    </ul>
                    <ul
                        class="w-full border-b border-gray-600 px-4 pb-6 pt-6 sm:w-1/2 sm:border-r lg:w-1/4 lg:border-b-0 lg:pt-3">
                        <div class="flex items-center">
                            <a href="/bazaar/prosfores-paidika">
                                <h3 class="text-bold text-es-blue mb-2 text-base font-bold">ΠΑΙΔΙΚΑ</h3>
                            </a>
                        </div>
                        @include('eshoes.elements.list', [
                        'parentid' => 735,
                        'append' => '/bazaar',
                        ])
                    </ul>

                </div>
            </div>
        </li>
        <li class="hoverable hover:bg-teal-700">
            <a href="/brands"
                class="text-es-blue relative block py-6 px-4 text-sm hover:bg-teal-700 lg:p-6 lg:text-base">BRANDS</a>
            <div class="mega-menu z-50 mb-16 bg-teal-700 bg-white p-6 shadow-xl sm:mb-0">
                <div class="container mx-auto flex w-full flex-wrap justify-between">
                    {{-- <div class="w-full  mb-8 text-center"> --}}
                        {{-- <h2 class="font-bold text-2xl">Κάποιο μήνυμα εδώ αν θέλουμε.</h2> --}}
                        {{-- <p>our droids. They’ll have to wait outside. We don’t want them here.</p> --}}
                        {{-- </div> --}}

                    <div class="grid grid-cols-1 md:grid-cols-12 md:gap-4">

                        @foreach ($allBrands as $key => $brand)
                        @if (count($brand) > 0 && $key != '')
                        <div class="p-1">

                            <h2 class="mt-1 border-b-2 border-gray-400 font-bold">{{ $key }}</h2>

                            @foreach ($brand as $key => $b)
                            @if ($key < 8) <div class="panel">
                                <a class="text-es-blue text-sm" href="/brands/{{ $b->name }}">{{ $b->title }}</a>
                        </div>
                        @endif
                        @endforeach
                    </div>
                    @endif
                    @endforeach
                </div>

            </div>
</div>
</li>


</ul>
</div>
<div class="bg-es-blue py-2 text-center font-medium text-white">
    <i class="fas fa-phone pr-2"></i> <a onclick="return gtag_report_conversion('tel:00302810228000');"
        href="tel:00302810228000">2810 228 000</a> | ΔΩΡΕΑΝ ΑΠΟΣΤΟΛΗ ΚΑΙ ΕΠΙΣΤΡΟΦΗ ΣΕ ΟΛΗ ΤΗΝ ΕΛΛΑΔΑ <i
        class="fas fa-plane pr-2"></i>
</div>
<!--mobile button expnd sidebar -->

<div class="searchbox hidden">
    {!! Form::open(['route' => ['front.search'], 'method' => 'get', 'class' => 'row']) !!}
    <div class="relative">
        <input name="q" value="{{ request()->get('q') ? request()->get('q') : null }}" type="text"
            class="z-0 h-14 w-full border-b pr-8 pl-5 focus:border-green-400 focus:shadow focus:outline-none md:w-96"
            placeholder="Αναζήτηση...">
        <div class="absolute top-4 right-0 pr-4">
            <button type="button">
                <i class="fa fa-search z-20 text-gray-400 hover:text-gray-500"></i>
            </button>
        </div>
    </div>
    {!! Form::close() !!}
</div>