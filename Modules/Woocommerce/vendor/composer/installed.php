<?php return array(
    'root' => array(
        'name' => 'nwidart/woocommerce',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => '3115dddebb1ca3bd567ae0b582883d0702da32b7',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'automattic/woocommerce' => array(
            'pretty_version' => '3.1.0',
            'version' => '3.1.0.0',
            'reference' => 'd3b292f04c0b3b21dced691ebad8be073a83b4ad',
            'type' => 'library',
            'install_path' => __DIR__ . '/../automattic/woocommerce',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'nwidart/woocommerce' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => '3115dddebb1ca3bd567ae0b582883d0702da32b7',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
