<?php

namespace Modules\Woocommerce\Console;

use App\Models\Category;
use App\Models\Redirect;
use App\Models\Setting;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Automattic\WooCommerce\Client;

class SyncCategories extends Command
{
    /**
     * The name and signature of the console command.
     */
    protected $signature = 'woocommerce:sync-categories';

    /**
     * The console command description.
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $page = 1;
        $perPage = 100;
        $allCategories = [];

        $woocommerce = new Client(
            env('WOOCOMMERCE_STORE_URL'), // Your store URL
            env('WOOCOMMERCE_CONSUMER_KEY'), // Your consumer key
            env('WOOCOMMERCE_CONSUMER_SECRET'), // Your consumer secret
            [
                'version' => 'wc/v3',
                'ssl_verify' => false
            ]
        );

        do {
            try {
                $response = $woocommerce->get('products/categories', [
                    'per_page' => $perPage,
                    'page' => $page
                ]);

                $wpCategories = json_decode(json_encode($response), true); // Μετατροπή του αντικειμένου σε πίνακα
                $allCategories = array_merge($allCategories, $wpCategories);

                echo "Page : {$page} - Categories Fetched : " . count($wpCategories) . "\n";

                $page++;
            } catch (Exception $e) {
                echo 'Error: ' . $e->getMessage();
                break; // Σπάσε τον βρόχο σε περίπτωση σφάλματος
            }
        } while (count($wpCategories) == $perPage); // Συνέχισε μέχρι να πάρεις λιγότερα αποτελέσματα από το perPage

        $this->info('Total Categories : ' . count($allCategories));

        $children = [];
        foreach ($allCategories as $key => $item) {
            $parentId = $item['parent'];
            if (!isset($children[$parentId])) {
                $children[$parentId] = [];
            }
            $children[$parentId][] = $item;
        }

// Δημιουργία της ιεραρχίας των κατηγοριών
        $rootCategories = [];

        foreach ($allCategories as $key => &$item) {


            if (isset($children[$item['id']])) {
                $item['children'] = $children[$item['id']];
                $this->info("Added children to category ID {$item['id']} (" . $item['name'] . "): " . count($children[$item['id']]));
            } else {
                $item['children'] = [];
            }

            // Αν η κατηγορία δεν έχει γονέα, είναι ριζική κατηγορία
            if ($item['parent'] == 0) {
                $rootCategories[] = &$item;
            }
        }

        $count = 0;
        foreach ($rootCategories as $category) {
//            dd($category['children']);
            if ($category['name'] == 'Uncategorized') {
                continue;
            }
            $ncat = $this->insertOrUpdateCategories($category, 0);


            $this->info('Main: ' . $category['name']);
            $this->processChildren($category, $ncat->id);
        }

    }

    public function processChildren($category, $parentId = null)
    {
        if (isset($category['children']) && count($category['children']) > 0) {
            foreach ($category['children'] as $child) {
                if($child['parent'] == 0){
                    continue;
                }
                $this->info('####Sub : ' . $child['name']);

                // Assuming $this->insertOrUpdateCategories() expects an array and an id.
                $c = $this->insertOrUpdateCategories($child, $parentId);
                if ($c) {

                    $this->processChildren($child, $c['id']);
                }
                // If the child has more children, process them recursively.
            }
        }
    }


    public function insertOrUpdateCategories($category, $parent)
    {

        // print_r($parent);
//dd('check this');
        $cat = Category::where('name', $category['slug'])->first();
        if (!$cat) {
            // $parentCategory = $parent ? Category::find($parent) : null;
            $cat = Category::where('erp_id', $category['id'])->where('parent_id', $parent)->first();
        }


        try {
            if ($cat) {
                $cat->update([
                    'erp_id' => $category['id'],
                    'title' => $category['name'],
                    'name' => $category['slug'],
                    'parent_id' => $parent,
                    'description' => $category['description'],
//                    'display' => $category->display,
                    'order' => $category['menu_order'],
                ]);
                //            $this->info("update : {$category->name}");
            } else {
                $cat = Category::create([
                    'erp_id' => $category['id'],
                    'title' => $category['name'],
                    'name' => $category['slug'],
                    'parent_id' => $parent,
                    'description' => $category['description'],
//                    'display' => $category->display,
                    'order' => $category['menu_order'],
                ]);
                Redirect::create([
                    'old_url' => $category['slug'],
                    'item_id' => $cat->id,
                    'method' => '301',
                    'type' => 'category'
                ]);

            }


        } catch (\Exception $e) {
            $this->info($e->getMessage());


            //            $this->info("create : {$category->name}");
        }
        return $cat;
    }

    /**
     * Get the console command arguments.
     */
    protected function getArguments(): array
    {
        return [
            ['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     */
    protected function getOptions(): array
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
