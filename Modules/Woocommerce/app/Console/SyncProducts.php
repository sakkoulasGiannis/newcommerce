<?php

namespace Modules\Woocommerce\Console;

use Illuminate\Console\Command;
use Automattic\WooCommerce\Client;
use App\Models\Product;
use App\Models\Category;
use App\Models\Attribute;
use App\Models\AttributeTerm;
use App\Models\AttributeProductTerm;
use Exception;
use Illuminate\Support\Facades\Log;

class SyncProducts extends Command
{
    /**
     * The name and signature of the console command.
     */
    protected $signature = 'woocommerce:sync-products {sku?}';

    /**
     * The console command description.
     */
    protected $description = 'Συγχρονίζει τα προϊόντα από το WooCommerce με το τοπικό σύστημα.';

    protected $woocommerce;

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();

        $this->woocommerce = new Client(
            env('WOOCOMMERCE_STORE_URL'), // Το URL του καταστήματός σας
            env('WOOCOMMERCE_CONSUMER_KEY'), // Το consumer key
            env('WOOCOMMERCE_CONSUMER_SECRET'), // Το consumer secret
            [
                'version' => 'wc/v3',
                'ssl_verify' => false
            ]
        );
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $sku = $this->argument('sku');

        $page = 1;
        $perPage = 50; // Αυξήστε το perPage για καλύτερη απόδοση
        $counter = 0;

        do {
            try {
                $query = [
                    'per_page' => $perPage,
                    'page' => $page,
                ];

                if ($sku) {
                    $query['sku'] = $sku;
                }

                $response = $this->woocommerce->get('products', $query);

                $wpProducts = json_decode(json_encode($response), true);

                if (empty($wpProducts)) {
                    break;
                }

                foreach ($wpProducts as $product) {

                    $this->info("Συγχρονισμός προϊόντος: {$product['sku']}");

                    if ($product['type'] === 'variable') {
                        $this->syncVariableProduct($product);
                    } else {
                        $this->syncSimpleProduct($product);
                    }
                    $counter++;
                }

                $page++;
            } catch (Exception $e) {
                $this->error('Σφάλμα: ' . $e->getMessage());
                Log::error('Error syncing products: ' . $e->getMessage());
                break; // Σπάει τον βρόχο σε περίπτωση σφάλματος
            }
        } while (count($wpProducts) == $perPage);

        $this->info("Ολοκληρώθηκε η συγχρονισμός. Συνολικά προϊόντα: {$counter}");
    }

    /**
     * Συγχρονίζει ένα απλό προϊόν.
     */
    protected function syncSimpleProduct(array $product)
    {
        try {
            $existingProduct = Product::where('old_id', $product['id'])->first();

            if ($existingProduct) {
                $this->updateProduct($existingProduct, $product);
            } else {
                $this->createProduct($product);
            }
        } catch (Exception $e) {
            $this->error("Σφάλμα συγχρονισμού απλού προϊόντος (ID: {$product['id']}): " . $e->getMessage());
            Log::error("Error syncing simple product ID {$product['id']}: " . $e->getMessage());
        }
    }

    /**
     * Συγχρονίζει ένα μεταβλητό προϊόν και τις παραλλαγές του.
     */
    protected function syncVariableProduct(array $product)
    {
        try {
            $existingProduct = Product::where('old_id', $product['id'])->first();

            if ($existingProduct) {
                $this->updateProduct($existingProduct, $product);
            } else {
                $existingProduct = $this->createProduct($product);
            }

            // Συγχρονισμός Παραλλαγών
            $this->syncVariations($existingProduct, $product);
        } catch (Exception $e) {
            $this->error("Σφάλμα συγχρονισμού μεταβλητού προϊόντος (ID: {$product['id']}): " . $e->getMessage());
            Log::error("Error syncing variable product ID {$product['id']}: " . $e->getMessage());
        }
    }

    /**
     * Δημιουργεί ή ενημερώνει ένα προϊόν.
     */
    protected function updateProduct(Product $existingProduct, array $product)
    {
        $existingProduct->update([
            'sku' => $product['sku'],
            'product_is_active' => ($product['status'] === 'publish') ? 1 : 0,
            'title' => $product['name'],
            'name' => $product['slug'],
            'product_type' => $product['type'],
            'price' => $product['regular_price'],
            'weight' => $product['weight'],
            'length' => $product['dimensions']['length'],
            'height' => $product['dimensions']['height'],
            'width' => $product['dimensions']['width'],
            'sale_price' => $product['sale_price'],
            'description' => $product['description'],
            'small_description' => $product['short_description'],
            'created_at' => $product['date_created'],
        ]);

        // Διαχείριση αποθέματος
        if ($existingProduct->product_type === 'simple') {
            $existingProduct->stock()->updateOrCreate(
                ['sku' => $product['sku'], 'product_id' => $existingProduct->id],
                [
                    'ean' => null,
                    'barcode' => null,
                    'price' => null,
                    'quantity' => (int)$product['stock_quantity'],
                ]
            );
        }

        // Ενημέρωση κατηγοριών
        $this->syncCategories($product['categories'], $existingProduct);

        // Ενημέρωση χαρακτηριστικών
        $this->addAttributesToProduct($product, $existingProduct);

        // Ενημέρωση εικόνων
        $this->syncImages($product['images'], $existingProduct);
    }

    /**
     * Δημιουργεί ένα νέο προϊόν.
     */
    protected function createProduct(array $product)
    {
        $newProduct = Product::create([
            'old_id' => $product['id'],
            'sku' => $product['sku'],
            'product_is_active' => ($product['status'] === 'publish') ? 1 : 0,
            'title' => $product['name'],
            'name' => $product['slug'],
            'product_type' => $product['type'],
            'price' => $product['regular_price'],
            'weight' => $product['weight'],
            'length' => $product['dimensions']['length'],
            'height' => $product['dimensions']['height'],
            'width' => $product['dimensions']['width'],
            'sale_price' => $product['sale_price'],
            'description' => $product['description'],
            'small_description' => $product['short_description'],
            'created_at' => $product['date_created'],
        ]);

        // Διαχείριση αποθέματος
        if ($newProduct->product_type === 'simple') {
            $newProduct->stock()->create([
                'sku' => $product['sku'],
                'ean' => null,
                'barcode' => null,
                'price' => null,
                'quantity' => (int)$product['stock_quantity'],
            ]);
        }

        // Ενημέρωση κατηγοριών
        $this->syncCategories($product['categories'], $newProduct);

        // Προσθήκη χαρακτηριστικών
        $this->addAttributesToProduct($product, $newProduct);

        // Προσθήκη εικόνων
        $this->syncImages($product['images'], $newProduct);

        // Δημιουργία redirect
        $newProduct->redirects()->create([
            'old_url' => $product['slug'],
            'item_id' => $newProduct->id,
            'type' => 'product',
            'sku' => $product['sku'],
            'method' => '301',
        ]);

        return $newProduct;
    }

    /**
     * Συγχρονίζει τις παραλλαγές ενός μεταβλητού προϊόντος.
     */
    protected function syncVariations(Product $product, array $wpProduct)
    {
        // Λήψη όλων των παραλλαγών από το WooCommerce
        $variations = $this->woocommerce->get("products/{$wpProduct['id']}/variations", [
            'per_page' => 100, // Προσαρμόστε ανάλογα με τις ανάγκες σας
        ]);

        $wpVariations = json_decode(json_encode($variations), true);

        $existingVariationIds = $product->variations()->pluck('old_id')->toArray();
        $wpVariationIds = [];

        foreach ($wpVariations as $variation) {
            $wpVariationIds[] = $variation['id'];
            $existingVariation = $product->variations()->where('old_id', $variation['id'])->first();

            if ($existingVariation) {
                $this->updateVariation($existingVariation, $variation);
            } else {
                $this->createVariation($product, $variation);
            }
        }

        // Διαγραφή παραλλαγών που δεν υπάρχουν πλέον στο WooCommerce
        $variationsToDelete = array_diff($existingVariationIds, $wpVariationIds);
        if (!empty($variationsToDelete)) {
            $product->variations()->whereIn('old_id', $variationsToDelete)->delete();
            $this->info("Διεγράφησαν οι παραλλαγές με IDs: " . implode(', ', $variationsToDelete));
        }
    }

    /**
     * Ενημερώνει μια υπάρχουσα παραλλαγή.
     */
    protected function updateVariation($existingVariation, array $variation)
    {
        $existingVariation->update([
            'sku' => $variation['sku'],
            'price' => $variation['regular_price'],
            'sale_price' => $variation['sale_price'],
            'stock_quantity' => $variation['stock_quantity'],
            'weight' => $variation['weight'],
            'length' => $variation['dimensions']['length'],
            'height' => $variation['dimensions']['height'],
            'width' => $variation['dimensions']['width'],
            'created_at' => $variation['date_created'],
        ]);

        // Ενημέρωση χαρακτηριστικών παραλλαγής
        $this->syncVariationAttributes($existingVariation, $variation['attributes']);
    }

    /**
     * Δημιουργεί μια νέα παραλλαγή.
     */
    protected function createVariation(Product $product, array $variation)
    {
        $newVariation = $product->variations()->create([
            'old_id' => $variation['id'],
            'sku' => $variation['sku'],
            'price' => $variation['regular_price'],
            'sale_price' => $variation['sale_price'],
            'stock_quantity' => $variation['stock_quantity'],
            'weight' => $variation['weight'],
            'length' => $variation['dimensions']['length'],
            'height' => $variation['dimensions']['height'],
            'width' => $variation['dimensions']['width'],
            'created_at' => $variation['date_created'],
        ]);

        // Συγχρονισμός χαρακτηριστικών παραλλαγής
        $this->syncVariationAttributes($newVariation, $variation['attributes']);
    }

    /**
     * Συγχρονίζει τα χαρακτηριστικά μιας παραλλαγής.
     */
    protected function syncVariationAttributes($variation, array $attributes)
    {
        foreach ($attributes as $attribute) {
            $localAttribute = Attribute::where('erp_id', $attribute['id'])->first();

            if (!$localAttribute) {
                $localAttribute = Attribute::whereJsonContainsLocale('title', 'el', $attribute['name'])->first();
            }

            if (!$localAttribute) {
                $this->error("Δεν βρέθηκε το χαρακτηριστικό: {$attribute['name']}");
                continue;
            }

            $localAttributeTerm = AttributeTerm::where('attribute_id', $localAttribute->id)
                ->whereJsonContainsLocale('title', 'el', $attribute['option'])
                ->first();

            if (!$localAttributeTerm) {
                $this->error("Δεν βρέθηκε ο όρος χαρακτηριστικού: {$attribute['option']} για το χαρακτηριστικό {$localAttribute->title}");
                continue;
            }

            // Ενημέρωση ή δημιουργία του AttributeProductTerm
            AttributeProductTerm::updateOrCreate(
                [
                    'product_id' => $variation->id,
                    'attribute_id' => $localAttribute->id,
                    'attribute_term_id' => $localAttributeTerm->id,
                ]
            );
        }
    }

    /**
     * Συγχρονίζει τις κατηγορίες ενός προϊόντος.
     */
    protected function syncCategories(array $categories, Product $product)
    {
        $categoryIds = [];

        foreach ($categories as $category) {
            $localCategory = Category::where('erp_id', $category['id'])->first();

            if ($localCategory) {
                $categoryIds[] = $localCategory->id;
            } else {
                $this->error("Δεν βρέθηκε η κατηγορία με ERPID: {$category['id']}");
            }
        }

        $product->categories()->sync($categoryIds);
    }

    /**
     * Προσθέτει ή ενημερώνει τα χαρακτηριστικά ενός προϊόντος.
     */
    protected function addAttributesToProduct(array $product, Product $localProduct)
    {
        foreach ($product['attributes'] as $attribute) {
            $localAttribute = Attribute::where('erp_id', $attribute['id'])->first();

            if (!$localAttribute) {
                $localAttribute = Attribute::whereJsonContainsLocale('title', 'el', $attribute['name'])->first();
            }

            if (!$localAttribute) {
                $this->error("Δεν βρέθηκε το χαρακτηριστικό: {$attribute['name']}");
                continue;
            }

            // Προσθήκη χαρακτηριστικού στο προϊόν
            $localProduct->attributes()->syncWithoutDetaching([
                $localAttribute->id => [
                    'position' => $attribute['position'],
                    'visible' => $attribute['visible'],
                    'variation' => $attribute['variation'],
                ]
            ]);

            // Προσθήκη όρων χαρακτηριστικού
            foreach ($attribute['options'] as $option) {
                $localAttributeTerm = AttributeTerm::where('attribute_id', $localAttribute->id)
                    ->whereJsonContainsLocale('title', 'el', $option)
                    ->first();

                if (!$localAttributeTerm) {
                    $this->error("Δεν βρέθηκε ο όρος χαρακτηριστικού: {$option} για το χαρακτηριστικό {$localAttribute->title}");
                    continue;
                }

                AttributeProductTerm::updateOrCreate([
                    'product_id' => $localProduct->id,
                    'attribute_id' => $localAttribute->id,
                    'attribute_term_id' => $localAttributeTerm->id,
                ]);
            }
        }
    }

    /**
     * Συγχρονίζει τις εικόνες ενός προϊόντος.
     */
    protected function syncImages(array $images, Product $product)
    {
        foreach ($images as $img) {
//            $this->info("Προσθήκη εικόνας: {$img['name']}");

            try {
                $imageContent = file_get_contents($img['src']);

                if ($imageContent === false) {
                    $this->error("Αποτυχία λήψης εικόνας από: {$img['src']}");
                    continue;
                }

                // Αποθήκευση της εικόνας στη συλλογή media
                $product->addMediaFromString($imageContent)
                    ->usingName($img['name'])
                    ->usingFileName(basename($img['src']))
                    ->toMediaCollection('product');
            } catch (Exception $e) {
                $this->error("Σφάλμα κατά την προσθήκη εικόνας: {$img['name']} - {$e->getMessage()}");
                Log::error("Error adding image {$img['src']} to product ID {$product->id}: " . $e->getMessage());
            }
        }
    }
}
