<?php

namespace Modules\Woocommerce\Console;

use Automattic\WooCommerce\Client;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SyncOrders extends Command
{
    /**
     * The name and signature of the console command.
     */
    protected $signature = 'woocommerce:sync-orders';

    /**
     * The console command description.
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $page = 1;
        $perPage = 100;
        $allOrders = [];

        $woocommerce = new Client(
            env('WOOCOMMERCE_STORE_URL'), // Your store URL
            env('WOOCOMMERCE_CONSUMER_KEY'), // Your consumer key
            env('WOOCOMMERCE_CONSUMER_SECRET'), // Your consumer secret
            [
                'version' => 'wc/v3',
                'ssl_verify' => false
            ]
        );

        do {
            try {
                $response = $woocommerce->get('orders', [
                    'per_page' => $perPage,
                    'page' => $page
                ]);

                $wpOrders = json_decode(json_encode($response), true); // Μετατροπή του αντικειμένου σε πίνακα
                dd($wpOrders[0]);
                $allOrders = array_merge($allOrders, $wpOrders);

                echo "Page : {$page} - Categories Fetched : " . count($wpOrders) . "\n";

                $page++;
            } catch (\Exception $e) {
                echo 'Error: ' . $e->getMessage();
                break; // Σπάσε τον βρόχο σε περίπτωση σφάλματος
            }
        } while (count($wpOrders) == $perPage); // Συνέχισε μέχρι να πάρεις λιγότερα αποτελέσματα από το perPage

        $this->info('Total Orders : ' . count($allOrders));


    }

    /**
     * Get the console command arguments.
     */
    protected function getArguments(): array
    {
        return [
            ['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     */
    protected function getOptions(): array
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
