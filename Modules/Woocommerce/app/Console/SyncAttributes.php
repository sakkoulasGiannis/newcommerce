<?php

namespace Modules\Woocommerce\Console;

use App\Models\Attribute;
use App\Models\AttributeTerm;
use Illuminate\Console\Command;
use Automattic\WooCommerce\Client;
use Exception;

class SyncAttributes extends Command
{
    /**
     * The name and signature of the console command.
     */
    protected $signature = 'woocommerce:sync-attributes {start=0}';

    /**
     * The console command description.
     */
    protected $description = 'Συγχρονίζει τα attributes και τα terms τους από το WooCommerce store με τη βάση δεδομένων.';

    protected $woocommerce;

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();

        // Δημιουργία ενός client WooCommerce κατά την αρχικοποίηση του command
        $this->woocommerce = new Client(
            env('WOOCOMMERCE_STORE_URL'), // Το URL του καταστήματός σας
            env('WOOCOMMERCE_CONSUMER_KEY'), // Το consumer key
            env('WOOCOMMERCE_CONSUMER_SECRET'), // Το consumer secret
            [
                'version' => 'wc/v3',
                'ssl_verify' => false, // Προσοχή: Σκεφτείτε να το ενεργοποιήσετε για ασφαλή επικοινωνία
            ]
        );
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $start = (int) $this->argument('start');

        $page = 1;
        $perPage = 100;
        $allAttributes = [];

        // Συγχρονισμός Attributes
        $this->info('Ξεκινώντας συγχρονισμό Attributes από WooCommerce.');

        do {
            try {
                $response = $this->woocommerce->get('products/attributes', [
                    'per_page' => $perPage,
                    'page' => $page
                ]);

                $wpAttributes = json_decode(json_encode($response), true); // Μετατροπή του αντικειμένου σε πίνακα
                $allAttributes = array_merge($allAttributes, $wpAttributes);

                $this->info("Page: {$page} - Attributes Fetched: " . count($wpAttributes));

                $page++;
            } catch (Exception $e) {
                $this->error('Σφάλμα κατά τη λήψη Attributes: ' . $e->getMessage());
                break; // Σπάει τον βρόχο σε περίπτωση σφάλματος
            }
        } while (count($wpAttributes) == $perPage); // Συνέχισε μέχρι να πάρεις λιγότερα αποτελέσματα από το perPage

        $totalFetchedAttributes = count($allAttributes);
        $this->info("Συνολικά Attributes που έχουν ληφθεί: {$totalFetchedAttributes}");

        $this->info('Έλεγχος και δημιουργία/ενημέρωση Attributes στη βάση δεδομένων');
        $prefix = 'pa_';

        foreach ($allAttributes as $key => $attribute) {
            // Παρακάμπτει τα attributes μέχρι να φτάσει στο σημείο έναρξης
            if ($key < $start) continue;

            $this->info("Attribute: {$attribute['name']}");

            $existingAttribute = Attribute::where('erp_id', $attribute['id'])->first();

            if ($existingAttribute) {
                // Ενημέρωση υπάρχοντος Attribute
                $existingAttribute->update([
                    'name' => str_replace($prefix, '', $attribute['slug']),
                    'type' => $attribute['type'],
                    'order_by' => $attribute['order_by'],
                    'has_archives' => $attribute['has_archives']
                ]);
                $this->info("  Ενημερώθηκε το Attribute: {$attribute['name']}");
            } else {
                // Δημιουργία νέου Attribute
                Attribute::create([
                    'erp_id' => $attribute['id'],
                    'title' => $attribute['name'],
                    'name' => str_replace($prefix, '', $attribute['slug']),
                    'type' => $attribute['type'],
                    'used_for_variations' => false,
                    'visible_on_product_page' => true,
                    'has_images' => false,
                    'has_colors' => false,
                    'order_by' => $attribute['order_by']
                ]);
                $this->info("  Δημιουργήθηκε το Attribute: {$attribute['name']}");
            }
        }

        // Συγχρονισμός Terms για κάθε Attribute
        $this->info('Ξεκινώντας συγχρονισμό Terms για κάθε Attribute.');

        $allLocalAttributes = Attribute::all();
        foreach ($allLocalAttributes as $attribute) {
            $this->info("Συγχρονισμός Terms για το Attribute: {$attribute->title} (ERPID: {$attribute->erp_id})");

            $terms = [];
            $page = 1;
            $perPage = 100;

            do {
                try {
                    $response = $this->woocommerce->get("products/attributes/{$attribute->erp_id}/terms", [
                        'per_page' => $perPage,
                        'page' => $page
                    ]);

                    $wpTerms = json_decode(json_encode($response), true); // Μετατροπή του αντικειμένου σε πίνακα
                    $terms = array_merge($terms, $wpTerms);

                    $this->info("  Page: {$page} - Terms Fetched: " . count($wpTerms));

                    $page++;
                } catch (Exception $e) {
                    $this->error("Σφάλμα κατά τη λήψη Terms για το Attribute ID {$attribute->erp_id}: " . $e->getMessage());
                    break; // Σπάει τον βρόχο σε περίπτωση σφάλματος
                }
            } while (count($wpTerms) == $perPage); // Συνέχισε μέχρι να πάρεις λιγότερα αποτελέσματα από το perPage

            $totalFetchedTerms = count($terms);
            $this->info("  Συνολικά Terms που έχουν ληφθεί για το Attribute '{$attribute->title}': {$totalFetchedTerms}");

            foreach ($terms as $term) {
                try {
                    $this->info("  Ενημέρωση/Δημιουργία Term: {$term['name']}");
                    $attribute->terms()->updateOrCreate(
                        ['erp_id' => $term['id']],
                        [
                            'title' => $term['name'],
                            'term_order' => $term['menu_order'],
                            'name' => $term['slug']
                        ]
                    );
                } catch (Exception $e) {
                    $this->error("  Σφάλμα κατά την ενημέρωση/δημιουργία του Term '{$term['name']}': " . $e->getMessage());
                }
            }
        }

        $this->info('Ολοκληρώθηκε ο συγχρονισμός των Attributes και των Terms τους.');
    }
}
