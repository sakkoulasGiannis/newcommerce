<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('test-template', function () {
    // read menu from //storage/menu.json
    $menu = json_decode(file_get_contents(public_path('storage/menu.json')));

    return view('test.template', ['menu' => $menu]);
});
Route::get('mail/{id}', function ($id) {
    $order = App\Models\Order::with('detail', 'products', 'voucher')->find($id);
    return new App\Mail\newOrder($order);
});

Route::get('mail-complete/{id}', function ($id) {
    $order = \App\Models\Order::with('detail', 'products', 'voucher')->find($id);
    return new App\Mail\CompleteOrder($order);
});

//	Route::get('test', [\App\Http\Controllers\TestController::class, 'filter']);
Route::get('buildcategories', [\App\Http\Controllers\TestController::class, 'updateCategories'])->name('buildcategories');
Route::get('login', [\App\Http\Controllers\CustomAuthController::class, 'index'])->name('login');
Route::post('custom-login', [\App\Http\Controllers\CustomAuthController::class, 'customLogin'])->name('login.user');
Route::get('register', [\App\Http\Controllers\CustomAuthController::class, 'registration'])->name('register');
Route::post('custom-registration', [\App\Http\Controllers\CustomAuthController::class, 'customRegistration'])->name('register.custom');
Route::get('/reset-password/{token}', function (string $token) {
    return view('auth.reset-password', ['token' => $token]);
})->middleware('guest')->name('password.reset');


Route::get('signout', [\App\Http\Controllers\CustomAuthController::class, 'signOut'])->name('signout');
Route::prefix('account')->middleware('auth:sanctum')->group(function () {
    Route::get('/', [\App\Http\Controllers\UserController::class, 'account'])->name('account');
    Route::get('/orders', [\App\Http\Controllers\UserController::class, 'orders'])->name('account.orders');
});


Route::get('/sitemap.xml', [\App\Http\Controllers\SitemapController::class, 'index']);
Route::get('/csv/products', [\App\Http\Controllers\SitemapController::class, 'products_csv']);
Route::get('/csv/products/facebook', [\App\Http\Controllers\SitemapController::class, 'products_csv_local']);

Route::get('/csv/products-all', [\App\Http\Controllers\SitemapController::class, 'products_csv_all']);
Route::get('/csv/adwords_feed', [\App\Http\Controllers\SitemapController::class, 'adwords_data_feed']);
Route::get('/csv/facebook', [\App\Http\Controllers\SitemapController::class, 'facebook']);
Route::get('/xml/products.xml', [\App\Http\Controllers\SitemapController::class, 'products']);
Route::get('/xml/skroutz.xml', [\App\Http\Controllers\SitemapController::class, 'skroutz']);
Route::get('/xml/glami.xml', [\App\Http\Controllers\SitemapController::class, 'glami']);


// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
// 	return Inertia::render('Dashboard');
// })->name('dashboard');

Route::get('/', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');

// render pages
Route::get('/content/{slug}', [\App\Http\Controllers\PageController::class, 'frontEndView']);
Route::get('/whishlist', [\App\Http\Controllers\WishListController::class, 'index'])->name('whishlist');
Route::post('/whishlist/store', [\App\Http\Controllers\WishListController::class, 'store'])->name('whishlist.store');

Route::post('/whishlist/destroy/{id}', [\App\Http\Controllers\WishListController::class, 'destroy'])->name('whishlist.destroy');

Route::post('/policy/accept', [\App\Http\Controllers\GdprController::class, 'aprove'])->name('policy.accept');
Route::get('/country/{country}/counties', [\App\Http\Controllers\CountyController::class, 'getCountiesByCountry']);

//Route::prefix('manage')->middleware(['role:Admin'])->group( function () {
Route::prefix('manage')->middleware('auth:sanctum')->group(function () {

    Route::get('/product/{product}/views', [\App\Http\Controllers\ProductViewController::class, 'views'])->name('product.views');
    Route::get('/get-menu', [\App\Http\Controllers\MenuController::class, 'get_menu'])->name('get_menu');

    Route::get('/theme-load', [\App\Http\Controllers\ThemeController::class, 'themes'])->name('theme');
//    Route::get('/theme/preview', [\App\Http\Controllers\ThemeController::class, 'preview'])->name('theme.preview');
    Route::post('/{theme}/save-theme-editor', [\App\Http\Controllers\ThemeController::class, 'save_theme_editor'])->name('theme_store');
    Route::resource('/theme', \App\Http\Controllers\ThemeController::class);

    Route::get('/theme/{theme}/component/{component}', [\App\Http\Controllers\ThemeController::class, 'component_edit'])->name('theme.component.edit');
    Route::post('/theme/{theme}/component/{component}/save-component', [\App\Http\Controllers\ThemeController::class, 'save_component']);
    Route::get('/theme/blade-content/{file}', [\App\Http\Controllers\ThemeController::class, 'bladeContent'])->name('theme.blade.content');
    Route::get('/load-component/{component}', [\App\Http\Controllers\ThemeController::class, 'bladeContent']);
    Route::get('/theme-components', [\App\Http\Controllers\ThemeController::class, 'components']);
    Route::post('/settings/store', [\App\Http\Controllers\SettingController::class, 'store'])->name('settings.store');
    Route::post('/upload-image', [\App\Http\Controllers\GeneralController::class, 'uploadImage'])->name('upload_image');
    Route::post('/{theme}/theme-images', [\App\Http\Controllers\GeneralController::class, 'ThemeImageUpload']);
    Route::get('/{theme}/theme-images', [\App\Http\Controllers\GeneralController::class, 'ThemeImage']);
    Route::resource('/components', \App\Http\Controllers\ComponentController::class);
    Route::post('/manage/save-component', [\App\Http\Controllers\ComponentController::class, 'saveComponent'])->name('save_component');
    Route::get('/scan-media', [\App\Http\Controllers\GeneralController::class, 'scanMedia'])->name('scan_media');

    Route::get('company', [\App\Http\Controllers\SettingController::class, 'company'])->name('company');
    Route::prefix('settings')->middleware('auth:sanctum')->group(function () {
        Route::get('/', [\App\Http\Controllers\GeneralController::class, 'settings'])->name('settings');
    });

    Route::get('/shipping-payment-rule', [\App\Http\Controllers\GeneralController::class, 'shipping_payment']);
    Route::post('/shipping-payment-rule', [\App\Http\Controllers\GeneralController::class, 'shipping_payment_store']);
    Route::delete('/shipping-payment-rule', [\App\Http\Controllers\GeneralController::class, 'delete_shipping_payment_store']);

    //    products

    Route::get('/', [\App\Http\Controllers\DashboardController::class, 'index'])->name('manage');
    Route::get('/clear-cache', [\App\Http\Controllers\GeneralController::class, 'clearCache'])->name('cache.clear');
    Route::get('/languages', [\App\Http\Controllers\LanguageController::class, 'index'])->name('languages');
    Route::get('/products', [\App\Http\Controllers\ProductController::class, 'index'])->name('products');
    Route::get('/products/create', [\App\Http\Controllers\ProductController::class, 'create'])->name('products.create');
    Route::post('/products/store', [\App\Http\Controllers\ProductController::class, 'store'])->name('products.store');
    Route::get('/products/filters', [\App\Http\Controllers\ProductController::class, 'filters'])->name('products.filter');
    Route::post('/products/filters/apply', [\App\Http\Controllers\RuleController::class, 'applyFIlters'])->name('products.filter.aply');
    Route::get('/product/{product}/fields', [\App\Http\Controllers\ProductController::class, 'fields'])->name('product.fields');
    Route::post('/getProduct/{id}', [\App\Http\Controllers\ProductController::class, 'getProductForEdit'])->name('product.store');
    Route::get('/product/{id}/cross-sell', [\App\Http\Controllers\ProductController::class, 'getProductCrossSell']);
    Route::put('/product/{product}/cross-sell', [\App\Http\Controllers\ProductController::class, 'updateProductCrossSell']);

    Route::get('/product/{id}/combos', [\App\Http\Controllers\ProductController::class, 'getProductCombos'])->name('product.combos');
    Route::put('/product/{product}/combos', [\App\Http\Controllers\ProductController::class, 'updateProductCombos']);
    Route::post('/getProduct/{product}/categories', [\App\Http\Controllers\ProductController::class, 'getProductCategories'])->name('products.categories');
    Route::get('/products/{product}/get_stock', [\App\Http\Controllers\ProductController::class, 'get_stock'])->name('get_product_stock');
    Route::post('/products/{product}/update', [\App\Http\Controllers\ProductController::class, 'update'])->name('product_update');
    Route::post('/products/{product}/stock/update/quantity', [\App\Http\Controllers\ProductController::class, 'stockUpdateQuantity'])->name('product_stock_update_quantity');
    Route::post('/products/{product}/stock/update', [\App\Http\Controllers\ProductController::class, 'stockUpdate'])->name('product_stock_update');
    Route::post('/products/{product}/categories/update', [\App\Http\Controllers\ProductController::class, 'updateCategories'])->name('product_categories_update');
    Route::post('/products/{product}/calculator/update', [\App\Http\Controllers\ProductController::class, 'calculatorUpdate'])->name('product_calculator_update');
    Route::post('/products/{product}/fields/update', [\App\Http\Controllers\ProductController::class, 'fieldsUpdate'])->name('product_fields_update');
    Route::get('/products/{product}/media', [\App\Http\Controllers\ProductController::class, 'getMedia'])->name('product.get.media');
    Route::post('/products/{product}/media', [\App\Http\Controllers\ProductController::class, 'addMedia'])->name('product.add.media');
    Route::post('/media/{id}/delete', [\App\Http\Controllers\ProductController::class, 'deleteMedia'])->name('product.delete.media');
    Route::post('/products/search', [\App\Http\Controllers\ProductController::class, 'search'])->name('product.search');
    Route::get('/products/{product}/edit', [\App\Http\Controllers\ProductController::class, 'edit'])->name('product_edit');
    Route::get('/products/{product}', [\App\Http\Controllers\ProductController::class, 'edit'])->name('product.get');
    Route::post('/products_list_json', [\App\Http\Controllers\ProductController::class, 'listJson'])->name('products_list_json');
    Route::post('/products_search_combos', [\App\Http\Controllers\ProductController::class, 'searchForGroup'])->name('products_group_product_search');

    Route::post('/product/addStockVariation', [\App\Http\Controllers\StockVariationController::class, 'create'])->name('add.stock.variation');
    Route::post('/products/{product}/destroy', [\App\Http\Controllers\ProductController::class, 'destroy'])->name('product.destroy');
    Route::post('/product/destory_stock_variation', [\App\Http\Controllers\StockVariationController::class, 'destroy'])->name('destory.stock.variation');
    Route::post('/product/destory_stock', [\App\Http\Controllers\StockController::class, 'destroy'])->name('destory.stock');
    Route::get('/product/elastic', [\App\Http\Controllers\ProductController::class, 'elastic']);
    // end products
    //attributes
    Route::post('json-attributes', [\App\Http\Controllers\AttributeController::class, 'attributes'])->name('attributes');
    Route::resource('/attributes', \App\Http\Controllers\AttributeController::class);
    Route::get('/attributes/{attribute}/terms', [\App\Http\Controllers\AttributeTermController::class, 'index']);
    Route::get('/attribute/{attribute}/term/values', [\App\Http\Controllers\AttributeTermController::class, 'term_values']);
    Route::put('/attribute/{attribute}/term/values/{id}', [\App\Http\Controllers\AttributeTermController::class, 'term_vavariationslues_update']);
    Route::post('/attribute/{attribute}/term/values/{id}/image', [\App\Http\Controllers\AttributeTermController::class, 'uploadFile']);


    // fields
//    Route::get('/fields', [\App\Http\Controllers\FieldController::class, 'index'])->name('fields');
    Route::get('/fields/get_fields', [\App\Http\Controllers\FieldController::class, 'get_fields'])->name('get_fields');
    Route::resource('/fields', \App\Http\Controllers\FieldController::class);
    Route::get('/fields/{field}/terms', [\App\Http\Controllers\FieldController::class, 'terms']);
    Route::get('/fields/{field}/term/values', [\App\Http\Controllers\FieldController::class, 'term_values']);
    Route::put('/fields/{field}/term/values/{id}', [\App\Http\Controllers\FieldController::class, 'term_values_update']);
    Route::post('/fields/{field}/term/values', [\App\Http\Controllers\FieldController::class, 'term_values_store']);
    Route::post('/fields/{field}/term/values/{id}/image', [\App\Http\Controllers\FieldController::class, 'uploadFile']);

    Route::post('/fields/add_field_attribute', [\App\Http\Controllers\FieldController::class, 'add_field_attribute'])->name('add_field_attribute');
    Route::post('/fields/get_field_values', [\App\Http\Controllers\FieldController::class, 'get_field_values'])->name('get_field_values');
    Route::post('/fields/get_field_values_all', [\App\Http\Controllers\FieldController::class, 'get_field_values_all'])->name('get_fields_values_all');
    //   end fields

    Route::resource('modules', \App\Http\Controllers\ModuleController::class);

    //  custom querybuild
    Route::post('/querybuilresults', [\App\Http\Controllers\ProductController::class, 'querybuilresults'])->name('query_builder');
    // end custom querybuild

    //    Categories
    Route::get('/categories', [\App\Http\Controllers\CategoryController::class, 'index'])->name('categories');
//    Route::post('/menu/{menu}/menuUpdateOrCreate', [\App\Http\Controllers\MenuController::class, 'menuUpdateOrStore'])->name('categories.menu.update');
    Route::resource('/menu', \App\Http\Controllers\MenuController::class);
    Route::get('/categories_list_json', [\App\Http\Controllers\CategoryController::class, 'categoriesTree'])->name('categories.json');
    Route::post('/categories/create', [\App\Http\Controllers\CategoryController::class, 'create'])->name('category.create');
    Route::get('/categories/{category}/edit', [\App\Http\Controllers\CategoryController::class, 'edit'])->name('category.edit');
    Route::post('/categories/store', [\App\Http\Controllers\CategoryController::class, 'index'])->name('category.store');
    Route::delete('/categories/{category}', [\App\Http\Controllers\CategoryController::class, 'destroy'])->name('category.destroy');

    Route::post('/categories/{category}/update', [\App\Http\Controllers\CategoryController::class, 'update'])->name('category.update');
    Route::get('/categories/{category}', [\App\Http\Controllers\CategoryController::class, 'category'])->name('category');
    Route::get('/get_categories_for_query/', [\App\Http\Controllers\CategoryController::class, 'get_categories_for_query'])->name('get_categories_for_query');
    // end Categories

    // Orders
    Route::post('/orders/store', [\App\Http\Controllers\OrderController::class, 'store_new_order'])->name('orders.store_new_order');
    Route::get('/unopend-orders', [\App\Http\Controllers\OrderController::class, 'unopendOrders'])->name('orders.unopened');
    Route::get('/orders', [\App\Http\Controllers\OrderController::class, 'index'])->name('orders');
    Route::get('/orders/filter', [\App\Http\Controllers\OrderController::class, 'filter'])->name('orders.filter');
    Route::post('/orders_list_json', [\App\Http\Controllers\OrderController::class, 'listJson'])->name('orders.json');
    //    Route::get('/orders/view', [\App\Http\Controllers\OrderController::class, 'view'])->name('orders.view');
    Route::get('/orders/create', [\App\Http\Controllers\OrderController::class, 'create'])->name('orders.create');
    Route::post('/orders/export', [\App\Http\Controllers\OrderController::class, 'export'])->name('orders.export');

    Route::get('/orders/{order}', [\App\Http\Controllers\OrderController::class, 'edit'])->name('orders.view');
    Route::get('/orders/{order}/edit', [\App\Http\Controllers\OrderController::class, 'edit'])->name('orders.edit');
    Route::post('/orders/{order}/destroy', [\App\Http\Controllers\OrderController::class, 'destroy'])->name('orders.destroy');
    Route::post('/orders/{order}/comment', [\App\Http\Controllers\OrderController::class, 'addComment'])->name('orders.add.comment');
    Route::post('/orders/{order}/', [\App\Http\Controllers\OrderController::class, 'update'])->name('orders.update');
    Route::post('/orders/{order}/replicate', [\App\Http\Controllers\OrderController::class, 'replicate'])->name('orders.replicate');
    // end orders

    // rules
    Route::post('/rules/{id}/destroy', [\App\Http\Controllers\RuleController::class, 'destroy'])->name('rules.delete');

    Route::get('/rules/query', [\App\Http\Controllers\RuleController::class, 'query'])->name('rules.query');

    Route::resource('/rules', \App\Http\Controllers\RuleController::class);
    // end rules

    //pages
    Route::resource('/pages', \App\Http\Controllers\PageController::class);
    //end pages

    //payemnts
    Route::get('/get-payments', [\App\Http\Controllers\PaymentController::class, 'getPayments'])->name('payments.all');
    Route::get('payments', [\App\Http\Controllers\PaymentController::class, 'index'])->name('payments');

    // couriers

    Route::get('/get-shippings', [\App\Http\Controllers\CourierController::class, 'getShippings'])->name('shippings.all');
    Route::resource('/couriers', \App\Http\Controllers\CourierController::class);

    Route::post('/rules_list_json', [\App\Http\Controllers\RuleController::class, 'listJson'])->name('rules.json');

    Route::get('/rules/json/{rule}', [\App\Http\Controllers\RuleController::class, 'json_rule'])->name('rule.json');

    Route::post('/rules/filter_products', [\App\Http\Controllers\RuleController::class, 'filter_products'])->name('filter_products');
    // end rules
    // vouchers
    Route::get('/rules/json/{voucher}', [\App\Http\Controllers\VoucherController::class, 'json_rule']);
    Route::post('/vouchers_list_json', [\App\Http\Controllers\VoucherController::class, 'listJson'])->name('vouchers.json');
    Route::post('/search-vouchers', [\App\Http\Controllers\VoucherController::class, 'filterVoucher'])->name('vouchers.search');
    Route::resource('/vouchers', \App\Http\Controllers\VoucherController::class);
    Route::get('/vouchers/{voucher}/edit', [\App\Http\Controllers\VoucherController::class, 'show']);
    // end vouchers

    //search
    Route::post('/admin_search', [\App\Http\Controllers\SearchController::class, 'adminSearch'])->name('admin.search');
    //end search

    // sliders
    //     Route::get('/sliders/{slider}/images', [\App\Http\Controllers\SliderController::class, 'getImages']);
    Route::resource('/sliders', \App\Http\Controllers\SliderController::class);
    //     Route::post('/sliders/{slider}/image', [\App\Http\Controllers\SliderController::class, 'addPhoto']);
    //     Route::get('/sliders/{image}/destroy', [\App\Http\Controllers\SliderController::class, 'destroyPhoto']);
    //     Route::post('/slider_list_json', [\App\Http\Controllers\SliderController::class, 'listJson'])->name('slider.json');

    Route::post('/sliders/{slider}/images', [\App\Http\Controllers\SliderController::class, 'addPhoto'])->name('sliders.addPhoto');
    Route::put('/image/{image}/update', [\App\Http\Controllers\SliderController::class, 'updateImage'])->name('slider.image.update');
    Route::delete('/image/{image}/destroy', [\App\Http\Controllers\SliderController::class, 'destroyPhoto'])->name('slider.image.destroy');

    // end sliders

    // Users
    Route::post('/users/search', [\App\Http\Controllers\UserController::class, 'searchUser'])->name('users.search');
    Route::post('/users_list_json', [\App\Http\Controllers\UserController::class, 'listJson'])->name('users.json');

    Route::resource('/users', \App\Http\Controllers\UserController::class);
    Route::post('/users/create_admin', [\App\Http\Controllers\UserController::class, 'createAdmin'])->name('users.create_admin');
    Route::post('/users/{user}/destroy', [\App\Http\Controllers\UserController::class, 'destroy'])->name('user.destroy');
    // end Users

    // general store
    Route::get('/get-languages-list', [\App\Http\Controllers\GeneralController::class, 'getLanguageList'])->name('get-languages-list');
    Route::get('/languages', [\App\Http\Controllers\LanguageController::class, 'index'])->name('languages');
    //countries
    Route::post('/countries/store_rule', [\App\Http\Controllers\CountryController::class, 'store_courier_payments'])->name('coungry.storerules');
    Route::get('/countries/all', [\App\Http\Controllers\CountryController::class, 'all'])->name('countries.all');

    Route::get('/export', [\App\Http\Controllers\GeneralController::class, 'export_index'])->name('general.index');
    Route::get('/query_all', [\App\Http\Controllers\GeneralController::class, 'query_all'])->name('general.search');

    Route::get('/builder', [\App\Http\Controllers\GeneralController::class, 'builder']);
});
Route::prefix('/cart')->group(function () {

    Route::get('/', [\App\Http\Controllers\CartController::class, 'index'])->name('cart');
    Route::get('/items', [\App\Http\Controllers\CartController::class, 'cartItems'])->name('cart.items');
    Route::post('/item/remove', [\App\Http\Controllers\CartController::class, 'itemRemove'])->name('cart.remove');
    Route::post('/item/quantity', [\App\Http\Controllers\CartController::class, 'itemQuantity'])->name('cart.quantity');
    Route::get('/checkout', [\App\Http\Controllers\CartController::class, 'checkout'])->name('cart.checkout');
    Route::post('/checkout', [\App\Http\Controllers\CartController::class, 'checkoutProcess'])->name('cart.checkout.complete');
    Route::post('/checkout/payments', [\App\Http\Controllers\CartController::class, 'payments'])->name('cart.checkout.payments');
    Route::post('/flush/{id}', [\App\Http\Controllers\CartController::class, 'delete'])->name('cart.deleteItem');

    Route::get('/checkout/success', [\App\Http\Controllers\CartController::class, 'checkoutWithStatus'])->name('checkout.success');
    Route::get('/checkout/complete', [\App\Http\Controllers\CartController::class, 'completed'])->name('checkout.completed');
    Route::get('/checkout/cancel', [\App\Http\Controllers\CartController::class, 'checkoutWithStatus'])->name('checkout.cancel');
    Route::get('/checkout/error', [\App\Http\Controllers\CartController::class, 'checkoutWithStatus'])->name('checkout.error');


    Route::get('/checkout/alphabank', [\App\Http\Controllers\AlphaBankController::class, 'checkout']);
    Route::post('/checkout/alphabank/success', [\App\Http\Controllers\AlphaBankController::class, 'success']);
    Route::post('/checkout/alphabank/fail', [\App\Http\Controllers\AlphaBankController::class, 'fail']);
});

Route::get('payment/confirm/{order}', [\App\Http\Controllers\AlphaBankController::class, 'check_response'])->name('payment.confirm');
Route::get('payment/cancel/{order}', 'AlphaBankController@cancel')->name('payment.cancel');


Route::post('/add_to_cart', [\App\Http\Controllers\CartController::class, 'add'])->name('cart.add');
Route::post('/check_for_voucher', [\App\Http\Controllers\CartController::class, 'check_for_voucher']);

// Route::get('/', function () {
// //    return view('dashboard.index');
// });
Route::get('/product/', function () {
    return view('dashboard.products.view');
});
Route::get('/products', function () {
    return view('dashboard.products.index');
});

Route::get('/search', [\App\Http\Controllers\SearchController::class, 'frontendSearch'])->name('front.search');
Route::get('/search-simple', [\App\Http\Controllers\SearchController::class, 'frontendSearchSimple'])->name('front.search.simple');
Route::post('/api-search', [\App\Http\Controllers\SearchController::class, 'apiSearch'])->name('front.api.search');
//Route::get('/brands', [\App\Http\Controllers\BrandsController::class, 'index'])->name('brands');
//Route::match(['GET', 'POST'], '/brands/{slug}', [\App\Http\Controllers\BrandsController::class, 'brand']);


// Route::match(['GET', 'POST'], '/{path?}/', [App\Http\Controllers\SegmentController::class, 'getCat'])->where('path', '[a-zA-Z0-9/._-]+');
Route::post('/loadfilters', [App\Http\Controllers\SegmentController::class, 'loadfilters'])->name('loadfilters');


// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
