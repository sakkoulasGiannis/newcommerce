const { exec } = require('child_process');
const path = require('path');

// Λάβετε το themeName από τα arguments
const themeName = process.argv[2];

if (!themeName) {
    console.error('Error: No theme name provided.');
    process.exit(1);
}

const inputFile = path.resolve(__dirname, 'resources/css/input.css');
const outputFile = path.resolve(__dirname, 'public/themes', themeName, 'style.css');

const command = `tailwindcss -i ${inputFile} -o ${outputFile} --minify`;

exec(command, (error, stdout, stderr) => {
    if (error) {
        console.error(`Error: ${error.message}`);
        return;
    }
    if (stderr) {
        console.error(`stderr: ${stderr}`);
        return;
    }
    console.log(`stdout: ${stdout}`);
});
