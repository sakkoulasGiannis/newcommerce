<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\Menu;
use App\Models\Theme;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Cache;
use function Symfony\Component\Translation\t;
use App\Services\HookService;

// use App\Fields\FieldBrand;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('hook', function () {
            return new HookService();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Δημιουργία της blade directive για τα hooks
        Blade::directive('hook', function ($hookName) {
            return "<?php echo app('hook')->executeHook({$hookName}); ?>";
        });

        $themeDir = config('view.theme');
        $functionsFile = base_path("public/themes/{$themeDir}/functions.php");
        // Ελέγχουμε αν υπάρχει το αρχείο functions.php και το φορτώνουμε
        if (file_exists($functionsFile)) {
            require_once $functionsFile;
        }


        if ($this->app->environment('production')) {
            \URL::forceScheme('https');
        }

        // if (\Illuminate\Support\Facades\Schema::hasTable('categories')) {

        // $brands = \App\Fields\FieldBrand::get();

        // $brands = $brands
        //    ->sortBy('title')
        //    ->unique('name')
        //    ->groupBy(
        //        function ($item, $key) {
        //            return (string)mb_substr($item->title, 0, 1);
        //        }
        //    );

//        if(\cache()->has('categories')){
//            $categories = \cache()->get('categories');
//        }else{
//            $categories = Category::with('children', 'children.children')->orderBy('order', 'asc')->get();
//            \cache()->put('categories', $categories, 60*60*24);
//        }
        // check if table categories exist

        // if file exist /storage/app/resources/menu.json
        $menu = [];
        if (Schema::hasTable('settings')) {

            $menu = Menu::where('active', 1)->first();
            if ($menu) {
                $menu = json_decode($menu->menu, true);
            } else {
                $menu = [];
            }
            $settings = Cache::get('settings', function () {
                return \App\Models\Setting::where('group', 'company')->select('key', 'value', 'title')->get()->keyBy('key');
            });
            View::share('settings', $settings);
            View::share('menu', $menu);
        }
//


//          if (file_exists(public_path('storage/menu.json'))) {
//             $menuJson = json_decode(file_get_contents(public_path('storage/menu.json')));
//              $menu = json_decode($menuJson->menu, true);
//
//
//            View::share('menu', $menu);
//        }
        $local = Config::get('app.locale');
        View::share('local', $local);


        if (Schema::hasTable('categories')) {
            $categories = Cache::get('categories', function () {
                return Category::where('parent_id', 0)->with('children', 'children.children')->orderBy('order', 'asc')->get();
            });
            //  View::share('allBrands', $brands);
            View::share('allCategories', $categories);
        }


        // theme directory
        if (!\Request::is('manage/*') && !\Request::is('api/*')) {

            // Μπορείς να καθορίσεις το directory δυναμικά εδώ
            $themeFolder = config('view.theme');
            // Αν δεν υπάρχει στο .env, ανακτά τη τιμή από τη βάση δεδομένων
            if (strlen($themeFolder) == null) {
                $activeTheme = Theme::where('active', 1)->first();
                if ($activeTheme) {
                    $themeFolder = $activeTheme->name;
                } else {
                    $themeFolder = 'default';
                }

            }
            // Αποθήκευση της τιμής στη config
            Config::set('view.theme', $themeFolder);
        }


//        if (\Request::is('manage/company') || !\Request::is('manage/*') && !\Request::is('api/*')) {



//        }


    }
}
