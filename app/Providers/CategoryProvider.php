<?php

namespace App\Providers;

use App\Models\Category;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;

class CategoryProvider extends ServiceProvider {
	protected $categories;

	/**
	 * Register services.
	 *
	 * @return void
	 */
	public function register() {
		$this->categories = Category::with('children')->get();

	}

	/**
	 * Bootstrap services.
	 *
	 * @return void
	 */
	public function boot(View $view) {
		$view->with('allCategories', $this->Categories);

	}
}
