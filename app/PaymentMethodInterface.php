<?php

namespace App;

use Illuminate\Http\Client\Request;

interface PaymentMethodInterface
{
    public function process(array $order, Request $request);

}
