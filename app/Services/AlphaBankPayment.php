<?php

namespace App\Services;

use App\PaymentMethodInterface;

class AlphaBankPayment implements PaymentMethodInterface
{
    public function process(array $order,  $request)
    {

        return redirect('/cart/checkout/alphabank?order_id=' . $order['id']);

        // Π.χ. επικοινωνία με API Alpha Bank
        return [
            'status' => 'success',
            'transaction_id' => 'ABC123456',
            'redirect' => '/cart/checkout/alphabank?order_id=' . $order['id']

        ];
    }
}
