<?php

namespace App\Services;

use App\PaymentMethodInterface;
use InvalidArgumentException;

class PaymentMethodFactory
{
    public static function create(string $method): PaymentMethodInterface
    {
        return match ($method) {
            'cod' => new CashOnDelivery(),
            'alpha_bank' => new AlphaBankPayment(),
            'bank_transfer' => new BankTransfer(),
            default => throw new InvalidArgumentException("Unsupported payment method: $method"),
        };
    }
}
