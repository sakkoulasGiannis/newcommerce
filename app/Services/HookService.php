<?php

namespace App\Services;

class HookService
{
    protected $hooks = [];

    /**
     * Register a new hook.
     *
     * @param string $hookName
     * @param callable $callback
     * @return void
     */
    public function addHook(string $hookName, callable $callback)
    {
        $this->hooks[$hookName][] = $callback;
    }

    /**
     * Execute all callbacks attached to a hook.
     *
     * @param string $hookName
     * @param array $params
     * @return string
     */
    public function executeHook(string $hookName, $params = [])
    {
        $output = '';

        if (isset($this->hooks[$hookName])) {
            foreach ($this->hooks[$hookName] as $callback) {
                $output .= call_user_func_array($callback, $params);
            }
        }

        return $output;
    }
}
