<?php
namespace App\Services;

use App\Mail\newOrder;
use App\Models\Order;
use App\PaymentMethodInterface;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
class CashOnDelivery implements PaymentMethodInterface
{


    public function process( $order,  $request)
    {

//        Mail::to($request->get('billing_email'))->send(new newOrder($order));

        $order = Order::with('detail')->find($order->id);
         return view(\Config::get('view.theme') . '.success_checkout', compact('order'));

    }
}
