<?php

namespace App\Exports;

use App\Models\Order;
use http\Env\Request;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Http\Resources\OrderCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class OrderExport implements FromCollection, WithHeadings
{

    use Exportable;

        protected $orders; 

    public function __construct(array $filters)
    {
        $this->filters = $filters;
    }

    public function collection()
    {
        $orders = Order::has('detail');

        if (count($this->filters) > 0) {
            foreach ($this->filters as $n => $v) {
                if ($v != null) {
                    $orders->whereHas('detail', function ($q) use ($n, $v) {
                        $q->where($n, $v);
                    });
                }
            }
        }


        $data = Collect(new OrderCollection($orders->get()));
        $this->orders = $data;
        return $data;
    }

    public function headings(): array
    {
        return [
            'id',
            'name',
            'mobile',
            'email',
            'subtotal',
            'total',
            'status',
            'created_at',
            'order_id',
            'courier_name',
            'order_num',
            'ip',
            'user_id',
            'invoice_status',
            'afm',
            'doy',
            'company',
            'payment_name',
            'tracking_number',
            'comment',
            'admin_comment_id',
            'slug',
            'billing_email',
            'shipping_email',
            'invoice_id',
            'payment_id',
            'shipping_id',
            'sub_total',
            'billing_name',
            'billing_lastname',
            'billing_country_id',
            'billing_region',
            'billing_city',
            'billing_street',
            'billing_zip',
            'billing_street_number',
            'billing_phone',
            'billing_mobile',
            'shipping_country_id',
            'shipping_region',
            'shipping_city',
            'shipping_street',
            'shipping_zip',
            'shipping_street_number',
            'shipping_phone',
            'shipping_mobile',
            'shipping_name',
            'shipping_lastname',

        ];
    }


    //    /**
    //     * @return \Illuminate\Support\Collection
    //     */
    //    public function collection(Request $request)
    //    {
    //
    //        dd($request->input());
    //        $orders = Order::with('detail');
    //        return new OrderCollection($orders->get());
    //        return Order::all();
    //    }
}