<?php

namespace App\Onix\Blocks;

use Mariojgt\Onix\Helpers\BaseOnixBlocks;

class Test extends BaseOnixBlocks
{
    public string $componentId = 'test';
    public string $label = 'test';
    public string $mediaPath = 'home-test';
    public string $contentPath = 'test';
    public string $category = 'test';
}
