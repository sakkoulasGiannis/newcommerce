<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use App\Models\Field;
use Illuminate\Support\Facades\Schema;

class FieldGenerate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'field:make {name=name} {title=title} {type=select} {erp_id=erp_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new Field';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $title = $this->argument('title')?? null;
        $name = $this->argument('name');
        $type = $this->argument('type');
        $erp_id = $this->argument('erp_id') ?? null;

        $this->field($name);
        $this->appendCodeToFIeldTrait($name, $type);
        $this->createField($name, $title, $type, $erp_id);
     }

    protected function getStub($type)
    {
        return file_get_contents(resource_path("stubs/$type.stub"));
    }
    protected function field($name)
    {

        $fileName = 'Field' . ucFirst($this->convertToCamelCase($name));


        $fieldTemplate = str_replace(
            [
                '{{className}}',
                '{{tableName}}',
                '{{columnName}}',
                '{{name}}',
            ],
            [
                'Field'.ucfirst($this->convertToCamelCase($name)),
                strtolower($this->convertToCamelCase($name)),
                strtolower(str_plural($this->convertToCamelCase($name))),
                strtolower($name),
            ],
            $this->getStub('newField')
        );

        if(!file_exists($path = app_path('/Http/Requests')))
            mkdir($path, 0777, true);

        if(!file_exists($fieldpath = app_path('/Fields')))
            mkdir($fieldpath, 0777, true);

        file_put_contents(app_path("Fields/{$fileName}.php"), $fieldTemplate);
    }
    function convertToCamelCase($string) {
        // Αρχικά αντικαθιστούμε τους χαρακτήρες '-' και '_' με κενά
        $string = str_replace(['-', '_'], ' ', $string);

        // Μετατρέπουμε τη συμβολοσειρά σε "Title Case" (Πρώτο γράμμα κάθε λέξης κεφαλαίο)
        $string = ucwords($string);

        // Αφαιρούμε τα κενά και συνενώνουμε τις λέξεις
        $string = str_replace(' ', '', $string);

        return $string;
    }
    function convertToFieldNameCase($string) {
        // Αρχικά αντικαθιστούμε τους χαρακτήρες '-' και '_' με κενά
        $string = str_replace(['-',' '], '_', $string);

        // Μετατρέπουμε τη συμβολοσειρά σε "Title Case" (Πρώτο γράμμα κάθε λέξης κεφαλαίο)
        $string = ucwords($string);

        // Αφαιρούμε τα κενά και συνενώνουμε τις λέξεις
//        $string = str_replace(' ', '', $string);

        return $string;
    }

    public function appendCodeToFIeldTrait($name, $type)
    {
        $fieldName = 'field' . ucFirst($this->convertToCamelCase($name));
        $name = ucFirst($name);
        if(!file_exists($path = app_path('../app/Traits/')))
            mkdir($path, 0777, true);

        if(!file_exists($traitpath = app_path('Http/Traits/')))
            mkdir($traitpath, 0777, true);

        if(!file_exists( app_path('Http/Traits/FieldTrait.php'))){
            $this->info('FieldTrait not exist');
            $fieldtrait = app_path('Http/Traits/FieldTrait.php');
            $srcfile = resource_path("stubs/FieldTrait.stub");
            copy($srcfile, $fieldtrait);
            $this->info('create FieldTrait');
        }

        $file_path = app_path('Http/Traits/FieldTrait.php');
        $insert_marker = '/\/\#### INSERT DYNAMIC FIELD CODE ####/';
        if($type == 'textarea'){
            $this->info('create belongs to hasOne field');
            $method = file_get_contents(resource_path("stubs/customProductHasOneMethod.stub"));
        }else{
            $this->info('create belongs to many field');
            $method = file_get_contents(resource_path("stubs/customProductMethod.stub"));
        }

        $method = str_replace('{{fieldName}}', ucfirst($this->convertToCamelCase($name)), $method);
        $method = str_replace('{{namePlural}}', str_plural(strtolower($this->convertToCamelCase($name))), $method);
        $method = str_replace('{{name}}', strtolower($this->convertToFieldNameCase($name)), $method);

         $this->insert_into_file($file_path, $insert_marker, $method, true);
        $this->info('append code to Product Trait');
    }
    public function insert_into_file($file_path, $insert_marker, $text, $after = true)
    {
        $contents = file_get_contents($file_path);
        $new_contents = preg_replace($insert_marker, ($after) ? '$0' . $text : $text . '$0', $contents);
        return file_put_contents($file_path, $new_contents);
    }

    public function createField($name, $title, $type, $erp_id){

        if (!Schema::hasTable('field_' . str_plural($this->convertToCamelCase($name)))) {
            $this->info('start create field with type '.$type);
            Field::create(
                [
                    'type' => $type,
                    'erp_id' => $erp_id,
                    'model_type' => "\App\Fields\Field".ucFirst($this->convertToCamelCase($name)),
//                    'model' => 'field_' . str_plural(strtolower($this->convertToCamelCase($name))),
                    'title' => ($title) ? ucFirst($title) : ucFirst($name),
                    'name' => strtolower($this->convertToFieldNameCase($name)),
                    'position' =>1,
                ]
            );

            if($type == 'text' || $type == 'select'){
                Schema::create('field_' . str_plural(strtolower($this->convertToCamelCase($name))), function ($table) {
                    $table->bigIncrements('id');
    //                $table->foreignId('product_id')->constrained()->onDelete('cascade');
                    $table->string('erp_id')->nullable();
                    $table->string('title')->required();
                    $table->string('name')->unique();
                    $table->string('color')->nullable();
                    $table->string('image')->nullable();
    //                $table->integer('attribute_field');
                    $table->timestamps();
                });

                $tableName = strtolower($this->convertToCamelCase($name));
                Schema::create('field_' . str_plural(strtolower($this->convertToCamelCase($name))).'_product', function ( $table) use ($tableName) {
                    $table->timestamps();
                });

                Schema::table('field_' . str_plural(strtolower($this->convertToCamelCase($name))).'_product', function($table) use ($tableName)  {
                    $table->foreignId('product_id')->before('updated_at')->constrained()->onDelete('cascade');

                    $table->foreignId('field_' . $tableName . '_id')
                        ->before('updated_at')
                        ->constrained()
                        ->onDelete('cascade')
                        ->name('fk_' . $tableName . '_id'); // Συντομότερο όνομα για το foreign key constraint

                    $table->primary(['product_id', 'field_' . $tableName.'_id']);
                });
            }elseif($type == 'textarea'){

                Schema::create('field_' . str_plural($name), function ($table)   {
                    $table->id();
                    $table->text('body')->nullable();
                    //                $table->integer('attribute_field');
                    $table->foreignId('product_id')->before('updated_at')->constrained()->onDelete('cascade');
                    $table->timestamps();
                });
            }



        } else {
            \Session::flash('flash_message', 'Table already exist.');
        }

    }
}
