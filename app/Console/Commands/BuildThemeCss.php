<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class BuildThemeCss extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:run-theme-build {folderName}';
    protected $description = 'Run npm build in the specified theme folder';


    /**
     * Execute the console command.
     */
    public function handle()
    {
        $folderName = $this->argument('folderName');
        $scriptPath = base_path('run-build.js'); // Path to your Node.js script

        $command = "node $scriptPath $folderName";
        $this->info("Executing command: $command");

        $output = [];
        $returnValue = 0;

        exec($command, $output, $returnValue);

        foreach ($output as $line) {
            $this->info($line);
        }

        if ($returnValue !== 0) {
            $this->error("Error: Command failed with exit code $returnValue.");
        }

    }
}
