<?php

namespace App\Console\Commands;

use App\Models\Field;
use Illuminate\Console\Command;

class resetCommerce extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:reset-commerce';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $fields = Field::all();
        foreach($fields as $f){
            $this->info('Deleting field: ' . $f->name);
            $this->call('field:delete', ['name' => $f->name]);
        }
        $this->info('Deleting all products');
        $this->call('migrate:fresh');
        $this->info('start Seeding');
        $this->call('db:seed');
    }
}
