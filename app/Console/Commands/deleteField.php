<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;
use App\Models\Field;
class deleteField extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'field:delete {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete field';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');
        $this->info('delete field' . $name);
        $this->deleteTable($name);
        $this->removeField(ucfirst($name));
        $this->deleteRecord($name);
    }

    /*
    * remove the method in the fieldtrait
    */
    public function removeField($name) {
        $this->info('remove method in fieldtrait');
        $name = ucFirst($name);

        try {
            // remove file
            $fieldFile = app_path('Fields/Field' . ucFirst($this->convertToCamelCase($name)) . '.php');
            if (file_exists($fieldFile)) {
                unlink($fieldFile);
            } else {
                throw new \Exception("File $fieldFile does not exist.");
            }

            // remove method from FieldTrait
            $blockStart = 'field_' . $this->convertToCamelCase($name);
            $blockEnd = 'end_field_' . $this->convertToCamelCase($name);
            $filePath = app_path('Http/Traits/FieldTrait.php');
            if (!file_exists($filePath)) {
                throw new \Exception("File $filePath does not exist.");
            }

            $file = file_get_contents($filePath);
            if ($file === false) {
                throw new \Exception("Failed to read file $filePath.");
            }

            $newFile = preg_replace('/\/\/' . $blockStart . '[\s\S]+?' . $blockEnd . '/', '', $file);
            $newFile = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $newFile);

            if (file_put_contents($filePath, $newFile) === false) {
                throw new \Exception("Failed to write to file $filePath.");
            }

            $this->info('end remove method in fieldtrait');
            return true;
        } catch (\Exception $e) {
            $this->error('Error in removeField method: ' . $e->getMessage());
            return false;
        }
    }


    public function deleteTable($name){
        Schema::dropIfExists('field_' . str_plural($name).'_product');
        Schema::dropIfExists('field_' . str_plural($name));
        $this->info('deleting tables');
    }
    public function deleteRecord($name){
        $field  = Field::where('name', $name)->first();
        if($field){
            Field::destroy($field->id);
            $this->info('destroy record');
        }

    }
    function convertToCamelCase($string) {
        // Αρχικά αντικαθιστούμε τους χαρακτήρες '-' και '_' με κενά
        $string = str_replace(['-', '_'], ' ', $string);

        // Μετατρέπουμε τη συμβολοσειρά σε "Title Case" (Πρώτο γράμμα κάθε λέξης κεφαλαίο)
        $string = ucwords($string);

        // Αφαιρούμε τα κενά και συνενώνουμε τις λέξεις
        $string = str_replace(' ', '', $string);

        return $string;
    }

}
