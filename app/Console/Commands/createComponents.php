<?php

namespace App\Console\Commands;

use App\Models\ThemeComponent;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use DOMDocument;

class createComponents extends Command
{
    protected $signature = 'app:create-components';
    protected $description = 'Command description';

    public function handle()
    {
        $this->parseThemeTemplates();
    }

    public function domNodeToArray($node, $fileName)
    {
        $output = [];
        $output['id'] = time().rand(10,10000);
        $output['tag'] = $node->nodeName;
        $output['class'] = $node->attributes->getNamedItem('class') ? $node->attributes->getNamedItem('class')->value : null;
        $output['style'] = $node->attributes->getNamedItem('style') ? $node->attributes->getNamedItem('style')->value : null;
        $output['attributes'] = [];

        if ($node->attributes) {
            foreach ($node->attributes as $attr) {
                $output['attributes'][$attr->nodeName] = $attr->nodeValue;
            }
        }

        $output['children'] = [];
        $textContent = '';

        foreach ($node->childNodes as $childNode) {
            if ($childNode->nodeType === XML_ELEMENT_NODE) {
                $output['children'][] = $this->domNodeToArray($childNode, $fileName);
            } elseif ($childNode->nodeType === XML_TEXT_NODE) {
                $textContent .= trim($childNode->nodeValue);
            }
        }

        if (!empty($textContent)) {
            $output['value'] = $textContent;
        } else {
            $output['value'] = null;
        }

        return $output;
    }

    public function htmlToStructure($html, $fileName)
    {
        $dom = new DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($html);
        libxml_clear_errors();

        $body = $dom->getElementsByTagName('body')->item(0);
        $structure = $this->domNodeToArray($body, $fileName);

        return $structure['children'];
    }

    public function parseThemeTemplates()
    {
        $path = resource_path('themeTemplates');
        $files = File::files($path);

        $themes = [];

        foreach ($files as $file) {
            $htmlContent = File::get($file->getPathname());

            // Εξαγωγή και εκτύπωση μεταδεδομένων για debugging
            $metadata = $this->extractMetadata($htmlContent);
            $this->info("Metadata for " . $file->getFilename() . ": " . json_encode($metadata));

            // Καθαρισμός του HTML από τα μεταδεδομένα σχόλια
            $cleanedHtml = preg_replace('/<!--.*?-->/s', '', $htmlContent);

            // Μετατροπή του HTML σε δομή
            $themeStructure = $this->htmlToStructure($cleanedHtml, $file->getFilename());

            // merge metadata and structure
            $data = array_merge($metadata, $themeStructure[0]);
            $themeData = $data;



            ThemeComponent::updateOrCreate(['name' => $file->getFilename(),],[
                'name' => $file->getFilename(),
                'data' => json_encode($themeData),
            ]);

            $themes[$file->getFilename()] = $themeData;
        }

        return response()->json($themes);
    }

    // Εξαγωγή μεταδεδομένων από σχόλια HTML
    private function extractMetadata($content)
    {
        $metadata = [];

        // Αναζήτηση πρώτου σχολίου HTML και εξαγωγή του
        if (preg_match('/<!--(.*?)-->/s', $content, $matches)) {
            $lines = explode("\n", trim($matches[1]));

            foreach ($lines as $line) {
                if (strpos($line, ':') !== false) {
                    list($key, $value) = explode(':', $line, 2);
                    $metadata[trim($key)] = trim($value);
                }
            }
        }

        return $metadata;
    }
}
