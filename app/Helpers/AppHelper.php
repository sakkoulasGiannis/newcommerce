<?php

namespace App\Helpers;

use App\Models\Attribute;
use App\Models\AttributeTerm;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class AppHelper
{
    public static function instance()
    {
        return new AppHelper();
    }

    /**
     * @param $query
     * @param $products
     * @return mixed
     */
    public static function querymanager($query, $products)
    {

        //			foreach ($queries as $query) {
        if (isset($query['label'])) {

            switch ($query['id']) {

                case 'category':
                    $productincategories = DB::table('product_category')
                        ->whereIn('category_id', $query['value'])
                        ->groupBy('product_id')
                        ->pluck('product_id');

                     return $products->whereIn('id', $productincategories);
                    break;
                case 'sku':
                    return $products->where('sku', $query['operator'], $query['value']);
                    break;
                case 'price':
                     return $products->where('price', $query['operator'], $query['value']);
                    break;
                case 'groupecolor':
                     $val = self::getFieldName('groupecolors', $query['value']);
                     return $products->filter('groupecolor', $val);
                    break;
                case 'brand':
                      $val = self::getFieldName('brands', $query['value']);
                      return $products->filter('brand', $val);
                     break;
                case 'color':
                    $val = self::getFieldName('colors', $query['value']);
                     return $products->filter('color', $val);
                    break;
                default:

                    $q = $query;
                    return $products->whereHas('attribute_terms', function ($query) use ($q) {
                        $query
//                        ->where('attribute_id', $attributeId)
                            ->whereIn('attribute_term_id', $q['value']);
                    });


//                    if ($query['matchOparator'] == 'or') {
//                        return $products->orWhereHas($query['id'], function ($q) use ($query) {
//                            $q->where('id', $query['operator'], $query['value']);
//                        });
//                    } else {
//                        return $products->whereHas($query['id'], function ($q) use ($query) {
//                            $q->where('id', $query['operator'], $query['value']);
//                        });
//                    }
            }
        } else {
            $querys = $query;

            foreach ($querys as $query) {
                switch ($query['label']) {
                    case 'Category':
                        $productincategories = DB::table('product_category')->whereIn('category_id', $query['value'])->groupBy('product_id')->pluck('product_id');
                        $products = $products->whereIn('id', $productincategories);
                        break;
                    case 'Sku':
                        $products = $products->where('sku', $query['operator'], $query['value']);
                        break;
                    case 'Price':

                        $products = $products->where('price', $query['operator'], $query['value']);
                        break;

                    case 'color':
                        $products = $products->filter('color', $query['value']);
                        break;

                    case 'created_at':
                    case 'Ημ/νια Δημιουργίας':

                        $products = $products->where('products.created_at', $query['operator'], now()->subDays($query['value'])->endOfDay());
                        break;

                    default:
                        if ($query['matchOparator'] == 'or') {

//                            $attributeId = Attribute::find($query['id']);
//                            dd($query);

                            $products->whereHas('attribute_terms', function ($q) use ($query) {
                                $q
                                    //                        ->where('attribute_id', $attributeId)
                                    ->whereIn('attribute_term_id', $query['value']);
                            });

//                            $products = $products->orWhereHas($a->name, function ($q) use ($query) {
//                                $q->where('id', $query['operator'], $query['value']);
//                            });
                        } else {
                            $products = $products->whereHas($query['id'], function ($q) use ($query) {
                                $q->where('id', $query['operator'], $query['value']);
                            });
                        }
                }
            }
        }


        //			}

        return $products;
    }

    static function getFieldName($field, $id)
    {
        if (is_array($id))
            return DB::table('field_' . $field)->whereIn('id', $id)->pluck('name')->toArray();
        else
            return DB::table('field_' . $field)->where('id', $id)->first()->name;
    }

}
