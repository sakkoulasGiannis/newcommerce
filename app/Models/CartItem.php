<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    use HasFactory;
    protected $fillable = ['quantity', 'price', 'itemable_id', 'itemable_type', 'group_id'];

   public function cart()
    {
        return $this->belongsTo(Cart::class);
    }

    public function stock()
    {
        return $this->morphTo('itemable', 'itemable_type', 'itemable_id');
    }
}
