<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Courier;
class OrderDetail extends Model
{
    protected $casts = [
//    'shipping_id' => 'integer',
];


	protected $fillable = [
		'order_date',
		'status',
		'invoice_id',
		'payment_id',
		'product_id',
		'shipping_id',
		'total',
		'sub_total',
		'shipping_cost',
		'payment_cost',
		'user_id',
		'billing_name',
		'billing_email',
		'billing_lastname',
		'billing_country_id',
		'billing_region',
		'billing_city',
		'billing_street',
		'billing_zip',
		'billing_street_number',
		'billing_phone',
		'billing_mobile',
		'shipping_country_id',
		'shipping_region',
		'shipping_city',
		'shipping_street',
		'shipping_zip',
		'shipping_street_number',
		'shipping_phone',
		'shipping_mobile',
		'shipping_email',
		'shipping_name',
		'shipping_lastname',
		'invoice_status',
		'courier_name',
		'payment_name',
		'afm',
		'doy',
		'company',
		'order_id',
		'activity',
	];

	public function order()
	{
		return $this->belongsTo(Order::class, 'id', 'order_id');
	}

    public function country()
    {
        return $this->belongsTo(Country::class, 'billing_country_id', 'id');
    }

	public function products()
	{
		return $this->hasMany(OrderProduct::class, 'order_detail_id', 'id');
	}

	public function vouchers()
	{
		return $this->hasMany(OrderVoucher::class, 'order_detail_id', 'id');
	}

    public function courier()
    {
        return $this->belongsTo(Courier::class, 'shipping_id', 'id');
    }
}
