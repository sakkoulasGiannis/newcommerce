<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ComboProductItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'combo_product_id',
        'product_id',
        'quantity',
        'sold_separately',
    ];
    protected $table = 'combo_product_items';
    public function comboProduct()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    // Εδώ ορίζετε τη σχέση για το product_id
    public function product()
    {
        return $this->belongsTo(Product::class, 'combo_product_id');
    }
}
