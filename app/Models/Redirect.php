<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Redirect extends Model
{
    use HasFactory;

    protected $fillable = [
        'old_url',
        'item_id',
        'type',
        'method',
        'sku',
        'entity_id',
        'entity_type'
    ];

    public function entity()
    {
        return $this->morphTo();
    }


}
