<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductView extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_id',
        'user_id',
        'ip_address',
        'user_agent',
        'referer',
        'device',
        'platform',
        'browser',
        'language',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
