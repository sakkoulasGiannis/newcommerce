<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Courier extends Model {
	use HasFactory , HasTranslations;

	protected $fillable = ['name', 'description', 'order', 'amount', 'order'];
	public $translatable = ['title', 'description'];

//	public function order() {
//		return $this->belongsToMany(OrderDetail::class);
//	}

    public function orders() {
        return $this->hasMany(OrderDetail::class);
    }
}
