<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ThemeComponent extends Model
{
    use HasFactory;
    protected $fillable = ['page_type', 'name', 'html', 'css', 'js', 'template'];

    public function theme()
    {
        return $this->belongsTo(Theme::class);
    }
}
