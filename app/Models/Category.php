<?php

namespace App\Models;

use App\Models\Language;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
class Category extends Model
{
    use HasTranslations;
     private $languages ;
     protected $availableLocales;
     protected $fillable = [
        'parent_id',
        'erp_id',
        'title',
        'name',
        'active',
        'hidden',
        'description',
        'meta_title',
        'meta_description',
        'order',
        'dynamic',
        'query',
        'mega_menu_is_active',
        'dynamic_query_is_active',
        'mega_menu_content',
        'dynamic_query_content'
    ];
    public $translatable = ['title', 'description','meta_description','meta_title'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->availableLocales = Language::all()->pluck('name');
    }

    public function redirects()
    {
        return $this->morphMany(Redirect::class, 'entity');
    }


    public function toArray()
    {
        $array = parent::toArray();
        if (request()->is('manage*')) {


            // Προσθήκη των μεταφρασμένων πεδίων στο array
            foreach ($this->translatable as $field) {
                foreach ($this->availableLocales as $locale) {
                    // Εάν η μετάφραση δεν υπάρχει, ορίζουμε κενό string
                    $array[$field][$locale] = $this->getTranslation($field, $locale) ?? null;
                }
            }
        }
        return $array;
    }

    // public function getQuery()
    // {
    //     return json_encode($this->query);
    // }


    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_category');
    }

    public function childs()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id')->orderBy('order', 'asc');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id')->orderBy('order', 'asc');
    }

    public function subChildren()
    {
        return $this->children();
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id', 'id');
    }
}
