<?php

namespace App\Models;

use App\Models\Field;
use App\Fields\FieldWidth;
use Illuminate\Database\Eloquent\Model;

class FieldValues extends Model
{

    protected $table = 'field_values';

    public function field()
    {
        return $this->belongsTo(Field::class, 'id', 'field_id');
    }

    public function width(){
        return $this->belongsToMany(FieldWidth::class, 'product_width', 'field_value_id', 'id', 'id');
    }

}
