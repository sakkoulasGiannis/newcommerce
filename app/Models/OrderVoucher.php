<?php
	
	namespace App\Models;
	
	use Illuminate\Database\Eloquent\Factories\HasFactory;
	use Illuminate\Database\Eloquent\Model;
	
	class
	
	OrderVoucher extends Model
	{
		use HasFactory;
		
		public $fillable = [
			'order_detail_id',
			'voucher_id',
			'type',
			'name',
			'value',
		];
		
		public function detail()
		{
			return $this->belongsTo(OrderDetail::class, 'id', 'order_detail_id');
		}
	}
