<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;

use Spatie\MediaLibrary\InteractsWithMedia;


class OrderProduct extends Model implements HasMedia
{

        protected $primaryKey = 'product_id';


     use InteractsWithMedia;

    public $fillable = [
        'stock_id',
        'product_id',
        'associated_id',
        'name',
        'sku',
        'price',
        'quantity',
        'order_detail_id',
        'order_id',
    ];

    public function order()
    {
        return $this->belongsTo(OrderDetail::class, 'id', 'order_detail_id');
    }

    public function Associated()
    {
        return $this->belongsTo(Associated::class, 'id', 'associated_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}
