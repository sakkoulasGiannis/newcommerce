<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{

    protected $fillable = [
        'title',
        'name',
        'type',
        'model_type',
        'positions',
        'position',
        'can_select_multiple',
        'is_read_only',
        'erp_id',
        'has_colors',
        'has_images',
        'is_enabled',
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'field_product', 'field_id', 'product_id');
    }


    //@todo delete this ;;;;
    // public function values($query)
    // {
    //     $modelType = $this->model_type;

    //     return $modelType::all();
    //     // return $this->hasMany(\App\Fields\FieldWidth::class, 'field_id', 'id');
    // }
}
