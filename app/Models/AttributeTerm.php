<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class AttributeTerm extends Model
{
    use HasFactory;
    use HasTranslations;

    public $translatable = ['title'];
    protected $availableLocales;
    protected $table = "attribute_terms";

    protected $fillable = [
        'erp_id',
        'attribute_id',
        'name',
        'title',
        'term_order',
        'color',
        'image',
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->availableLocales = Language::all()->pluck('name');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'attribute_product_terms', 'attribute_term_id', 'product_id');
    }

    public function attribute()
    {
        return $this->belongsTo(Attribute::class);
    }
    public function attributeProductTerms()
    {
        return $this->hasMany(AttributeProductTerm::class, 'product_id');
    }

    /**
     * fix missing translations
     * @return array
     */
    public function toArray()
    {
        $array = parent::toArray();

        // Προσθήκη των μεταφρασμένων πεδίων στο array
        foreach ($this->translatable as $field) {
            foreach ($this->availableLocales as $locale) {
                // Εάν η μετάφραση δεν υπάρχει, ορίζουμε κενό string
                $array[$field][$locale] = $this->getTranslation($field, $locale) ?? null;
            }
        }

        return $array;
    }
}
