<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Country extends Model {
	use HasFactory, HasTranslations;

    public $translatable = ['title'];

	protected $fillable = [
		'title', 'iso', 'is_active', 'currency', 'currency_title', 'payment_rules',
	];

	public function Counties() {
		return $this->hasMany(County::class, 'country_id', 'id');
	}

}
