<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Attribute extends Model
{
    use HasFactory;
    use HasTranslations;
    public $translatable = ['title'];
    protected $availableLocales;

    protected $fillable = [
        'title',
        'erp_id',
        'name',
        'type',
        'order_by',
        'visible_on_product_page',
        'used_for_variations',
        'highlight_attribute',
        'is_enabled',
        'has_images',
        'has_colors',
        'can_select_multiple',
    ];
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->availableLocales = Language::all()->pluck('name');
    }

    public function toArray()
    {
        $array = parent::toArray();

        // Προσθήκη των μεταφρασμένων πεδίων στο array
        foreach ($this->translatable as $field) {
            foreach ($this->availableLocales as $locale) {
                // Εάν η μετάφραση δεν υπάρχει, ορίζουμε κενό string
                $array[$field][$locale] = $this->getTranslation($field, $locale) ?? null;
            }
        }

        return $array;
    }

    public function index(Request $request)
    {
        $attributes = Attribute::with('terms')
            ->where('name', 'LIKE', '%' . $request->get('q') . '%')
            ->get();
        return response()->json($attributes);
    }

    public function product_terms()
    {
        return $this->hasMany(AttributeProductTerm::class, 'attribute_id');
    }


    public function products()
    {
        return $this->belongsToMany(Product::class, 'attribute_products');
    }

    public function terms()
    {
        return $this->hasMany(AttributeTerm::class, 'attribute_id');
    }
    public function attributeTerms()
    {
        return $this->hasMany(AttributeTerm::class);
    }


}
