<?php

namespace App\Models;

use App\Http\Traits\FieldTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;

use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Translatable\HasTranslations;
use App\Scopes\ActiveProducts;

class Product extends Model implements HasMedia, Auditable
{

    use \OwenIt\Auditing\Auditable;

    public function getPrice(): int
    {
        return $this->price;
    }

    //@todo gdpr
    use HasFactory;
    use Searchable;
    use FieldTrait;
    use HasTranslations;
    use InteractsWithMedia;

    public $translatable = ['title', 'description', 'small_description', 'meta_title', 'meta_description'];
    protected $availableLocales;
    protected $fillable = [
        'weight',
        'length',
        'width',
        'height',
        'title',
        'name',
        'small_description',
        'description',
        'sku',
        'product_type',
        'price',
        'sale_price',
        'meta_title',
        'meta_description',
        'rule_id',
        'cross_sell_method',
        'sort',
        'group',
        'is_featured',
        'product_is_active',
        'tax_class_id',
        'new_from',
        'new_to',
        'old_id'
    ];

    protected $appends = ['image', 'thumb'];


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->availableLocales = Language::all()->pluck('name');
    }



    protected static function boot()

    {
        parent::boot();
//        static::addGlobalScope(new ActiveProducts);
    }



    /**
     * fix missing translations
     * @return array
     */
    public function toArray()
    {
        $array = parent::toArray();

        // Προσθήκη των μεταφρασμένων πεδίων στο array
        foreach ($this->translatable as $field) {
            foreach ($this->availableLocales as $locale) {
                // Εάν η μετάφραση δεν υπάρχει, ορίζουμε κενό string
                $array[$field][$locale] = $this->getTranslation($field, $locale) ?? null;
            }
        }

        return $array;
    }
//    protected static function booted()
//    {
//        static::addGlobalScope(new ActiveProducts);
//    }


    public function priceHistory()
    {
        return $this->hasMany(ProductPriceHistory::class);
    }

    public function crossSells()
    {
        return $this->hasManyThrough(
            Product::class,
            CrossSellProduct::class,
            'product_id', // Foreign key on CrossSellProduct table...
            'id', // Foreign key on Product table...
            'id', // Local key on Product table...
            'cross_sell_product_id' // Local key on CrossSellProduct table...
        );
    }

    public function views()
    {
        return $this->hasMany(ProductView::class);
    }

    public function crossSell()
    {
        return $this->hasMany(CrossSellProduct::class);
    }

    public function combos()
    {
        return $this->hasMany(ComboProductItem::class, 'product_id', 'id');
    }

    // Εδώ ορίζετε τη σχέση για το product_id
    public function comboProductItemsAsProduct()
    {
        return $this->hasMany(ComboProductItem::class, 'combo_product_id');
    }

    public function images()
    {
        return $this->media()->where('collection_name', 'product');
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(326)
            ->height(320)
            ->sharpen(10);
    }

    public function firstGalleryMedia(): MorphOne
    {
        return $this->morphOne(config('media-library.media_model'), 'model');
    }

    //@todo  το χρειάζομαι αυτό ;
    public function getMainImageUrlAttribute(): string
    {

        return $this->firstGalleryMedia->getUrl();
    }


    /**
     * Get the index name for the model.
     */
    public function searchableAs()
    {
        return 'product_index';
    }

    public function getImageAttribute()
    {

        //        if ($this->getMedia('default')->first())
        //            return $this->getFirstMediaUrl();
        //        return null;
        return $this->getFirstMediaUrl('product');
    }
    public function getThumbAttribute()
    {

        //        if ($this->getMedia('default')->first())
        //            return $this->getFirstMediaUrl();
        //        return null;
        return $this->getFirstMediaUrl('product', 'thumb');
    }


    // scout search fields
    public function toSearchableArray()
    {

        //        if ($this->stock()->where('quantity', '>', 0)->count() < 1) {
        //            return;
        //        }
        //
        $productFieds = [];
        //        foreach (Field::where('position', 2)->get() as $f) {
        //            $field = $f->name;
        //            $data = $this->$field->pluck('title');
        //            if (count($data) > 0) {
        //                $productFieds[$f->title] = $data;
        //            }
        //        }


        return $array = [
            'title' => $this->title,
            'name' => $this->name,

            //            'greeklish' => $this->greek_to_greeglish($this->title),
            'description' => $this->description,
            'price' => $this->price,
            'originalPrice' => $this->originalPrice,
            'sale_price' => $this->sale_price,
            'product_is_active' => $this->product_is_active,
            'sku' => $this->sku,
            'categories' => $this->categories()->pluck('title')->toArray(),
            //            'stock' => $this->variations(),
            'image' => $this->getFirstMediaUrl('product'),
            'thumb' => $this->getFirstMediaUrl('product', 'thumb'),
            //            'color' => ($this->color->first()) ? $this->color->first()->title : null,
            "attributes" => app()->call('App\Http\Controllers\ProductController@attributeValues', ['product' => $this])->toArray(),
        ];

        // Customize the data array...

        return array_merge($array, $productFieds);
    }


    public function greek_to_greeglish($string)
    {
        $g2l["ου"] = "ou";
        $g2l["Ου"] = "Ou";
        $g2l["ού"] = "ou";
        $g2l["Ού"] = "Ou";
        $g2l["α"] = "a";
        $g2l["ά"] = "a";
        $g2l["Α"] = "A";
        $g2l["Ά"] = "A";
        $g2l["β"] = "v";
        $g2l["Β"] = "V";
        $g2l["γ"] = "g";
        $g2l["Γ"] = "G";
        $g2l["δ"] = "d";
        $g2l["Δ"] = "D";
        $g2l["ε"] = "e";
        $g2l["έ"] = "e";
        $g2l["Ε"] = "E";
        $g2l["Έ"] = "E";
        $g2l["ζ"] = "z";
        $g2l["Ζ"] = "Z";
        $g2l["η"] = "i";
        $g2l["ή"] = "i";
        $g2l["Η"] = "I";
        $g2l["Ή"] = "I";
        $g2l["θ"] = "th";
        $g2l["Θ"] = "Th";
        $g2l["ι"] = "i";
        $g2l["ί"] = "i";
        $g2l["ϊ"] = "i";
        $g2l["ΐ"] = "i";
        $g2l["Ι"] = "I";
        $g2l["Ί"] = "I";
        $g2l["Ϊ"] = "I";
        $g2l["κ"] = "k";
        $g2l["Κ"] = "K";
        $g2l["λ"] = "l";
        $g2l["Λ"] = "L";
        $g2l["μ"] = "m";
        $g2l["Μ"] = "M";
        $g2l["ν"] = "n";
        $g2l["Ν"] = "N";
        $g2l["ξ"] = "ks";
        $g2l["Ξ"] = "Ks";
        $g2l["ο"] = "o";
        $g2l["ό"] = "o";
        $g2l["Ο"] = "O";
        $g2l["Ό"] = "O";
        $g2l["π"] = "p";
        $g2l["Π"] = "P";
        $g2l["ρ"] = "r";
        $g2l["Ρ"] = "R";
        $g2l["σ"] = "s";
        $g2l["ς"] = "s";
        $g2l["Σ"] = "S";
        $g2l["τ"] = "t";
        $g2l["Τ"] = "T";
        $g2l["υ"] = "u";
        $g2l["ύ"] = "u";
        $g2l["ϋ"] = "u";
        $g2l["ΰ"] = "u";
        $g2l["Υ"] = "U";
        $g2l["Ύ"] = "U";
        $g2l["Ϋ"] = "I";
        $g2l["φ"] = "f";
        $g2l["Φ"] = "F";
        $g2l["χ"] = "h";
        $g2l["Χ"] = "H";
        $g2l["ψ"] = "ps";
        $g2l["Ψ"] = "Ps";
        $g2l["ω"] = "o";
        $g2l["ώ"] = "o";
        $g2l["Ω"] = "O";
        $g2l["Ώ"] = "O";
        // $g2l[" "] = "_";
        $g2l["-"] = "_";
        $g2l["."] = "_";
        $g2l["/"] = "_";
        $output = strtr($string, $g2l);

        return $output;
    }

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class, 'attribute_products');
    }

    public function attribute_terms()
    {
        return $this->hasMany(AttributeProductTerm::class, 'product_id');
    }
    public function attributeProducts()
    {
        return $this->hasMany(AttributeProduct::class);
    }


    public function attributeProductTerms()
    {
        return $this->hasMany(AttributeProductTerm::class, 'product_id');
    }

    public function attributeTerms()
    {
        return $this->hasManyThrough(
            AttributeTerm::class,
            AttributeProductTerm::class,
            'product_id', // Foreign key on AttributeProductTerm table...
            'id', // Foreign key on AttributeTerm table...
            'id', // Local key on Product table...
            'attribute_term_id' // Local key on AttributeProductTerm table...
        );
    }

//    public function getAttribute($attributeName)
//    {
//        // Βρίσκουμε το Attribute βάσει ονόματος (π.χ. 'brand')
//        $attribute = Attribute::where('name', $attributeName)->first();
//
//        if (!$attribute) {
//            return null; // Αν δεν βρεθεί το attribute, επιστρέφει null
//        }
//
//        // Βρίσκουμε την εγγραφή στο attribute_products
//        $attributeProduct = $this->attributeProducts()
//            ->where('attribute_id', $attribute->id)
//            ->first();
//return $attributeProduct;
//        if (!$attributeProduct) {
//            return null; // Αν δεν υπάρχει σύνδεση μεταξύ του product και του attribute
//        }
//
//        // Τέλος, επιστρέφουμε την τιμή από το attribute_product_terms
//        return AttributeTerm::where('attribute_product_id', $attributeProduct->id)->first()->value ?? null;
//    }


    public function terms()
    {
//        return $this->hasMany(AttributeProductTerm::class, 'product_id');
        return $this->belongsToMany(AttributeTerm::class, 'attribute_product_terms', 'product_id', 'attribute_term_id');
//         return $this->hasMany(AttributeProductTerm::class, 'product_id', 'id');
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('product');
        // you can define as many collections as needed
        $this->addMediaCollection('thumb');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'product_category')->withTimestamps();
    }

    public function rules()
    {
        return $this->hasOne(Rule::class, 'id', 'rule_id');
    }

    public function stock()
    {

        return $this->hasMany(Stock::class, 'product_id', 'id');
        return $this->hasMany(Stock::class, 'product_id', 'id')->with('variations');
        //        return $this->hasMany(Stock::class)->with('variations');
    }

    // επιστρέφει όλα τα variations
    public function scopeVariations()
    {

        $this->stock()->where('quantity', '>', 0)->pluck('model_title');
        $variants = $this->hasManyThrough(StockVariation::class, Stock::class)->get();
        $variationFields = [];
        foreach ($variants as $v) {
            $field = Field::find($v->field_id)->title;
            $model = $v->model_type;
            $value = $model::find($v->model_id)->title;
            if ($v->stock->quantity > 0) {
                $variationFields[] = $value;
            }
        }

        return $variationFields;
    }
    public function redirects()
    {
        return $this->morphMany(Redirect::class, 'entity');
    }

    public function fields()
    {
        return $this->belongsToMany(Field::class, 'field_product', 'product_id', 'field_id');
    }

    public function whishlist()
    {
        return $this->belongsToMany(WishList::class, 'wish_lists', 'product_id', 'id');
    }

    public function scopeVariants()
    {

        return $this->hasManyThrough(StockVariation::class, Stock::class);
    }

    // public function values() {
    // 	return $this->belongsToMany(FieldValues::class, 'product_values', 'product_id', 'id');
    // }

    public function calculator()
    {
        return $this->hasOne(Calculator::class);
    }

    public function scopeHasStock($query)
    {

        return $query->whereHas('stock', function ($query) {
            $query->where('quantity', '>', 0);
        });
    }

    /**
     * return new price if product has discount
     *
     * @param $value
     * @return mixed
     */
    public function getPriceAttribute($value)
    {
        //

        if (request()->is('manage/*')) {
            return $value;
        } else {

            // check user role is b2b
            if (auth()->user() && auth()->user()->hasRole('b2b')) {
                $this->oldPrice = false;
//                $this->discount = ;
//                $discount = $value - $discountValue;
//                $this->originalPrice = $value;
//                return round($discount);
                return $value;
            }

            $this->oldPrice = false;
            if ($this->sale_price > 0) {
                $this->originalPrice = $value;
                return $this->sale_price;
            } elseif ($this->rules) {
                $discountValue = $this->rules->discount_value;
                $discountType = $this->rules->discount_type;
                $this->oldPrice = true;

                if ($discountType == 1) {
                    $this->discount = $this->rules->discount_value;
                    $discount = $value - $discountValue;
                    $this->originalPrice = $value;
                    return round($discount);
                }

                if ($discountType == 0) {
                    $finalPrice = $value - ($value * ($discountValue / 100));
                    $this->discount = $this->rules->discount_value;
                    $this->originalPrice = $value;
                    return round($finalPrice);
                }
            }
            // self::$hasDiscount = 110;
            return $value;
        }
    }

    /*
             * get relative products
    */
    public function scopeGetRelative($query)
    {
        return $query->where('group', $this->group)
            ->where('id', '!=', $this->id)->get();
    }

    public function scopeInStock($query)
    {
        return $query->join('stocks', 'products.id', '=', 'stocks.product_id')
            ->groupBy('products.id')
            ->havingRaw('SUM(stocks.quantity) > 0')
            ->select([
                'products.*',
            ]);
    }

    public function getDiscountAttribute()
    {
        $discount = null;
        if ($this->sale_price > 0) {
            $discount = (($this->getOriginal('price') - $this->sale_price) / $this->getOriginal('price')) * 100;
        } elseif ($this->rules) {
            $discountValue = $this->rules->discount_value;
            $discountType = $this->rules->discount_type;
            if ($discountType == 1) {
                $discount = $this->rules->discount_value;
            } elseif ($discountType == 2) {
                $discount = (($this->price - $discountValue) / $this->price) * 100;
            }
        }

        return round($discount);
    }

    public function scopeFilter($query, $field, $name)
    {
        if (is_array($name)) {
            return $query->whereHas($field, function ($q) use ($name) {
                $q->whereIn('name', $name);
            });
        } else {
            return $query->whereHas($field, function ($q) use ($name) {
                $q->where('name', $name);
            });
        }

    }

    public function GetflatcategoriesAttribute()
    {
        $cat = $this->categories()->get(['id', 'parent_id', 'title'])->toArray();
        usort(
            $cat,
            function ($prev, $next) {
                return $prev['parent_id'] > $next['parent_id'];
            }
        );

        return collect($cat);

    }

    public function meta()
    {
        return $this->hasMany(ProductMeta::class);
    }

    public function getMeta($key)
    {
        $meta = $this->meta()->where('meta_key', $key)->first();
        return $meta ? $meta->meta_value : null;
    }

    public function setMeta($key, $value)
    {
        $meta = $this->meta()->updateOrCreate(
            ['meta_key' => $key],
            ['meta_value' => $value]
        );
        return $meta;
    }
}
