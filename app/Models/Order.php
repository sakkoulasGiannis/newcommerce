<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Order extends Model
{
    public $fillable = [
        'id',
        'order_num',
        'comment',
        'ip',
        'user_id',
        'order_comment',
        'shipping_id',
        'slug',
        'tracking_number'
    ];


//    public function getOrder_numAttribute()
//    {
//        return $this->order_num + "-";
//    }

    protected function orderNum(): Attribute
    {
        return Attribute::make(
            get: fn (string $value) => $value .  ($this->details()->count() > 1 ? "-" . $this->details()->count()  : null),
        );
    }


    // public function setOrderNumAttribute($value)
    // {
    //     if ($this->detail()->count() > 0) {
    //         $this->attributes['order_num'] = $value . "-" . ($this->detail()->count());
    //     } else {
    //         $this->attributes['order_num'] = $value;
    //     }
    // }

    public function getCreatedAtAttribute($value)
    {
        return date("d/m/Y H:i", strtotime($value));
    }


    public function comments()
    {
        return $this->hasMany(OrderComment::class, 'order_id', 'id');
    }

    public function products()
    {
        return $this->hasManyThrough(OrderProduct::class, OrderDetail::class, 'order_id', 'order_detail_id', 'id', 'id');
    }

    public function voucher()
    {
        return $this->hasOneThrough(OrderVoucher::class, OrderDetail::class, 'order_id', 'order_detail_id', 'id', 'id');
    }

    public function details()
    {
        return $this->hasMany(OrderDetail::class, 'order_id', 'id');
    }

    public function detail()
    {
        return $this->hasOne(OrderDetail::class, 'order_id', 'id');
    }

    //		public function details()
    //		{
    //			return $this->hasMany(OrderDetail::class, 'order_id', 'id');
    //		}


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
