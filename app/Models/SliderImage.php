<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SliderImage extends Model
{
    protected $fillable = [ 'url', 'alt', 'href', 'slider_id', 'order_num'];

    public function slider()
    {
        return $this->belongsTo(Slider::class, "id", "slider_id");
    }
}
