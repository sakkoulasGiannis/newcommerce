<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Payment extends Model
{
    use HasFactory, HasTranslations;

    protected $fillable = ['title', 'name', 'description', 'active', 'model', 'order'];
    public $translatable = ['title', 'description'];
}
