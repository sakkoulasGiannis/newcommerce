<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'session_id', 'discount', 'voucher_id', 'shipping_extra', 'payment_extra'];

    public function items()
    {
        return $this->hasMany(CartItem::class);
    }

    public function total($shipping_extra = 0, $payment_extra = 0)
    {
        $total = 0;
        $items = $this->items;

        foreach ($items as $item) {
            $total += $item->quantity * $item->price;
        }
        // if discount is applied
        if ($this->discount > 0) {
            $total -= $this->discount;
        }
        if ($shipping_extra > 0) {
            $total += $shipping_extra;
        }
        if ($payment_extra > 0) {
            $total += $payment_extra;
        }

        if ($this->voucher_id != null) {
            if ($this->voucher->is_fixed) {
                $total -= $this->voucher->discount_amount;
            } else {
                $total -= $total * $this->voucher->discount_amount / 100;
            }

        }

        return $total;
    }

    public function voucher_discount()
    {
        if ($this->voucher_id != null) {
            return $this->voucher;
        }
        return null;
    }

    public function subTotal()
    {
        $total = 0;
        $items = $this->items;

        foreach ($items as $item) {
            $total += $item->quantity * $item->price;
        }

        return $total;
    }

    public
    function voucher()
    {
        return $this->belongsTo(Voucher::class);
    }

}
