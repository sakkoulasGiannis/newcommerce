<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rule extends Model
{
    public $fillable = ['discount_type', 'is_active','discount_value', 'query', 'starts_at', 'title'];

    protected $casts = [
        'query' => 'json',
    ];


    public function discount()
    {
        // get all routes
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'rule_id', 'id')->withTimestamps();
    }

    public function variations(){
        return $this->hasMany(RuleVariation::class);
    }

//    public function scopeInventory($query, $qty)
//    {
//        return $query->inventory($data['data']['rules'][$key]['value'])->pluck('product_id');
//    }
}
