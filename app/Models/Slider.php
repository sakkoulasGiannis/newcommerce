<?php

namespace App\Models;
use App\Models\SliderImage;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = ['title', 'body', 'published'];

    public function images()
    {
        return $this->hasMany(SliderImage::class, 'slider_id', 'id')->orderBy('order_num', 'asc');
    }
}
