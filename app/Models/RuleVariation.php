<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RuleVariation extends Model
{
    protected $table = 'rule_variation';
    public $fillable = ['product_id', 'rule_id'];


    public function rule()
    {
        return $this->hasMany(Product::class, 'rule_id', 'id')->withTimestamps();
    }

}
