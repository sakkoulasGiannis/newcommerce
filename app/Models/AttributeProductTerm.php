<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AttributeProductTerm extends Model
{
    use HasFactory;
    protected $fillable = [
        'product_id',
        'attribute_term_id',
        'attribute_id'

    ];
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    public function attribute()
    {
        return $this->belongsTo(Attribute::class);
    }

    public function attributeTerm()
    {
        return $this->belongsTo(AttributeTerm::class);
    }

    public function term()
    {
        return $this->belongsTo(AttributeTerm::class, 'attribute_term_id', 'id');
    }
}
