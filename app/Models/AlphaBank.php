<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AlphaBank extends Model
{
    use HasFactory;

    protected $fillable = [];
    public static $order_status = 'pending' ; // Pending
    public static $mid;
    public static $secret;
    public static $confirm_url;
    public static $fail_url;
    public static $demo;
    public static $url;

    public function __construct()
    {
        $this->demo = true;
        $this->mid = env('ALPHA_BANK_MID');
        $this->secret = env('ALPHA_BANK_SECRET');
        $this->confirm_url =  env('WEBSITE_URL').'/cart/checkout/alphabank/success';
        $this->fail_url =  env('WEBSITE_URL').'/cart/checkout/alphabank/fail';
        if ($this->demo == 1) {
            $this->url = 'https://alpha.test.modirum.com/vpos/shophandlermpi';
        } else {
            $this->url = 'https://www.alphaecommerce.gr/vpos/shophandlermpi';
        }
    }

    public function info()
    {
        return response()->json([
            'form_url' => '/cart/checkout/alphabank',
            'order_status' => 4,
            'extra_charge' => 0
        ]);
    }
}
