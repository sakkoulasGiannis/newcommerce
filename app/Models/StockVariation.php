<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockVariation extends Model {
	protected $fillable = ['stock_id', 'attribute_id', 'attribute_term_id'];

	public function stock() {
		return $this->belongsTo(Stock::class, 'stock_id', 'id');
	}

    public function attributes() {
        return $this->belongsTo(Attribute::class, 'attribute_id', 'id');
    }
    public function attributeTerm()
    {
        return $this->belongsTo(AttributeTerm::class, 'attribute_term_id', 'id');
    }
	public function fields() {
		$this->belongsTo(Field::class, 'id', 'model_id');
	}

    public function product() {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}
