<?php

namespace App\Models;

use App\Models\StockVariation;
use Illuminate\Database\Eloquent\Model;

//@todo add weight column in stock table

class Stock extends Model
{

    public function getPrice():int
    {
        return $this->product()->price;
    }


    protected $casts = [
        'quantity' => 'integer',

        'sku' => 'string',
    ];
    protected $fillable = [
        'product_id',
        'sku',
        "ean",
        'barcode',
        'model_type',
        'model_id',
        'quantity',
        'model_title',
        'price',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function variations()
    {
        return $this->hasMany(StockVariation::class, 'stock_id', 'id');
    }

    // public function fields() {
    // 	return $this->hasManyThrough(Field::class, StockVariation::class, 'stock_id', 'id');
    // }
    public function getAvailableStockAttribute()
    {
        return $this->total_stock - $this->reserved_stock;
    }

    public function reservedStock()
    {
        return $this->hasMany(ReservedStock::class, 'stock_id', 'id');
    }

    public function getQuantityAttribute($value)
    {
        $reservedStock = $this->reservedStock()->sum('quantity');

        return $value - $reservedStock;
    }




    public function scopeStockItems($query)
    {
//        return $query->each(function ($q) {
//            $q->sku = 'ssdf';
//        });
    }
}
