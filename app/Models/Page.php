<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    public $fillable = ['title', 'body', 'active', 'slug', 'meta_description'];

}
