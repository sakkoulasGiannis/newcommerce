<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Tax extends Model
{
    use HasFactory, HasTranslations;
    public $translatable = ['tax_name'];

    protected $fillable = [
        'country_code',
        'county_code',
        'zip_codes',
        'tax_rate',
        'tax_name',
    ];
}
