<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrossSellProduct extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_id',
        'cross_sell_product_id',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
