<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Language;
use App\Models\Setting;
use App\Models\Theme;
use Illuminate\Http\Request;

class GeneralController extends Controller
{
    public function getLanguageList()
    {
        //@todo make this dynamic
        return Language::get();
    }
public function theme()
{
    return view('manage.theme.index');
}
public function settings()
    {
        $languages = Language::all();
        return view('manage.settings.index', compact('languages'));
    }

    public function export_index()
    {
    }

    public function query_all(Request $request)
    {

        $r = [
            'entities' => ['products', 'orders'],
        ];

        foreach ($r['entities'] as $entity) {
            return $entity;
        }
    }

    public function builder()
    {
        return view('manage.builder.index');
    }


    public function clearCache()
    {
        \Artisan::call('optimize:clear');
        \Artisan::call('optimize');
        \Session::flash('message', "Rebuild Cache complete");

        return redirect()->back();
    }

    public function delete_shipping_payment_store(Request $request)
    {
        // Validate the request input
        $request->validate([
            'rules' => 'required|array', // Ensure 'menu' is an array
        ]);


        // Retrieve and JSON encode the menu
        $menu = json_encode($request->rules, JSON_THROW_ON_ERROR);
        dd($menu);
    }

    public function shipping_payment_store(Request $request)
    {
        // Validate the request input
        $request->validate([
            'rules' => 'required|array', // Ensure 'menu' is an array
        ]);

        try {
            // Retrieve and JSON encode the menu
            $menu = json_encode($request->rules, JSON_THROW_ON_ERROR);
//
//            // Define the storage path
//            $path = storage_path('app/resources/rules.json');
//
//            // Create the directory if it doesn't exist
//            if (!file_exists(dirname($path))) {
//                mkdir(dirname($path), 0755, true);
//            }

            // Write the JSON data to the file
            Setting::updateOrCreate(['key' => 'shipping_payment_rule'], ['value' => $menu]);
//            file_put_contents($path, $menu);
            return response()->json(['message' => 'Menu saved successfully ']);
            // Return a success response

        } catch (JsonException $e) {
            // Handle JSON encoding errors
            return response()->json(['error' => 'Failed to encode menu to JSON: ' . $e->getMessage()], 500);
        } catch (Exception $e) {
            // Handle other errors (like file write issues)
            return response()->json(['error' => 'Failed to save menu: ' . $e->getMessage()], 500);
        }
    }

    public function shipping_payment()
    {
        $rules = Setting::where('key', 'shipping_payment_rule')->first();
        if(!$rules){
            $rules = json_encode([]);
        }else{
            $rules = $rules->value;
        }

//        $rules = [];
//        $countries = Country::get();
//        foreach ($countries as $c) {
//            $rules[] = [
//                'country' => $c->title,
//                'county' => json_decode($c->payment_rules, true),
//            ];
//        }

        return view('manage.shipping.shipping-payment', compact('rules'));
    }

    function sanitizePath($path) {
        //sanitize, remove double dot .. and remove get parameters if any
        $path = preg_replace('@/+@' , DIRECTORY_SEPARATOR, preg_replace('@\?.*$@' , '', preg_replace('@\.{2,}@' , '', preg_replace('@[^\/\\a-zA-Z0-9\-\._]@', '', $path))));
        return $path;
    }

    public function scanMedia(){

        if (isset($_POST['mediaPath']) && ($path = $this->sanitizePath(substr($_POST['mediaPath'], 0, 256)))) {
            define('UPLOAD_PATH', $path);
        } else {
            define('UPLOAD_PATH', public_path('/images'));
        }

        $scandir =  public_path('/images');
 // Run the recursive function
// This function scans the files folder recursively, and builds a large array

        $scan = function ($dir) use ($scandir, &$scan) {
            $files = [];

            // Is there actually such a folder/file?

            if (file_exists($dir)) {
                foreach (scandir($dir) as $f) {
                    if (! $f || $f[0] == '.') {
                        continue; // Ignore hidden files
                    }

                    if (is_dir($dir . '/' . $f)) {
                        // The path is a folder

                        $files[] = [
                            'name'  => $f,
                            'type'  => 'folder',
                            'path'  =>  '/' . $f,
//                            'path'  => str_replace($scandir, '/images', $dir) . '/' . $f,
                            'items' => $scan($dir . '/' . $f), // Recursively get the contents of the folder
                        ];
                    } else {
                        // It is a file

                        $files[] = [
                            'name' => $f,
                            'type' => 'file',
//                            'path' => str_replace($scandir, '/images', $dir) . '/' . $f,
                            'path' =>   '/' . $f,
                            'size' => filesize($dir . '/' . $f), // Gets the size of this file
                        ];
                    }
                }
            }

            return $files;
        };

        $response = $scan($scandir);

// Output the directory listing as JSON

        header('Content-type: application/json');

        return json_encode([
            'name'  => '',
            'type'  => 'folder',
            'path'  => '',
            'items' => $response,
        ]);

    }

    public function ThemeImage(Theme $theme){
         $mediaItems = $theme->getMedia('uploads');
        $images = [];
        foreach ($mediaItems as $mediaItem) {
            $images[] = [
                'src' => $mediaItem->getUrl(),
            ];
        }

        return json_encode($images);
    }

    public function ThemeImageUpload(Theme $theme, Request $request){
        $request->validate([
            'files.*' => 'required|file|mimes:jpg,jpeg,png,gif,pdf,doc,docx', // Εξαρτάται από τους τύπους αρχείων που δέχεσαι
        ]);


        $files = $request->file('files');
        $uploadedFiles = [];

        foreach ($files as $file) {
            $mediaItem = $theme->addMedia($file)->toMediaCollection('uploads'); // Εξαρτάται από το collection που χρησιμοποιείς
            $uploadedFiles[] = [
                'name' => $mediaItem->file_name,
                'type'=>'image',
                'src' => $mediaItem->getUrl(),
            ];
        }
        $response = array( 'data' => $uploadedFiles );
        return json_encode($response);


    }



    public function uploadImage(Request $request){
        $request->validate([
            'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $imageName = time().'.'.$request->file->extension();

        $request->file->move(public_path('images'), $imageName);

        return response()->json(['ok'=>true, 'url' => '/images/'.$imageName]);
    }
}
