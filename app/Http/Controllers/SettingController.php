<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function company()
    {
        return view('manage.settings.company');
    }


    public function store(Request $request)
    {

        if ($request->get('type') == 'logo') {
            $request->validate([
                'title' => 'required|string|max:255',
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $imageName = time() . '.' . $request->image->extension();
            $request->image->move(public_path('images'), $imageName);
            // check if settings has already logo
            $setting = Setting::where('key', 'logo')->first();
            if ($setting) {
                $setting->update([
                    'value' => '/images/' . $imageName
                ]);
            } else {
                $settings = new Setting();
                $settings->title = $request->get('title');
                $settings->key = 'logo';
                $settings->value = 'images/' . $imageName;
                $settings->group = 'company';
                $settings->save();
            }


            return back()->with('success', 'Image uploaded successfully.');
        } else {
            $request->validate([

                'group' => 'required|string|max:255',
            ]);

             $toJson = false;
            if($request->has('encode') && $request->get('encode') ){
                $toJson = true;
                $values = [];
                foreach ($request->input() as $key =>  $i) {

                    if ($key == '_token' || $key == 'group') {
                        continue;
                    }
                    $values[$key] = $i;
              }
                $values = json_encode($values);
                $setting = Setting::where('key', $request->get('key'))->first();
                if ($setting) {
                    $setting->update([
                        'value' => $values
                    ]);
                } else {
                    $settings = new Setting();
                    $settings->title = $request->get('group');
                    $settings->key = $request->get('key');
                    $settings->value = $values;
                    $settings->group = $request->get('group');
                    $settings->save();
                }


            }


            return back()->with('success', 'Setting updated successfully.');
        }

    }

}
