<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function frontendSearchSimple(Request $request)
    {
        $products = Product::search($request->get('q'))->paginate();
        return view(\Config::get('settings.theme') . '.search-simple', compact('products'));
    }
    public function search(Request $request = null)
    {
        $paginate = 200;

        // set pagination

        if (request()->has('q')) {
            //            $products = Product::hasInventory();
            $products = Product::query();

            $products = $products
                ->hasStock()
                ->where('is_active', 1)
                ->where('products.sku', 'LIKE', '%' . request()->get('q') . '%')
                ->orWhere('title', 'LIKE', '%' . request()->get('q') . '%')
                ->orWhere('description', 'LIKE', '%' . request()->get('q') . '%')
                ->get();

            return view(\Config::get('settings.theme') . '.search', compact('products'));
        }
        return back();
    }

    public function adminSearch()
    {
        if (request()->has('q')) {
            //            $products = Product::hasInventory();
            $products = Product::query();
            $data = [];
            $products = $products
                // ->hasStock()
                // ->where('is_active', 1)
                ->where('products.sku', 'LIKE', '%' . request()->get('q') . '%')
                // ->orWhere('title', 'LIKE', '%' . request()->get('q') . '%')
                ->limit(20)
                ->get();

            $orders = Order::with('detail')
                ->where('detail.billing_email', '%' . request()->get('q') . '%');
            $data['products'] = $products;
            $data['orders'] = $orders;
            //
            return $data;

            return view(setting('theme') . '.search', compact('products'));
        }
    }

    public function apiSearch(Request $request)
    {

        return Product::search($request->get('q'))->get();
    }

    public function frontendSearch(Request $request)
    {

        // $products = Product::with('categories')->search($request->get('q'))->get();
        $products = Product::search($request->get('q'))->get();
        return view(\Config::get('settings.theme') . '.search', compact('products'));
    }
}
