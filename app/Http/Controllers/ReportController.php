<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function import(){
        return view('manage.reports.import');
    }

    public function getHeader(Request $request){
        $path = $request->file('csv_file')->getRealPath();
        $h = fopen($path, "r");
        $data = fgetcsv($h, 1000, ",");
        return $data;
    }
    public function importCsv(Request $request){

        $path = $request->file('csv_file')->getRealPath();
//        $file = file($path);
        $h = fopen($path, "r");
        $data = fgetcsv($h, 1000, ",");
//        return $data;
        while (($data = fgetcsv($h, 1000, ",")) !== FALSE)
        {



            /*
             * doubleprice
             * special_double_price
             * categories 9
             */

            $cproduct = Product::where('name', $data[3])->first();

            if(!$cproduct){
                $product = new Product();
                $product->is_active =1;
                $product->product_type = (isset($data[1]))?$data[1]:null;
                $product->tax_class_id = 1;
                $product->group = 1;
                $product->title = (isset($data[2]))?$data[2]:null;
                $product->name = (isset($data[3]))?$data[3]:rand(0,100000000);
                $product->meta_title = (isset($data[4]))?$data[4]:null;
                $product->meta_description = (isset($data[5]) && $data[5]!='')?$data[5]:'';
                $product->price = (isset($data[6])&&$data[6]!='')?str_replace(',', '.', $data[6]):0;
                $product->sale_price = (isset($data[7])&&$data[7]!='')?str_replace(',', '.', $data[7]):0;
                $product->sku = (isset($data[10]))?str_slug($data[10], '-') : rand(0,100000000000);
                $product->description = (isset($data[12]))?$data[12]:null;
                $product->save();
                $the_big_array[] = $data;
            }
            elseif($request->has('update') && $cproduct){
                $cproduct->is_active =1;
                $cproduct->product_type = (isset($data[1]))?$data[1]:null;
                $cproduct->tax_class_id = 1;
                $cproduct->group = 1;
                $cproduct->title = (isset($data[2]))?$data[2]:null;
                $cproduct->name = (isset($data[3]))?$data[3]:rand(0,100000000);
                $cproduct->meta_title = (isset($data[4]))?$data[4]:null;
                $cproduct->meta_description = (isset($data[5]) && $data[5]!='')?$data[5]:'';
                $cproduct->price = (isset($data[6])&&$data[6]!='')?str_replace(',', '.', $data[6]):0;
                $cproduct->sale_price = (isset($data[7])&&$data[7]!='')?str_replace(',', '.', $data[7]):0;
                $cproduct->sku = (isset($data[10]))?str_slug($data[10], '-') : rand(0,100000000000);
                $cproduct->description = (isset($data[12]))?$data[12]:null;
                $cproduct->save();
            }

        }
        fclose($h);

        return back();
//        dd($the_big_array);
    }
}
