<?php

namespace App\Http\Controllers;

use Algolia\AlgoliaSearch\SearchClient;
use App\Http\Controllers\Calculator;
use App\Models\AttributeProduct;
use App\Models\AttributeProductTerm;
use App\Models\ComboProductItem;
use App\Models\CrossSellProduct;
use App\Models\Field;
use App\Models\Product;
use App\Models\ProductAttributeTerm;
use App\Models\Stock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

use App\Helpers\AppHelper;

class ProductController extends Controller
{

    public function search(Request $request)
    {

        //		$products = Product::search('Adf')->get();

        return Stock::with('product')
            ->where('quantity', '>', 0)
            ->where('sku', 'like', "%" . $request->get('search') . "%")
            // ->orWhere('sku', 'like', "%{$request->get('search')}%")
            ->limit(20)
            ->get();
    }


    public function fields(Product $product)
    {

        $fields = $product->fields;
        foreach ($fields as $field) {
            $fieldName = $field->name;
            $field['values'] = $product->$fieldName->toArray();
        }
        return $fields;
    }


    // function calcResult($query){

    // }

    // function calcGroupToChild($group){

    // }
    // query builder
    public function querybuilresults(Request $request)
    {

        $products = Product::query();
         $query = $request->all();

        foreach ($query['query'] as $q) {

            $products = AppHelper::queryManager($q, $products);

        }

        return $products->get();
        /// old query
        // $globalLogicalOperator = $input['logicalOperator'];
        // $this->groupOr = ($globalLogicalOperator == 'any')? true:false;
        // foreach($input['children'] as $q){
        //     if($q['type'] == 'query-builder-rule'){

        //         $products = $this->querymanager($q, $products );
        //     }elseif($q['type'] == 'query-builder-group'){
        //         $this->groupOr = false;
        //         $this->calcGroupToChild($q, $products);
        //     }
        // }

        // return $products->get(['id', 'sku', 'title', 'price', 'sale_price','rule_id']);

    }

    function getProductCategories(Product $product)
    {
        return $product->categories()->pluck('id');
    }

    function calcToQuery($input)
    {
        if (isset($input['children'])) {
            foreach ($input['children'] as $child) {
                if ($child['type'] == 'query-builder-rule') {
                    $this->calcToQuery($child);
                } elseif ($child['type'] == 'query-builder-group') {
                    $this->calcGroupToChild();
                }
            }
        }
    }


    public function index(Request $request)
    {
        //search query
        //        return Product::whereHas('Brand', function ( $query) { $query->where('title', 'brand1');})->get();


        $products = Product::with('categories')
            ->when($request->has('sku'), function ($query) use ($request) {
                return $query->where('sku', 'like', '%' . $request->get('sku') . '%');
            })
            ->when($request->has('title'), function ($query) use ($request) {
                return $query->where('title', 'like', '%' . $request->get('title') . '%');
            })
            ->paginate($request->has('per_page') ? $request->get('per_page') : 10);



        return view('manage.products.index', compact('products'));
    }

    public function searchForGroup(Request $request)
    {
        $products = Product::where('sku', 'like', $request->input('sku') . "%")
            ->get(['id', 'sku', 'price', 'title', 'sale_price']);

        $products->map(function ($product) {
            $product->quantity = 1; // Αρχικοποιούμε το quantity σε 1
            return $product;
        });
        return $products;
    }

    public function listJson(Request $request)
    {
        $products = Product::query();

        // Search by 'sku' or 'title'
        if ($search = $request->input('search')) {
            $products->where(function ($query) use ($search) {
                $query->where('sku', 'like', "%{$search}%")
                    ->orWhere('title', 'like', "%{$search}%");
            });
        }

        // Filter by fields
        if ($fields = $request->input('fields')) {
            foreach ($fields as $field) {
                if (count($field['values']) > 0) {
                    $products->whereHas($field['name'], function ($query) use ($field) {
                        $query->whereIn('id', $field['values']);
                    });
                }

            }
        }

        if($attributes = $request->get('attributes')){
            $products ->when(count($attributes) > 0, function ($q) use ($attributes) {

                foreach ($attributes as $attribute) {
                    $q->whereHas('attribute_terms', function ($q) use ($attribute) {
                        return  $q->where('attribute_id',  $attribute['id'])
                            ->whereIn('attribute_term_id', $attribute['values']);
                    });
                }
            });
        }

        // Filter by categories
        if ($categories = $request->input('categories')) {
            $products->whereHas('categories', function ($query) use ($categories) {
                $query->whereIn('id', $categories);
            });
        }

        // Additional search headers
        if ($searchHeaders = $request->input('searchHeader')) {
            foreach ($searchHeaders as $header) {
                if (!empty($header['value'])) {
                    $products->where($header['name'], 'like', '%' . $header['value'] . '%');
                }
            }
        }

        // Pagination and ordering
        $itemsPerPage = $request->input('itemsPerPage', 15); // default to 15 if not provided
        return $products->orderBy('id', 'desc')->paginate($itemsPerPage);
    }

    public function create()
    {
        return view('manage.products.create');
    }

    public function filters()
    {
        return view('manage.products.filters');
    }

    public function updateProductCombos(Product $product, Request $request)
    {
        $combos = [];
        $product->combos()->delete();
        foreach ($request->get('combos') as $item) {
            $product->combos()->updateOrCreate(
                ['combo_product_id' => $item['id']], // Αναζητούμε με βάση το product_id
                $item // Ενημερώνουμε ή δημιουργούμε με βάση τα δεδομένα
            );
        }


        return $product->combos;
        $product->combos()->createMany($combos);
        return $product->combos;
    }

    public function updateProductCrossSell(Product $product, Request $request)
    {
        $crossSell = [];
        $product->crossSell()->delete();
        foreach ($request->get('cross') as $item) {
            $product->crossSell()->updateOrCreate(
                ['cross_sell_product_id' => $item['id']], // Αναζητούμε με βάση το product_id
                $item // Ενημερώνουμε ή δημιουργούμε με βάση τα δεδομένα
            );
        }
    }

    public function getProductCrossSell($id)
    {
        $crossSellIds = CrossSellProduct::where('product_id', $id)->pluck('cross_sell_product_id')->toArray();
        return Product::whereIn('id', $crossSellIds)->get();
    }
    public function getProductCombos($id)
    {
//return Product::find($id)->combos;
        $comboIds = ComboProductItem::where('product_id', $id)->pluck('combo_product_id')->toArray();
        return Product::select('products.*', 'combo_product_items.quantity')
            ->join('combo_product_items', 'products.id', '=', 'combo_product_items.combo_product_id')
            ->where('combo_product_items.product_id', $id)
            ->get();

    }

    public function getProductForEdit($id)
    {
        // return Product::find($id);
          $product = Product::with('attributes')->with( Field::where('position', '!=', 1)->pluck('name', 'id')->toArray())->find($id);

        $termIds = [];




        foreach ($product->attributes as $key => $attribute) {
            // Φέρετε τις τιμές από τον πίνακα product_attribute_terms που ταιριάζουν με το product_id και το attribute_id
            $matchingTerms = $product->terms->where('attribute_id', $attribute->id);
            foreach ($matchingTerms as $term) {
                $termIds[] = $term->id;
            }
            $product->attributes[$key]->setAttribute('termids', $termIds);
            $termIds = [];
        }
        return $product;
      $productCategories = $product->categories()->pluck('id');
    }

    public function edit(Product $product)
    {

        return view('manage.products.edit', compact('product'));
    }

    public function update(Product $product, Request $request)
    {
        $product->update($request->all());
        $product->save();
        $termIds = [];
        $attributeIds = [];
        $existingAttributes = AttributeProduct::where('product_id', $product->id)->pluck('id');
        $oldTerms = $product->terms()->pluck('attribute_term_id')->toArray();

        foreach ($request->get('attributes') as $attribute) {
            $a = AttributeProduct::firstOrCreate([
                'product_id' => $product->id,
                'attribute_id' => $attribute['id'],
            ]);
            $attributeIds[] = $a->id;
            foreach ($attribute['termids'] as $term) {
                $termIds[] = $term;
                if (AttributeProductTerm::where('product_id', $product->id)->where('attribute_id', $attribute['id'])->where('attribute_term_id', $term)->first()) {
                    continue;
                }
                AttributeProductTerm::create([
                    'product_id' => $product->id,
                    'attribute_id' => $attribute['id'],
                    'attribute_term_id' => $term
                ]);
            }
        }

        $diffTerms = array_diff($oldTerms, $termIds);
        foreach ($diffTerms as $term) {
            ProductAttributeTerm::where('product_id', $product->id)->where('attribute_term_id', $term)->delete();
        }

        $difAttributes = array_diff($existingAttributes->toArray(), $attributeIds);

        foreach ($difAttributes as $attribute) {
            AttributeProduct::where('id', $attribute)->delete();
        }

//        ResponseCache::clear();
        return $product;
    }

    public function updateCategories(Product $product, Request $request)
    {
        return $product->categories()->sync($request->input());
    }

    public function calculatorUpdate(Product $product, Request $request)
    {

        $query = json_encode($request->input());
        if (!$product->calculator) {
            $product->calculator()->create(['query' => $query]);
        } else {
            $product->calculator()->update(['query' => $query]);
        }
        return $product->calculator;
    }

    public function fieldsUpdate(Product $product, Request $request)
    {
        $fieldIds = [];
        foreach ($request->input() as $fields) {
            $fieldIds[] = $fields['id'];
        }
        $product->fields()->sync($fieldIds);


        foreach (Field::all() as $f) {
            $field = $f->name;
            $product->$field()->detach();
        }


        //add cron to remove unused field text values
        foreach ($request->input() as $r) {
            $model = $r['model_type'];
            if ($r['type'] == 'text' || $r['type'] == 'textarea') {
                if (isset($r['values']) && is_array($r['values'])) {
                    foreach ($r['values'] as $value) {
                        $newInput = $model::create(['created_at' => now(), 'title' => $value, 'name' => str_slug($value->el, '-') . '-' . time()]);
                        $field = $r['name'];
                    }
                    $product->$field()->sync($newInput->id);
                } else {
                    $newInput = $model::create(['created_at' => now(), 'title' => $r['values'], 'name' => str_slug($r['values'], '-') . '-' . time()]);
                    $field = $r['name'];
                    $product->$field()->sync($newInput->id);
                }
            } elseif ($r['type'] == 'select') {

                $field = $r['name'];
                $values = [];
                foreach ($r['values'] as $value) {
                    $values[] = $value['id'];
                }

                $product->{$field}()->sync($values);


            }
        }
        return true;
    }

    public function stockUpdateQuantity(Product $product, Request $request){
        $stock = $request->all();
          $product->stock()->updateOrCreate(
            ['sku' => $request->has('sku') ? $request->get('sku') : null, 'product_id' => $product->id],
            [
                'quantity' => (int)$request->get('quantity'),
            ]
        );
        return json_encode(['success'=>true]);
    }
    public function stockUpdate(Product $product, Request $request)
    {
        $stock = $request->all();
        $existingStockIds = $product->stock()->pluck('id')->toArray(); // Αποθήκευση των υπαρχόντων stock IDs
        $processedStockIds = [];

        for ($i = 0; $i < count($stock); $i++) {
            $stockItem = $product->stock()->updateOrCreate(
                ['sku' => isset($stock[$i]['sku']) ? $stock[$i]['sku'] : null, 'product_id' => $product->id],
                [
                    'sku' => $stock[$i]['sku'],
                    'ean' => isset($stock[$i]['ean']) ? $stock[$i]['ean'] : null,
                    'barcode' => isset($stock[$i]['barcode']) ? $stock[$i]['barcode'] : null,
                    'price' => isset($stock[$i]['price']) ? $stock[$i]['price'] : null,
                    'quantity' => (int)$stock[$i]['quantity'],
                ]
            );

            $processedStockIds[] = $stockItem->id; // Αποθήκευση του ID του επεξεργασμένου stock item

            $existingVariationIds = $stockItem->variations()->pluck('id')->toArray(); // Αποθήκευση των υπαρχόντων variation IDs
            $processedVariationIds = [];

            for ($b = 0; $b < count($stock[$i]['variations']); $b++) {
                $variation = $stockItem->variations()->updateOrCreate(
                    ['stock_id' => $stockItem->id, 'attribute_id' => $stock[$i]['variations'][$b]['attribute_id']],
                    [
                        'stock_id' => $stockItem->id,
                        'attribute_id' => $stock[$i]['variations'][$b]['attribute_id'],
                        'attribute_term_id' => $stock[$i]['variations'][$b]['attribute_term_id']
                    ]
                );

                $processedVariationIds[] = $variation->id; // Αποθήκευση του ID του επεξεργασμένου variation
            }


            // Διαγραφή των variations που δεν περιλαμβάνονται στο request
            $stockItem->variations()->whereNotIn('id', $processedVariationIds)->delete();
        }

        // Διαγραφή των stock items που δεν περιλαμβάνονται στο request
        $product->stock()->whereNotIn('id', $processedStockIds)->delete();

        return $product->stock;
    }


    public function get_stock(Product $product)
    {

         return $product->stock()->with('variations.attributes.terms')->get();

        foreach ($product->stock as $key => $stock) {
            if (isset($stock->variations)) {
                foreach ($stock->variations as $vkey => $variation) {
//                    $field = Field::where('id', $variation['field_id'])->first();
//                    $product->stock[$key]['variations'][$vkey]['field'] = $field;
//                    $model = $variation['model_type'];
                    if (isset($product->stock[$key]['variations'][$vkey]['field'])) {
                        $product->stock[$key]['variations'][$vkey]['field']['values'] = $model::all();
                    }
                }
            }
        }
        return $product->stock;
    }

    public function getMedia(Product $product)
    {

        $media = $product->getMedia('product');
        // foreach ($media as $m) {
        // 	$m['path'] = $m->getUrl();
        // }
        return $media;
    }

    public function addMedia(Product $product, Request $request)
    {
        $image = $request->all();

        $image = $image['image'];

        //        if (is_a($image, 'Illuminate\Http\UploadedFile')) {
        $product->addMediaFromBase64($image)->toMediaCollection();

        //            $product->addMediaFromBase64($image)->toMediaCollection('product');
        // $mediaItems[0]->delete();
        return true;
        //        }

        return 'true';
    }

    public function deleteMedia($id)
    {

        return Media::where('uuid', $id)->first()->delete();
        return true;
    }

    public function store(Request $request)
    {

        $validator = $request->validate([
            'title' => 'required',
            'name' => 'required',
            'sku' => 'required',
        ]);


//        $client = SearchClient::create(
//            'QOXUJDLYXT',
//            'dc0800d5e68023cda7bf62cbb5f59dcf'
//        );
//        $index = $client->initIndex('products');
      $product =  Product::create($request->input());
//        $product = new Product;
//        $product->title = $request->get('title');
//        $product->price = 0.00;
//        $product->group = 0;
//        $product->description = '';
//        $product->small_description = '';
//        $product->meta_title = '';
//        $product->meta_description = '';
//        $product->name = $request->get('name');
//        $product->sku = $request->get('sku');
//        $product->product_type = $request->get('product_type');
//        $product->product_is_active = $request->get('is_active');
//        $product->tax_class_id = 1;
//        $product->save();

//        $index->saveObject(
//            [
//                'objectID' => $product->id,
//                'title' => $product->title,
//                'title_en' => $product->title,
//                'price' => $product->price,
//                'sku' => $product->sku,
//                'is_active' => $product->is_active,
//            ]
//        );

        return $product;
        return redirect('/manage/products/' . $product['id']);
    }

    public function destroy(Product $product)
    {
        Product::destroy($product->id);
    }

    /*
     * return array of attribute and terms
     */
    public function attributeValues(Product $product)
    {


        if (!$product) {
            return collect();
        }

        // Λήψη των attributes
        $attributes = $product->attributes;

        // Λήψη των terms
        $terms = $product->terms;

        // Δημιουργία της επιθυμητής δομής δεδομένων
        $result = $attributes->map(function ($attribute) use ($terms) {
            return [
                'attribute_title' => $attribute->title,
                'terms' => $terms->where('attribute_id', $attribute->id)->pluck('title'),
            ];
        });

        // Επιστροφή της δομής δεδομένων
        return $result;    }

}
