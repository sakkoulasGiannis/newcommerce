<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index()
    {
        $pages = Page::all();
        return view('manage.pages.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('manage.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //validate
        $request->validate([
            'title' => 'required',
            'body' => 'required',
            'slug' => 'required',
        ], [
            'title.required' => 'Ο τίτλος της σελίδας είναι υποχρεωτικός',
            'body.required' => 'Το κείμενο της σελίδας είναι υποχρεωτικό',
        ]);


        $page = Page::create($request->input());

        return redirect(route('pages.edit', $page->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page $page
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = Page::findOrFail($id);
        return view('manage.pages.show', compact('page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page $page
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::findOrFail($id);
        return view('manage.pages.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Page $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $page = Page::findOrFail($id);
        $page->update($request->input());
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        Page::destroy($page->id);
        $pages = Page::all();
        return view('manage.pages.index', compact('pages'));
    }

    public function frontEndView($slug)
    {
           $page = Page::where('slug', $slug)->first();
        return view(config('view.theme') . '.page', compact('page'));
    }
}
