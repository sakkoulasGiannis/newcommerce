<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class SitemapController extends Controller
{


    public function products()
    {
        $products = Product::where()->get();
        return response()->view('default.xml.products', [
            'products' => $products,
        ])->header('Content-Type', 'text/xml');
    }

    public function skroutz()
    {
        $products = $this->getProducts();

        $output = response()->view('default.xml.skroutz', [
            'products' => $products, 'domain' => env('APP_URL')
        ])->render();


        $skroutz = 'skroutz.xml';

        $xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> \n <Document xmlns=\"urn:iso:std:iso:20022:tech:xsd:pain.008.001.02\">" . $output;

        $response = Response::create($xml, 200);
        $response->header('Content-Type', 'text/xml');
        $response->header('Cache-Control', 'public');
        $response->header('Content-Description', 'File Transfer');
        $response->header('Content-Disposition', 'attachment; filename=' . $skroutz . '');
        $response->header('Content-Transfer-Encoding', 'binary');
        return $response;


    }


    public function getProducts()
    {
        return $products = Product::query()->whereHas('stock', function ($q) {
            $q->select(\DB::raw('SUM(quantity) as q'))
                ->havingRaw('q > 0');
        })
            ->where('product_is_active', 1)
            ->orderBy('id', 'Desc')
            ->get();
    }


}
