<?php

namespace App\Http\Controllers;

use App\Models\Country;

class CountyController extends Controller
{
    public function getCountiesByCountry(Country $country)
    {
        return $country->counties()->orderBy('title')->get();
    }
}
