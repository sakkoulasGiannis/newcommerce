<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderCollection;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\ReservedStock;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\OrderExport;
use App\Models\Product;
use App\Models\Stock;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx\Rels;

class OrderController extends Controller
{

    public function index()
    {

        return view('manage.orders.index');
    }

    public function listJson(Request $request)
    {


        $orders = Order::has('detail')->orderBy('id', 'DESC');
        if ($request->has('search')) {
            $s  = $request->get('search');
            $orders
                ->whereHas('detail', function ($q) use ($s) {
                    $q->where('order_num', $s)
                        ->orWhere('billing_email', 'like', '%' . $s . '%')
                        ->orWhere('billing_lastname', 'like', '%' . $s . '%')
                        // ->orWhereRaw("CONCAT_WS(' ',`country_code`, `phone_number`) = ? ",  [$s])
                    ;
                });
        }
        if ($request->has('searchHeader')) {
            foreach ($request->get('searchHeader') as $h) {
                if (isset($h['value']) && $h['value'] != null && $h['value'] != '') {
                    $orders->whereHas('detail', function ($q) use ($h) {
                        $q->where($h['name'], 'like', '%' . $h['value'] . '%');
                    });
                }
            }
        }
        if ($request->has('filters')) {
            foreach ($request->get('filters') as $f) {
                if ($f['selected'] != null) {
                    $orders->whereHas('detail', function ($q) use ($f) {
                        $q->where($f['name'], $f['selected']);
                    });
                }
            }
        }
        return $orders->with('detail', 'user')->paginate($request->get('itemsPerPage'));



        //        return Order::with('detail', 'user')->orderBy('created_at', 'desc')->paginate($request->get('paginate'));
    }

    public function create()
    {

        return view('manage.orders.create');
    }

    public function filter()
    {

        return view('manage.orders.filter');
    }

    public function replicate(Request $request)
    {

        $order = $request->get('order');
        $order = Order::findOrFail($request->get('order_id'));

//        $newOrderNumber = $request->get('orderNumber');
//        $detail = OrderDetail::find($order->detail->id);

        $newOrder = $order->replicate();


        $newOrder->save();

        $newdetail = $detail->replicate()->fill([
            'order_id' => $newOrder->id,
            'created_at' => Carbon::now()
        ]);
        $newdetail->save();

        $detail->status = 'canceled';
        $detail->save();
        return $newOrder;
    }

    public function edit(Order $order)
    {

        $detail = $order->detail;
        $products = $detail->products;
        $vouchers = $detail->vouchers;
        $comments = $order->comments;

        //			$order = Order::with('detail', 'user', 'detail.products')->findOrFail($order)->toArray();
        return view('manage.orders.edit', compact('order'));
    }

    public function show(Order $order)
    {
        $order->opened = 1;
        $order->save();

        $detail = $order->detail;
        $comments = $order->comments;
        $products = $detail->products;
        //			$order = Order::with('detail', 'user', 'detail.products')->findOrFail($order)->toArray();
        return view('manage.orders.show', compact('order'));
    }

    /**
     * Προσθήκη Σχολίου διαχειρστή κατά την επεξεργασία της παραγγελίας.
     * @param Order $order
     * @param Request $request
     * @return void
     */
    public function addComment(Order $order, Request $request)
    {

        $order->comments()->create([
            'user_role' => 'admin',
            'user_name' => Auth::User()->lastname,
            'comment' => $request->get('message'),
        ]);
    }

    public function update(Order $order, Request $request)
    {

        $details = $request->get('detail');

        if ($request->has('tracking_number')) {
            $order->tracking_number = $request->get('tracking_number');
            $order->save();
        }
        switch ($details['status']) {
            case 'canceled':
               ReservedStock::where('order_id', $order->id)->delete();
                break;
            case 'completed':
                ReservedStock::where('order_id', $order->id)->delete();
                foreach($details['products'] as $product){
                    $stock = Stock::find($product['stock_id']);
                    $stock->quantity = $stock->quantity - $product['quantity'];
                    $stock->save();
                }
                break;
        }

        unset($details['updated_at']);

        $detail = OrderDetail::find($details['id']);

        $detail->update($details);

        $detail->products()->delete();
        $detail->products()->createMany($details['products']);

        $detail->vouchers()->delete();
        $detail->vouchers()->createMany($details['vouchers']);
        return $request->input();
    }


    public function filterQuery(Request $request)
    {
        $request = ['shipping_id'];

        $e = Order::query();
        $e = $e->with('detail', 'products', 'user')
            ->when(true, function ($query) {
                $query->whereHas('shipping_id', function ($q) {
                    $q->where('id', '=', 7);
                });
            })
            ->get();

        return $e;
    }


    public function store_new_order(Request $request)
    {

        //        return $request->all();
        $validatedData = \Validator::make($request->get('detail'), [
            //            'billing_email' => 'required|email',
            'billing_city' => 'required',
            'billing_lastname' => 'required',
            'billing_mobile' => 'required',
            'billing_name' => 'required',
            'billing_street' => 'required',
            'billing_zip' => 'required',
            'detail.billing_mobile' => '',
            //            'selectedCounty.id' => 'required',
            //            'shipping.id' => 'required'
        ]);
        if ($validatedData->fails()) {
            return response()->json(['error' => $validatedData->errors()]);
        }


        $invoice_id = Order::count() + 1;

        // return $request->input();
        $order = new Order();
        $order->order_num = $invoice_id;
        $order->user_id = $request->get('user_id');
        // $order->shipping_id = $request->get('shipping_id');
        $order->shipping_id = $request->get('shipping_id');
        $order->tracking_number = $request->get('tracking_number');
        // $order->payment_id = $request->get('payment_id');

        $order->save();
        $details = $request->get('detail');

        $detail = $order->detail()->create($details);

        foreach ($details['products'] as $cartProduct) {
            // if product is dynamic


            $associated = Stock::find($cartProduct['stock_id']);
            $newQuantity = $associated->quantity - $cartProduct['quantity'];
            $associated->update(['quantity' => $newQuantity]);
            $product = Product::find($cartProduct['product_id']);
            $order['products'][] = [
                'product_id' => $cartProduct['product_id'],
                'stock_id' => $associated->id,
                'name' => $product->title,
                'sku' => $associated->sku,
                'price' => $cartProduct['product_id'],
                'quantity' => $cartProduct['quantity'],
            ];
        }

        $detail->products()->createMany($details['products']);
        return $order;
    }


    public function unopendOrders()
    {
        return Order::where('opened', 0)->count();
    }

    /**
     * @return void
     * export to excel
     */
    public function export(Request $request)
    {

        $filters = $request->input();

        return Excel::download(new OrderExport($filters), 'invoices.xlsx');


        return (new OrderExport($filters))->download('orders-' . today() . '.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }


    public function destroy(Order $order)
    {
        $order->delete();
    }
}
