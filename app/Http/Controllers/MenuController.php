<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\Setting;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function get_menu(Request $request){
        $menu = Menu::get()->first();
        return response()->json($menu);
    }
    public function index()
    {
        $menus = Menu::all();
        return view('manage.menu.index', compact('menus'));
    }

    public function create()
    {
        return view('manage.menu.create');
    }

    public function store(Request $request){
        $request->validate([
            'title' => 'required|string',
        ]);

        $menu = new Menu();
        $menu->title = $request->title;
        $menu->active = (Menu::where('active', 1)->count() === 0)? true : false;
        $menu->menu = json_encode([]);
        $menu->save();
        return redirect()->route('menu.edit', $menu->id);
    }

    public function update(Menu $menu, Request $request){
        $request->validate([
            'data' => 'required|array', // Ensure 'menu' is an array
        ]);

        try {
            // Retrieve and JSON encode the menu
            $data = json_encode($request->data, JSON_THROW_ON_ERROR);

            // delete this if exist storage_path('app/public/menu.json')
            if (file_exists(storage_path('app/public/menu.json'))) {
                unlink(storage_path('app/public/menu.json'));
            }

            // Define the storage path
            $path = storage_path('app/public/menu.json');

            // Create the directory if it doesn't exist
            if (!file_exists(dirname($path))) {
                mkdir(dirname($path), 0755, true);
            }

            // Write the JSON data to the file
            Menu::updateOrCreate(['id' => $menu->id], ['menu' => $data]);
            file_put_contents($path, $menu);
            return response()->json(['message' => 'Menu saved successfully ', $path]);
            // Return a success response
            return response()->json(['message' => 'Menu saved successfully']);

        } catch (JsonException $e) {
            // Handle JSON encoding errors
            return response()->json(['error' => 'Failed to encode menu to JSON: ' . $e->getMessage()], 500);
        } catch (Exception $e) {
            // Handle other errors (like file write issues)
            return response()->json(['error' => 'Failed to save menu: ' . $e->getMessage()], 500);
        }
    }

    public function edit(Menu $menu)
    {

        $menuJson = $menu->menu;
        if (!$menuJson) {
            $menuJson = json_encode([]);
        } else {
            $menuJson = $menuJson;
        }

        return view('manage.menu.edit', compact('menuJson', 'menu'));
    }

}
