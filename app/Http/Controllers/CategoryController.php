<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Setting;
use Illuminate\Http\Request;
// use Inertia\Inertia;

class CategoryController extends Controller
{
    public function index()

    {
        return view('manage.categories.index');
    }


    public function listJson(Request $request)
    {
        return $products = Category::paginate($request->get('paginate'));
    }

    public function categoriesTree()
    {
        return Category::with('children', 'children.children')
            ->when(request('parent_id'), function ($query) {
                $query->where('parent_id', request('parent_id'));
            })->when(!request('parent_id'), function ($query) {
                $query->where('parent_id', 0);
            })
            ->orderBy('title')
            ->get();
    }

    public function edit(Category $category)
    {

        return view('manage.categories.edit', compact('category'));
    }

    public function destroy(Category $category)
    {
        $category->delete();

    }
    public function update(Category $category, Request $request)
    {

        return  $category->update($request->input());
    }
    public function create(Request $request)
    {
        Category::create($request->all());
    }
    public function category(Category $category)
    {
        return $category;
    }

    public function get_categories_for_query()
    {
        return Category::with('children')
            ->where('parent_id', 0)
            ->orderBy('order')
            ->get();
    }

    //store menu to file in server

}
