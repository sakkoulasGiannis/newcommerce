<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Traits\OrderTrait;
use App\Models\Order;
use App\Mail\NewOrder;
use App\Models\AlphaBank;

use App\Models\OrderDetail;

class AlphaBankController extends Controller
{
     public $alphabank;
        public $order_id;
        public $mid ;
        public $secret;
        public $mode = 1;
        public $alpha_authorize = "no";
    public function __construct()
    {
        $this->mid = env('ALPHA_BANK_MID');
        $this->secret = env('ALPHA_BANK_SECRET');
        $this->alphabank = new AlphaBank;
     }



    public function checkout(Request $request)
    {
        $order_id = $request->input('order_id');
        $order = Order::find($order_id);
        $merchant_reference = uniqid() . $order_id;
        $installments = $request->input('installments', 1);

        $post_url = $this->mode === 1
            ? 'https://alphaecommerce-test.cardlink.gr/vpos/shophandlermpi'
            : 'https://www.alphaecommerce.gr/vpos/shophandlermpi';

        $form_data_array = [
            'version' => 2,
            'mid' => $this->mid,
            'lang' => app()->getLocale(),
//            'deviceCategory' => $request->isMobile() ? 1 : 0,
            'deviceCategory' => 0,
            'orderid' => $merchant_reference,
            'orderDesc' => 'Order: ' . $order_id . ' Name: ' . $order->billing_name,
            'orderAmount' => number_format($order->detail->total, 2, '.', ''),
            'currency' => 'EUR',
            'payerEmail' => $order->detail->billing_email,
            'payerPhone' => $this->formatPhone($order->billing_phone, $order->billing_country),
            'billCountry' => $order->detail->billing_country,
            'billZip' => $order->detail->billing_zip,
            'billCity' => $order->detail->billing_city,
            'billAddress' => $order->detail->billing_address_1,
            'shipCountry' => $order->detail->shipping_country,
            'shipZip' => $order->detail->shipping_zip,
            'shipCity' => $order->detail->shipping_city,
            'shipAddress' => $order->detail->shipping_address_1,
            'confirmUrl' => route('payment.confirm', ['order' => $order->id]),
            'cancelUrl' => route('payment.cancel', ['order' => $order->id]),
        ];

        if ($order->billing_country == "GR") {
            unset($form_data_array['billState']);
            unset($form_data_array['shipState']);
        }

        if ($installments > 1) {
            $form_data_array['extInstallmentoffset'] = 0;
            $form_data_array['extInstallmentperiod'] = $installments;
        }

        $digest = $this->calculateDigest($form_data_array);

//        DB::table('alphabank_transactions')->insert([
//            'reference' => $order->billing_email,
//            'merch_ref' => $merchant_reference,
//            'orderid' => $order_id,
//            'timestamp' => now()
//        ]);

        $form_html = '<form id="shopform1" name="shopform1" method="POST" action="' . $post_url . '" accept-charset="UTF-8">';
        foreach ($form_data_array as $key => $value) {
            $form_html .= '<input type="hidden" id="' . $key . '" name="' . $key . '" value="' . $value . '"/>';
        }
        $form_html .= '<input type="hidden" id="digest" name="digest" value="' . $digest . '"/>
            <div class="payment_buttons">
                <input type="submit" class="button alt" id="submit_alphabank_payment_form" value="Pay via Alpha Bank" />
                <a class="button cancel" href="' . route('checkout.cancel', ['order' => $order->id]) . '">Cancel order &amp; restore cart</a>
            </div>
            <script type="text/javascript">
                document.querySelector(".payment_buttons").style.display = "none";
                document.getElementById("shopform1").submit();
            </script>
        </form>';

        return response($form_html);

    }

    protected function calculateDigest($form_data)
    {
        $concatenated_values = implode('', array_values($form_data)) . $this->secret;
        return base64_encode(hash('sha256', $concatenated_values, true));
    }
    public function form($order)
    {
        $form_data_array = [];
        $fieldsArr = [];

        $fieldsArr['mid'] = $this->alphabank->mid;
        $form_data_array[1] = $fieldsArr['mid'] ;                                                   //Req
        $fieldsArr['lang'] = 'el';
        $form_data_array[2] = $fieldsArr['lang'];                                                   //Opt

        $fieldsArr['orderid'] = $order->order_num;
        $form_data_array[4] = $fieldsArr['orderid'];                                                //Req

        $fieldsArr['orderDesc'] = 'Eshoes Order';
        $form_data_array[5] = $fieldsArr['orderDesc'];                                              //Opt

        $fieldsArr['orderAmount'] = number_format($order->detail->total, 2);
        $form_data_array[6] = $fieldsArr['orderAmount'];

        $form_data_array[7] = 'EUR';                                               //Req
        $fieldsArr['currency'] = $form_data_array[7];

        $fieldsArr['payerEmail'] = $order->detail->billing_email;
        $form_data_array[8] = $fieldsArr['payerEmail'];

        $fieldsArr['billCountry'] = "GR";
        $form_data_array[10] = $fieldsArr['billCountry'];

        $fieldsArr['billState'] = $order->detail->billing_region;
        $form_data_array[11] = $fieldsArr['billState'];

        $fieldsArr['billZip'] = $order->detail->billing_zip;
        $form_data_array[12] = $fieldsArr['billZip'];

        $fieldsArr['billCity'] = $order->detail->billing_city;
        $form_data_array[13] = $fieldsArr['billCity'];

        $fieldsArr['billAddress'] = $order->detail->billing_street. ' '. $order->detail->billing_street_number;
        $form_data_array[14] = $fieldsArr['billAddress'];

        $fieldsArr['trType'] = 2;
        $form_data_array[26] = $fieldsArr['trType'];

        $fieldsArr['confirmUrl'] = $this->alphabank->confirm_url;
        $form_data_array[33] = $fieldsArr['confirmUrl'];
        //Req
        $fieldsArr['cancelUrl'] = $this->alphabank->fail_url;
        $form_data_array[34] = $fieldsArr['cancelUrl'];                                             //Req

        $form_secret = $this->alphabank->secret;
        $form_data_array[40] = $form_secret;                                                        //Req

        // $digest = base64_encode(sha1($form_data, true));
        $digest = base64_encode(sha1(implode('', $form_data_array), true));
        $fieldsArr['digest'] = $digest;

        return $fieldsArr;
    }

    /**
         * Verify a successful Payment!
    * */
    public function success(Request $request)
    {
        $response = false;
        $response = $this->check_response($request);
        if ($response) {
            return $response;

        } else { //is suuceeess
            $order = Order::where('order_num', $request->orderid)->firstOrFail();
            //update status id of order detail
            $order->details()->update(['status_id' => 2]);
            $order->comments()->create(
                ['body' => 'Order: ' . $request->get('status') . '<br/> <b>PaymentRef:</b> ' . $request->get('paymentRef') . '<br/> <b>TxId:</b> ' . $request->get('txId')]
            );
            $order->comments()->create(['body' => '<b>Capture Amount :</b>' . $request->get('paymentTotal')]);
            \Mail::to($order->user->email)->send(new NewOrder($order));
            \Mail::to(config('settings.notification_email'))->send(new NewOrder($order));
            \Cart::clear();
            return $this->complete($order);
        }
    }

    public function complete($order)
    {
        return redirect()->route('checkout.completed')->with('order', $order->id);
    }

    public function fail(Request $request)
    {
        $order = Order::where('order_num', $request->orderid)->firstOrFail();
        $error = $order->comment;
        $response = $this->check_response($request);
        if ($response) {
            $error .= '<br/>' . $response;
        }

        $error .= ' ' . 'status ' . $request->status;
        if ($request->get('digest')) {
            $order->update(['comment' => $error]);
        }

        return view(setting('theme') . '.fail', compact('order'));
    }

    public function check_response($request)
    {
        $required_response = [
            'mid' => '',
            'orderid' => '',
            'status' => '',
            'orderAmount' => '',
            'currency' => '',
            'paymentTotal' => '',
        ];

        $notrequired_response = [
            'message' => '',
            'riskScore' => '',
            'payMethod' => '',
            'txId' => '',
            'sequence' => '',
            'seqTxId' => '',
            'paymentRef' => ''
        ];

        if (!$request->get('digest')) {
            return "'Alpha Bank Request Failure', 'Alpha Bank Gateway''response' 500";
        }

        foreach ($required_response as $key => $value) {
            if ($request->get($key)) {
                $required_response[$key] = $request->get($key);
            } else {
                \Log::channel('alphabank')->info('Alpha Bank Request Failure\', \'Alpha Bank Gateway\'\'response\' 500 - 2');

                return "'Alpha Bank Request Failure', 'Alpha Bank Gateway''response' 500 - 2";
                // required parameter not set
            }
        }

        foreach ($notrequired_response as $key => $value) {
            if ($key == 'riskScore') {
                $required_response[$key] = ($value == 0) ? 0 : $value;
            } elseif ($request->get($key)) {
                $required_response[$key] = (string)$request->get($key);
            } else {
            }
        }

        $string_form_data = array_merge($required_response, ['secret' => $this->alphabank->secret]);
        // return $string_form_data;
        $digest = base64_encode(sha1(implode('', $string_form_data), true));
        if ($digest != $request->get('digest')) {
            return "'Alpha Bank Request Failure', 'Alpha Bank Gateway''response' 500";
        }

        if ($request->get('cancel')) {
            // set cancel message to order

            // $order = wc_get_order(wc_clean($_REQUEST['cancel']));
            // if (isset($order)) {
            //     $order->add_order_note('Alpha Bank Payment <strong>' . $required_response['status'] . '</strong>. txId: ' . $required_response['txId'] . '. ' . $required_response['message']);
            //     wp_redirect($order->get_cancel_order_url_raw());
            //     exit();
            // }
        } elseif ($request->get('confirm')) {
            //get order
            dd($request->all());
            // $order = wc_get_order(wc_clean($_REQUEST['confirm']));
            if (isset($order)) {
                // if ($required_response['orderAmount'] == wc_format_decimal($order->get_total(), 2, false)) {
                //     $order->add_order_note('Alpha Bank Payment <strong>' . $required_response['status'] . '</strong>. txId: ' . $required_response['txId'] . '. payMethod: ' . $required_response['payMethod'] . '. paymentRef: ' . $required_response['paymentRef'] . '. ' . $required_response['message']);
                //     $order->payment_complete('Alpha Bank Payment ' . $required_response['status'] . '. txId: ' . $required_response['txId']);
                //     wp_redirect($this->get_return_url($order));
                //     exit();
                // } else {
                //     $order->add_order_note('Payment received with incorrect amount. Alpha Bank Payment <strong>' . $required_response['status'] . '</strong>. ' . $required_response['message']);
                // }
            }
        }

        // something went wrong so die
        return null;
    }

    function formatPhone($phone,$country = null) {
        $country_codes = array('AC' => '247', 'AD' => '376', 'AE' => '971', 'AF' => '93', 'AG' => '1268', 'AI' => '1264', 'AL' => '355', 'AM' => '374', 'AO' => '244', 'AQ' => '672', 'AR' => '54', 'AS' => '1684', 'AT' => '43', 'AU' => '61', 'AW' => '297', 'AX' => '358', 'AZ' => '994', 'BA' => '387', 'BB' => '1246', 'BD' => '880', 'BE' => '32', 'BF' => '226', 'BG' => '359', 'BH' => '973', 'BI' => '257', 'BJ' => '229', 'BL' => '590', 'BM' => '1441', 'BN' => '673', 'BO' => '591', 'BQ' => '599', 'BR' => '55', 'BS' => '1242', 'BT' => '975', 'BW' => '267', 'BY' => '375', 'BZ' => '501', 'CA' => '1', 'CC' => '61', 'CD' => '243', 'CF' => '236', 'CG' => '242', 'CH' => '41', 'CI' => '225', 'CK' => '682', 'CL' => '56', 'CM' => '237', 'CN' => '86', 'CO' => '57', 'CR' => '506', 'CU' => '53', 'CV' => '238', 'CW' => '599', 'CX' => '61', 'CY' => '357', 'CZ' => '420', 'DE' => '49', 'DJ' => '253', 'DK' => '45', 'DM' => '1767', 'DO' => '1809', 'DO' => '1829', 'DO' => '1849', 'DZ' => '213', 'EC' => '593', 'EE' => '372', 'EG' => '20', 'EH' => '212', 'ER' => '291', 'ES' => '34', 'ET' => '251', 'EU' => '388', 'FI' => '358', 'FJ' => '679', 'FK' => '500', 'FM' => '691', 'FO' => '298', 'FR' => '33', 'GA' => '241', 'GB' => '44', 'GD' => '1473', 'GE' => '995', 'GF' => '594', 'GG' => '44', 'GH' => '233', 'GI' => '350', 'GL' => '299', 'GM' => '220', 'GN' => '224', 'GP' => '590', 'GQ' => '240', 'GR' => '30', 'GT' => '502', 'GU' => '1671', 'GW' => '245', 'GY' => '592', 'HK' => '852', 'HN' => '504', 'HR' => '385', 'HT' => '509', 'HU' => '36', 'ID' => '62', 'IE' => '353', 'IL' => '972', 'IM' => '44', 'IN' => '91', 'IO' => '246', 'IQ' => '964', 'IR' => '98', 'IS' => '354', 'IT' => '39', 'JE' => '44', 'JM' => '1876', 'JO' => '962', 'JP' => '81', 'KE' => '254', 'KG' => '996', 'KH' => '855', 'KI' => '686', 'KM' => '269', 'KN' => '1869', 'KP' => '850', 'KR' => '82', 'KW' => '965', 'KY' => '1345', 'KZ' => '7', 'LA' => '856', 'LB' => '961', 'LC' => '1758', 'LI' => '423', 'LK' => '94', 'LR' => '231', 'LS' => '266', 'LT' => '370', 'LU' => '352', 'LV' => '371', 'LY' => '218', 'MA' => '212', 'MC' => '377', 'MD' => '373', 'ME' => '382', 'MF' => '590', 'MG' => '261', 'MH' => '692', 'MK' => '389', 'ML' => '223', 'MM' => '95', 'MN' => '976', 'MO' => '853', 'MP' => '1670', 'MQ' => '596', 'MR' => '222', 'MS' => '1664', 'MT' => '356', 'MU' => '230', 'MV' => '960', 'MW' => '265', 'MX' => '52', 'MY' => '60', 'MZ' => '258', 'NA' => '264', 'NC' => '687', 'NE' => '227', 'NF' => '672', 'NG' => '234', 'NI' => '505', 'NL' => '31', 'NO' => '47', 'NP' => '977', 'NR' => '674', 'NU' => '683', 'NZ' => '64', 'OM' => '968', 'PA' => '507', 'PE' => '51', 'PF' => '689', 'PG' => '675', 'PH' => '63', 'PK' => '92', 'PL' => '48', 'PM' => '508', 'PR' => '1787', 'PR' => '1939', 'PS' => '970', 'PT' => '351', 'PW' => '680', 'PY' => '595', 'QA' => '974', 'QN' => '374', 'QS' => '252', 'QY' => '90', 'RE' => '262', 'RO' => '40', 'RS' => '381', 'RU' => '7', 'RW' => '250', 'SA' => '966', 'SB' => '677', 'SC' => '248', 'SD' => '249', 'SE' => '46', 'SG' => '65', 'SH' => '290', 'SI' => '386', 'SJ' => '47', 'SK' => '421', 'SL' => '232', 'SM' => '378', 'SN' => '221', 'SO' => '252', 'SR' => '597', 'SS' => '211', 'ST' => '239', 'SV' => '503', 'SX' => '1721', 'SY' => '963', 'SZ' => '268', 'TA' => '290', 'TC' => '1649', 'TD' => '235', 'TG' => '228', 'TH' => '66', 'TJ' => '992', 'TK' => '690', 'TL' => '670', 'TM' => '993', 'TN' => '216', 'TO' => '676', 'TR' => '90', 'TT' => '1868', 'TV' => '688', 'TW' => '886', 'TZ' => '255', 'UA' => '380', 'UG' => '256', 'UK' => '44', 'US' => '1', 'UY' => '598', 'UZ' => '998', 'VA' => '379', 'VA' => '39', 'VC' => '1784', 'VE' => '58', 'VG' => '1284', 'VI' => '1340', 'VN' => '84', 'VU' => '678', 'WF' => '681', 'WS' => '685', 'XC' => '991', 'XD' => '888', 'XG' => '881', 'XL' => '883', 'XN' => '857', 'XN' => '858', 'XN' => '870', 'XP' => '878', 'XR' => '979', 'XS' => '808', 'XT' => '800', 'XV' => '882', 'YE' => '967', 'YT' => '262', 'ZA' => '27', 'ZM' => '260', 'ZW' => '263');
        $default_country = 'GR';
        if (strpos($default_country, ':') !== false) {
            $expl=explode(":",$default_country);
            $default_country=$expl[0];
        }
        $default_country_code = $country_codes[$default_country];
        $numbers = [$phone => $country];
        foreach ($numbers as $n => $c) {
            $n = preg_replace("/\([0-9]+?\)/", "", $n);
            $n = preg_replace("/[^0-9]/", "", $n);
            $n = ltrim($n, '0');
            if (array_key_exists($c, $country_codes)) {
                $pfx = $country_codes[$c];
            } else {
                $pfx = $default_country_code;
            }
            if (!preg_match('/^' . $pfx . '/', $n)) {
                $n = $pfx .'-'. $n;
            }

            if (strpos($n,'-')===false) {
                $n=substr_replace( $n, "-", 2, 0 );
            }
            return $n;
        }
    }
}
