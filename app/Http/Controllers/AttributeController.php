<?php

namespace App\Http\Controllers;

use App\Models\Attribute;
use Illuminate\Http\Request;

class AttributeController extends Controller
{
    public function index()
    {
        $attributes = Attribute::get();
        return view('manage.attributes.index', compact('attributes'));
    }

    public function create()
    {
        return view('attribute.create');
    }

    public function store(Request $request)
    {
        $attribute = new Attribute();
        $attribute->title = $request->title;
        $attribute->erp_id = $request->erp_id;
        $attribute->name = $request->slug;
        $attribute->type = $request->type;
        $attribute->order_by = $request->order_by;
        $attribute->visible_on_product_page = $request->visible_on_product_page;
        $attribute->used_for_variations = $request->used_for_variations;
        $attribute->has_colors = $request->has_colors;
        $attribute->has_images = $request->has_images;
        $attribute->save();
        return redirect()->route('manage.attributes.index');
    }

    public function edit($id)
    {
        $attribute = Attribute::find($id);
        return view('manage.attributes.edit', compact('attribute'));
    }

    public function update(Request $request, $id)
    {
        $attribute = Attribute::find($id);
        $attribute->title = $request->title;
        $attribute->erp_id = $request->erp_id;
        $attribute->name = $request->slug;
        $attribute->type = $request->type;
        $attribute->order_by = $request->order_by;
        $attribute->visible_on_product_page = $request->visible_on_product_page;
        $attribute->has_colors = $request->has_colors;
        $attribute->has_images = $request->has_images;
        $attribute->save();
        return redirect()->route('attributes.index');
    }

    public function destroy($id)
    {
        $attribute = Attribute::find($id);
        $attribute->delete();
        return redirect()->route('attribute.index');
    }

    public function attributes(Request $request)
    {

       return $attributes = Attribute::with('terms')
//            ->whereJsonContainsLocale('title', 'el', $request->get('q').' in%', 'like')->get()
            ->get();
         return response()->json($attributes);
    }

    public function values(Attribute $attribute)
    {
        return $attribute->values()->pluck('title', 'id');
    }


}
