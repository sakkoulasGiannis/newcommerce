<?php

namespace App\Http\Controllers;

use App\Models\Attribute;
use App\Models\AttributeTerm;
use Illuminate\Http\Request;

class AttributeTermController extends Controller
{

    public function index(Attribute $attribute)
    {

        return view('manage.attributes.terms', compact('attribute'));
    }

    public function term_values(Attribute $attribute)
    {
        return $terms = $attribute->terms;

    }

    public function term_values_update(Request $request, Attribute $attribute, $id)
    {
        AttributeTerm::find($id)->update($request->all());

        return $attribute->terms()->find($id);
    }
    public function uploadFile(Request $request, Attribute $attribute, $id)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);





        if ($request->file('image')) {
            $image = $request->file('image');
            $imageName = time().'.'.$image->extension();
            $image->move(public_path('images'), $imageName);

            $f = AttributeTerm::find($id);
            $f->image = $imageName;
            $f->save();


            return response()->json(['success' => 'Image uploaded successfully.', 'image' => $imageName]);
        }

        return response()->json(['error' => 'Image upload failed.'], 400);
    }

}
