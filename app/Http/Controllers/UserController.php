<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{

    public function index()
    {

        $data_url = '/manage/users_list_json';
        $edit_url = '/manage/users';
        $headers = [
            ['text' => 'Id', 'value' => 'id'],
            ['text' => 'Όνομα', 'value' => 'name'],
            ['text' => 'Επίθετο', 'value' => 'lastname'],
            ['text' => 'Email', 'value' => 'email'],
            ['text' => 'Ρόλος', 'value' => 'roles[0].name'],
            ['text' => 'Επιλογές', 'value' => 'actions'],
        ];
        return view('manage.users.index', compact('data_url', 'edit_url', 'headers'));
    }

    public function edit(User $user)
    {

        // $user->roles()->get();
        $roles = Role::all();

        return view('manage.users.edit', compact('user', 'roles'));
    }

    public function orders()
    {
        $orders = Auth::user()->orders()->where('');
        return view(\Config('view.theme') . '.account.orders', compact('orders'));
    }

    public function account()
    {
        $user = Auth::user();

        return view(\Config('view.theme') . '.account', compact('user'));
    }

    public function listJson(Request $request)
    {
        return $products = User::with('roles', 'permissions')->paginate($request->get('paginate'));
    }

    public function show(User $user)
    {
        return view('manage.users.show', compact('user'));
    }

    /**
     * @param Request $request
     * @return void
     * Create user from admin page
     */
    public function createAdmin(Request $request)
    {

        $user = User::create([
            'name' => $request->user['name'],
            'lastname' => $request->user['lastname'],
            'email' => $request->user['email'],
            "password" => Hash::make($request->user['password']),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        $user->assignRole($request->user['role']);
    }

    public function create()
    {
        $roles = Role::get();
        return view('manage.users.create', compact('roles'));
    }

    public function store(Request $request)
    {
        $user = User::create([
            'name' => $request->user['name'],
            'lastname' => $request->user['lastname'],
            'mobile' => $request->user['mobile'],
            'phone' => "",
            'email' => $request->user['email'],
            "password" => Hash::make($request->user['password']),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        $user->assignRole($request->get('roles'));
    }

    public function searchUser(Request $request)
    {
        return User::with('detail')
            ->where('email', 'like', '%' . $request->get('q') . '%')
            ->orWhere('lastname', 'like', '%' . $request->get('q') . '%')
            ->orWhere('mobile', 'like', '%' . $request->get('q') . '%')
            ->get();
    }

    public function update(User $user, Request $request)
    {
         if (!$request->has('updatePassword')) {
            $user->update([
                'name' => $request->get('name'),
                'email' => $request->get('email'),

            ]);
        } else {
            $user->update([
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'password' => Hash::make($request->get('password')),
            ]);
        }

        $user->syncPermissions($request->get('permissions'));
        $user->syncRoles($request->get('role'));

        return back();
    }

    public function destroy(User $user)
    {
        return User::destroy($user->id);
    }
}
