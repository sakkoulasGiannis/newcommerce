<?php

namespace App\Http\Controllers;

//region use

use App\Helpers\AppHelper;
use App\Mail\newOrder;
use App\Models\Cart;
use App\Models\Country;
use App\Models\Courier;
use App\Models\Field;

//use App\Models\Http\Requests\CheckoutRequest;
use App\Models\Order;
use App\Models\Payment;
use App\Models\Product;
use App\Models\Stock;
use App\Models\User;
use App\Models\Voucher;
use App\PaymentMethodInterface;
use App\Services\PaymentMethodFactory;
use Carbon\Carbon;
use Dotenv\Exception\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cookie;

class CartController extends Controller
{
    // use NationalBankOfGreeceTrait;
    /**
     * cart page
     * @return $this
     */
    public function index()
    {
        if (!Cookie::get('cart_id')) {
            Cookie::queue('cart_id', uniqid(), 60 * 24 * 7); // 7 ημέρες
        }

        $cartId = Cookie::get('cart_id');
        $cart = Cart::with('items')->where('session_id', $cartId)->first();
        return view(\Config::get('view.theme') . '.cart')->with(compact('cart'));
    }

    public function itemQuantity(Request $request)
    {
        if (!Cookie::get('cart_id')) {
            Cookie::queue('cart_id', uniqid(), 60 * 24 * 7); // 7 ημέρες
        }

        $cartId = Cookie::get('cart_id');
        $cart = Cart::with('items')->where('session_id', $cartId)->first();
        $cart->items()->where('id', $request->get('product_id'))->update(['quantity' => $request->get('quantity')]);
        return true;
    }

    public function itemRemove(Request $request)
    {
        if (!Cookie::get('cart_id')) {
            Cookie::queue('cart_id', uniqid(), 60 * 24 * 7); // 7 ημέρες
        }

        $cartId = Cookie::get('cart_id');

        $cart = Cart::with('items')->where('session_id', $cartId)->first();

        if ($cart->group_id != null) {

            $items = $cart->items()->where('id', $request->get('id'))->first();
            $cart->items()->where('group_id', $items->group_id)->delete();
        } else {
            $cart->items()->where('id', $request->get('id'))->delete();
        }

        return redirect()->back();
    }

    /**
     * checkout page
     * @return $this
     */
    public function checkout()
    {
        if (!Cookie::get('cart_id')) {
            Cookie::queue('cart_id', uniqid(), 60 * 24 * 7); // 7 ημέρες
        }

        $cartId = Cookie::get('cart_id');
        $couriers = [];
        $countries = Country::with('counties')->where('is_active', 1)->get();

        $payments = [];
        $cart = Cart::with('items')->where('session_id', $cartId)->first();
        if (!$cart) {
            return view(\Config::get('view.theme') . '.checkoutNoProducts');
        }

        return view(\Config::get('view.theme') . '.checkout')->with(compact('countries', 'cart', 'payments', 'couriers'));
    }

    // @todo delete this
    public function completed(Request $request)
    {

        $order = $request->get('order_id');

        return view(\Config::get('view.theme') . '.success_checkout', compact('order'));


    }

    /**
     * submit checkout form
     * @param CheckoutRequest $request
     * @return mixed
     */
    public function checkoutProcess(Request $request)
    {
        $this->validateCheckout($request);

        $cart = $this->getCart();
        if (!$cart || $cart->items->isEmpty()) {
            abort(404, 'Cart is empty.');
        }


        $invoiceId = $this->generateInvoiceId();

        $orderData = $this->prepareOrderData($request, $cart, $invoiceId);
        // Create order and handle authentication

        $order = $this->checkAuthAndCreateOrder($orderData);

        // delete user cookie
        Cookie::queue(Cookie::forget('cart_id'));

        // Process payment
        return $this->processPayment($request, $order);
    }

    private function validateCheckout(Request $request)
    {
        $rules = [
            'billing_email' => 'required|email',
            'billing_city' => 'required',
            'billing_name' => 'required',
            'billing_lastname' => 'required',
            'billing_mobile' => 'required',
            'billing_street' => 'required',
            'payment_id' => 'required',
        ];

        $request->validate($rules);
    }

    private function getCart()
    {
        if (!Cookie::get('cart_id')) {
            Cookie::queue('cart_id', uniqid(), 60 * 24 * 7); // 7 days
        }

        $cartId = Cookie::get('cart_id');
        return Cart::with('items')->where('session_id', $cartId)->first();
    }

    private function generateInvoiceId()
    {
        return Order::count() + 1;
    }

    private function prepareOrderData(Request $request, $cart, $invoiceId)
    {
        $order = [
            'order' => [
                'order_num' => $invoiceId,
                'ip' => $request->ip(),
                'slug' => substr(md5(rand()), 0, 100) . time(),
            ],
            'comment' => $request->get('comment'),
            'detail' => $this->prepareOrderDetails($request, $cart),
            'products' => $this->prepareOrderProducts($cart),
            'user' => $this->prepareUserData($request),
            'address' => $request->get('address'),
            'shipping' => $request->get('shipping'),
        ];

        return $order;
    }

    private function prepareOrderDetails(Request $request, $cart)
    {
        return [
            'status_id' => 1,
            'shipping_cost' => $cart['shipping_extra'],
            'payment_cost' => $cart['payment_extra'],
            'invoice_id' => null,
            'payment_id' => $request->get('payment_id'),
            'shipping_id' => $request->get('shipping_id'),
            'courier_name' => Courier::find($request->get('shipping_id'))->title,
            'payment_name' => Payment::find($request->get('payment_id'))->title,
            'total' => $cart->total($cart['shipping_extra'], $cart['payment_extra']),
            'sub_total' => $cart->subTotal(),
            'billing_email' => $request->get('billing_email'),
            'billing_name' => $request->get('billing_name'),
            'billing_lastname' => $request->get('billing_lastname'),
            'billing_country_id' => $request->get('billing_country_id'),
            'billing_city' => $request->get('billing_city'),
            'billing_street' => $request->get('billing_street'),
            'billing_zip' => $request->get('billing_zip'),
            'billing_mobile' => $request->get('billing_mobile'),
            'shipping_name' => $request->get('shipping_name') ?? $request->get('billing_name'),
            'shipping_email' => $request->get('shipping_email') ?? $request->get('billing_email'),
            'created_at' => now(),
            'afm' => $request->get('afm'),
            'doy' => $request->get('doy'),
            'company' => $request->get('company'),
            'activity' => $request->get('activity'),
        ];
    }

    private function prepareOrderProducts($cart)
    {
        $products = [];
        foreach ($cart->items as $item) {
            $associatedStock = $item->stock;
            $associatedStock->decrement('quantity', $item->quantity);

            $products[] = [
                'product_id' => $associatedStock->product->id,
                'stock_id' => $associatedStock->id,
                'name' => $associatedStock->product->title,
                'sku' => $associatedStock->sku,
                'price' => $item->price,
                'quantity' => $item->quantity,
            ];
        }

        return $products;
    }

    private function prepareUserData(Request $request)
    {
        return [
            'newsletter' => $request->get('newsletter'),
            'name' => $request->get('name'),
            'lastname' => $request->get('lastname'),
            'email' => $request->get('email'),
            'mobile' => $request->get('billing_mobile'),
        ];
    }

    private function processPayment(Request $request, $order)
    {
        try {
            $paymentMethodId = $request->get('payment_id');
            $paymentName = Payment::find($paymentMethodId)->name;
            $paymentService = PaymentMethodFactory::create($paymentName);

            return $paymentService->process($order, $request);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage(),
            ], 400);
        }
    }


    public function payments(Request $payment)
    {
        $paymentSelected = Payment::find($payment->get('payment_id'));

        /**
         * find and return the path of module
         */
        $paymentType = $this->PaymentClass($paymentSelected);

        $getModule = \Module::find($paymentType);
        $aName = (string)$getModule->getName();
        $payment = "\Modules\\$aName\Entities\\$aName";

        $payment = new $payment;
        return $payment->info();
    }

    /**
     * find payment class
     * @param $paymentSelected
     * @return string
     */
    protected function PaymentClass($paymentSelected)
    {
        $paymentType = \Module::find($paymentSelected->class);
        //        $paymentType = "App\\Extensions\\" . $paymentSelected->class;

        return $paymentType;
    }


    /*
     * @param CheckoutRequest $request
     */
    protected function checkAuthAndCreateOrder($request)
    {
        $user = $this->handleUserAuthentication($request);

        if ($user) {
            $order = $user->orders()->create($request['order']);
        } else {
            $order = Order::create($request['order']);
        }

        if (!empty($request['comment'])) {
            $this->addOrderComment($order, $request['detail']['billing_name'], $request['comment']);
        }

        $detail = $order->detail()->create($request['detail']);

        $this->addOrderProducts($detail, $request['products'], $order->id);

        return $order;
    }

    private function handleUserAuthentication($request)
    {
        if (Auth::check()) {
            return Auth::user();
        }

        if (isset($request->createAccount) && $request->createAccount == 1) {
            return $this->createOrUpdateUser($request);
        }

        return User::where('email', $request['user']['email'])->first();
    }

    private function createOrUpdateUser($request)
    {
        $validatedData = request()->validate([
            'password' => 'required|confirmed',
            'email' => 'required|email',
        ]);

        DB::beginTransaction();

        try {
            $user = User::firstOrNew(['email' => $request['user']['email']]);
            $user->fill($request['user']);
            $user->password = Hash::make(request()->password);
            $user->subscribed = 1;
            $user->newsletter = $request['user']['newsletter'];
            $user->save();

            if ($user->wasRecentlyCreated || !$user->address) {
                $user->address()->create($request['address']);
            }

            DB::commit();
            return $user;
        } catch (ValidationException $e) {
            DB::rollBack();
            throw $e;
        }
    }

    private function addOrderComment($order, $userName, $comment)
    {
        $order->comments()->create([
            'user_role' => 'user', // @todo make this dynamic
            'user_name' => $userName,
            'comment' => $comment,
        ]);
    }

    private function addOrderProducts($detail, $products, $orderId)
    {
        foreach ($products as &$product) {
            $product['order_id'] = $orderId;
        }

        $detail->products()->createMany($products);
    }


    /*
     * Products in Cart
    */
    public function cartItems()
    {
        $items = [];

        \Cart::getContent()->each(function ($item) use (&$items) {
            $items[] = $item;
        });
        return $items;
    }

    /*
     * Cart Details
     * total
    */
    public function cartDetails()
    {
        $cartConditions = \Cart::getConditions();
        $conditions = [];
        foreach ($cartConditions as $key => $condition) {
            $conditions[$key]['target'] = $condition->getTarget(); // the target of which the condition was applied
            $conditions[$key]['name'] = $condition->getName(); // the name of the condition
            $conditions[$key]['type'] = $condition->getType(); // the type
            $conditions[$key]['value'] = $condition->getValue(); // the value of the condition
            $conditions[$key]['order'] = $condition->getOrder(); // the order of the condition
            $conditions[$key]['attributes'] = $condition->getAttributes(); // the attributes of the condition, returns an empty [] if no attributes added
        }

        return [
            'total_quantity' => \Cart::getTotalQuantity(),
            'sub_total' => \Cart::getSubTotal(),
            'total' => \Cart::getTotal(),
            'conditions' => $conditions
        ];
    }

    /*
             * add product in cart
    */
    public function add(Request $request)
    {

        switch ($request->get('type')) {
            case 'simple':
                return $this->addSimple($request);
                break;
            case 'configurable':
                return $this->addConfigurable($request);
                break;
            case 'dynamic':
                return $this->addDynamic($request);
                break;
        }

        $stock = Stock::find($request->get('stock'));

        if (is_null($stock)) {
            abort(404); //@todo return custom error
        }

        $product = $stock->product;
        $saleCondition = null;

        // return $stock->variations;
        $attributes = [];

        foreach ($stock->variations as $sv) {
            $name = Field::find($sv->field_id)->name;
            $field = $sv->model_type;
            $value = $field::find($sv->model_id)->title;
            $attributes[] = [
                $name => $value,
                'img_url' => $product->getFirstMediaUrl('product'),
                'title' => $product->titld
            ];
        }
        /**
         * Assign products to cart
         */
        $product = [
            'id' => $stock->id,
            'name' => $product->title,
            'price' => $product->price,
            'quantity' => ($request->get('quantity') ? $request->get('quantity') : 1),
            'product_id' => $stock->product->id,
            'product_sku' => $product->sku,
            'attributes' =>
            // [
                $attributes,
            // 'associated_id' => $stock->id,
            // 'size' => $stock->value->title,
            // // 'color' => $associated->product->color->title,
            // 'img_url' => $product->getFirstMediaUrl('product'),
            // ],
            'conditions' => $saleCondition,
        ];

        \Cart::add($product);
        \Session::flash('message', 'Το Προϊόν Προστέθηκε στο Καλάθι σας.');

        return redirect()->back();
    }

    public function addConfigurable($request)
    {

        $stock = Stock::findOrFail($request->get('stock'));
        $product = $stock->product;

        $price = $product->price;
        $product = [
            'sku' => $stock->sku,
            'id' => $stock->id,
            'name' => $product->title,
            'price' => $price,
            'quantity' => ($request->get('quantity') ? $request->get('quantity') : 1),
            'product_id' => $product->id,
            'attributes' => [
                'img_url' => $product->getFirstMediaUrl(),
                'sku' => $stock->sku,
                'product' => $product->id,
                'product_sku' => $product->sku,
                'product_url' => $product->name,
            ],
            // 'conditions' => $saleCondition,
        ];
        \Cart::add($product);
        // show modal for cart redirect
        $request->session()->flash('modal', true);
        \Session::flash('message', 'Το Προϊόν Προστέθηκε στο Καλάθι σας.');

        return redirect()->back();
    }

    public function addSimple($request)
    {
        $product = Product::where('id', $request->get('product'))->first();
        $price = $product->price;
        $product = [
            'id' => $product->id,
            'name' => $product->name,
            'price' => $price,
            'quantity' => ($request->get('quantity') ? $request->get('quantity') : 1),
            'product_id' => $product->id,
            'attributes' => [
                'img_url' => $product->getFirstMediaUrl('product'),
            ],
            // 'conditions' => $saleCondition,
        ];

        \Cart::add($product);
        return redirect()->route('cart');
    }

    public function dynamic_price(Request $request)
    {

        $product = \App\Product::find($request->get('product'));
        $width = $request->get('width');
        $depth = $request->get('depth');

        $fields = [
            'width' => $request->get('width'),
            'depth' => $request->get('depth'),
        ];

        $price = $product->price;
        $query = json_decode($product->calculator->query, true);

        for ($a = 0; $a < count($query['children']); $a++) {
            echo '-' . $a . "\n";
            $item = $query['children'][$a]['query']['children'];

            echo '-- ' . $item[0]['query']['rule'] . "\n";
            if (isset($fields[$item[0]['query']['rule']])) {

                if ($fields[$item[0]['query']['rule']] >= $item[0]['query']['value']) {
                    echo $item[1]['query']['value'] . "\n";
                    if ($fields[$item[0]['query']['rule']] <= $item[1]['query']['value']) {
                        continue;
                    }

                    //                    echo $item[2]['query']['value']. ' ';
                    $price += $item[2]['query']['value'];
                    //                     echo $price;
                    //                    echo 'sdf '. $item[2]['query']['value'];
                }
            }
        }
        return $price;
    }

    public function addDynamic($request)
    {
        \Cart::clearCartConditions();

        $product = Product::where('id', $request->get('product'))->first();
        if (is_null($product)) {
            abort(404); //@todo return custom error
        }

        $saleCondition = null;

        $width = $request->get('width');
        $depth = $request->get('depth');

        $fields = [
            'width' => $request->get('width'),
            'depth' => $request->get('depth'),
        ];

        $price = $product->price;
        $query = json_decode($product->calculator->query, true);

        for ($a = 0; $a < count($query['children']); $a++) {
            $item = $query['children'][$a]['query']['children'];
            if (isset($fields[$item[0]['query']['rule']])) {

                if ($fields[$item[0]['query']['rule']] >= $item[0]['query']['value'] && $fields[$item[0]['query']['rule']] <= $item[1]['query']['value']) {
                    $price += $item[2]['query']['value'];
                }
            }
        }

        /**
         * Assign products to cart
         */
        $product = [
            'id' => $product->id,
            'name' => $product->name,
            'price' => $price,
            'quantity' => ($request->get('quantity') ? $request->get('quantity') : 1),
            'product_id' => $product->id,
            'attributes' => [
                'width' => $width,
                'size' => $depth,
                'img_url' => $product->getFirstMediaUrl('product'),
            ],
            // 'conditions' => $saleCondition,
        ];

        \Cart::add($product);
        return redirect()->route('cart');
    }

    public function voucher_remove($name)
    {
        \Cart::removeCartCondition($name);
        return back();
    }


    //    function add_voucher(Request $request)
    function check_for_voucher(Request $request)
    {
        \Cart::clearCartConditions();
        $productWithSpecialPriceAmmound = 0;
        $productWithDiscounts = false;
        $checkCartTotal = false;
        $productWithSpecialPrice = [];
        $VoucherAConditionChecker = [];
        //        $VoucherAConditionChecker['totalCart'] = false;
        $request->validate(['code:required']);
        $voucher = Voucher::where('code', $request->code)->first();

        if ($voucher->uses > $voucher->max_uses) {
            return ['status' => true, 'message' => 'Το κουπόνι δεν μπορεί να χρισημοποιηθεί.'];
        }

        if (!$voucher) {
            $request->session()->flash('message', 'Δέν βρέθηκε ο εκπτωτικός κωδικός!');
            if ($request->has('redirect')) {
                return back();
            } else {
                return ['status' => true, 'message' => 'Το κουπόνι καταχωρήθηκε'];
            }
        }
        //        if ($voucher->expires_at && $voucher->expires_at <= Carbon::now()) {
        //            $VoucherAConditionChecker['voucher_expired'] = false;
        //            $request->session()->flash('message', 'O εκπτωτικός κωδικός έχει λύξει!');
        //            return 'O εκπτωτικός κωδικός έχει λύξει';
        //        }

        if ($voucher->query) {
            $query = json_decode($voucher->query, true);
            $cartProducts = \Cart::getContent();
            foreach ($cartProducts as $p) {

                //                \Cart::clearItemConditions($p['id']);

                $product = Product::where('id', $p['attributes']['product'])->get();
                //                if ($product->sale_price > 0 || $product->rule_id > 0) {
                //                    $productWithSpecialPrice[$product->id] = true;
                //                } else {
                //
                //                    $productWithSpecialPriceAmmound += $product->price;
                //                    $productWithSpecialPrice[$product->id] = false;
                //                }

                foreach ($query as $q) {


                    $validProducts = AppHelper::querymanager($q, $product);
                    if ($validProducts) {
                        $VoucherAConditionChecker[$q['label']] = true;
                    } else {
                        $VoucherAConditionChecker[$q['false']] = false;
                    }
                }

                $voucherPassQueryChecker = !in_array(false, $VoucherAConditionChecker, true);
                if (!isset($VoucherAConditionChecker['totalCart']) || $VoucherAConditionChecker['totalCart'] == false) {
                    if ($voucherPassQueryChecker) {
                        //                        $this->applyVoucher($voucher);

                        $productID = $p['id'];
                        $value = ($voucher->is_fixed == 1) ? "-" . $voucher->discount_amount : "-" . $voucher->discount_amount . '%';

                        //                        $coupon = new \Darryldecode\Cart\CartCondition(array(
                        //                            'name' => 'sadf',
                        //                            'type' => 'coupon',
                        //                            'value' => '-10',
                        //                        ));
                        //
                        //                        \Cart::addItemCondition($p['id'], $coupon);

                        $condition = new \Darryldecode\Cart\CartCondition(array(
                            'name' => $voucher->name,
                            'type' => 'coupon',
                            'target' => 'total', // this condition will be applied to cart's total when getTotal() is called.
                            'value' => $value,
                            'order' => 1 // the order of calculation of cart base conditions. The bigger the later to be applied.
                        ));
                        \Cart::condition($condition);
                        //                        return \Cart::getConditions();

                        //                        return $this->cartDetails();

                        //                        return \Cart::getConditionsByType('coupon');

                        $request->session()->flash('flash_message', 'Έγινε εφαρμογή του Eκπτωτικού κουπονιού!');
                    }
                }
            }
            if ($request->has('redirect')) {
                return back();
            } else {
                return ['status' => true, 'message' => 'Το κουπόνι καταχωρήθηκε'];
            }
        }

        return back();
    }

    public function applyVoucher(Voucher $voucher)
    {
        $condition = new \Darryldecode\Cart\CartCondition([
            'name' => $voucher->code,
            'type' => 'ΚΟΥΠΟΝΙ', // sale , promo , misk ,
            'target' => 'total', // this condition will be applied to cart's subtotal when getSubTotal() is called.
            'value' => ($voucher->is_fixed == 1) ? "-" . $voucher->discount_amount : "-" . $voucher->discount_amount . '%',
            'attributes' => [ // attributes field is optional
                'description' => $voucher->description,
            ],
        ]);
        \Cart::condition($condition);
    }

    /*
     * delete product in cart
    */
    public function delete($id)
    {
        \Cart::clearCartConditions();
        \Cart::remove($id);
        if (request()->ajax()) {
            return 'success';
        }
        return back();
        return response([
            'success' => true,
            'data' => $id,
            'message' => "cart item {$id} removed.",
        ], 200, []);
    }
}
