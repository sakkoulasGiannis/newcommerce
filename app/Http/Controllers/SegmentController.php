<?php

namespace App\Http\Controllers;

use App\Models\Attribute;
use App\Models\AttributeProductTerm;
use App\Models\AttributeTerm;
use App\Models\Category;
use App\Models\Field;
use App\Models\Language;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use hisorange\BrowserDetect\Facade as Browser;
use Illuminate\Support\Facades\Config;

class SegmentController extends Controller
{

    public $product;
    public $category;
    public $currentCategory;
    public $path;
    public $tree;
    public $breadcrumb;
    public $path_N;

    //    public function returnRequest($slug, $parent = null)
    //    {
    //        $category = Category::where('name', $slug)->first();
    //        if ($category) {
    //            return $this->renderCategoryPage($category);
    //        }
    //        $product = Product::where('name', $slug)->first();
    //        if ($product) {
    //            $this->product = $product;
    //            return $this->renderProductPage($product);
    //        }
    //
    //        return App::abort(404);
    //    }


    public function result()
    {
        $path = request()->path();
        $path = explode('/', $path);
        $this->tree = Category::where('parent_id', 0)->get();
        $this->breadcrumb = Category::whereIn('name', $path)->orderBy('parent_id', 'asc')->get();
        $this->path = $path;
        $this->path_N = 0;

        // if is translation
        if (Language::where('name', $this->path[0])->first()) {
            // set language
            \app()->setLocale($this->path[0]);
            $this->path_N = 1;

            if (!isset($this->path[$this->path_N])) {
                return view(config::get('view.theme') . '.home');
            } else {
                $tree = $this->tree->where('name', $this->path[$this->path_N])->first();
            }

            return $this->checkCat_rec($tree);
        } else {
            $tree = $this->tree->where('name', $this->path[$this->path_N])->first();
            return $this->checkCat_rec($tree);
        }
    }

    public function category($categorySlug, $ProductOrCategory, $parent = null)
    {
        $parentCategory = Category::where('name', $categorySlug)->firstOrFail();
        $category = Category::where([['name', '=', $ProductOrCategory], ['parent_id', '=', $parentCategory->id]])->first();
        if ($category) {
            $parentCategory = Category::where('name', $categorySlug)->first();

            if ($category->parent->slug === $categorySlug) {
                if (!is_null($parent)) {
                    return $this->checkForProduct($ProductOrCategory, $parent);
                } else {

                    return $this->renderCategoryPage($category);
                }
            } else {
                return App::abort(404);
            }
        }
        return $this->checkForProduct($categorySlug, $ProductOrCategory);
    }

    //    public function subCategory($categorySlug, $subCategorySlug, $ProductOrCategory)
    //    {
    //        $category = Category::where('name', $subCategorySlug)->first();
    //        if ($category) {
    //            return $this->category($categorySlug, $subCategorySlug, $ProductOrCategory);
    //        }
    //    }

    public function checkForProduct($categorySlug, $ProductOrCategory)
    {
        $product = Product::where('name', $ProductOrCategory)->first();

        if ($product) {
            if ($product->categories()->where('name', $categorySlug)->firstOrFail()) {
                return $this->renderProductPage($product);
            } else {
                return App::abort(404);
            }
        }

        return App::abort(404);
    }

    /**
     * render product page
     */
    public function renderProductPage($product)
    {


        // create productview record
        $product->views()->create([
            'user_id' => auth()->id(),
            'ip_address' => request()->ip(),
            'user_agent' => Browser::userAgent(),
            'referer' => request()->header('referer'),
            'device' => Browser::deviceFamily(), // Συσκευή
            'platform' => Browser::platformName(), // Λειτουργικό σύστημα
            'browser' => Browser::browserName(), // Πρόγραμμα περιήγησης
            'language' => request()->getPreferredLanguage(), // Γλώσσα
            'created_at' => Carbon::now(),
        ]);


        $breadcrumbs = $this->breadcrumb;
        if (Cache::get('product_' . $product->slug)) {
            return Cache::get('product_' . $product->slug);
        }
        $stock = $product->stock;

        $block = null;
        //        $fields = Field::where('position', '!=', 1)->get();
        $inStock = ($product->stock()->sum('quantity') > 0) ? true : false;
        //@todo make the take number dynamic
        $upsells = [];
        if (count($product->categories) > 0) {
            $upsells = Product::where('id', '!=', $product->id)->whereHas('categories', function ($q) use ($product) {
                $q->where('id', $product->categories->last()->id);
            })->take(6)->get();
        }


        return view(config::get('view.theme') . '.product', compact('product', 'stock', 'block', 'upsells', 'inStock', 'breadcrumbs'));
        // request()->session()->push('alsoView', $product);
        // return  $alsoViews   = request()->session()->get('alsoView');
        return view(config::get('view.theme') . '.product', compact('product', 'stock', 'block', 'fields', 'upsells', 'inStock', 'breadcrumbs'));
    }

    /**
     * render the category with products
     *
     * @param Category $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function renderCategoryPage(Category $category)
    {


        // Breadcrumbs και αρχικοποίηση μεταβλητών
        $breadcrumbs = $this->breadcrumb;
        $defaultPaginate = 24;

        // Διαχείριση pagination
        $paginate = request('pagination', session('paginate', $defaultPaginate));
        session()->put('paginate', $paginate);

        $page = request('page', 1);
        $filters = request()->except(['pagination', 'query', 'page', 'per_page', 'sort']);

        // Cache key generation για τα προϊόντα
        $cacheKey = "category_products_{$category->id}_page_{$page}_paginate_{$paginate}_" . md5(json_encode($filters));

        // Αν υπάρχει cache για τα προϊόντα, τα ανακτούμε
//        if (Cache::has($cacheKey)) {
//            $products = Cache::get($cacheKey);
//        } else {
            // Λήψη προϊόντων βάσει κατηγορίας
            $categoryIds = $category->children()->pluck('id')->push($category->id);

            $productsQuery = $category->dynamic_query_is_active && !is_null($category->query)
                ? $this->runQuery($category->query)
                : Product::whereHas('categories', fn($query) => $query->whereIn('categories.id', $categoryIds));

            // Επιλογή πεδίων
            $productsQuery->select([
                'products.id',
                'products.title',
                'products.name',
                'products.price',
                'products.sale_price',
                'products.sku',
            ]);

            // Εφαρμογή φίλτρων
            foreach ($filters as $attribute => $terms) {
                $attributeModel = Attribute::where('name', $attribute)->first();
                if ($attributeModel) {
                    $attributeId = $attributeModel->id;
                    $termIds = AttributeTerm::whereIn('name', (array) $terms)->pluck('id');
                    $productsQuery->whereHas('attribute_terms', fn($query) => $query->whereIn('attribute_term_id', $termIds));
                }
            }
             // Εφαρμογή ταξινόμησης
            if ($sort = request('sort')) {
                $this->applySort($productsQuery, $sort);
            }

            // Paginate προϊόντων
            $products = $productsQuery->paginate($paginate)->withQueryString();

            // Αποθήκευση των προϊόντων στη μνήμη cache
            Cache::put($cacheKey, $products, now()->addMinutes(30)); // Cache για 30 λεπτά
//        }

        // Απόκτηση χαρακτηριστικών
        $attributes = $this->attributesByProductId($products->pluck('id'));

        // Επιστροφή του view
        return view(config('view.theme') . '.category', compact('products', 'attributes', 'category', 'breadcrumbs'));
    }

    /**
     * Εφαρμογή ταξινόμησης σε προϊόντα.
     */
    private function applySort($query, $sort)
    {


        switch ($sort) {
            case 'price_asc':
            case 'price_desc':
                $direction = $sort === 'price_asc' ? 'ASC' : 'DESC';

                $query->leftJoin('rules', 'products.rule_id', '=', 'rules.id')
                    ->orderByRaw("
                    CASE
                        WHEN products.sale_price < products.price AND products.sale_price > 1 THEN products.sale_price
                        WHEN products.rule_id > 0 THEN products.price - (products.price * (rules.discount_value / 100))
                        ELSE products.price
                    END $direction
                  ");
                break;

            case 'discount_asc':
            case 'discount_desc':
                $direction = $sort === 'discount_asc' ? 'ASC' : 'DESC';
                $query->leftJoin('rules', 'products.rule_id', '=', 'rules.id')
                    ->orderByRaw("
                    CASE
                        WHEN products.sale_price > 2 THEN ((products.price - products.sale_price) / products.price) * 100
                        ELSE rules.discount_value
                    END $direction
                  ");
                break;

            case 'season_asc':
            case 'season_desc':
                $direction = $sort === 'season_asc' ? 'ASC' : 'DESC';
                $query->orderBy('products.created_at', $direction);
                break;

            default:
                $query->orderBy('products.id', 'DESC');
                break;
        }
    }




    public function renderCategoryPageOld(Category $category, Request $request = null)
    {

        $breadcrumbs = $this->breadcrumb;

        $paginate = 24;
        // set pagination
        if (request()->has('pagination')) {
            session()->put('paginate', request()->get('pagination'));
        } elseif (!session()->exists('paginate')) {
            session()->put('paginate', $paginate);
        }
        $page = request()->has('page') ? request()->get('page') : 1;

        // if (Cache::get('category_' . $category->id . '_page_' . $page . \Request::fullUrl() . '_pagination_' . session()->get('paginate')),) {
        // 	return Cache::get('category_' . $category->id . '_page_' . $page . \Request::fullUrl() . '_pagination_' . session()->get('paginate'));
        // 	dd();
        // }

        $path = request()->path();


        $categoryIds = $category->children()->pluck('id')->push($category->id);


        //        $productsQuery = Product::whereHas('categories', function ($query) use ($categoryIds) {
        //            $query->whereIn('categories.id', $categoryIds);
        //        });


        if ($category->dynamic_query_is_active && !is_null($category->query)) {
            $products = $this->runQuery($category->query);
            $products = $products
                ->with('brand')
                ->select(['products.id', 'products.title', 'products.name', 'products.price', 'products.sale_price', 'products.sku'])
                ->orderBy('products.id', 'DESC');
        } else {

            $products = Product::whereHas('categories', function ($query) use ($categoryIds) {
                $query->whereIn('categories.id', $categoryIds);
            })->select(['products.id', 'products.title', 'products.name', 'products.price', 'products.sale_price', 'products.sku'])
                ->orderBy('id', 'DESC');


            $filters = request()->input();

            foreach ($filters as $attribute => $terms) {

                if ($attribute === 'pagination' || $attribute === 'query' || $attribute === 'page' || $attribute === 'per_page') continue;
                $attributeId = Attribute::where('name', $attribute)->first()->id;

                $termIds = AttributeTerm::whereIn('name', $terms)->pluck('id');

                $products->whereHas('attribute_terms', function ($query) use ($attributeId, $termIds) {
                    $query
                        //                        ->where('attribute_id', $attributeId)
                        ->whereIn('attribute_term_id', $termIds);
                });
            }
        }
        //attributes
        $productsIds = $products->pluck('id'); // Assuming $products is a collection of products
        $attributes = $this->attributesByProductId($productsIds);


        if (request()->has('sort') && request()->get('sort') == 'price_asc' || request()->get('sort') == 'price_desc') {

            $sort = (request()->get('sort') == 'price_asc') ? 'ASC' : 'DESC';

            $products
                ->leftjoin('rules', 'products.rule_id', '=', 'rules.id')
                ->select('products.*')
                ->orderByRaw('CASE WHEN products.sale_price < products.price AND products.sale_price > 1 THEN products.sale_price
                    WHEN products.rule_id > 0 THEN products.price - (products.price * (rules.discount_value / 100))
                    ELSE
                    products.price
                    END ' . $sort);
        } elseif (request()->has('sort') && request()->get('sort') == 'discount_asc' || request()->get('sort') == 'discount_desc') {

            $sort = (request()->get('sort') == 'discount_asc') ? 'ASC' : 'DESC';

            $products
                ->leftjoin('rules', 'products.rule_id', '=', 'rules.id')
                ->orderByRaw('CASE WHEN products.sale_price > 2  THEN ((products.price - products.sale_price)/products.price) * 100
                    ELSE
                    rules.discount_value
                    END ' . $sort)
                ->select('products.*');
        } elseif (request()->has('sort') && request()->get('sort') == 'season_asc' || request()->get('sort') == 'season_desc') {

            $sort = (request()->get('sort') == 'season_asc') ? 'ASC' : 'DESC';

            $products
                ->orderBy('products.created_at', $sort);
        } else {
            $products
                //                ->join('field_seasons_product', 'products.id', '=', 'field_seasons_product' . '.product_id')
                ->orderBy('id');
        }


        //        $products = $products->fastPaginate()->withQueryString();
        $products = $products->paginate(session()->get('paginate'))->withQueryString();
        return view(config::get('view.theme') . '.category', compact('products', 'attributes', 'category', 'breadcrumbs'));
    }

    public function attributesByProductId($productIds)
    {
        // Βήμα 1: Λήψη των διαθέσιμων product_attribute_terms με βάση τα productIds
        $productAttributeTerms = DB::table('attribute_product_terms')
            ->join('products', 'attribute_product_terms.product_id', '=', 'products.id')
            ->leftJoin('stock_variations', 'products.id', '=', 'stock_variations.stock_id')
            ->whereIn('products.id', $productIds)
            //            ->where(function ($query) {
            //                $query->where(function ($query) {
            //                    $query->where('products.product_type', 'simple')
            //                        ->where('products.quantity', '>', 0);
            //                })->orWhere(function ($query) {
            //                    $query->where('products.product_type', 'stock_variations')
            //                        ->where('stock_variations.stock_quantity', '>', 0);
            //                });
            //            })
            ->select('attribute_product_terms.*')
            ->distinct()
            ->get();

        // Λήψη των attribute_ids και attribute_term_ids από τα διαθέσιμα product_attribute_terms
        $attributeIds = $productAttributeTerms->pluck('attribute_id')->unique();
        $termIds = $productAttributeTerms->pluck('attribute_term_id')->unique();

        // Βήμα 2: Λήψη των attributes και attribute_terms με βάση τα διαθέσιμα attribute_ids και term_ids
        $attributes = DB::table('attributes')
            ->whereIn('id', $attributeIds)
            ->get();

        $attributeTerms = DB::table('attribute_terms')
            ->whereIn('id', $termIds)
            ->get();

        // Δομή των αποτελεσμάτων σε dimensional array
        $local = \app()->getLocale();
        $attributesWithTerms = $attributes->map(function ($attribute) use ($attributeTerms, $local) {
            $attribute_title = json_decode($attribute->title, true);

            return [
                'id' => $attribute->id,
                'title' => $attribute_title[$local],
                'name' => $attribute->name,
                'terms' => $attributeTerms->where('attribute_id', $attribute->id)->map(function ($term, $local) {
                    $termTitle = json_decode($term->title, true);
                    return [
                        'id' => $term->id,
                        'title' => $termTitle,
                        'name' => $term->name,
                    ];
                })->values()
            ];
        })->toArray();

        return $attributesWithTerms;
    }


    public function searchStockFieldValues($field, $ids)
    {
        //        if ($ids == null) {
        //            return null;
        //        }
        //
        //        $field = strtolower($field);
        //
        //        $query = "
        //        SELECT DISTINCT fs.id,
        //            JSON_UNQUOTE(JSON_EXTRACT(fs.title, '$.el[0]')) AS title,
        //            fs.name
        //        FROM field_sizes AS fs
        //        LEFT JOIN stock_variations AS sv ON fs.id = sv.model_id
        //        LEFT JOIN stocks AS s ON s.id = sv.stock_id
        //        WHERE s.product_id IN (" . $ids . ")
        //            AND s.quantity >= 1
        //        ORDER BY title ASC
        //    ";
        //
        //        return DB::select($query);
    }


    /**
     * return category filter values
     * @param mixed $field
     * @param mixed $ids
     * @return mixed
     */
    public function searchProductFieldValues($field, $ids)
    {

        if ($ids == null) {
            return null;
        }
        $field = strtolower($field);
        return \DB::select(\DB::raw(
            "select
            distinct fc.id,
            fc.name,
            fcp.product_id,
            fcp.field_" . $field . "_id,
            JSON_UNQUOTE(JSON_EXTRACT(fc.title,'$.el[0]')) as title

            from  field_" . $field . "s as fc
            LEFT JOIN field_" . $field . "s_product fcp ON fcp.field_" . $field . "_id = fc.id
            WHERE fcp.product_id IN (" . $ids . ")

            GROUP BY fc.id
            ORDER BY title ASC
            "

        ));
    }

    public function search($products, $field)
    {
        $fieldModel = $field->model_type;
        $value = $fieldModel::where('name', request()->get($field->name))->first();
        $products->whereHas($field->name, function ($query) use ($value) {
            $query->where('attribute_field', $value->id);
        });

        return $products;
    }


    public function checkCat_rec($category)
    {
        $isCategory = Category::where('name', end($this->path))->first();

        if ($isCategory) {
            return $this->renderCategoryPage($isCategory);
        } else {
            $product = Product::where('name', end($this->path))->first();
            if ($product) {
                if ($this->currentCategory && !$this->currentCategory->dynamic) {
                    return $this->renderProductPage($product);
                } elseif ($this->currentCategory && $this->currentCategory->products->where('name', $this->path[$this->path_N])) {

                    return $this->renderProductPage($product);
                } else {
                    return $this->renderProductPage($product);
                }
            }

            return App::abort(404);
        }


        // old way ;

        if (count($this->path) > 0 && $this->path_N < count($this->path) - 1 && $category instanceof Category && $category && $category->children) {

            $this->path_N++;

            $category = $category->children->where('name', $this->path[$this->path_N])->first();
            if ($category && $category instanceof Category) {
                $this->currentCategory = $category;
            }

            return $this->checkCat_rec($category);
        } elseif ($category && $category instanceof Category) {
            return $this->renderCategoryPage($category, null);
        } else {
            $product = Product::where('name', $this->path[$this->path_N])->first();
            if ($product) {
                if ($this->currentCategory && !$this->currentCategory->dynamic) {
                    return $this->renderProductPage($product);
                } elseif ($this->currentCategory && $this->currentCategory->products->where('name', $this->path[$this->path_N])) {
                    return $this->renderProductPage($product);
                } else {
                    return $this->renderProductPage($product);
                }
            }

            return App::abort(404);
        }
    }

    public function runQuery($query)
    {

        $products = Product::hasStock();

        foreach (json_decode($query, true) as $rule) {
            switch ($rule['id']) {
                case 'category':
                    $products = $products->whereHas('categories', function ($query) use ($rule) {
                        $query->whereIn('category_id', $rule['value']);
                    });
                    // $productincategories = DB::table('product_category')
                    //     ->whereIn('category_id', $rule['value'])
                    //     ->groupBy('product_id')
                    //     ->pluck('product_id');
                    // $products = $products->whereIn('id', $productincategories);
                    break;
                case 'price':
                    $products = $products->where($rule['type'], $rule['query'], $rule['value']);
                    break;
                case 'inventory':
                    $products = $products->inventory($rule['value'], $rule['query']);
                    break;
                case 'created_at':

                    $products = $products->where('products.created_at', $rule['operator'], now()->subDays($rule['value'])->endOfDay());
                    break;
                case 'discounted':
                    $products = $products->where('sale_price', '>', 0)
                        ->orWhere('rule_id', '>', 0);
                    break;
                default:

                    //                    $products = $products->filter('color', 'mple');
                    $products = $this->filter($products, $rule);
                    break;
            }
        }

        return $products;
    }

    public function filter($products, $data)
    {
        // search by date
        if (isset($data['inputType']) && $data['inputType'] == 'datetime') {
            return $products->where($data['type'], $data['query'], $data['value']);
        }

        if (isset($data['inputType']) && $data['inputType'] == 'sku') {
            return $products->where($data['type'], $data['query'], $data['value']);
        }

        //search by attribute
        $products->whereHas($data['type'], function ($query) use ($data) {

            if (is_array($data['value'])) {
                if ($data['query'] == '=') {
                    $query->whereIn('attribute_field', $data['value']);
                } else {
                    $query->whereNotIn('attribute_field', $data['value']);
                }
            } else {
                $query->where('attribute_field', $data['query'], $data['value']);
            }
        });

        return $products;
    }

    //// delete this
    public function getCategoryFields(array $categoryIds)
    {
        // Step 1: Retrieve all fields dynamically
        $fields = DB::table('fields')
            ->select('name', 'model_type')
            ->get();

        // Step 2: Create dynamic SELECT part of the query
        $selectParts = [];
        $joins = [];

        foreach ($fields as $field) {
            $modelClass = $field->model_type;
            $model = new $modelClass;
            $table = $model->getTable();

            // Ensure the table name is unique in the query to avoid conflicts
            $alias = $table . '_' . $field->name;

            $selectParts[] = "   {$alias}.title  as {$field->name}";
            $joins[] = "LEFT JOIN {$table}_product fp_{$alias} ON p.id = fp_{$alias}.product_id";
            $joins[] = "LEFT JOIN {$table} {$alias} ON fp_{$alias}.field_{$field->name}_id = {$alias}.id";
        }

        // Ensure that selectParts and joins are not empty
        if (empty($selectParts) || empty($joins)) {
            return response()->json(['error' => 'No fields found for the specified category'], 400);
        }

        $selectClause = implode(', ', $selectParts);
        $joinClause = implode(' ', $joins);

        // Step 3: Create the final query
        $sql = "
            SELECT
                c.title as category,
                $selectClause
            FROM
                products p

            $joinClause
            WHERE
                c.id IN (" . implode(',', array_fill(0, count($categoryIds), '?')) . ")

        ";

        // Step 4: Execute the query
        $results = DB::select($sql, $categoryIds);

        return response()->json($results);
    }
}
