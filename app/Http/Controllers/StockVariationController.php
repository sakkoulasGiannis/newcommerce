<?php

namespace App\Http\Controllers;

use App\Models\Field;
use App\Models\Stock;
use App\Models\StockVariation;
use Illuminate\Http\Request;

class StockVariationController extends Controller
{

    public function create(Stock $stock, Request $request ){

 
        return $request->input();
        $field = Field::where('name', $request->get('fieldId'))->first();

        $stockId = $request->get('stockId');

        if($stockId == null ){
            $stock = Stock::create(
                [
                    // 'sku'=> 
                    // 'product_id'=> 
                    // 'sku'=> 
                    // 'sku'=> 
                    // 'sku'=> 
                    // 'sku'=> 
                ]
            );
        }

        StockVariation::create([
            'stock_id'=> $stockId,
            'field_id'=>$field->id,
            'model_type' => "\App\Fields\Field".ucFirst($request->get('fieldId')),
            'model_id'=>$request->get('valueId')
        ]);
        return $stock->variations;
    }
    public function destroy(Request $request){
        StockVariation::destroy($request->get('id'));
    }
}
