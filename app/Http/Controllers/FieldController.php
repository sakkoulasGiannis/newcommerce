<?php

namespace App\Http\Controllers;

use App\Models\Language;
use App\Models\Product;
use App\Scopes\ActiveProducts;
use Illuminate\Http\Request;
use App\Models\Field;
use Illuminate\Support\Facades\Artisan;

class FieldController extends Controller
{

    public function index(Request $request)
    {
        $fields = Field::orderBy('positions', 'asc')->get();
        return view('manage.fields.index', compact('fields'));
    }

    public function create()
    {
        return view('manage.fields.create');
    }

    public function term_values(Field $field)
    {
        $model = $field->model_type;
        return $terms = $model::get();
    }

    public function term_values_update(Request $request, Field $field, $id)
    {
        $model = $field->model_type;

        $model = new $model;
        $tableName = "field_" . $field->name;
        $table = app($model::class)->getTable();
        // form validation
        $validated = $request->validate([
            'title' => 'required',
            'name' => 'required'
        ], [
            'name.required' => 'The name field is mandatory.',
            'title.required' => 'An email address is required.',

        ]);
         $model::find($id)->update($request->all());

        return $model::find($id);
    }

    public function uploadFile(Request $request, Field $field, $id)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);
        $model = $field->model_type;

        $model = new $model;
        $tableName = "field_" . $field->name;


        if ($request->file('image')) {
            $image = $request->file('image');
            $imageName = time().'.'.$image->extension();
            $image->move(public_path('images'), $imageName);

            $f = $model::find($id);
            $f->image = $imageName;
            $f->save();


            return response()->json(['success' => 'Image uploaded successfully.', 'image' => $imageName]);
        }

        return response()->json(['error' => 'Image upload failed.'], 400);
    }


    public function term_values_store(Request $request, Field $field)
    {
        $model = $field->model_type;
        $model = new $model;
        $tableName = "field_" . $field->title;
        $table = app($model::class)->getTable();
        // form validation

        $validated = $request->validate([
            'title' => 'required',
            'name' => 'required|unique:' . $table
        ], [
            'name.required' => 'The name field is mandatory.',
            'name.unique' => 'The name field must be unique.',
            'title.required' => 'An email address is required.',

        ]);
        $model->name = $request->get('name');
        $title = $request->get('title');
        if($request->has('color')  ){

            $model->color = $request->get('color');
        }
        foreach ($title as $l => $t) {
            $model->setTranslation('title', $l, $t);

        }
        $model->save();
        return $model;
//            $model::create($request->all());
        // return success response with message
        return response()->json(['success' => 'Term added successfully.']);
    }

    public function terms(Field $field)
    {
        return view('manage.fields.terms', compact('field'));
    }

    public function edit(Field $field)
    {
        return view('manage.fields.edit', compact('field'));
    }

    public function store(Request $request)
    {
        $name = $request->get('name');
        // run artisan command make:field name
        Artisan::call('field:make', ['name' => $name]);
        return redirect()->route('fields.index');
    }

    public function update(Request $request, Field $field)
    {

        $can_select_multiple = ($request->has('can_select_multiple')) ? 1 : 0;
        $is_read_only = ($request->has('is_read_only')) ? 1 : 0;
        $is_enabled = ($request->has('is_enabled')) ? 1 : 0;
        $show_in_filters = ($request->has('show_in_filters')) ? 1 : 0;
        $has_colors = ($request->has('has_colors')) ? 1 : 0;
        $has_images = ($request->has('has_images')) ? 1 : 0;

        if($has_colors && $has_images){
            $has_colors = 0;
        }

        $request->merge(['can_select_multiple' => $can_select_multiple]);
        $request->merge(['is_read_only' => $is_read_only]);
        $request->merge(['is_enabled' => $is_enabled]);
        $request->merge(['show_in_filters' => $show_in_filters]);
        $request->merge(['has_colors' => $has_colors]);
        $request->merge(['has_images' => $has_images]);

        $field->update($request->all());
        return redirect()->route('fields.edit', $field->id);
    }

    public function get_fields()
    {
        return Field::where('is_enabled', 1)->orderBy('title', 'asc')->get();
    }

    // create new attribute term

    public function add_field_attribute(Request $request)
    {

        $field = $request->get('field');
        $value = $request->get('value');
        $model = $field['field']['model_type'];
        return $newInput = $model::create(['created_at' => now(), 'title' => $value, 'name' => str_slug($value, '-') . '-' . time()]);
    }

    public function get_field_values(Request $request)
    {
         $field = Field::where('name', $request->get('field'))->first();
        $fieldName = $request->get('field');
         $model = $field->model_type;

        if ($field->type == 'text') {
//            if(count(Product::find($request->get('product'))->$fieldName) > 0){
            return Product::withoutGlobalScope(ActiveProducts::class)->find($request->get('product'))->$fieldName->first();
//            }
        } elseif ($field->type == 'textarea') {
             return Product::withoutGlobalScope(ActiveProducts::class)->find($request->get('product'))->$fieldName->first()->get(['id', 'body']);
        } elseif ($field->type == 'select') {
             $model = new $model;
             return $model->get();
        }

//        $model = 'App\Fields\Field' . ucfirst($field->name);
//        return $model::get();
    }

    public function get_field_values_all(Request $request)
    {


        $field = Field::where('name', $request->get('field'))->first();

        if ($field->type == 'text') {
            $fieldName = $field->name;
            return Product::find($request->get('product'))->$fieldName->get(['id', 'body']);
        }

        $model = 'App\Fields\Field' . ucfirst($field->name);
        return $model::get(['id', 'title']);
    }
}
