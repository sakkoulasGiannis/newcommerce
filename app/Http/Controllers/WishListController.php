<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;

class WishListController extends Controller
{
    public function index()
    {
        $products = [];

        if(Auth::check() && Auth()->user()->wishlist->count() > 0){
            $products = Auth()->user()->wishlist->map(function($item){
                return $item->product;
            });
        }else{
            return redirect()->back()->with('error', 'No products in wishlist');
        }


            return view(config::get('view.theme') . '.wishlist', compact('products'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'product_id' => 'required|integer|exists:products,id',

        ]);

        $wishlist = Auth()->user()->wishlist()->where('product_id', $request->product_id)->first();

        if($wishlist){
            $wishlist->update([
                'quantity' => $wishlist->quantity + $request->quantity
            ]);
        }else{
            Auth()->user()->wishlist()->create([
                'product_id' => $request->product_id,
                'quantity' => $request->quantity??1
            ]);
        }

        return redirect()->back()->with('success', 'Product added to wishlist');
    }

    public function destroy($id)
    {
        Auth()->user()->wishlist()->where('product_id', $id)->delete();

        return redirect()->back()->with('success', 'Product removed from wishlist');
    }
}
