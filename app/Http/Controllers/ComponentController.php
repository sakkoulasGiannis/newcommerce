<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ComponentController extends Controller
{
    public function index()
    {
        return view('components.index');
    }

    public function create()
    {
        return view('components.create');
    }

    public function update(Request $request, Component $component)
    {
        $request->validate([
            'name' => 'required',
            'category' => 'required',
            'components' => 'required',
        ]);

        $component->update($request->all());

        return redirect()->route('components.index')
            ->with('success', 'Component updated successfully');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'category' => 'required',
            'components' => 'required',
        ]);

        $component = Component::create($request->all());
        return $component;
        return redirect()->route('components.index')
            ->with('success', 'Component created successfully.');
    }
}
