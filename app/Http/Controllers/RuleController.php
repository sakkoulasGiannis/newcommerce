<?php

namespace App\Http\Controllers;

use Algolia\AlgoliaSearch\Exceptions\RetriableException;
use App\Models\Field;
use App\Models\Product;
use App\Models\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RuleController extends Controller
{
    var $groupOr = false;

    public function index()
    {
        $edit_url = '/manage/rules';
        $data_url = '/manage/rules_list_json';
        $headers = [
            [
                'text' => 'Title',
                'align' => 'start',
                'sortable' => true,
                'value' => 'title',
            ],
            ['text' => 'Ποσοστό Έκπτωσης', 'value' => 'discount_value'],
            ['text' => 'Ενεργό', 'value' => 'is_active'],
            ['text' => 'Επιλογές', 'value' => 'actions'],

        ];

        return view('manage.rules.index', compact('headers', 'data_url', 'edit_url'));
    }

    public function listJson(Request $request)
    {
        return $products = Rule::orderBy('created_at', 'desc')->paginate($request->get('paginate'));
    }

    public function json_rule(Rule $rule)
    {
        return $rule;
    }

    public function queryls($rules, $results)
    {
        foreach ($rules as $rule) {
            switch ($rule['type']) {
                case 'category':
                    $results = $results->when($rule['type'] == 'category', function ($query) use ($rule) {
                        return $query->byCategory($rule['value']);
                    });
                    break;
            }
        }

    }

    /*
             * used in rule
             * to search products
    */
    public function filter_products(Request $request)
    {

        return $this->query($request->get('query'));
    }

    public function query($input)
    {

        $products = Product::query();

        $productModel = new Product();

        foreach ($input as $q) {
            $products = \App\Helpers\AppHelper::querymanager($q, $products);

//            $products = $productModel->querymanager($q, $products);
        }
        return $products;

        $globalLogicalOperator = $input['logicalOperator'];
        $this->groupOr = ($globalLogicalOperator == 'any') ? true : false;
        foreach ($input['children'] as $q) {
            if ($q['type'] == 'query-builder-rule') {

                $products = $this->querymanager($q, $products);
            } elseif ($q['type'] == 'query-builder-group') {
                $this->groupOr = false;
                $this->calcGroupToChild($q, $products);
            }
        }

        return $products->get(['id', 'sku', 'title', 'price', 'sale_price', 'rule_id']);
    }

    function calcGroupToChild($group, $products)
    {

        foreach ($group['query']['children'] as $q) {

            if ($q['type'] == 'query-builder-rule') {
                $products = \App\Helpers\AppHelper::instace()->queryManager($q, $products);
//                $products = $this->querymanager($q, $products);
            } elseif ($q['type'] == 'query-builder-group') {
                $this->calcGroupToChild($q, $products);
            }
            $this->groupOr = true;
        }
    }


    function calcToQuery($input)
    {
        if (isset($input['children'])) {
            foreach ($input['children'] as $child) {
                if ($child['type'] == 'query-builder-rule') {
                    $this->calcToQuery($child);
                } elseif ($child['type'] == 'query-builder-group') {
                    $this->calcGroupToChild();
                }
            }
        }

    }

    public function store(Request $request)
    {


        $rule = new Rule();
        $rule->title = $request->title;
        $rule->is_active = $request->is_active;
        $rule->discount_type = $request->discount_type;
        $rule->query = $request->get('query');
        $rule->discount_value = $request->discount_value;
        $rule->save();
        $products = Product::query();
        $products = \App\Helpers\AppHelper::queryManager($request->get('query'), $products);
        $products = Product::whereIn('id', $products->pluck('id'))->update(['rule_id' => $rule->id]);
        return $rule;
    }

    public function show(Rule $rule)
    {

        return view('manage.rules.edit', compact('rule'));
    }


    public function applyFIlters(Request $request)
    {
        $rule = $request->get('rule');
        $products = $this->query($rule['query'])->get();
        foreach ($request->get('form') as $key => $v) {

//            if ($key != 'attributes' && $key != 'fields' &&  $v['is_active']) {
//                continue;
//            }
             foreach ($products as $p) {

                switch ($key) {
                    case 'sale_price':
                        if ($v['is_active'] && $v['value'] == 0 || $v['value'] == null) {
                            $special = null;
                        } else {
                            if ($v['type'] == 'percentage') {
                                $special = $p->price - ($p->price * ($v['value'] / 100));
                            } else {
                                $special = $v['value'];

                            }
                            $p->update([$key => $special]);
                        }
                        break;
                    case 'price':
                        if ($v['is_active'] &&  $v['value'] > 0) {
                            $p->update([$key => $v]);
                        }
                        break;

                    case 'categories':
                        if(!$v['is_active']){
                            break;
                        }
                        if ($v['type'] == 'add') {
                            $p->categories()->attach($v['values']);
                        } elseif ($v['type'] == 'replace') {
                            $p->categories()->sync($v['values']);
                        } elseif ($v['type'] == 'remove') {
                            $p->categories()->detach($v['values']);
                        }

                        break;
                    case 'fields':
                        if(!$v['is_active']){
                            break;
                        };
                        foreach ($v as  $field) {
                            if (!$field['is_active']) {
                                break;
                            }

                            // if type add
                            if($field['type'] == 'add') {
                                if ($p->fields()->where('field_id', $field['field'])->count() > 0) {
//                                $p->fields()->detach($field['field']);
                                } else {
                                    $p->fields()->attach($field['field']);
                                }
                                $fieldName = $field['name'];

//                                if($p->{$fieldName}()->find($field['values'])->count() > 0) {
                                    foreach($field['values'] as $value) {

                                        if($p->{$fieldName}()->find($value)){
    //                                        $p->{$fieldName}()->updateExistingPivot($value, ['field_value_id' => $value]);
                                        }else{
                                            $p->{$fieldName}()->attach($value);
                                        }

                                    }
//                                }else{
//                                    $p->{$fieldName}()->attach($field['values']);
//                                }

                            }elseif ($field['type'] == 'replace') {
                                // Έλεγξε αν το πεδίο υπάρχει ήδη
                                if ($p->fields()->where('field_id', $field['field'])->count() > 0) {
                                    // Αν υπάρχει ήδη, αποσύνδεσέ το (αν χρειάζεται να το αποσυνδέσεις, ξεκίνα το σχόλιο στην παρακάτω γραμμή)
                                    // $p->fields()->detach($field['field']);
                                } else {
                                    // Αν δεν υπάρχει, σύνδεσέ το
                                    $p->fields()->attach($field['field']);
                                }

                                // Πάρε το όνομα του πεδίου
                                $fieldName = $field['name'];

                                // Σβήσε τις υπάρχουσες εγγραφές στο πεδίο
                                $p->{$fieldName}()->detach();

                                // Σύνδεσε τις νέες τιμές στο πεδίο
                                $p->{$fieldName}()->sync($field['values']);
                            }elseif($field['type'] == 'remove') {


                                $fieldName = $field['name'];
                                foreach($field['values'] as $value) {
                                    $p->{$fieldName}()->detach($value);
                                }



                            }elseif($field['type'] == 'removeField') {
                                $fieldName = $field['name'];
                                $p->{$fieldName}()->detach();
                                $p->fields()->detach($field['field']);
                            }
                        }
                        break;
                    case 'attributes':

                        foreach ($v as $attribute) {
                            if (!$attribute['is_active']) {
                                continue;
                            }
                            if ($attribute['type'] == 'add') {
                                $p->attributes()->attach($attribute['attribute'], ['attribute_term_id' => $attribute['value']]);
                            } elseif ($attribute['type'] == 'replace') {
                                $p->attributes()->sync([$attribute['attribute'] => ['attribute_term_id' => $attribute['value']]]);
                            } elseif ($attribute['type'] == 'remove') {
                                $p->attributes()->detach($attribute['attribute']);
                            }
                        }
                        break;

                    default:
                        $p->update([$key => $v]);
                }

            }
        }

        return true;
    }


    public
    function update(Rule $rule, Request $request)
    {

        $rule->update($request->input());
        $products = $this->query($request->get('query'))->get();

//        Product::whereIn('id', $products->pluck('id'))->update(['rule_id' => null]);
        Product::whereIn('id', $products->pluck('id'))->update(['rule_id' => $rule->id]);
        return true;
    }

    public
    function create()
    {
        return view('manage.rules.create');
    }

    public
    function destroy($id)
    {

        $rule = Rule::findOrFail($id);
        $products = $this->query($rule->query);
        Product::whereIn('id', $products->pluck('id'))->update(['rule_id' => null]);
        Rule::destroy($id);
        return $this->index();
    }

}
