<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Theme;
use App\Models\ThemeComponent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use PHPageBuilder\PHPageBuilder;
use function Laravel\Prompts\error;

class ThemeController extends Controller
{
    public function index()
    {


        $themes = Theme::all();
        return view('manage.theme.index', compact('themes'));
//          return view('manage.theme-builder');

    }

    public function themes()
    {
        return [];
    }

    public function preview()
    {
        return view('manage.theme.preview');
    }

    public function create()
    {
        return view('manage.theme.create');
    }

    public function edit(Theme $theme)
    {
        $components = $theme->components;
        $component = $theme->components()->first();
        return redirect()->route('theme.component.edit', ['theme' => $theme, 'component' => $component, 'components' => $components]);
        return view('manage.theme.edit', compact('components','component', 'theme'));
//        return view('builder.editor', ['theme'=>$theme]);
    }

    public function component_edit(Theme $theme,ThemeComponent $component)
    {
        $components = $theme->components;
         return view('manage.theme.edit', compact('components', 'component', 'theme'));
    }

    public function store(Request $request)
    {

        $request->validate([
            'title' => 'required|string',
            'name' => 'required|string',
        ]);

        $theme = Theme::updateOrCreate(
            ['name' => $request->get('name')],
            ['title' => $request->get('title'), 'active' => $request->get('active')]);


        $componentsToCreate = [
            'header',
            'footer',
            'account',
            'breadcrumb',
            'cart',
            'category',
            'checkout',
            'checkoutNoProducts',
            'menu',
            'pagination',
            'product',
            'home',
            'login',
            'register'
        ];
        foreach ($componentsToCreate as $component) {
            $theme->components()->create(['page_type' => $component]);
        }

        $themeDirectory = public_path('builder/themes/' . $theme->name);

// Έλεγχος αν ο φάκελος υπάρχει, αλλιώς τον δημιουργούμε
        if (!File::exists($themeDirectory)) {
            File::makeDirectory($themeDirectory, 0755, true);
        }

// Δημιουργία αρχείων HTML για κάθε component μέσα στο φάκελο του theme
        foreach ($componentsToCreate as $component) {
            $filePath = $themeDirectory . '/' . $component . '.html';

            // Έλεγχος αν το αρχείο υπάρχει ήδη, αν ναι, παραλείπουμε τη δημιουργία
            if (File::exists($filePath)) {
                continue; // Αν υπάρχει ήδη, πηγαίνουμε στο επόμενο component
            }


            // Δημιουργούμε το αρχικό περιεχόμενο HTML για κάθε αρχείο
            $htmlContent = "<!DOCTYPE html>\n<html>\n<head>\n\t<title>$component Page</title>\n</head>\n<body>\n\t<h1>$component Page</h1>\n</body>\n</html>";

            // Δημιουργούμε το αρχείο και γράφουμε το περιεχόμενο
            File::put($filePath, $htmlContent);

            // Αποθηκεύουμε το component στη βάση δεδομένων
//            $theme->components()->create(['page_type' => $component]);
        }


        return redirect()->route('theme.index');
    }

    public function save_theme_editor(Theme $theme, Request $request)
    {
        define('MAX_FILE_LIMIT', 1024 * 1024 * 2);//2 Megabytes max html file size
        define('ALLOW_PHP', false);//check if saved html contains php tag and don't save if not allowed
        define('ALLOWED_OEMBED_DOMAINS', [
            'https://www.youtube.com/',
            'https://www.vimeo.com/',
            'https://www.x.com/',
            'https://x.com/',
            'https://publish.twitter.com/',
            'https://www.twitter.com/',
            'https://www.reddit.com/',
        ]);//load urls only from allowed websites for oembed

        if (isset($request->html)) {
            $html = substr($_POST['html'], 0, MAX_FILE_LIMIT);
            if (!ALLOW_PHP) {
                //if (strpos($html, '<?php') !== false) {
                if (preg_match('@<\?php|<\? |<\?=|<\s*script\s*language\s*=\s*"\s*php\s*"\s*>@', $html)) {
                    showError('PHP not allowed!');
                }
            }

//            αποθήκευση main component
            if ($request->has('html')) {

                // storer to live theme
                if ($request->has('file')) {
                    $fileName = $request->get('file') . '.blade.php';
                    $filePath = public_path('themes/' . $theme->name . '/' . $fileName);
                    $dir = dirname($filePath);

                    // Έλεγχος αν ο φάκελος υπάρχει, αν όχι τον δημιουργούμε
                    if (!File::isDirectory($dir)) {
                        echo "$dir folder does not exist\n";
                        if (File::makeDirectory($dir, 0777, true)) {
                            echo "$dir folder was created\n";
                        } else {
                            return showError("Error creating folder '$dir'\n");
                        }
                    }

                    // Χρησιμοποιούμε τη DOMDocument για να εξάγουμε το περιεχόμενο του data-main-content
                    $html = $request->get('html');
                    $dom = new \DOMDocument();

                    // Απενεργοποίηση της αναφοράς σφαλμάτων για να αποφύγουμε τα warning από κακόμορφο HTML
                    libxml_use_internal_errors(true);

                    // Φορτώνουμε το HTML στο DOMDocument
                    $dom->loadHTML($html);

                    // Παίρνουμε το περιεχόμενο του div με το data-main-content
                    $xpath = new \DOMXPath($dom);
                    $mainContentDiv = $xpath->query('//*[@data-main-content]')->item(0);
                    $mainContent = '';
                    $layoutStart = '';
                    $layoutEnd = '';

                    if ($request->get('file') != 'Navigation' && $request->get('file') != 'Footer') {
                        $layoutStart .= "@extends('default.Layout') @section('content')";
                    }

                    // Αρχικοποίηση περιεχομένου

                    // Αν βρεθεί το div, αποθηκεύουμε το εσωτερικό του περιεχόμενο
                    if ($mainContentDiv) {
                        // Αποθηκεύουμε μόνο το περιεχόμενο του div χωρίς το ίδιο το div tag
                        foreach ($mainContentDiv->childNodes as $childNode) {
                            $mainContent .= $dom->saveHTML($childNode);
                        }
                    }

                    // Minify το περιεχόμενο
                    $mainContent = $this->minifyHtml($mainContent);
                    if ($request->get('file') != 'Navigation' && $request->get('file') != 'Footer') {
                        $layoutEnd .= "@endsection";
                    }
                    $mainContent = $layoutStart . $mainContent . $layoutEnd;
                    // Ελέγχουμε αν το αρχείο υπάρχει ήδη
                    if (!File::exists($filePath)) {
                        if (File::put($filePath, $mainContent)) {
                            echo "File saved '$fileName'\n";
                        } else {
                            return showError("Error saving file '$fileName'\nPossible causes are missing write permission or incorrect file path!");
                        }
                    } else {
                        File::put($filePath, $mainContent);
                        echo "File '$fileName' Έγινε ενημέρωση του αρχείου .\n";
                    }
                } else {
                    return showError('Filename is empty!');
                }


                //store to vvveb theme folder
                if ($request->has('file')) {
                    $fileName = $request->get('file') . '.php';
                    $filePath = public_path('/builder/themes/' . $theme->name . '/' . $fileName);
                    $dir = dirname($filePath);
                    $bodyContent = $request->get('html');
                    // Έλεγχος αν ο φάκελος υπάρχει, αν όχι τον δημιουργούμε
                    if (!File::isDirectory($dir)) {
                        echo "$dir folder does not exist\n";
                        if (File::makeDirectory($dir, 0777, true)) {
                            echo "$dir folder was created\n";
                        } else {
                            return showError("Error creating folder '$dir'\n");
                        }
                    }
                    // Χρησιμοποιούμε τη DOMDocument για να εξάγουμε το περιεχόμενο του body

                    if (!File::exists($filePath)) {
                        if (File::put($filePath, $bodyContent)) {
                            echo "File saved '$fileName'\n";
                        } else {
                            return showError("Error saving file '$fileName'\nPossible causes are missing write permission or incorrect file path!");
                        }
                    } else {
                        File::put($filePath, $bodyContent);
                        echo "File '$fileName' Έγινε ενημέρωση του αρχείου .\n";
                    }
                } else {
                    return showError('Filename is empty!');
                }
            } else {
                return showError('Html content is empty!');
            }

            return $request->all();
        }
    }

// Συνάρτηση για minification του HTML
    function minifyHtml($html)
    {
        // Αφαιρεί κενά, tabs και νέες γραμμές
        return preg_replace(['/>\s+</', '/\s+/', '/<!--.*?-->|\t|(?:\r?\n[ \t]*)+/s'], ['><', ' ', ''], $html);
    }

    public function save_component(Theme $theme, ThemeComponent $component,  Request $request)
    {



        $theme->components()->updateOrCreate(
            ['id' => $component->id],
            ['html' => $request->get('html'), 'css' => $request->get('css'), 'js' => $request->get('js'), 'template' => $request->get('template')]);


        // check if directory exist if not create it
        $directory_name = 'themes/' . $theme->name;
        if (!file_exists($directory_name)) {
            mkdir($directory_name, 0777, true);
        }

        // save the component
        $file_name = $directory_name . '/' . $component->page_type . '.blade.php';
         $updatedComponent = $this->fixComponent($request->get('html'), $theme, $component);

        $updatedComponent = $this->minifyHtml($updatedComponent);
        $updatedComponent .= "<style>" . $request->get('css') . "</style>";

        $file = fopen($file_name, "w") or die("Unable to open file!");


        fwrite($file, $updatedComponent);
        fclose($file);


        return response()->json(['message' => 'Component saved successfully!']);


        $htmlContent = $validated['content'];
        $cleanHtml = preg_replace('/\s*(data-v-\w+|data-draggable|draggable)="[^"]*"/', '', $htmlContent);

        $htmlHeader = $validated['header'];
        $cleanHeader = preg_replace('/\s*(data-v-\w+|data-draggable|draggable)="[^"]*"/', '', $htmlHeader);

        $footer = $validated['footer'];
        $cleanFooter = preg_replace('/\s*(data-v-\w+|data-draggable|draggable)="[^"]*"/', '', $footer);
        dd($cleanHtml);
//        $filePath = resource_path('views/home.blade.php');
//
//        // Αποθηκεύουμε το HTML περιεχόμενο στο αρχείο home.blade.php
//        try {
//            File::put($filePath, $content);
//            return response()->json(['message' => 'Theme saved successfully!']);
//        } catch (\Exception $e) {
//            return response()->json(['message' => 'Failed to save theme!', 'error' => $e->getMessage()], 500);
//        }

        $theme = Theme::create([
            'selected_header' => $validated['selectedHeader'],
            'header_text' => $validated['headerText'],
        ]);

        return response()->json($theme);
    }

    public function fixComponent($content, $theme, $component)
    {
        // Δημιουργήστε ένα νέο DOMDocument αντικείμενο
        $htmlContent = $content;

        // Βρείτε όλα τα στοιχεία που έχουν το attribute 'data-product-title'
        $productTitlePlaceholder = '{{$product->title}}';
        $pattern = '/(<span\s+data-product-title[^>]*>)(.*?)(<\/span>)/i';
        $replacement = '$1' . $productTitlePlaceholder . '$3';
        $updatedHtmlContent = preg_replace($pattern, $replacement, $htmlContent);

        // Ενημερώστε το anchor περιεχόμενο με το
        $productHrefPlaceholder = '{{$product->name}}';
        $pattern = '/(<a\s+[^>]*data-product-name[^>]*href=")[^"]*("[^>]*>)/i';
        $replacement = '$1' . $productHrefPlaceholder . '$2';
        $updatedHtmlContent = preg_replace($pattern, $replacement, $updatedHtmlContent);


        // Αφαίρεση ολόκληρου του <div> που περιέχει το attribute 'data-dynamic-menu' και αντικατάστασή του με $replacement
        $replacement = "@include('$theme->name.desktop_menu')";
        $pattern = '/<div[^>]*\bdata-dynamic-menu\b[^>]*>.*?<\/div>/is';
        $updatedHtmlContent = preg_replace($pattern, $replacement, $updatedHtmlContent);


        $replacement = "@livewire('search-products')";
        $pattern = '/<div[^>]*\data-search-input\b[^>]*>.*?<\/div>/is';
        $updatedHtmlContent = preg_replace($pattern, $replacement, $updatedHtmlContent);

        $replacement = ' ';
        // remove data-product-name="" from $updaetdHtmlContent
        $pattern = '/\s*data-product-name=""\s*/';
        $updatedHtmlContent = preg_replace($pattern, $replacement, $updatedHtmlContent);

        // remove data-product-name="" from $updaetdHtmlContent
        $pattern = '/\s*data-product-title=""\s*/';
        $updatedHtmlContent = preg_replace($pattern, $replacement, $updatedHtmlContent);
        // Αφαίρεση μόνο των <body> και </body> tags, αφήνοντας το περιεχόμενο
        $pattern = '/<\/?body[^>]*>/i';
        $updatedHtmlContent = preg_replace($pattern, '', $updatedHtmlContent);

        if($component->page_type == 'header' || $component->page_type == 'footer'){

        }else{
            $updatedHtmlContent = "@extends('default.layout') @section('content')" . $updatedHtmlContent . "@endsection";
        }


        return $updatedHtmlContent;

// Ενημερώστε το validated['content'] με το νέο περιεχόμενο

    }

    public function bladeContent($file, Request $request)
    {

        if (!$request->has('type')) {
            return response()->json(['error' => 'type need it'], 404);
        }
        switch ($request->get('type')) {
            case 'product':
                $data = ['product' => Product::first()];
                if (!$data) {
                    return response()->json(['error' => 'product not found'], 404);
                }
                break;
            case 'grid':
                $data = [];
                break;
            default:
                return response()->json(['error' => 'type need it'], 404);

                break;
        }

        if ($request->has('render')) {
            return view('manage.theme.components.' . $file, $data);
        }

        try {

            $content = view('manage.theme.components.' . $file, $data)->render();
            return response()->json(['content' => $content]);
        } catch (\Exception $e) {
            return response()->json(['error' => 'File not found'], 404);
        }
    }

    public function components()
    {
        // Αρχικά φέρνουμε όλα τα components από το μοντέλο ThemeComponent
        $components = ThemeComponent::get()->map(function ($component) {
            // Μετατρέπουμε το component_data από JSON string σε αντικείμενο/array
            $data = json_decode($component->data, true);
            return $data; // Χρησιμοποιούμε true για array
        });

        // Επιστρέφουμε τα components ως JSON response
        return response()->json($components);
    }

    function sanitizeFileName($file, $allowedExtension = 'html')
    {
        $basename = basename($file);
        $disallow = ['.htaccess', 'passwd'];
        if (in_array($basename, $disallow)) {
            showError('Filename not allowed!');
            return '';
        }

        //sanitize, remove double dot .. and remove get parameters if any
        $file = preg_replace('@\?.*$@', '', preg_replace('@\.{2,}@', '', preg_replace('@[^\/\\a-zA-Z0-9\-\._]@', '', $file)));

        if ($file) {
            $file = __DIR__ . DIRECTORY_SEPARATOR . $file;
        } else {
            return '';
        }

        //allow only .html extension
        if ($allowedExtension) {
            $file = preg_replace('/\.[^.]+$/', '', $file) . ".$allowedExtension";
        }
        return $file;
    }


}
