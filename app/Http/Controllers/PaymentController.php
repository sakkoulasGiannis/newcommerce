<?php

	namespace App\Http\Controllers;

	use App\Models\Payment;
    use App\Models\Setting;
    use Illuminate\Http\Request;

	class PaymentController extends Controller
	{

		/*
		 * used in edit order
		 */
		public function getPayments()
		{
			return Payment::get(['id', 'title']);
		}

        public function index(){

               $settings = Setting::where('group', 'payments')->get()->groupBy('key')->map(function ($group) {
                 return $group->map(function ($item) {
                     $item->value = json_decode($item->value, true);
                     return $item;
                 });
             })->toArray();

            $payments = Payment::all();

            return view('manage.payments.index', compact('payments', 'settings'));
        }
	}
