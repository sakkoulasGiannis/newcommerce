<?php

namespace App\Http\Controllers;

use App\Models\Slider;
use Illuminate\Http\Request;
use App\Models\SliderImage;

class SliderController extends Controller
{
    /**
     * @todo remove the show funciton from web
     *
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::get();

        return view('manage.sliders.index', compact('sliders'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('manage.sliders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Slider::create(['title' => $request->title, 'is_active' => true]);
        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        return view('manage.sliders.show', compact('slider'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {
//           SliderImage::where('slider_id', $slider->id)->orderBy('order_num', 'asc')->get();

        return view('manage.sliders.edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider)
    {
        $slider->update($request->input());

        return back();
    }

    public function updateImage(SliderImage $image, Request $request)
    {
        $image->update($request->input());
        return back();
    }

    public function getImages(Slider $slider)
    {

        return $slider->images()->orderBy('order_num', 'ASC')->get();
    }

    public function addPhoto(Slider $slider, Request $request)
    {


         $slider = Slider::findOrFail($slider->id);
        $file = $request->file('url');
        $name = time() . '.' . $file->getClientOriginalExtension();
        $path = $file->storeAs("sliders/$slider->id", $name, 'public');

        $slider->images()->create(
            [
                'url' => $path,
                'alt' => $request->alt,
                'href' => $request->href,
                'order_num' => $request->order_num ?? 0,
                'body' => $request->body,
            ]
        );
         return $this->edit($slider);
    }

    public function destroyPhoto(SliderImage $image)
    {
        $image->destroy($image->id);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider)
    {
        $slider->destroy();
        return $this->index();
    }
}
