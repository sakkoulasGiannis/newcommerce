<?php

namespace App\Http\Controllers;

use App\Models\Language;
use Illuminate\Http\Request;

class LanguageController extends Controller
{
    public function index(){
        $languages = Language::get();
        return view('manage.languages.index', compact('languages'));
    }

    public function all_languages()
    {
        return Language::get();
    }
}
