<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\Product;
 use Illuminate\Support\Facades\Config;

class HomeController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index()
	{


//        $menu = Menu::where('active', 1)->first();
//        if ($menu) {
//            $menu = json_decode($menu->menu, true);
//        }else{
//            $menu = [];
//        }
//return $menu;


        $desktopSlider = \App\Models\Slider::find(1);
		if ($desktopSlider) {
			$desktopSlider = \App\Models\Slider::find(1)->images()->get();
		}
		$mobileSlider = \App\Models\Slider::find(2);
		if ($mobileSlider) {

			$mobileSlider = \App\Models\Slider::find(2)->images()->get();
		}


 		$view = config::get('view.theme') . '.home';

		return view($view);
	}
}
