<?php

	namespace App\Http\Controllers;

	use App\Models\Courier;
	use Illuminate\Http\Request;

	class CourierController extends Controller
	{


		public function getShippings()
		{
			return Courier::get();
		}

		public function index()
		{
			$couriers = Courier::all();
			return view('manage.couriers.index', compact('couriers'));
		}

		/**
		 * Show the form for creating a new resource.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function create()
		{
			return view('manage.couriers.create');
		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @param \Illuminate\Http\Request $request
		 * @return \Illuminate\Http\Response
		 */
		public function store(Request $request)
		{
			$courier = Courier::create($request->input());

			return redirect(route('courier.edit', $courier->id));
		}

		/**
		 * Display the specified resource.
		 *
		 * @param \App\Courier $courier
		 * @return \Illuminate\Http\Response
		 */
		public function show(Courier $courier)
		{
			//
		}

		/**
		 * Show the form for editing the specified resource.
		 *
		 * @param \App\Courier $courier
		 * @return \Illuminate\Http\Response
		 */
		public function edit(Courier $courier)
		{
			return view('manage.couriers.edit', compact('courier'));
		}

		/**
		 * Update the specified resource in storage.
		 *
		 * @param \Illuminate\Http\Request $request
		 * @param \App\Courier $courier
		 * @return \Illuminate\Http\Response
		 */
		public function update(Request $request, Courier $courier)
		{
			$courier->update($request->input());

			return redirect(route('courier.edit', $courier->id));
		}

		/**
		 * Remove the specified resource from storage.
		 *
		 * @param \App\Courier $courier
		 * @return \Illuminate\Http\Response
		 */
		public function destroy(Courier $courier)
		{
			//
		}
	}
