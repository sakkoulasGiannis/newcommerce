<?php

namespace App\Http\Controllers;

use App\Models\Voucher;
use Illuminate\Http\Request;

class VoucherController extends Controller
{
    public function index()
    {

        $edit_url = '/manage/rules';
        $data_url = '/manage/rules_list_json';
        $headers = [
            [
                'text' => 'Title',
                'align' => 'start',
                'sortable' => true,
                'value' => 'title',
            ],
            ['text' => 'Επιλογές', 'value' => 'actions']

        ];

        return view('manage.voucher.index', compact('headers', 'data_url', 'edit_url'));
    }

    public function listJson(Request $request)
    {
        return $products = Voucher::paginate($request->get('paginate'));
    }


    public function filterVoucher(Request $request)
    {

 
        return Voucher::where('code', $request->get('code'))->get();
    }

    public function json_rule(Voucher $voucher)
    {
        return $voucher;
    }


    public function update(Voucher $voucher, Request $request)
    {

         $voucher->update($request->input());
         return $voucher;
    }

    public function create()
    {
        return view('manage.voucher.create');
    }

    public function show(Voucher $voucher)
    {
         return view('manage.voucher.show', compact('voucher'));
    }

    public function store(Request $request)
    {
        $request = $request->get('voucher');

        $v = new Voucher();
        $v->code = $request['code'];
        $v->description = $request['description'];
        $v->query = json_encode($request['query']);
        $v->discount_amount = $request['discount_amount'];
        $v->name = $request['name'];
        $v->description = $request['description'];
        $v->uses = $request['uses'];
        $v->max_uses = $request['max_uses'];
        $v->max_uses_user = $request['max_uses_user'];
        $v->type = 0;
        $v->discount_amount = $request['discount_amount'];
        $v->is_fixed = ($request['is_fixed'] == 1) ? 1 : 0;

        $v->save();


        return $v;
    }
}
