<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductViewController extends Controller
{


    public function views(Product $product)
    {

        $views = DB::table('product_views')
            ->selectRaw('DATE_FORMAT(created_at, "%Y-%m-%d") as day, count(*) as views')
            ->where('product_id', $product->id)
            ->where('created_at', '>=', Carbon::now()->subMonth()) // Προβολές του τελευταίου έτους
            ->groupBy('day')
            ->orderBy('day', 'asc')
            ->get();

// Διασφάλιση ότι περιλαμβάνονται όλες οι ημέρες, ακόμα κι αν δεν υπάρχουν προβολές
        $days = collect(range(0, 31))->map(function ($i) {
            return Carbon::now()->subDays($i)->format('Y-m-d');
        })->reverse();

        $viewsByDay = $days->mapWithKeys(function ($day) use ($views) {
            $view = $views->firstWhere('day', $day);
            return [$day => $view ? $view->views : 0];
        });

        return response()->json($viewsByDay);

    }
}
