<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FieldValue;
use App\Models\Product;
use App\Models\Field;
use Illuminate\Support\Facades\Schema;
use App\Traits\SearchTraits;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class BrandsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $brands = \DB::select(\DB::raw(
            "SELECT b.id , b.name, p.id,
            JSON_UNQUOTE(JSON_EXTRACT(b.title,'$.el[0]')) as title
            from field_brands as b
            LEFT JOIN field_brands_product bp ON b.id = bp.field_brand_id
            LEFT JOIN products p ON bp.product_id = p.id
			RIGHT JOIN  stocks stock ON stock.product_id = p.id
            where stock.quantity > 0
            group by b.id
  "
        ));
        $brands = collect($brands);

        $brands =  $brands->sortBy('title')
            ->unique('name')
            ->groupBy(
                function ($item, $key) {
                    return (string)mb_substr($item->title, 0, 1);
                }
            );

        // $brands = \App\Fields\FieldBrand::get();

        //  $brands = $brands
        //     ->sortBy('title')
        //     ->unique('name')
        //     ->groupBy(
        //         function ($item, $key) {
        //             return (string)mb_substr($item->title, 0, 1);
        //         }
        //     );

        return view(\Config::get('settings.theme')  . '.brands', compact('brands'));
    }

    public function brand($name)
    {
        $paginate = 48;

        // set pagination
        if (request()->has('pagination')) {
            session()->put('paginate', request()->get('pagination'));
        } elseif (!session()->exists('paginate')) {
            session()->put('paginate', $paginate);
        }

        $value = \App\Fields\FieldBrand::where('name', $name)->first();

        $products = $value->product()->hasStock();

        if (Cache::has('brand' . $value->id)) {
            $fields = Cache::get('brand' . $value->id);
        } else {

            $fields = Field::where('is_enabled', 1)->where('position', 2)->get();

            // build fields array
            foreach ($fields as $val => $f) {
                if ($f->name != null) {
                    $searchProducts = $products->with($f->name)->get()->pluck($f->name)->flatten()->unique('id');
                    $fields[$val]['values'] = collect($searchProducts);
                }
            }
            Cache::put('brand' . $value->id, $fields);
        }

        // filter products
        if (request()->has('query')) {
            foreach ($fields as $field) {
                if (request()->has($field->name) && request()->get($field->name) != '') {
                    $products = $products->filter($field->name, request()->get($field->name));
                }
            }
            if (request()->has('discounted') && request()->get('discounted') == 'true') {
                $products = $products->where('sale_price', "!=", null)->orWhere('rule_id', "!=", null);
            }
        } //end elseif

        if (request()->has('sort') && request()->get('sort') == 'price_asc' || request()->get('sort') == 'price_desc') {


            $sort = (request()->get('sort') == 'price_asc') ? 'ASC' : 'DESC';


            // $products->sortBy('price', $sort);
            $products->leftjoin('rules', 'products.rule_id', '=', 'rules.id')
                ->orderByRaw('CASE WHEN products.sale_price < products.price AND products.sale_price > 1 THEN products.sale_price
                    WHEN products.rule_id > 0 THEN products.price - (products.price * (rules.discount_value / 100))
                    ELSE
                    products.price
                    END ' . $sort);
        } elseif (request()->has('sort') && request()->get('sort') == 'discount_asc' || request()->get('sort') == 'discount_desc') {
            $sort = (request()->get('sort') == 'discount_asc') ? 'ASC' : 'DESC';
            $products->leftjoin('rules', 'products.rule_id', '=', 'rules.id')
                ->orderByRaw('CASE WHEN products.sale_price > 2  THEN ((products.price - products.sale_price)/products.price) * 100
                    ELSE
                    rules.discount_value
                    END ' . $sort);
        } elseif (request()->has('sort') && request()->get('sort') == 'season_asc' || request()->get('sort') == 'season_desc') {
            $sort = (request()->get('sort') == 'season_asc') ? 'ASC' : 'DESC';
            $products
                ->orderBy('products.created_at', $sort);
        } else {
            // $products->orderBy('products.created_at', 'DESC');


            $products
                ->join('field_seasons_product', 'products.id', '=', 'field_seasons_product' . '.product_id')
                ->orderByRaw('id');
        }

        $products = $products->paginate(session()->get('paginate'));

        return view(\Config::get('settings.theme')  . '.brand', compact('products',  'fields'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $brand = Brand::findOrFail($id);
        $file = $request->file('url');
        $name = time() . '.' . $file->getClientOriginalExtension();
        $path = $file->storeAs("brands/$brand->id", $name, 'public');

        $brand->update(
            [
                'image' => $path,
                'description' => $request->description,
                'name' => $request->name,
                'title' => $request->title,
            ]
        );
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
