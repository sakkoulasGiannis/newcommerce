<?php

namespace App\Http\Controllers;

use App\Models\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
	public function store_courier_payments(Request $request)
	{
		foreach ($request->get('rules') as $r) {

			$country = Country::find($r['country']['id']);
			if ($country) {
				$country->payment_rules = $r['county'];
				$country->save();
			}
		}
	}

	public function all()
	{
		return Country::with('counties')->get();
	}
}
