<?php

namespace App\Http\Api;

use App\Fields\FieldGroupecolor;
use App\Fields\FieldSize;
use App\Models\Category;
use App\Models\Field;
use App\Models\Product;
use App\Models\StockVariation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\OrderDetail;

class OrderController extends Controller
{
    public function all(Request $request)
    {
        if ($request->get('api_key') !== 'IqDcNdpGfQS1L8dPmBBIBsWm1SftQ26Q') {
            return "api error";
        }
        $date = new Carbon;
        $date->subWeek(); // or $date->subDays(7),  2014-03-27 13:58:25

        $today_start = Carbon::today();
        $today_end = Carbon::today()->addRealMinutes(14);

         $orders = Order::where('created_at', '>', $today_start->subDays(27))
            ->with('user', 'detail', 'detail.products')
            ->whereHas('detail', function ($query) {
                return $query->where('status', '=', "pending")
                    ->orWhere('status', '=', "processing");
            })
            ->get();

//        $orders->transform(function ($item, $key) {
////            $courier = Courier::find($item['detail']['shipping_id']);
////            $courier = ($courier && isset($courier->name)) ? $courier->name : 'Δέν βρέθηκε ή έχει διαγραφεί';
//
////            $item->detail['courier_name'] = ($item['detail']['shipping_id'] != null) ? $courier : 'not found';
////            $item->detail['payment_name'] = ($item['detail']['payment_id'] != null) ? Payment::find($item['detail']['payment_id'])->title : 'not found';
//
//            if (count($item['detail']['products']) > 1) {
//                foreach ($item['detail']['products'] as $key => $product) {
//                    $asociatedProduct = Associated::where('sku', $product['sku'])->first();
//                    $product['barcode'] = ($asociatedProduct['barcode']) ? $asociatedProduct['barcode'] : '-';
//                }
//                return $item;
//            } else {
//                foreach ($item['detail']['products'] as $key => $product) {
//                    $asociatedProduct = Stock::where('sku', $product['sku'])->first();
//                    $product['barcode'] = ($asociatedProduct['barcode']) ? $asociatedProduct['barcode'] : '-';
//                    return $item;
//                }
//            }
//
//        });

        return $orders;
    }

}