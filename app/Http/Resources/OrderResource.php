<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'name' => ($this->detail) ? $this->detail->billing_name . " " . $this->detail->billing_lastname : null,
            'mobile' => (isset($this->detail->billing_mobile)) ? $this->detail->billing_mobile : null,
            'email' => $this->detail->billing_email,
            'subtotal' => $this->detail->sub_total,
            'total' => $this->detail->total,
            'status' => $this->detail->status,
            'created_at' => $this->created_at,
            'order_id' => $this->detail->order_id,
            'courier_name' => $this->detail->courier_name,

            'order_num' => $this->order_num,
            'ip' => $this->ip,
            'user_id' => $this->user_id,
            'tracking_number' => $this->tracking_number,
            'comment' => $this->comment,
            'admin_comment_id' => $this->admin_comment_id,
            'slug' => $this->slug,

            'billing_email' => $this->detail->billing_email,
            'shipping_email' => $this->detail->shipping_email,

            'invoice_id' => $this->detail->invoice_id,
            'payment_id' => $this->detail->payment_id,
            'shipping_id' => $this->detail->shipping_id,
            'sub_total' => $this->detail->sub_total,
            'billing_name' => $this->detail->billing_name,
            'billing_lastname' => $this->detail->billing_lastname,
            'billing_country_id' => $this->detail->billing_country_id,
            'billing_region' => $this->detail->billing_region,
            'billing_city' => $this->detail->billing_city,
            'billing_street' => $this->detail->billing_street,
            'billing_zip' => $this->detail->billing_zip,
            'billing_street_number' => $this->detail->billing_street_number,
            'billing_phone' => $this->detail->billing_phone,
            'billing_mobile' => $this->detail->billing_mobile,
            'shipping_country_id' => $this->detail->shipping_country_id,
            'shipping_region' => $this->detail->shipping_region,
            'shipping_city' => $this->detail->shipping_city,
            'shipping_street' => $this->detail->shipping_street,
            'shipping_zip' => $this->detail->shipping_zip,
            'shipping_street_number' => $this->detail->shipping_street_number,
            'shipping_phone' => $this->detail->shipping_phone,
            'shipping_mobile' => $this->detail->shipping_mobile,
            'shipping_name' => $this->detail->shipping_name,
            'shipping_lastname' => $this->detail->shipping_lastname,
            //            'payment_title' => $this->details->payment->payment_title,
            //            'status' => $this->details->status->title,
        ];
    }
}
