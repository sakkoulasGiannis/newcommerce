<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/manage/orders/export',
        '/manage/theme/save-component',
        '/manage/upload-image',
        '/manage/*/theme-images',

    ];
}
