<?php

namespace App\Scopes;

use App\Product;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class ActiveProducts implements Scope
{


    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {

        if (!\Request::is('manage/*') && !\Request::is('api/*')) {
            $builder->with('media', 'stock')
//                ->when(true, function ($query) {
//                    return $query->has('media', '>', 0);
//                })
                ->where('product_is_active', '=', 1)
                ->whereHas('stock', function ($q) {
                    $q->select(\DB::raw('SUM(quantity) as q'))
                        ->havingRaw('q > 0');
                });

        }
    }
}
