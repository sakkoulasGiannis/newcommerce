<?php

namespace App\Livewire;

use App\Models\Product;
use Livewire\Attributes\On;
use Livewire\Component;
use Algolia\AlgoliaSearch\SearchClient;

class SearchProducts extends Component
{
    public $search = '';
    public $products = [];
    public $dropdownOpen = false;
    public function updatedSearch()
    {
        if (strlen($this->search) > 3) {
            $client = SearchClient::create(env('ALGOLIA_APP_ID'), env('ALGOLIA_SECRET'));
            $index = $client->initIndex('product_index');

            $results = $index->search($this->search);
            $this->products = $results['hits'];
        }
    }

    public function render()
    {
        return view('livewire.search-products');
    }

}


