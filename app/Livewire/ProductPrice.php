<?php

namespace App\Livewire;

use Livewire\Component;

class ProductPrice extends Component
{
    public function render()
    {
        return view('livewire.product-price');
    }
}
