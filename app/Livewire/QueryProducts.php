<?php

namespace App\Livewire;

use App\Models\Product;
use Livewire\Component;

class QueryProducts extends Component
{
    public $category_id;
    public $total_items = 10;
    public $priceBetween;
    public $products = [];
    public $sort = 'title';
    public $sortDirection = 'Asc';
    public $totalColumns = 4;
    public $title;
    public $moreLink;
    public function mount($category_id, $total_items, $sort, $sortDirection, $totalColumns, $title, $priceBetween = null, $moreLink = null)
    {
        $this->category_id = $category_id;
        $this->total_items = $total_items;
        $this->sort = ($sort) ? $sort : 'title';
        $this->sortDirection = ($sortDirection) ? $sortDirection : 'Asc';
        $this->totalColumns = ($totalColumns) ? $totalColumns : 4;
        $this->title = $title;
        $this->priceBetween = $priceBetween;
        $this->moreLink = $moreLink;

        $this->fetchProducts();
    }


    public function updated($field)
    {
        $this->validateOnly($field, [
            'category_id' => 'required|integer',
            'total_items' => 'required|integer|min:1',
        ]);

        $this->fetchProducts();
    }

    public function fetchProducts()
    {
        if($this->total_items > 20){
            $this->total_items = 20;
        }

        $this->products = Product::whereHas('categories', function($query) {
            $query->where('id', $this->category_id);
        })->when($this->priceBetween, function($query) {
            $query->whereBetween('price', $this->priceBetween);
        })

            ->take($this->total_items)->orderBy($this->sort, $this->sortDirection)->get();
    }

    public function render()
    {
        return view('livewire.query-products');
    }
}
