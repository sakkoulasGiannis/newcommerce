<?php

namespace App\Livewire;

use App\Models\Cart;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use Livewire\Attributes\On;
use Livewire\Component;

class TotalCartItems extends Component
{

    public $totalItems;


    public function mount()
    {
        $this->totalItems = 0;
    }

    #[On('cartUpdated')]
    public function render()
    {
        $this->totalItems = 0;
        if (!Cookie::get('cart_id')) {
            Cookie::queue('cart_id', uniqid(), 60 * 24 * 7); // 7 ημέρες
        }

        $cartId = Cookie::get('cart_id');
        $cart = Cart::where('session_id', $cartId)->first();
//        dd(app(CartController::class)->getCartSummary());
        if ($cart) {
            $this->totalItems = $cart->items()->sum('quantity');;
        }


        return view('livewire.total-cart-items');
    }

}
