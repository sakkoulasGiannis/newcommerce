<?php

namespace App\Livewire;

use App\Helpers\AadeAfm;
use App\Models\Courier;
use App\Models\OrderVoucher;
use App\Models\Payment;
use App\Models\Setting;
use App\Models\User;
use App\Models\Voucher;
use Livewire\Attributes\Validate;
use Livewire\Component;

class CheckoutForm extends Component
{
    public $shippings = [];
    public $payments = [];
    public $deliverMethod = null;
    public $paymentMethod = null;
    public $paymentObject = [];
    public $deliveryObject = [];
    public $availableShippings = [];
    public $availablePayments = [];

    public $country = 1;
    public $county;
    public $countries;
    public $cart;
    public $postalCode;
    public $messages = ['email' => '', 'postalCode' => '', 'country' => '', 'voucher' => ''];
    public $email;
    public $billing_name;
    public $billing_lastname;
    public $billing_mobile;
    public $billing_street;

    public $paymentMethodUnique;
    public $shippingMethodUnique;
    public $payment_id;
    public $shipping_id;

    public $messageUser;
    public $voucher = null;
    public $voucherSuccess = false;
    public  $vat;
    public $activity;
    public $doy;
    public $company;
    public $billing_phone;
    public $billing_street_number;
    public $billing_city;
    public $completeOrder;



    // Έλεγχος αν όλα τα πεδία έχουν συμπληρωθεί
    public function checkIfComplete()
    {
        $this->completeOrder = $this->billing_city && $this->billing_phone  && $this->billing_street && $this->billing_name && $this->billing_lastname && $this->email && $this->postalCode && $this->country && $this->shippingMethodUnique && $this->paymentMethodUnique;
    }
    public function submitOrder()
    {
        if (!$this->completeOrder) {
            return; // Προστασία από υποβολή αν δεν είναι έτοιμο
        }

        // Λογική για την υποβολή της παραγγελίας
    }

    public function updatedPaymentMethodUnique($value)
    {
        // Ενημερώνουμε το paymentMethod με βάση το paymentMethodUnique
        $this->payment_id = $this->availablePayments[$this->paymentMethodUnique]['id'];
        $this->cart->payment_extra = $this->availablePayments[$this->paymentMethodUnique]['extra_price'];
        $this->cart->save();
    }

    public function updatedShippingMethodUnique($value)
    {
        // Ενημερώνουμε το paymentMethod με βάση το paymentMethodUnique
        $this->shipping_id = $this->availableShippings[$this->shippingMethodUnique]['id'];
        $this->cart->shipping_extra = $this->availableShippings[$this->shippingMethodUnique]['extra_price'];
        $this->cart->save();
    }

    public function updated($propertyName)
    {
        // Αποθήκευση της τιμής στο session όταν αλλάζει κάποιο property
        session([
            'checkout_' . $propertyName => $this->$propertyName,
        ]);

        if($this->deliverMethod != null){
            foreach ($this->shippings as $shipping){
                if($shipping['id'] == $this->deliverMethod){
                    $this->deliveryObject = $shipping;
                }
            }
        }
        $this->checkIfComplete();
//        if($this->paymentMethod != null){
//            $this->paymentObject = 23;
//        }
    }
    public function updatedCountry()
    {
        $this->messages['country'] = "You have selected the country: {$this->country}";
//        $this->addMessage('country', "You have selected the country: {$this->country}");
    }

    public function updatedPostalCode()
    {
//        $this->addMessage('postalCode', "You have entered the postal code: {$this->postalCode}");
        $this->deliveryMethods();
    }


    public function deliveryMethods()
    {

        if ($this->postalCode == '1000') {
            $this->deliveryMethods = [
                'standard' => 'Standard Delivery',
                'express' => 'Express Delivery',
            ];
        } else {
            $this->deliveryMethods = [];
        }

    }


    public function checkUserEmail()
    {


        // check if $this->email is like email
        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $this->messages['email'] = "Το email δεν είναι έγκυρο";
            return;
        }

        $user = User::where('email', $this->email)->first();
        if ($user) {
            $this->messages['email'] = "Καλώς ήρθες πίσω {$user->name}! θέλεις να συνδεθείς;";
            if($this->cart->user_id == null){
                $this->cart->user_id = $user->id;
                $this->cart->save();
            }

        } else {
            $this->messages['email'] = null;

        }

    }

//    public function checkVoucher()
//    {
//        if (($this->voucher == null || $this->voucher == '') && ($this->cart->voucher_id == null || $this->cart->voucher_id == '')) {
//
//            $this->voucherSuccess = false;
//            $this->cart->voucher_id = null;
//            $this->cart->save();
//            $this->cart = $this->cart->fresh();
//
//            return;
//        } elseif ($this->voucher != null) {
//                $voucher = Voucher::where('code', $this->voucher)->where('voucher_active',1)->first();
//                $orderVouchers = OrderVoucher::where('voucher_id', $voucher->id);
//            if ($orderVouchers->count() >= $voucher->uses ) {
//                $this->voucherSuccess = false;
//                $this->messages['voucher'] = '<p class="mt-2 text-sm text-red-500" id="email-success">Το κουπόνι έχει χρισημοποιηθεί ή δεν υπάρχει</p>';
//
//                return;
//            }
//            // check if voucher is applicable
//
//            if ($voucher && $this->voucherSuccess == false) {
//                $this->voucherSuccess = true;
//                $this->cart->voucher_id = $voucher->id;
//                $this->cart->save();
//                $this->cart = $this->cart->fresh();
//                $this->messages['voucher'] = '<p class="mt-2 text-sm text-green-600" id="email-success">Το κουπόνι προστέθηκε</p>';
//                return;
//            }
//            $this->voucherSuccess = false;
//            $this->cart->voucher_id = null;
//            $this->voucher = null;
//            $this->cart->save();
//            $this->cart = $this->cart->fresh();
//
//            if ($voucher) {
//                $this->messages['voucher'] = '<p class="mt-2 text-sm text-red-500" id="email-success">Το κουπόνι αφαιρέθηκε</p>';
//            } else {
//                $this->messages['voucher'] = '<p class="mt-2 text-sm text-red-500" id="email-success">Το κουπόνι δεν βρέθηκε</p>';
//            }
//            return;
//        } elseif ($this->cart->voucher_id != '' && $this->cart->voucher_id != null) {
//            $v = Voucher::find($this->cart->voucher_id)->first();
//
//            $this->voucher = $v->code;
//            $this->voucherSuccess = true;
//            return;
//        }
//
//
//    }
    public function checkVoucher()
    {
        // Case 1: No voucher provided
        if (empty($this->voucher) && empty($this->cart->voucher_id)) {
            $this->resetVoucher();
            return;
        }

        // Case 2: A voucher code is entered
        if (!empty($this->voucher)) {
            $voucher = Voucher::where('code', $this->voucher)
                ->where('voucher_active', 1)
                ->first();

            // If the voucher is invalid or fully used
            if (!$voucher || $this->isVoucherFullyUsed($voucher)) {
                $this->invalidateVoucher('<p class="mt-2 text-sm text-red-500" id="email-success">Το κουπόνι έχει χρισημοποιηθεί ή δεν υπάρχει</p>');
                return;
            }

            // If the voucher is valid and can be applied
            $this->applyVoucher($voucher);
            return;
        }

        // Case 3: Voucher already applied in the cart
        if (!empty($this->cart->voucher_id)) {
            $existingVoucher = Voucher::find($this->cart->voucher_id);
            $this->voucher = $existingVoucher?->code;
            $this->voucherSuccess = true;
            return;
        }

        // Default case: Reset voucher
        $this->resetVoucher('<p class="mt-2 text-sm text-red-500" id="email-success">Το κουπόνι αφαιρέθηκε</p>');
    }

    /**
     * Check if the voucher has been fully used.
     */
    private function isVoucherFullyUsed($voucher)
    {
        $orderVouchersCount = OrderVoucher::where('voucher_id', $voucher->id)->count();
        return $orderVouchersCount >= $voucher->uses;
    }

    /**
     * Apply the voucher to the cart.
     */
    private function applyVoucher($voucher)
    {
        $this->voucherSuccess = true;
        $this->cart->voucher_id = $voucher->id;
        $this->cart->save();
        $this->cart = $this->cart->fresh();
        $this->messages['voucher'] = '<p class="mt-2 text-sm text-green-600" id="email-success">Το κουπόνι προστέθηκε</p>';
    }

    /**
     * Invalidate the voucher and show a message.
     */
    private function invalidateVoucher($message)
    {
        $this->voucherSuccess = false;
        $this->cart->voucher_id = null;
        $this->voucher = null;
        $this->cart->save();
        $this->cart = $this->cart->fresh();
        $this->messages['voucher'] = $message;
    }

    /**
     * Reset the voucher in the cart.
     */
    public function resetVoucher($message = null)
    {

        $this->voucherSuccess = false;
        $this->cart->voucher_id = null;
        $this->cart->save();
        $this->cart = $this->cart->fresh();
        session(['checkout_voucher' => null]);
        $this->voucher = null;
        $this->messages['voucher'] = '<p class="mt-2 text-sm text-red-500" id="email-success">'.$message.'</p>' ?? '<p class="mt-2 text-sm text-red-500" id="email-success">Δεν βρέθηκε το κουπόνι</p>';
    }
    public function mount()
    {
        $this->country = session('checkout_country', 1);
//        $this->county = session('checkout_county', null);

//        $this->countries = session('checkout_countries', []);
        $this->deliverMethod = session('checkout_deliver_method', null);
        $this->billing_mobile = session('checkout_billing_mobile', null);
        $this->paymentMethod = session('checkout_payment_method', null);
        $this->vat = session('checkout_vat', '');
        $this->billing_city = session('checkout_billing_city', '');
        $this->company = session('checkout_company', '');
        $this->doy = session('checkout_doy', '');
        $this->billing_street = session('checkout_billing_street', '');
        $this->billing_street_number = session('checkout_billing_street_number', '');
        $this->billing_phone = session('checkout_billing_phone', '');
        $this->email = session('checkout_email', '');
        $this->billing_name = session('checkout_billing_name', '');
        $this->billing_lastname = session('checkout_billing_lastname', '');
        $this->postalCode = session('checkout_postalCode', '');
        $this->voucher = session('checkout_voucher', null);
        if($this->county == null){
            $this->cart->update(['payment_extra' => 0, 'shipping_extra' => 0]);
        }else{
            $this->completeOrder = true;
        }
//        $this->checkVoucher();
    }

    public function render()
    {
        return view('livewire.checkout-form');
    }

    protected function addMessage($type, $message)
    {
//        if (!isset($this->messages[$type])) {
//            $this->messages[$type] = [];
//        }
//        $this->messages[$type][] = $message;
    }

//    function calculateShippingAndPayment()
//    {
//        $config = Setting::where('key', 'shipping_payment_rule')->first()->value;
//        $config = json_decode($config, true);
//        $countryId = $this->country;
//        $countyId = $this->county;
//        $cartTotal = $this->cart->subTotal();
//
//        $availableShippings = [];
//        $availablePayments = [];
//        // Βρες τη χώρα
////        $countryConfig = collect($config[0['countries']['counties'])->firstWhere('ids', $countryId);
//
//        foreach ($config as $c) {
//            // if the country is found in array
//
//            if (in_array($countryId, $c['countries']['ids'])) {
//                foreach ($c['countries']['counties'] as $county) {
//                    if (in_array($countyId, $county['ids'])) {
//
//                        foreach ($county['cart_rules'] as $keyRyle => $rule) {
//                            foreach ($rule['rules'] as $keySubRule=> $subRule) {
//
//                                if (isset($subRule['cart'])) {
//                                    $cartRule = $subRule['cart'];
//                                    if ($cartRule['method'] == '>' && $cartTotal > $cartRule['min_price']) {
//                                        foreach ($rule['rules'] as $keyShipping=> $shippingRule) {
//                                            if (isset($shippingRule['shippings'])) {
//                                                 foreach ($shippingRule['shippings']['ids'] as $shippingId) {
//                                                    if (!isset($availableShippings[$shippingId])) {
//                                                        $c = Courier::find($shippingId);
//                                                        $value = [
//                                                            'unique_id' => 'a' . $countryId . $keyRyle . $keySubRule . $keyShipping . $shippingId,
//                                                            'id' => $shippingId,
//                                                            'title' => $c->title,
//                                                            'model' => $c->model,
//                                                            'name' => $c->name,
//                                                            'extra_price' => $shippingRule['shippings']['extra_price'],
//                                                        ];
//                                                        $availableShippings[$shippingId] = $value;
//                                                        $this->availableShippings[] = $value;
//                                                    }
//
//                                                    foreach ($shippingRule['shippings']['payments'] as $payment) {
//                                                        if ($payment['county_id'] === null || $payment['county_id'] == $countyId) {
//                                                            $paymentObject = Payment::find($payment['ids']);
//                                                            if ($paymentObject) {
//                                                                $paymentName = $paymentObject->name;
//                                                                if (!in_array($paymentName, array_column($availablePayments, 'name'))) {
//                                                                    $value =  [
//                                                                        'id' => $payment['ids'],
//                                                                        'unique_id' => 'b'.$countryId.$keyRyle.$keySubRule.$keyShipping.$payment['ids'],
//                                                                        'title' => $paymentObject->title,
//                                                                        'name' => $paymentName,
//                                                                        'model' => $paymentObject->model,
//                                                                        'extra_price' => $payment['extra_price'],
//                                                                    ];
//
//                                                                    $availablePayments[$paymentObject->name] =  $value;
//                                                                    $this->availablePayments[] = $value;
//                                                                }
//                                                            }
//                                                        }
//                                                    }
//                                                }
//                                            }
//                                        }
//                                    } elseif ($cartTotal >= $cartRule['min_price'] && $cartTotal <= $cartRule['max_price']) {
//                                        foreach ($rule['rules'] as $keyShippingRule => $shippingRule) {
//                                            if (isset($shippingRule['shippings'])) {
//                                                foreach ($shippingRule['shippings']['ids'] as $shippingId) {
//                                                    if (!isset($availableShippings[$shippingId])) {
//                                                        $c = Courier::find($shippingId);
//                                                        $value =   [
//                                                            'unique_id' => 'c'.$countryId.$keyRyle.$keySubRule.$keyShippingRule.$shippingId,
//                                                            'id' => $shippingId,
//                                                            'title' => $c->title,
//                                                            'model' => $c->model,
//                                                            'name' => $c->name,
//                                                            'extra_price' => $shippingRule['shippings']['extra_price'],
//
//                                                        ];
//                                                        $availableShippings[$shippingId] =  $value;
//                                                        $this->availableShippings[] = $value;
//                                                    }
//
//
//                                                    foreach ($shippingRule['shippings']['payments'] as $keyPayment => $payment) {
//                                                        if ($payment['county_id'] === null || $payment['county_id'] == $countyId) {
//                                                            // Βρίσκουμε το αντικείμενο Payment
//                                                            $paymentObject = Payment::find($payment['ids']);
//
//                                                            // Ελέγχουμε αν βρέθηκε το Payment
//                                                            if ($paymentObject) {
//                                                                $paymentName = $paymentObject->name;
//
//                                                                // Ελέγχουμε αν το availablePayments δεν έχει ήδη αυτή την πληρωμή
//                                                                if (!in_array($paymentName, array_column($availablePayments, 'name'))) {
//                                                                    $value = [
//                                                                        'unique_id' => 'd'.$countryId.$keyRyle.$keySubRule.$keyPayment.$payment['ids'],
//                                                                        'id' => $payment['ids'],
//                                                                        'title' => $paymentObject->title,
//                                                                        'model' => $paymentObject->model,
//                                                                        'name' => $paymentName,
//                                                                        'extra_price' => $payment['extra_price'],
//                                                                    ];
//                                                                    $availablePayments[$paymentObject->name] =  $value;
//                                                                    $this->availablePayments[] = $value;
//                                                                }
//                                                            }
//                                                        }
//                                                    }
//
//                                                }
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//
//            } else {
//                $this->messages['errors'][] = "Δεν βρέθηκε η χώρα";
//            }
//        }
//
//
//        // Έλεγχος για τους κανόνες του καλαθιού
//        $this->shippings = $availableShippings;
//        $this->payments = $availablePayments;
//    }

    function calculateShippingAndPayment()
    {
        $config = Setting::where('key', 'shipping_payment_rule')->value('value');
        $config = json_decode($config, true);

        $countryId = $this->country;
        $countyId = $this->county;
        $cartTotal = $this->cart->subTotal();

        $availableShippings = [];
        $availablePayments = [];

        foreach ($config as $countryConfig) {
            if (!in_array($countryId, $countryConfig['countries']['ids'])) {
                $this->messages['errors'][] = "Country not found in configuration.";
                continue;
            }

            foreach ($countryConfig['countries']['counties'] as $county) {
                if (!in_array($countyId, $county['ids'])) {
                    continue;
                }

                foreach ($county['cart_rules'] as $cartRule) {
                    $isRuleMatched = $this->isCartRuleMatched($cartTotal, $cartRule['rules']);

                    if ($isRuleMatched) {
                        $this->processShippingRules(
                            $cartRule['rules'],
                            $countryId,
                            $countyId,
                            $availableShippings,
                            $availablePayments
                        );
                    }
                }
            }
        }

        $this->shippings = $availableShippings;
        $this->payments = $availablePayments;
    }

    /**
     * Check if cart rules match the current cart total.
     */
    private function isCartRuleMatched($cartTotal, $rules)
    {
        foreach ($rules as $rule) {
            if (isset($rule['cart'])) {
                $cartRule = $rule['cart'];
                $minPrice = $cartRule['min_price'] ?? 0;
                $maxPrice = $cartRule['max_price'] ?? PHP_INT_MAX;

                if ($cartRule['method'] === '>' && $cartTotal > $minPrice) {
                    return true;
                }

                if ($cartTotal >= $minPrice && $cartTotal <= $maxPrice) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Process shipping and payment rules for matched cart rules.
     */
    private function processShippingRules($rules, $countryId, $countyId, &$availableShippings, &$availablePayments)
    {
        foreach ($rules as $rule) {
            if (isset($rule['shippings'])) {
                foreach ($rule['shippings']['ids'] as $shippingId) {
                    if (!isset($availableShippings[$shippingId])) {
                        $courier = Courier::find($shippingId);
                        if ($courier) {
                            $uniqueId = uniqid('shipping_');
                            $value = [
                                'unique_id' => $uniqueId,
                                'id' => $shippingId,
                                'title' => $courier->title,
                                'model' => $courier->model,
                                'name' => $courier->name,
                                'extra_price' => $rule['shippings']['extra_price'] ?? 0,
                            ];
                            $availableShippings[$shippingId] = $value;
                            $this->availableShippings[$uniqueId] = $value;
                        }
                    }

                    $this->processPaymentRules(
                        $rule['shippings']['payments'] ?? [],
                        $countyId,
                        $availablePayments
                    );
                }
            }
        }
    }

    /**
     * Process payment rules for a specific shipping rule.
     */
    private function processPaymentRules($payments, $countyId, &$availablePayments)
    {
        foreach ($payments as $payment) {
            if ($payment['county_id'] === null || $payment['county_id'] == $countyId) {
                $paymentObject = Payment::find($payment['ids']);
                if ($paymentObject) {
                    $paymentName = $paymentObject->name;
                    if (!isset($availablePayments[$paymentName])) {
                        $uniqueId = uniqid('payment_');

                        $value = [
                            'unique_id' => $uniqueId,
                            'id' => $payment['ids'],
                            'title' => $paymentObject->title,
                            'model' => $paymentObject->model,
                            'name' => $paymentName,
                            'extra_price' => $payment['extra_price'] ?? 0,
                        ];
                        $availablePayments[$paymentName] = $value;
                        $this->availablePayments[$uniqueId] = $value;
                    }
                }
            }
        }
    }



    public function getInvoiceDetails(){
        if(strlen($this->vat) == 9){
            $aade = new AadeAfm('WEBORANGE123', '1q2w3e4r5t6y!~', $this->vat);
            $data = $aade->info($this->vat);
            $response =  json_encode($data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            $response = json_decode($response, true);
            $this->company = $response['business']['onomasia'];
            $this->activity = $response['business']['drastiriotita'];
            $this->doy = $response['business']['doyDescr'];


        }



    }

}
