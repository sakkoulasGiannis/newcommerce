<?php

namespace App\Livewire;

use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Product;
use App\Models\Stock;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use Livewire\Attributes\On;
use Livewire\Component;

class AddVariableToCart extends Component
{

    public $product;
    public $stock;
    public $selectedStock = null;
    public $can_submit = false;

    #[On('AddToCart')]
    public function addItemToCart()
    {

        $user = Auth::user();
        $this->can_submit = false;
        if ($this->selectedStock) {
            // Κάνε κάτι με το selectedStock, π.χ., προσθήκη στο καλάθι
            $this->addStockToCart = Stock::find($this->selectedStock);
        }
        if (!Cookie::get('cart_id')) {
            Cookie::queue('cart_id', uniqid(), 60 * 24 * 7); // 7 ημέρες
        }

        $cartId = Cookie::get('cart_id');

        if ($user != null) {
            $cart = Cart::query()->firstOrCreate(['user_id' => $user->id, 'session_id' => $cartId]);
        } else {
            $cart = Cart::query()->firstOrCreate(['session_id' => $cartId]);
        }

        $cartItem = $cart->items()->where('itemable_id', $this->addStockToCart->id)->first();

        if ($cartItem) {
            $cartItem->quantity++;
            $cartItem->save();
        } else {
            $cartItem = new CartItem([
                'itemable_id' => $this->addStockToCart->id,
                'itemable_type' => get_class($this->addStockToCart),
                'price' => ($this->product->sale_price > 0) ? $this->product->sale_price : $this->product->price,
                'quantity' => 1,
            ]);
            $cart->items()->save($cartItem);
        }
        $this->dispatch('cartUpdated');

//            $this->emit('cartUpdated'); // Εκπομπή γεγονότος για ενημέρωση καλαθιού


    }

    public function render()
    {


        return view('livewire.add-variable-to-cart');
    }
}
