<?php

namespace App\Livewire;

use Illuminate\Support\Facades\Cookie;
use Livewire\Component;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Session;
use Livewire\Attributes\On;

class AddToCart extends Component
{
    public $stock;
    public $product;
    public $crossSellRequired = false;
    public $productId;
    public $extraProducts = [];

//    #[On('AddToCart')]
    public function addItemToCart()
    {
        $user = Auth::user();

        if($this->product->cross_sell_method == 'required') {
            if($this->extraProducts == []) {
                $this->dispatch('AddMoreItemsToCart');
                return;
            }
        }
        if (!Cookie::get('cart_id')) {
            Cookie::queue('cart_id', uniqid(), 60 * 24 * 7); // 7 ημέρες
        }

        $cartId = Cookie::get('cart_id');


        if ($this->product) {
            if($user != null) {
                $cart = Cart::query()->firstOrCreate(['user_id' => $user->id,'session_id' => $cartId]);
            }else{
             $cart = Cart::query()->firstOrCreate(['session_id' => $cartId]);
            }

             $cartItem = $cart->items()->where('itemable_id', $this->stock['id'])->first();

              if ($cartItem) {
                $cartItem->quantity++;
                $cartItem->save();
            } else {
                 $cartItem = new CartItem([
                    'itemable_id' => $this->stock['id'],
                    'itemable_type' => get_class($this->stock),
                    'price' => ($this->product->sale_price > 0) ? $this->product->sale_price : $this->product->price,
                    'quantity' => 1,
                     'group_id' => ($this->product->cross_sell_method == 'required' && count($this->product->crossSell) > 0 )? $this->product->id : null
                ]);
                $cart->items()->save($cartItem);
            }


            $this->dispatch('cartUpdated', productId: $this->product->id);

//            $this->emit('cartUpdated'); // Εκπομπή γεγονότος για ενημέρωση καλαθιού
        }

        if ($this->extraProducts != []) {

            foreach($this->extraProducts as $cp){
                $cartItem = $cart->items()->where('itemable_id', $this->stock['id'])->first();

                $csproduct = Product::find($cp);

                $cartItem = new CartItem([
                    'itemable_id' => $csproduct->stock()->first()->id,
                    'itemable_type' => get_class($this->stock),
                    'price' => ($csproduct->sale_price > 0) ? $csproduct->sale_price : $csproduct->price,
                    'quantity' => 1,
                    'group_id' => ($this->product->cross_sell_method == 'required' && count($this->product->crossSell) > 0 )? $this->product->id : null


                ]);
                $cart->items()->save($cartItem);

            }

        }

    }

    public function render()
    {
        if($this->product->cross_sell_method == 'required') {
            $this->crossSellRequired = true;
        }
        return view('livewire.add-to-cart');
    }

}
