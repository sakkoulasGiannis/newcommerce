<?php

namespace App\Livewire;

use App\Models\Product;
use Livewire\Attributes\On;
use Livewire\Component;

class NewItemToCartInfo extends Component
{
    public $message = '';
    public $type = 'success';
    public $productId;

    #[On('cartUpdated')]
    public function message()
    {
        $this->type='success';

        $this->message = 'Το προϊόν προστέθηκε στο <a class="text-blue-500 underline" href="/cart">καλάθι</a> ';
    }

    #[On('AddMoreItemsToCart')]
    public function addMoreItems()
    {
        $this->type='error';
        $this->message = 'Για να προσθέσετε το προϊόν στο καλάθι θα πρέπει να επιλέξετε και κάποιο ακόμα απο τις επιλογές ';
    }
    public function render()
    {
        return view('livewire.new-item-to-cart-info');
    }
}
