<?php

return [
	'enable_session' => (env('ENABLE_SESSION', false)),
	'cache_session_hours' => (env('CACHE_SESSION_HOURS', 24)),
];
