<html>

<head>
    <title>App Name - @yield('title')</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://kit-pro.fontawesome.com/releases/v5.12.1/css/pro.min.css">

</head>

<body>

    <!-- end sidbar -->

    <div class="container">
        @yield('content')
    </div>
{{--    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>--}}

</body>

</html>
