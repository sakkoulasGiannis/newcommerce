<!DOCTYPE HTML>
<html lang="en" class="ah-full bg-white">

<head>
    <title> @yield('title')</title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body class="ah-full">


<div x-data="{ sidenav: false, profileDropDown: false, query:'', ...searchComponent()   }" @keydown.window="handleKeydown">
    <!-- Off-canvas menu for mobile, show/hide based on off-canvas menu state. -->
    <div x-show="sidenav" class="relative z-50 lg:hidden hide" role="dialog" aria-modal="true">
        <!--
          Off-canvas menu backdrop, show/hide based on off-canvas menu state.

          Entering: "transition-opacity ease-linear duration-300"
            From: "opacity-0"
            To: "opacity-100"
          Leaving: "transition-opacity ease-linear duration-300"
            From: "opacity-100"
            To: "opacity-0"
        -->
        <div class="fixed inset-0 bg-gray-900/80" aria-hidden="true"></div>

        <div class="fixed inset-0 flex">
            <!--
              Off-canvas menu, show/hide based on off-canvas menu state.

              Entering: "transition ease-in-out duration-300 transform"
                From: "-translate-x-full"
                To: "translate-x-0"
              Leaving: "transition ease-in-out duration-300 transform"
                From: "translate-x-0"
                To: "-translate-x-full"
            -->
            <div x-show="sidenav" x-transition:enter="transition ease-in-out duration-300 transform"
                 x-transition:enter-start="-translate-x-full" x-transition:enter-end="translate-x-0"
                 x-transition:leave="transition ease-in-out duration-300 transform"
                 x-transition:leave-start="translate-x-0" x-transition:leave-end="-translate-x-full"
                 x-description="Off-canvas menu, show/hide based on off-canvas menu state."
                 class="relative mr-16 flex w-full max-w-xs flex-1" @click.away="sidenav = false">
                <!--
                  Close button, show/hide based on off-canvas menu state.

                  Entering: "ease-in-out duration-300"
                    From: "opacity-0"
                    To: "opacity-100"
                  Leaving: "ease-in-out duration-300"
                    From: "opacity-100"
                    To: "opacity-0"
                -->
                <div class="absolute left-full top-0 flex w-16 justify-center pt-5">
                    <button type="button" class="-m-2.5 p-2.5" @click="sidenav=false">
                        <span class="sr-only">Close sidebar</span>
                        <svg class="h-6 w-6 text-white" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                             stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12"/>
                        </svg>
                    </button>
                </div>

                <!-- Sidebar component, swap this element with another sidebar if you like -->
                <div class="flex grow flex-col gap-y-5 overflow-y-auto bg-gray-900 px-6 pb-4 ring-1 ring-white/10">
                    <div class="flex h-16 shrink-0 items-center">
                        <img class="h-8 w-auto" src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=500"
                             alt="Your Company">
                    </div>
                    <nav class="flex flex-1 flex-col">
                        @include('layouts.admin.sidebar')
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <!-- Static sidebar for desktop -->
    <div class="hidden lg:fixed lg:inset-y-0 lg:z-50 lg:flex lg:w-72 lg:flex-col">
        <!-- Sidebar component, swap this element with another sidebar if you like -->
        <div class="flex grow flex-col gap-y-5 overflow-y-auto bg-gray-900 px-6 pb-4">
            <div class="flex h-16 shrink-0 items-center">
                @if(isset($settings['logo']))
                    <img class="h-8 w-auto" src="/{{$settings['logo']['value']}}" alt="Your Company">
                @else
                    <div class="font-black text-2xl text-white text-center">MyCommerce</div>
                @endif

            </div>
            <nav class="flex flex-1 flex-col">
        @include('layouts.admin.sidebar')
            </nav>
        </div>
    </div>

    <div class="lg:pl-72">
        <div
            class="sticky top-0 z-40 flex h-16 shrink-0 items-center gap-x-4 border-b border-gray-200 bg-white px-4 shadow-sm sm:gap-x-6 sm:px-6 lg:px-8">
            <button @click="sidenav = true" type="button" class="-m-2.5 p-2.5 text-gray-700 lg:hidden">
                <span class="sr-only">Open sidebar</span>
                <svg class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
                     aria-hidden="true">
                    <path stroke-linecap="round" stroke-linejoin="round"
                          d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"/>
                </svg>
            </button>

            <div class="h-6 w-px bg-gray-900/10 lg:hidden" aria-hidden="true"></div>

            <div class="flex flex-1 gap-x-4 self-stretch lg:gap-x-6">
                <div class="relative flex flex-1">
                     <div class="relative   flex items-center">
                        <input type="text"
                               placeholder="αναζήτηση ... "
                               @focus="showResults = true" @blur="handleBlur"
                               x-model="query"
                               @keyup="search()"
                               x-ref="searchField"
                                autocomplete="false"
                               name="search" id="search" class="px-4 block w-full lg:w-96 rounded-md border-0 py-2 pr-14 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                        <div class="absolute inset-y-0 right-0 flex py-1.5 pr-1.5">
                            <kbd class="inline-flex items-center rounded     px-1 font-sans text-xs text-gray-400">⌘K</kbd>
                        </div>
                    </div>
                    <ul @click.away="hideResults = true" x-show="showResults" class="top-12 absolute z-10 mt-1 max-h-60 w-96 overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm" id="options" role="listbox">

                        <!-- Display Products -->
                        <template x-if="results.products.length">
                            <template x-for="product in results.products" :key="product.id">
                                <li class=" flex  border-b relative cursor-default select-none py-2 pl-3 pr-9 text-gray-900" role="option" tabindex="-1">
                                   <span class="font-black">Προϊον   </span>

                                    <svg class="h-5 w-5 inlne flex-shrink-0 text-gray-400" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path fill-rule="evenodd" d="M7.21 14.77a.75.75 0 01.02-1.06L11.168 10 7.23 6.29a.75.75 0 111.04-1.08l4.5 4.25a.75.75 0 010 1.08l-4.5 4.25a.75.75 0 01-1.06-.02z" clip-rule="evenodd" />
                                    </svg>
                                    <a :href="'/manage/products/'+product.id+'/edit/'" class="block truncate" x-html="'<b>'+product.sku+'</b> - '+product.title.el || product.name  "></a>
                                </li>
                            </template>

                            <!-- Display Orders -->                        </template>

                        <template x-if="results.orders.length">
                            <template x-for="order in results.orders" :key="order.id">
                                <li class="relative cursor-default select-none py-2 pl-3 pr-9 text-gray-900" role="option" tabindex="-1">
                                    <span class="block truncate" x-text="`Order ID: ${order.id}`"></span>
                                </li>
                            </template>
                        </template>
                    </ul>
                </div>

                <div class="flex items-center gap-x-4 lg:gap-x-6">
                   <a href="/">
                       <svg class="h-6 w-6 text-gray-400" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                            stroke="currentColor" aria-hidden="true">
                           <path stroke-linecap="round" stroke-linejoin="round"
                                 d="M2.25 12l8.954-8.955c.44-.439 1.152-.439 1.591 0L21.75 12M4.5 9.75v10.125c0 .621.504 1.125 1.125 1.125H9.75v-4.875c0-.621.504-1.125 1.125-1.125h2.25c.621 0 1.125.504 1.125 1.125V21h4.125c.621 0 1.125-.504 1.125-1.125V9.75M8.25 21h8.25"/>
                       </svg>
                   </a>


                    <button type="button" class="-m-2.5 p-2.5 text-gray-400 hover:text-gray-500">
                        <span class="sr-only">View notifications</span>
                        <svg class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
                             aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                  d="M14.857 17.082a23.848 23.848 0 005.454-1.31A8.967 8.967 0 0118 9.75v-.7V9A6 6 0 006 9v.75a8.967 8.967 0 01-2.312 6.022c1.733.64 3.56 1.085 5.455 1.31m5.714 0a24.255 24.255 0 01-5.714 0m5.714 0a3 3 0 11-5.714 0"/>
                        </svg>
                    </button>

                    <!-- Separator -->
                    <div class="hidden lg:block lg:h-6 lg:w-px lg:bg-gray-900/10" aria-hidden="true"></div>

                    <form method="POST" action="{{ route('logout') }}">
                        @csrf
                        <button class="block px-2 py-1 text-sm leading-6 text-gray-900">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 text-red-600 inline">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 9V5.25A2.25 2.25 0 0 0 13.5 3h-6a2.25 2.25 0 0 0-2.25 2.25v13.5A2.25 2.25 0 0 0 7.5 21h6a2.25 2.25 0 0 0 2.25-2.25V15M12 9l-3 3m0 0 3 3m-3-3h12.75" />
                            </svg>

                        </button>
                    </form>
                    <!-- Profile dropdown -->
                    <div class="relative">
                        <button @click="profileDropDown = !profileDropDown" @click.away="profileDropDown = false"
                                type="button" class="-m-1.5 flex items-center p-1.5" id="user-menu-button"
                                aria-expanded="false" aria-haspopup="true">
                            <span class="sr-only">Open user menu</span>
                            @if(isset($settings['logo']))
                                <img class="h-8 w-8 rounded-full bg-gray-50" src="/{{$settings['logo']['value']}}" alt="Your Company">

                            @endif

                            <span class="hidden lg:flex lg:items-center">
                                <span class="ml-4 text-sm font-semibold leading-6 text-gray-900" aria-hidden="true">
                                    @if(Auth::user()->name || Auth::user()->lastname)
                                     <a href="/manage/users/{{Auth::user()->id}}">   {{ Auth::user()->name }} {{ Auth::user()->lastname }}</a>
                                    @else
                                        <a href="/manage/users/{{Auth::user()->id}}">{{ Auth::user()->email }}</a>
                                    @endif

                                </span>

                          </span>

                        </button>

                        <!--
                          Dropdown menu, show/hide based on menu state.

                          Entering: "transition ease-out duration-100"
                            From: "transform opacity-0 scale-95"
                            To: "transform opacity-100 scale-100"
                          Leaving: "transition ease-in duration-75"
                            From: "transform opacity-100 scale-100"
                            To: "transform opacity-0 scale-95"
                        -->
{{--                        <div x-show="profileDropDown"--}}
{{--                             class="absolute right-0 z-10 mt-2.5 w-32 origin-top-right rounded-md bg-white py-2 shadow-lg ring-1 ring-gray-900/5 focus:outline-none"--}}
{{--                             role="menu" aria-orientation="vertical" aria-labelledby="user-menu-button" tabindex="-1">--}}
{{--                            <!-- Active: "bg-gray-50", Not Active: "" -->--}}
{{--                            <a href="/manage/users/{{Auth::user()->id}}"--}}
{{--                               class="block px-3 py-1 text-sm leading-6 text-gray-900" role="menuitem" tabindex="-1"--}}
{{--                               id="user-menu-item-0">Προφίλ</a>--}}
{{--                            <form method="POST" action="{{ route('logout') }}">--}}
{{--                                @csrf--}}
{{--                                <button class="block px-3 py-1 text-sm leading-6 text-gray-900">--}}
{{--                                    Αποσύνδεση--}}
{{--                                </button>--}}
{{--                            </form>--}}
{{--                        </div>--}}
                    </div>

                </div>
            </div>



        </div>

        <main>
            <div id="app">
                @if (\Session::has('message'))

                    <ul>
                        <li>{!! \Session::get('success') !!}</li>
                    </ul>

                @endif

                @yield('content')

            </div>
        </main>
    </div>
</div>

<script src="https://cdn.tiny.cloud/1/i6ol3skk26piihq4d797m468ychzfkiigtvu9zh9npt4r5i5/tinymce/7/tinymce.min.js" referrerpolicy="origin"></script>
<script src="{{ asset('js/manage.js?') . time() }}" defer ></script>
{{--<script defer src="https://cdn.jsdelivr.net/npm/alpinejs@3.x.x/dist/cdn.min.js"></script>--}}

<script>

    // function searchComponent() {
    //     console.log('searching');
    //     if(this.query.length < 3) {
    //         this.results = [];
    //         return;
    //     }
    //     return {
    //         query: '',
    //         results: [],
    //         search() {
    //             axios.post('/manage/admin_search', { q: this.query })
    //                 .then(response => {
    //                     this.results = response.data;
    //                 })
    //                 .catch(error => {
    //                     console.error('Error:', error);
    //                 });
    //         }
    //     }
    // };


    // function handleKeydown(event) {
    //     if ((event.metaKey || event.ctrlKey) && event.key === 'k') {
    //         event.preventDefault(); // Αποτροπή της προεπιλεγμένης συμπεριφοράς του πληκτρολογίου
    //         this.$refs.searchField.focus(); // Τοποθέτηση του κέρσορα στο πεδίο αναζήτησης
    //     }
    // }

    function searchComponent() {
        return {
            query: '',
             hideResults: false,
            showResults: false,
            results: { // Βεβαιώσου ότι το results αρχικοποιείται σωστά
                products: [],
                orders: [],
                customers: []
            },
            search() {
                if (this.query.length > 2) { // Έλεγχος αν η αναζήτηση είναι αρκετά μεγάλη
                    axios.post('/manage/admin_search', { q: this.query })
                        .then(response => {
                        this.showResults = true;
                            this.results = response.data; // Υποθέτουμε ότι η API επιστρέφει { products: [], orders: [], customers: [] }
                        })
                        .catch(error => {
                            console.error('Error:', error);
                        });
                } else {
                    this.results = { products: [], orders: [], customers: [] }; // Καθαρισμός των αποτελεσμάτων αν η αναζήτηση είναι μικρή
                }
            },
            handleFocus() {
                this.showResults = true;
                this.hideResults = false;
            },
            handleBlur() {
                setTimeout(() => {
                    if (!this.$refs.searchField.matches(':focus')) {
                        this.showResults = false;
                    }δ
                }, 100); // delay to allow click on results
            },
            handleKeydown(event) {
                if ((event.metaKey || event.ctrlKey) && event.key === 'k') {
                    event.preventDefault();
                    this.$refs.searchField.focus();
                }
            }
        };
    }

    document.addEventListener("DOMContentLoaded", function () {
        document.addEventListener("click", function (event) {
            // Ελέγχουμε αν το στοιχείο που κλικήθηκε έχει την κλάση product_details_btn
            if (event.target.classList.contains("product_details_btn")) {
                let productId = event.target.getAttribute("data-id");
                console.log('clicked', productId);
                let row = document.querySelector(`.product_id_${productId}`);
                if (row) {
                    row.classList.toggle("hidden");
                }
            }
        });
    });


</script>

@yield('scripts')

</body>
</html>
