<!DOCTYPE HTML>
<html lang="en" class="ah-full bg-white">

<head>
    <title> @yield('title')</title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
{{--    <link href="https://cdn.jsdelivr.net/npm/tailwindcss@3.0.0/dist/tailwind.min.css" rel="stylesheet">--}}

    <link href="/css/grapes.min.css" rel="stylesheet">
     <script src="/js/grapes.min.js"></script>
     <script src="https://cdn.jsdelivr.net/npm/grapesjs-preset-webpage@1.0.3/dist/index.min.js"></script>
    <script src="https://unpkg.com/@truenorthtechnology/grapesjs-code-editor"></script>
{{--    <script src="https://unpkg.com/grapesjs-code-editor"></script>--}}
{{--    <script src="https://unpkg.com/grapesjs-blocks-flexbox"></script>--}}


    <script src="https://unpkg.com/grapesjs-blocks-basic"></script>
    <script src="/js/grapesjs-html-block.js"></script>
{{--    <script src="/js/grapejs-header-block.js"></script>--}}
{{--    <script src="/js/grapesjs-blocks-avance.min.js"></script>--}}
    <script src="/js/grapejs_store_components.js"></script>

{{--    <script src="/js/grapejs-global-settings.js"></script>--}}
{{--    <script src="/js/customStyleManager.js"></script>--}}

{{--    <link href="https://unpkg.com/grapesjs-component-code-editor/dist/grapesjs-component-code-editor.min.css" rel="stylesheet">--}}
{{--    <script src="https://unpkg.com/grapesjs-component-code-editor"></script>--}}
{{--    <script src="https://unpkg.com/grapesjs-tailwind"></script>--}}


{{--    <script src="https://unpkg.com/grapesjs-user-blocks"></script>--}}
    <link href="https://unpkg.com/grapesjs-rte-extensions/dist/grapesjs-rte-extensions.min.css" rel="stylesheet">
    <script src="https://unpkg.com/grapesjs-rte-extensions"></script>
    <script src="https://unpkg.com/grapesjs-google-material-icons/dist/index.js"></script>
    <link href="https://unpkg.com/grapick/dist/grapick.min.css" rel="stylesheet">
     <script src="path/to/grapesjs-style-bg.min.js"></script>


    {{--    <script src="https://unpkg.com/@silexlabs/grapesjs-symbols"></script>--}}
{{--    <script src="https://unpkg.com/grapesjs-advance-components"></script>--}}
{{--    <script src="https://unpkg.com/grapesjs-custom-code"></script>--}}



{{--    <script src="https://unpkg.com/grapesjs-parser-postcss"></script>--}}

    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body class=" ">


<div >
        <main>

                @if (\Session::has('message'))

                    <ul>
                        <li>{!! \Session::get('success') !!}</li>
                    </ul>

                @endif

                @yield('content')

         </main>

</div>

{{--<script src="{{ asset('js/manage.js?') . time() }}" defer></script>--}}


</body>
</html>
