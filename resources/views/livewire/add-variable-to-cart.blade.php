<div class="mt-4">
    <span class="block mb-2">Επιλογή παραλαγής:</span>
    @foreach($stock as $s)
        @if($s->quantity > 0)
            {{ implode(' - ', $s->variations->pluck('attributeTerm.title')->toArray()) }}
            <input required type="radio" name="selectedStock" wire:model.live="selectedStock" value="{{$s->id}}">
        @endif
    @endforeach
         <button wire:click="addItemToCart"
                id="addToCartButton"
                class="disabled:bg-slate-50 disabled:text-slate-500 disabled:shadow-none  disabled:border-slate-200 mt-4 flex relative w-full items-center justify-center rounded-md border border-transparent bg-[#c09578] px-8 py-3 text-base font-medium text-white shadow-lg hover:shadow-sm hover:bg-[#ab856c] focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                 @if(is_null($selectedStock)) disabled @endif>

            Προσθήκη στο καλάθι
        </button>

</div>
