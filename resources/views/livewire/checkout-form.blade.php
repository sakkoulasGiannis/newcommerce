<form

    x-data="{ shipToDiferentAddress: false, openInvoice: false }"
    x-init="window.Livewire.hook('component.initialized', () => { $wire = Livewire.find(document.querySelector('[wire\\:id]').getAttribute('wire:id')); })"
    x-on:change="$wire.calculateShippingAndPayment()" class=" " method="post" action="/cart/checkout">
    @csrf
    <div class="lg:grid lg:grid-cols-2 lg:gap-x-12 xl:gap-x-16">

        <div>
            <div>
                <div>
                    <div>
                        <label for="voucher" class="block text-sm font-medium leading-6 text-gray-900">Κωδικούς
                            Κουπονιού</label>
                        <div class="mt-2 flex rounded-md shadow-sm">
                            <div class="relative flex flex-grow items-stretch focus-within:z-10">
                                <div class="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                         stroke-width="1.5" stroke="currentColor"
                                         class="w-5 h-5 {{($voucherSuccess)?'text-green-500':'text-gray-400'}}">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                              d="M16.5 6v.75m0 3v.75m0 3v.75m0 3V18m-9-5.25h5.25M7.5 15h3M3.375 5.25c-.621 0-1.125.504-1.125 1.125v3.026a2.999 2.999 0 0 1 0 5.198v3.026c0 .621.504 1.125 1.125 1.125h17.25c.621 0 1.125-.504 1.125-1.125v-3.026a2.999 2.999 0 0 1 0-5.198V6.375c0-.621-.504-1.125-1.125-1.125H3.375Z"/>
                                    </svg>

                                </div>

                                <input wire:model="voucher" type="text" name="voucher" id="voucher"
                                       class="block w-full rounded-none rounded-l-md border-0 py-1.5 pl-10 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                       placeholder="abc123">
                            </div>
                            @if($voucherSuccess)
                                <button   wire:click="resetVoucher('Το κουπόνι αφαιρέθηκε')" type="button"
                                        class="relative -ml-px inline-flex items-center gap-x-1.5 rounded-r-md px-3 py-2 text-sm font-semibold text-gray-900 ring-1 ring-inset ring-gray-300 hover:bg-gray-50">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                         stroke-width="1.5"
                                         stroke="currentColor"
                                         class="-ml-0.5 h-5 w-5  text-red-500'c">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                              d="M9 12.75 11.25 15 15 9.75M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                                    </svg>
                                    Αφαίρεση
                                </button>
                            @else
                                <button   wire:click="checkVoucher" type="button"
                                        class="relative -ml-px inline-flex items-center gap-x-1.5 rounded-r-md px-3 py-2 text-sm font-semibold text-gray-900 ring-1 ring-inset ring-gray-300 hover:bg-gray-50">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                         stroke-width="1.5"
                                         stroke="currentColor"
                                         class="-ml-0.5 h-5 w-5 {{($voucherSuccess)?'text-green-500':'text-gray-400'}}">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                              d="M9 12.75 11.25 15 15 9.75M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                                    </svg>
                                    Εφαρμογή
                                </button>
                            @endif
                        </div>
                        @if($messages['voucher'])
                            {!! $messages['voucher'] !!}
                        @endif

                    </div>
                </div>

                <h2 class="text-lg font-medium text-gray-900">Στοιχεία Επικοινωνίας</h2>

                <div class="mt-4">
                    <label for="email" class="block text-sm font-medium text-gray-700">Email<abbr
                            title="required">*</abbr></label>
                    <div class="mt-1">
                        <input x-on:focusOut="$wire.checkUserEmail()" wire:model="email" required type="email"
                               id="email" name="billing_email" autocomplete="email"
                               class="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                    </div>

                    @if($messages['email'])
                        <p class="mt-2 text-sm text-gray-500" id="email-error">{{ $messages['email'] }}</p>
                    @endif


                </div>
            </div>

            {{--billing information--}}
            <div class="mt-10 border-t border-gray-200 pt-10">
                <h2 class="text-lg font-medium text-gray-900">Shipping information</h2>

                <div class="mt-4 grid grid-cols-1 gap-y-6 sm:grid-cols-6 sm:gap-x-4">


                    <div class="col-span-3">
                        <label for="first-name" class="block text-sm font-medium text-gray-700">Όνομα<abbr
                                title="required">*</abbr></label>
                        <div class="mt-1">
                            <input wire:model.defer="billing_name" type="text" required id="first-name"
                                   name="billing_name" autocomplete="given-name"
                                   class="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                        </div>
                    </div>

                    <div class="col-span-3">
                        <label for="last-name" class="block text-sm font-medium text-gray-700">Επίθετο<abbr
                                title="required">*</abbr></label>
                        <div class="mt-1">
                            <input wire:model.defer="billing_lastname" type="text" required id="last-name"
                                   name="billing_lastname" autocomplete="family-name"
                                   class="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                        </div>
                    </div>
                    <div class="col-span-3">
                        <label for="billing_mobile" class="block text-sm font-medium text-gray-700">Κινητό τηλέφωνο<abbr
                                title="required">*</abbr></label>
                        <div class="mt-1">
                            <input wire:model.defer="billing_mobile" type="text" required name="billing_mobile"
                                   id="billing_mobile" autocomplete="tel"
                                   class="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                        </div>
                    </div>
                    <div class="col-span-3">
                        <label for="billing_phone" class="block text-sm font-medium text-gray-700">Σταθερό
                            Τηλέφωνο</label>
                        <div class="mt-1">
                            <input wire:model.defer="billing_phone" type="text" name="billing_phone" id="billing_phone"
                                   autocomplete="tel"
                                   class="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                        </div>
                    </div>


                    <div class="col-span-6 md:col-span-4">
                        <label for="billing_street" class="block text-sm font-medium text-gray-700">Δ/νση<abbr
                                title="required">*</abbr></label>
                        <div class="mt-1">
                            <input wire:model.defer="billing_street" type="text" name="billing_street"
                                   id="billing_street" autocomplete="street-address"
                                   class="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                        </div>
                    </div>

                    <div class="col-span-6 md:col-span-2">
                        <label for="billing_street_number" class="block text-sm font-medium text-gray-700">Αρ.
                            Οικίας/Κτιρίου</label>
                        <div class="mt-1">
                            <input wire:model.defer="billing_street_number" type="text" name="billing_street_number"
                                   id="billing_street_number"
                                   class="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                        </div>
                    </div>


                    <div class="col-span-3">
                        <label for="country" class="block text-sm font-medium text-gray-700">Χώρα <abbr
                                title="required">*</abbr></label>
                        <div class="mt-1">
                            <select wire:model.change="country" id="country" name="billing_country_id" required
                                    autocomplete="country-name"
                                    class="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                                <option value="1">Ελλάδα</option>
                                <option value="2">Κύπρος</option>

                            </select>
                        </div>
                    </div>
                    <div class="col-span-3">

                        <label for="region" class="block text-sm font-medium text-gray-700">Νομός<abbr title="required">*</abbr></label>
                        <div class="mt-1">
                            @if($this->country)
                                <select wire:model.change="county" id="county" name="billing_county_id" required
                                        autocomplete="country-name"
                                        class="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                                    <option value="">Επιλογή Νομού</option>
                                    @foreach($countries->find( $this->country)->counties as $county)
                                        <option value="{{$county->id}}">{{$county->title}}</option>
                                    @endforeach

                                </select>
                            @endif
                        </div>
                    </div>
                    <div class="col-span-3">
                        <label for="city" class="block text-sm font-medium text-gray-700">Πόλη <abbr
                                title="required">*</abbr></label>
                        <div class="mt-1">
                            <input wire:model.defer="billing_city" type="text" name="billing_city" id="city"
                                   autocomplete="Πόλη" required
                                   class="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                        </div>
                    </div>

                    <div class="col-span-3">
                        <label for="postal-code" class="block text-sm font-medium text-gray-700">Ταχυδρομικός κώδικας
                            <abbr title="required">*</abbr></label>
                        <div class="mt-1">
                            <input wire:model.live="postalCode" required type="text" name="billing_zip" id="postal-code"
                                   autocomplete="postal-code"
                                   class="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                        </div>
                    </div>


                </div>
            </div>
            {{--end billing information --}}


            {{--Timologio--}}
            <div class="mt-10 border-t border-gray-200 pt-10">
                <h2 x-on:click="openInvoice = !openInvoice" class="text-lg font-medium text-gray-900 cursor-pointer">
                    Έκδοση τιμολογίου</h2>

                <div x-show="openInvoice" x-transition class="mt-4 grid grid-cols-1 gap-y-6 sm:grid-cols-6 sm:gap-x-4">

                    <div class="col-span-6">
                        <label for="vat" class="block text-sm font-medium text-gray-700">ΑΦΜ</label>
                        <div class="mt-1">
                            <input x-on:focusOut="$wire.getInvoiceDetails()" wire:model="vat" type="text" id="vat"
                                   name="afm" autocomplete="ΑΦΜ"
                                   class="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                        </div>
                    </div>

                    <div class="col-span-6">
                        <label for="activity" class="block text-sm font-medium text-gray-700">Επωνυμία επιχείρησης<abbr
                                title="required">*</abbr></label>
                        <div class="mt-1">
                            <input type="text" id="activity" wire:model="activity" name="activity" autocomplete=""
                                   class="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                        </div>
                    </div>
                    <div class="col-span-6">
                        <label for="doy" class="block text-sm font-medium text-gray-700">ΔΟΥ<abbr
                                title="required">*</abbr></label>
                        <div class="mt-1">
                            <input wire:model="doy" type="text" name="doy" id="doy" autocomplete="tel"
                                   class="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                        </div>
                    </div>
                    <div class="col-span-6">
                        <label for="company" class="block text-sm font-medium text-gray-700">Σταθερό Τηλέφωνο</label>
                        <div class="mt-1">
                            <input wire:model="company" type="text" name="company" id="company" autocomplete="tel"
                                   class="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                        </div>
                    </div>


                </div>
            </div>
            {{--Timologio --}}


            {{--shipping information--}}
            <div class="mt-10 border-t border-gray-200 pt-10">
                <h2 x-on:click="shipToDiferentAddress = !shipToDiferentAddress"
                    class="text-lg font-medium text-gray-900 cursor-pointer">Αποστολή σε διαφορετική Δ/νση</h2>

                <div x-show="shipToDiferentAddress" class="mt-4 grid grid-cols-1 gap-y-6 sm:grid-cols-6 sm:gap-x-4">


                    <div class="col-span-3">
                        <label for="first-name" class="block text-sm font-medium text-gray-700">Όνομα<abbr
                                title="required">*</abbr></label>
                        <div class="mt-1">
                            <input type="text" id="first-name" name="shipping_name"
                                   autocomplete="name"
                                   class="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                        </div>
                    </div>

                    <div class="col-span-3">
                        <label for="last-name" class="block text-sm font-medium text-gray-700">Επίθετο<abbr
                                title="required">*</abbr></label>
                        <div class="mt-1">
                            <input type="text" id="last-name" name="shipping_lastname" autocomplete="lastname"
                                   class="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                        </div>
                    </div>
                    <div class="col-span-3">
                        <label for="shipping_mobile" class="block text-sm font-medium text-gray-700">Κινητό
                            τηλέφωνο<abbr title="required">*</abbr></label>
                        <div class="mt-1">
                            <input type="text" name="shipping_mobile" id="shipping_mobile" autocomplete="tel"
                                   class="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                        </div>
                    </div>
                    <div class="col-span-3">
                        <label for="shipping_phone" class="block text-sm font-medium text-gray-700">Σταθερό
                            Τηλέφωνο</label>
                        <div class="mt-1">
                            <input type="text" name="shipping_phone" id="shipping_phone" autocomplete="tel"
                                   class="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                        </div>
                    </div>


                    <div class="col-span-6 md:col-span-4">
                        <label for="shipping_street" class="block text-sm font-medium text-gray-700">Δ/νση<abbr
                                title="required">*</abbr></label>
                        <div class="mt-1">
                            <input type="text" name="shipping_street" id="shipping_street" autocomplete="street-address"
                                   class="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                        </div>
                    </div>

                    <div class="col-span-6 md:col-span-2">
                        <label for="shipping_street_number" class="block text-sm font-medium text-gray-700">Αρ.
                            Οικίας/Κτιρίου</label>
                        <div class="mt-1">
                            <input type="text" name="shipping_street_number" id="shipping_street_number"
                                   class="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                        </div>
                    </div>


                    <div class="col-span-3">
                        <label for="country" class="block text-sm font-medium text-gray-700">Χώρα <abbr
                                title="required">*</abbr></label>
                        <div class="mt-1">
                            <select wire:model.change="country" id="country" name="shipping_country_id"
                                    autocomplete="country-name"
                                    class="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                                <option value="1">Ελλάδα</option>
                                <option value="2">Κύπρος</option>

                            </select>
                        </div>
                    </div>
                    <div class="col-span-3">

                        <label for="region" class="block text-sm font-medium text-gray-700">Νομός<abbr title="required">*</abbr></label>
                        <div class="mt-1">
                            @if($this->country)
                                <select wire:model.change="county" id="county" name="shipping_region"
                                        autocomplete="country-name"
                                        class="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                                    <option value="">Επιλογή Νομού</option>
                                    @foreach($countries->find( $this->country)->counties as $county)
                                        <option value="{{$county->id}}">{{$county->title}}</option>
                                    @endforeach

                                </select>
                            @endif
                        </div>
                    </div>
                    <div class="col-span-3">
                        <label for="city" class="block text-sm font-medium text-gray-700">Πόλη <abbr
                                title="required">*</abbr></label>
                        <div class="mt-1">
                            <input type="text" name="shipping_city" id="city" autocomplete="city"
                                   class="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                        </div>
                    </div>


                    <div class="col-span-3">
                        <label for="postal-code" class="block text-sm font-medium text-gray-700">Ταχυδρομικός κώδικας
                            <abbr title="required">*</abbr></label>
                        <div class="mt-1">
                            <input wire:model.live="postalCode" type="text" name="shipping_zip" id="postal-code"
                                   autocomplete="postal-code"
                                   class="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                        </div>
                    </div>


                </div>
            </div>
            {{--end shipping information --}}


        </div>
        <div class="mt-10 lg:mt-0">
            <h2 class="text-lg font-medium text-gray-900">Order summary</h2>

            <div class="mt-4 rounded-lg border border-gray-200 bg-white shadow-sm">
                <h3 class="sr-only">Items in your cart</h3>
                <ul role="list" class="divide-y divide-gray-200">
                    @foreach($cart->items as $item)

                        <li class="flex px-4 py-6 sm:px-6">
                            <div class="flex-shrink-0">
                                <img src="{{$item->stock->product->getFirstMediaUrl('product')}}"
                                     alt="{{$item->stock->product->getFirstMedia('product')->name}}"
                                     class="w-20 rounded-md">

                            </div>

                            <div class="ml-6 flex flex-1 flex-col">
                                <div class="flex">
                                    <div class="min-w-0 flex-1">
                                        <h4 class="text-sm">
                                            <a href="#"
                                               class="font-medium text-gray-700 hover:text-gray-800">{{$item->stock->product->title}}</a>
                                        </h4>
                                        <p class="mt-1 text-sm text-gray-500">{{$item->stock->sku}}</p>
                                    </div>


                                </div>

                                <div class="flex flex-1 items-end justify-end pt-2">
                                    <p class="mt-1 text-sm font-medium text-gray-900">{{$item->price}}€</p>


                                </div>
                            </div>
                        </li>
                    @endforeach
                    <!-- More products... -->
                </ul>

                <dl class="space-y-2 border-t border-gray-200 px-4 py-6 sm:px-6">
                    <div class="flex items-center justify-between">
                        <dt class="text-sm">Κόστος</dt>
                        <dd class="text-sm font-medium text-gray-900">{{$this->cart->subTotal()}}€</dd>
                    </div>


                    @if($shippingMethodUnique != null && isset($availableShippings[$shippingMethodUnique]))
                        <div class="flex items-center justify-between">
                            <dt class="text-sm">{{$availableShippings[$shippingMethodUnique]['title']}}</dt>
                            <dd class="text-sm font-medium text-gray-900">{{$availableShippings[$shippingMethodUnique]['extra_price']}}€
                            </dd>
                        </div>
                    @endif

                    @if($paymentMethodUnique != null && isset($availablePayments[$paymentMethodUnique]))
                        <div class="flex items-center justify-between">
                            <dt class="text-sm">{{$availablePayments[$paymentMethodUnique]['title']}}</dt>
                            <dd class="text-sm font-medium text-gray-900">{{$availablePayments[$paymentMethodUnique]['extra_price']}}€
                            </dd>
                        </div>
                    @endif

                    @if($this->cart->voucher_id != null)

                        <div class="flex items-center justify-between">
                            <dt class="text-sm">Κουπόνι: {{$this->cart->voucher_discount()->code}}</dt>
                            <dd class="text-sm font-medium text-gray-900">
                                - {{$this->cart->voucher_discount()->discount_amount}}€
                            </dd>
                        </div>
                    @endif

                    <div class="flex items-center justify-between border-t border-gray-200 pt-6">
                        <dt class="text-base font-medium">Τελικό Σύνολο</dt>
                        <dd class="text-base font-medium text-gray-900">{{$cart->total((isset($availableShippings[$shippingMethodUnique]['extra_price'])?$availableShippings[$shippingMethodUnique]['extra_price']:null),(isset($availablePayments[$paymentMethodUnique]['extra_price'])?$availablePayments[$paymentMethodUnique]['extra_price']:null))}}€</dd>
                    </div>
                </dl>


                <div class="  border-t px-4 border-gray-200 pt-4 mb-4">
                    <fieldset>
                        <legend class="text-lg font-medium text-gray-900">Τρόποι Αποστολής</legend>
                        <div class="mt-4 grid grid-cols-1 gap-y-6 sm:grid-cols-2 sm:gap-x-4">
                            <!--
                              Checked: "border-transparent", Not Checked: "border-gray-300"
                              Active: "ring-2 ring-indigo-500"
                            -->
                            @foreach($shippings as $deliver)
                                <label aria-label="Standard"
                                       class="relative flex cursor-pointer rounded-lg border bg-white p-4 shadow-sm focus:outline-none">
                                    <input wire:model.defer="shippingMethodUnique" type="radio"
                                           name="shipping_unique_id"
                                           value="{{$deliver['unique_id']}}" class="sr-only peer">

                                    <input wire:model.defer="shipping_id" name="shipping_id" type="hidden"
                                           value="{{$deliver['id']}}"
                                           class="h-4 w-4 border-gray-300 text-indigo-600 focus:ring-indigo-500">
                                    <span class="flex flex-1">
                                        <span class="flex flex-col">
                                          <span
                                              class="block text-sm font-medium text-gray-900">{{$deliver['title']}}</span>
                                          <span
                                              class="mt-1 flex items-center text-sm text-gray-500">4–10 business days</span>
                                          <span class="mt-2 text-sm font-medium text-gray-900"></span>
                                        </span>
                                      </span>
                                    <svg class="h-5 w-5 text-indigo-600 hidden peer-checked:block"
                                         viewBox="0 0 20 20"
                                         fill="currentColor" aria-hidden="true">
                                        <path fill-rule="evenodd"
                                              d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z"
                                              clip-rule="evenodd"/>
                                    </svg>
                                    <span
                                        class="pointer-events-none absolute -inset-px rounded-lg border-2 peer-checked:border-indigo-500"
                                        aria-hidden="true"></span>
                                </label>
                            @endforeach


                        </div>
                    </fieldset>
                </div>

                <!-- Payment -->
                <div class="border-t px-4 mb-4 border-gray-200 pt-4">
                    <h2 class="text-lg font-medium text-gray-900">Τρόποι Πληρωμής</h2>

                    @foreach($payments as $p)
                        <fieldset class="mt-4">
                            <div class="space-y-4 sm:flex sm:items-center sm:space-x-10 sm:space-y-0">
                                <div class="flex items-center">
                                    <input wire:model="paymentMethodUnique" id="{{$p['name']}}" name="payment_unique_id"
                                           type="radio"

                                           value="{{$p['unique_id']}}"

                                           class="h-4 w-4 border-gray-300 text-indigo-600 focus:ring-indigo-500">
                                    <input wire:model.defer="payment_id" name="payment_id" type="hidden"

                                           value="{{$p['id']}}"

                                           class="h-4 w-4 border-gray-300 text-indigo-600 focus:ring-indigo-500">
                                    <label for="{{$p['name']}}"
                                           class="ml-3 block text-sm font-medium text-gray-700">{{$p['title']}}</label>
                                </div>


                            </div>
                        </fieldset>
                    @endforeach

                </div>


                <div class="border-t border-gray-200 px-4 py-6 sm:px-6">
                    <button type="submit"
                            @if(!$completeOrder) disabled @endif
                    class="disabled:opacity-25 w-full rounded-md border border-transparent bg-[#c09578] px-4 py-3 text-base font-medium text-white shadow-sm hover:bg-black focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 focus:ring-offset-gray-50">
                        Ολοκλήρωση Παραγγελίας
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
</div>

@push('scripts')
    <script>
        // Your JS here.
    </script>
@endpush
