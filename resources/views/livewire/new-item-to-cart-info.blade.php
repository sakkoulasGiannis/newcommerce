<div>

    @if($message != '')
        <div class="  mt-4">
            <div class="alert {{($type=='success')?'alert-success':'alert-error'}} ">
                <ul>
                    <li>
                        <div
                            class="flex items-center p-4 mb-4 text-sm  border  {{($type=='success')?'border-blue-300 bg-blue-50 text-blue-800':'border-red-300 bg-red-50 text-red-500'}}   rounded-lg  dark:bg-gray-800 dark:text-blue-400 dark:border-blue-800"
                            role="alert">
                            <svg class="flex-shrink-0 inline w-4 h-4 me-3" aria-hidden="true"
                                 xmlns="http://www.w3.org/2000/svg"
                                 fill="currentColor" viewBox="0 0 20 20">
                                <path
                                    d="M10 .5a9.5 9.5 0 1 0 9.5 9.5A9.51 9.51 0 0 0 10 .5ZM9.5 4a1.5 1.5 0 1 1 0 3 1.5 1.5 0 0 1 0-3ZM12 15H8a1 1 0 0 1 0-2h1v-3H8a1 1 0 0 1 0-2h2a1 1 0 0 1 1 1v4h1a1 1 0 0 1 0 2Z"/>
                            </svg>
                            <span class="sr-only">Info</span>
                            <div class="ml-2">
                                 {!! $message !!}
                            </div>
                        </div>

                    </li>
                </ul>
            </div>

        </div>
    @endif

</div>
