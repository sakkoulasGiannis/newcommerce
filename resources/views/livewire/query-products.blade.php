<div>
    @if(count($products) > 0)
    <div class="bg-white">
        <div class="mx-auto max-w-2xl px-4 py-16 sm:px-6 sm:py-24 lg:max-w-7xl lg:px-8">
            <div class="md:flex md:items-center md:justify-between">
                <h2 class="text-2xl font-bold tracking-tight text-gray-900">{{$title[$local]}}</h2>

                @if(isset($moreLink )&& $moreLink != null)
                <a href="{{$moreLink}}" class="hidden text-sm font-medium text-indigo-600 hover:text-indigo-500 md:block">
                    Περισσότερα
                    <span aria-hidden="true"> &rarr;</span>
                </a>
                @endif
            </div>
                <div class="mt-6 grid md:grid-cols-4 grid-cols-2 gap-x-4 gap-y-10 sm:gap-x-6  md:gap-y-0 lg:gap-x-8">

                @foreach($products as $product)
                    @include(\Config::get('view.theme') . '.product_card', ['product' => $product])
                @endforeach
                </div>
            @if(isset($moreLink )&& $moreLink != null)

            <div class="mt-8 text-sm md:hidden">
                <a href="{{$moreLink}}" class="font-medium text-indigo-600 hover:text-indigo-500">
                    Περισσότερα
                    <span aria-hidden="true"> &rarr;</span>
                </a>
            </div>
            @endif
        </div>
    </div>
    @endif
</div>
