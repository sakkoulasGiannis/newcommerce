<div x-data="{ open: @entangle('dropdownOpen') }" class="relative" @click.away="open = false">
    <div class="relative mt-2">
        <div class="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 text-gray-400">
                <path stroke-linecap="round" stroke-linejoin="round" d="m21 21-5.197-5.197m0 0A7.5 7.5 0 1 0 5.196 5.196a7.5 7.5 0 0 0 10.607 10.607Z" />
            </svg>
        </div>
        <input id="search-input" type="text" wire:model.live.debounce.150ms="search" placeholder="αναζήτηση"
               @focus="open = true"
               class="block w-full md:w-48 border-0 py-1.5 pl-10 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
    </div>

    @if(!empty($products))
        <div x-show="open" class="flex flex-col md:flex-row gap-4 bg-white p-4 rounded-md shadow-2xl z-50 absolute w-[48rem]">
            <ul class="md:order-1 order-2" id="searchResults ">
                @foreach($products as $product)
                    <li class="border-b py-2">
                        <div class="flex">
                            <div class="mr-4 flex-shrink-0">
                                <a href="/{{$product['name']}}">
                                    <img src="{{$product['image']}}" alt="{{$product['name']}}" class="h-20 w-20 object-cover object-center">
                                </a>
                            </div>
                            <div>
                                <a href="/{{$product['name']}}">
                                    <h4 class="text-md font-medium">{{$product['title']}}</h4>

                                </a>
                                <p class="text-md font-medium text-gray-900 text-right">
                                    <span class="font-bold text-[#c09578] ">{{$product['price']}}€</span>

                                    @if(isset($product['originalPrice']))
                                        <span class="ml-2 text-md font-medium  text-gray-700 line-through">{{$product['originalPrice']}}€</span>
                                    @endif
                                </p>
{{--                                <div class="fw-bold text-[#c09578]">{{$product['price']}}€</div>--}}
                                <p class="mt-1">{{$product['description']}}</p>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
            <div class="order-1 md:order-2 max-w-max">
                @if(count($products) > 0)
                    @include(\Config::get('view.theme').'.product_card', ['product' => \App\Models\Product::find($products[0]['objectID'])])
                @endif
            </div>
        </div>
    @endif
</div>
