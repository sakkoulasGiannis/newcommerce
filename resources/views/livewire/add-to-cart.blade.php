<div>
{{--    <button wire:click="addItemToCart"--}}
{{--            id="addToCartButton"--}}
{{--            class="mt-8 flex relative w-full items-center justify-center rounded-md border border-transparent bg-[#c09578] px-8 py-3 text-base font-medium text-white hover:bg-[#ab856c] focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">--}}
{{--        Προσθήκη στο καλάθι--}}
{{--    </button>--}}
    <button wire:click="addItemToCart" id="addToCartButton" type="button" class="mt-4 inline-flex items-center gap-x-1.5 rounded-md bg-[#c09578] px-4 py-3 text-sm font-semibold text-white shadow-sm hover:bg-[#ab856c] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">

        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="-mr-0.5 size-5">
            <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 10.5V6a3.75 3.75 0 1 0-7.5 0v4.5m11.356-1.993 1.263 12c.07.665-.45 1.243-1.119 1.243H4.25a1.125 1.125 0 0 1-1.12-1.243l1.264-12A1.125 1.125 0 0 1 5.513 7.5h12.974c.576 0 1.059.435 1.119 1.007ZM8.625 10.5a.375.375 0 1 1-.75 0 .375.375 0 0 1 .75 0Zm7.5 0a.375.375 0 1 1-.75 0 .375.375 0 0 1 .75 0Z" />
        </svg>
        Προσθήκη στο καλάθι
    </button>


@if($product->crossSells)
{{--        @if($product->cross_sell_method == 'required')--}}
            <div class="grid grid-cols-2 gap-4 md:grid-cols-3 mt-4">
                @foreach($product->crossSells as $crossSell)
                    <label aria-label="Standard" class="relative flex cursor-pointer rounded-lg border bg-white p-4 shadow-sm focus:outline-none">
                        <input wire:model.live="extraProducts" type="checkbox" name="cross_sell" value="{{$crossSell->id}}" class="sr-only peer">
                        <span class="flex flex-1">
                            <span class="flex flex-col">
                                <img src="{{$crossSell->thumb}}" alt="">
                              <span class="block text-sm font-medium text-gray-900">{{$crossSell->title}}</span>
                               <p class="text-md font-medium text-gray-900 text-right">
                                <span class="font-bold text-[#c09578]">{{$crossSell->price}}€</span>
                                   @if(isset($crossSell->originalPrice))
                                       <span class="ml-2 text-lg font-medium text-red-500 line-through">{{$crossSell->originalPrice}}€</span>
                                   @endif
                                </p>
                            </span>
                        </span>
{{--                        <svg class="h-5 w-5 text-indigo-600 hidden peer-checked:block" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">--}}
{{--                            <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z" clip-rule="evenodd"/>--}}
{{--                        </svg>--}}
                        <span class="pointer-events-none absolute -inset-px rounded-lg border-2 peer-checked:border-indigo-500" aria-hidden="true"></span>
                    </label>
                @endforeach
            </div>

{{--        @else--}}
{{--            --}}
{{--            --}}

{{--        @endif--}}
    @endif
</div>
