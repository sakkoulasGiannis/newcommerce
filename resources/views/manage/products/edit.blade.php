@extends('layouts.admin')
@section('content')

    <product-edit :id="{{$product->id}}"></product-edit>

@endsection
@section('scripts')
    @parent

@endsection
