@extends('layouts.admin')
@section('content')
    <div class="px-4 sm:px-6 lg:px-8">
        <div class="sm:flex sm:items-center">
            <div class="sm:flex-auto">
                <h1 class="text-base font-semibold text-gray-900">Products</h1>
                {{--                <p class="mt-2 text-sm text-gray-700">A list of all the users in your account including their name, title, email and role.</p>--}}
            </div>
            <div class="mt-4 sm:ml-16 sm:mt-0 sm:flex-none">
                <a href="/manage/products/create" type="button"
                   class="block rounded-md bg-indigo-600 px-3 py-2 text-center text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">Νέο
                    Προϊον</a>
            </div>
        </div>
        <div class="mt-8 flow-root">
            <div class="-mx-4 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
                    <div class="">
                        <form method="get" action="{{ url('/manage/products') }}" class="flex flex-row justify-between">
                            <div>
                                <input type="text" name="sku" value="{{request()->get('sku')}}"
                                       class="border border-gray-300   px-3 py-2  " placeholder="Κωδικός">
                                <input type="text" name="title" value="{{request()->get('title')}}"
                                       class="ml-2 border border-gray-300   px-3 py-2  " placeholder="Τίτλος">
                                <button type="submit" class="ml-2  bg-green-400 px-2 py-3 text-xs text-white">
                                    Αναζήτηση
                                </button>
                                <a href="/manage/products" class="ml-2    text-red-600">
                                    X
                                </a>
                            </div>


                            <select name="per_page" id="perPage" onchange="this.form.submit()">
                                <option value="20" {{(request()->get('perPage') == 20)?'selected':''}}>10</option>
                                <option value="20" {{(request()->get('perPage') == 20)?'selected':''}}>20</option>
                                <option value="30" {{(request()->get('perPage') == 30)?'selected':''}}>30</option>
                                <option value="40" {{(request()->get('perPage') == 40)?'selected':''}}>40</option>
                                <option value="50" {{(request()->get('perPage') == 50)?'selected':''}}>50</option>
                                <option value="100" {{(request()->get('perPage') == 100)?'selected':''}}>100</option>
                            </select>


                        </form>
                    </div>

                    <table class="min-w-full divide-y divide-gray-300">
                        <thead>
                        <tr>
                            <th scope="col"
                                class="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-0">Id
                            </th>
                            <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                Φωτογραφία
                            </th>
                            <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">Τίτλος
                            </th>
                            <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">Κωδικός
                            </th>
                            <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">Τιμή</th>
                            <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">Εκπ/Καν
                            </th>
                            <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                Κατάσταση
                            </th>
                            <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">Προβολή
                            </th>
                            <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                Επεξεργασία
                            </th>
                        </tr>

                        </thead>

                        <tbody class="divide-y divide-gray-200 bg-white">

                        @foreach($products as $product)
                            <tr>

                                <td class="whitespace-nowrap px-3 py-5 text-sm text-gray-500">{{$product->id}}

                                    <button class="product_details_btn" data-id="{{$product->id}}">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                             stroke-width="1.5" stroke="currentColor" class="size-6 inline">
                                            <path stroke-linecap="round" stroke-linejoin="round"
                                                  d="m19.5 8.25-7.5 7.5-7.5-7.5"/>
                                        </svg>

                                    </button>

                                </td>
                                <td class="whitespace-nowrap py-3 pl-4 pr-3 text-sm sm:pl-0">
                                    <div class="flex items-center">
                                        <div class="size-11 shrink-0">
                                            <img class="size-11 " src="{{$product->thumb}}" alt="">
                                        </div>
                                        {{--                                    <div class="ml-4">--}}
                                        {{--                                        <div class="font-medium text-gray-900">{{$product->title}}</div>--}}
                                        {{--                                        <div class="mt-1 text-gray-500">lindsay.walton@example.com</div>--}}
                                        {{--                                    </div>--}}
                                    </div>
                                </td>
                                <td class="whitespace-nowrap px-3 py-5 text-sm text-gray-500">
                                    <div class="text-gray-900">{{$product->title}}</div>
                                    {{--                                <div class="mt-1 text-gray-500">Optimization</div>--}}
                                </td>
                                <td class="whitespace-nowrap px-3 py-5 text-sm text-gray-500">
                                    <div class="text-gray-900">{{$product->sku}}</div>
                                    {{--                                <div class="mt-1 text-gray-500">Optimization</div>--}}
                                </td>
                                <td class="whitespace-nowrap px-3 py-5 text-sm text-gray-500">
                                    <div class=" text-gray-900">{{$product->sale_price}}€</div>
                                    <div class="mt-1 text-gray-400">{{$product->price}}€</div>
                                </td>
                                <td class="whitespace-nowrap px-3 py-5 text-sm text-gray-500">
                                    <div class="text-gray-900">{{($product->rules)?$product->rules->id:'-'}}</div>
                                    {{--                                <div class="mt-1 text-gray-500">Optimization</div>--}}
                                </td>
                                <td class="whitespace-nowrap px-3 py-5 text-sm text-gray-500">
                                    <span
                                        class="inline-flex items-center rounded-md  {{($product->product_is_active)?'bg-green-50':'bg-red-50'}} px-2 py-1 text-xs font-medium text-green-700 ring-1 ring-inset ring-green-600/20">{{($product->product_is_active)?'Ενεργό':'Ανενεργό'}}</span>
                                </td>

                                <td class="whitespace-nowrap px-3 py-5 text-sm text-gray-500">
                                    <a href="/product/{{$product->name}}" target="_blank"
                                       class="text-indigo-600 hover:text-indigo-900">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                             stroke-width="1.5" stroke="currentColor" class="size-6">
                                            <path stroke-linecap="round" stroke-linejoin="round"
                                                  d="M2.036 12.322a1.012 1.012 0 0 1 0-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178Z"/>
                                            <path stroke-linecap="round" stroke-linejoin="round"
                                                  d="M15 12a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z"/>
                                        </svg>
                                    </a>
                                </td>

                                {{--                            <td class="whitespace-nowrap px-3 py-5 text-sm text-gray-500">Member</td>--}}
                                <td class="whitespace-nowrap px-3 py-5 text-sm text-gray-500">
                                    <a href="/manage/products/{{$product->id}}"
                                       class="text-indigo-600 hover:text-indigo-900">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                             stroke-width="1.5" stroke="currentColor" class="size-6">
                                            <path stroke-linecap="round" stroke-linejoin="round"
                                                  d="m16.862 4.487 1.687-1.688a1.875 1.875 0 1 1 2.652 2.652L10.582 16.07a4.5 4.5 0 0 1-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 0 1 1.13-1.897l8.932-8.931Zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0 1 15.75 21H5.25A2.25 2.25 0 0 1 3 18.75V8.25A2.25 2.25 0 0 1 5.25 6H10"/>
                                        </svg>

                                    </a>
                                </td>
                            </tr>
                            {{--                            <tr class="product_id_{{$product->id}} hidden">--}}
                            @foreach($product->stock as $stock)
                                <tr class="product_id_{{$product->id}} hidden">
                                    <td colspan="5">
                                        <form method="post"
                                              action="/manage/products/{{$product->id}}/stock/update/quantity">
                                            @csrf
                                            Sku: {{$stock->sku}} |
                                            <input type="number" name="quantity" value="{{$stock->quantity}}">
                                            <input type="text" readonly name="sku" value="{{$stock->sku}}">
                                            <!-- Αλλάξαμε type σε button -->
                                            <button type="button" class="updateQty bg-green-400 py-2 px-4 text-white"
                                                    onclick="console.log('Inline click works!');">Update
                                            </button>
                                        </form>

                                    </td>
                                </tr>
                            @endforeach
                            {{--                            </tr>--}}
                        @endforeach

                        <!-- More people... -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{ $products->links(config('view.theme').'.pagination') }}


    {{--    <product-index></product-index>--}}

@endsection
@section('scripts')
    <script>
        document.addEventListener("click", function (event) {
            if (event.target.matches(".updateQty")) {
                event.preventDefault(); // Αποτρέπει το default action

                // Βρίσκει το κοντινότερο parent form
                let form = event.target.closest("form");
                if (!form) return;

                let formData = new FormData(form);
                let actionUrl = form.getAttribute("action");

                fetch(actionUrl, {
                    method: "POST",
                    body: formData,
                    headers: {
                        "X-Requested-With": "XMLHttpRequest"
                    }
                })
                    .then(response => response.json())
                    .then(data => {
                        if (data.success) {
                            alert("Quantity updated successfully!");
                        } else {
                            alert("Failed to update quantity!");
                        }
                    })
                    .catch(error => console.error("Error:", error));
            }
        });


    </script>
@endsection

