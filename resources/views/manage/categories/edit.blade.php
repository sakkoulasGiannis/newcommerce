@extends('layouts.admin')
@section('content')
    <categories-edit :category="{{ json_encode($category) }}" />
@endsection
@section('scripts')
    @parent
@endsection
