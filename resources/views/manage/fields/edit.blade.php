@extends('layouts.admin')
@section('content')
    <div class="py-4">
        <div class="mx-auto justify-start max-w-7xl px-4 sm:px-6 lg:px-8">
            <form method="POST" action="/manage/fields/{{$field->id}}" class="justify-start">
                @csrf
                @method('PUT')

                <div class="space-y-12">
                    <div class="border-b border-gray-900/10 pb-12">
                        <h2 class="text-base font-semibold leading-7 text-gray-900">Profile</h2>

                        <div class="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">

                            <div class="sm:col-span-4">
                                <label for="attribute_title" class="block text-sm font-medium leading-6 text-gray-900">Τίτλος
                                    Ιδιότητας
                                </label>
                                <div class="mt-2">
                                    <div
                                        class="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                                        <input
                                               type="text" name="title" id="attribute_title"
                                               value="{{$field->title}}"
                                               class="block w-full rounded-md border-0 px-4 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                               placeholder="size"></div>
                                </div>
                            </div>

                            <div class="sm:col-span-4">
                                <label for="attribute_title" class="block text-sm font-medium leading-6 text-gray-900">Όνομα
                                    Ιδιότητας
                                </label>
                                <div class="mt-2">
                                    <div
                                        class="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                                        <input readonly
                                               type="text" name="name" id="attribute_title"
                                               value="{{$field->name}}"
                                               class="block w-full rounded-md border-0 px-4 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                               placeholder="size"></div>
                                </div>
                            </div>

                            <div class="sm:col-span-3">
                                <div>
                                    <label for="type" class="block text-sm font-medium leading-6 text-gray-900">Τύπος
                                        πεδίου</label>
                                    <select id="type" name="type"
                                            class="mt-2 block w-full rounded-md border-0 py-1.5 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6">
                                        <option value="text" {{($field->type == 'text') ?'selected':null}} >Text
                                        </option>
                                        <option value="select" {{($field->type == 'select') ?'selected':null}}>Select
                                        </option>
                                        <option value="checkbox" {{($field->type == 'checkbox') ?'selected':null}}>
                                            Checkbox
                                        </option>
                                        <option value="radio" {{($field->type == 'radio') ?'selected':null}}>Radio
                                        </option>
                                        <option value="textarea" {{($field->type == 'textarea') ?'selected':null}}>
                                            Textarea
                                        </option>
                                    </select>
                                    <small>Παρακαλώ να μην γίνεται αλλαγή του τύπου εάν έχουν δημιουργηθεί ιδιότητες</small>
                                </div>
                            </div>

                            <div class="sm:col-span-4">
                                <fieldset>
                                     <div class="mt-6 space-y-6">
                                        <div class="relative flex gap-x-3">
                                            <div class="flex h-6 items-center">
                                                <input value="1" {{ old('can_select_multiple', $field->can_select_multiple) ? 'checked' : '' }} id="can_select_multiple" name="can_select_multiple" type="checkbox"
                                                       class="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600">

                                             </div>
                                            <div class="text-sm leading-6">
                                                <label for="can_select_multiple" class="font-medium text-gray-900">Πολλαπλή επιλογή</label>

                                            </div>
                                        </div>
                                        <div class="relative flex gap-x-3">
                                            <div class="flex h-6 items-center">
                                                <input {{ old('is_read_only', $field->is_read_only) ? 'checked' : '' }}  value="1"   id="is_read_only" name="is_read_only" type="checkbox"
                                                       class="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600">
                                            </div>
                                            <div class="text-sm leading-6">
                                                <label for="is_read_only"
                                                       class="font-medium text-gray-900">Μόνο για ανάγνωση</label>

                                            </div>
                                        </div>
                                        <div class="relative flex gap-x-3">
                                            <div class="flex h-6 items-center">
                                                <input value="1" {{($field->is_enabled)?'checked':null}}  id="is_enabled" name="is_enabled" type="checkbox"
                                                       class="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600">
                                            </div>
                                            <div class="text-sm leading-6">
                                                <label for="is_enabled" class="font-medium text-gray-900">Ενεργοποιημένο</label>

                                            </div>
                                        </div>
                                         <div class="relative flex gap-x-3">
                                            <div class="flex h-6 items-center">
                                                <input  value="1" {{($field->show_in_filters)?'checked':null}} id="show_in_filters" name="show_in_filters" type="checkbox"
                                                       class="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600">
                                            </div>
                                            <div class="text-sm leading-6">
                                                <label for="show_in_filters" class="font-medium text-gray-900">Εμφάνιση στα φίλτρα</label>

                                            </div>
                                        </div>
                                         <div class="relative flex gap-x-3">
                                             <div class="flex h-6 items-center">
                                                 <input  value="1" {{($field->has_colors)?'checked':null}} id="has_colors" name="has_colors" type="checkbox"
                                                         class="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600">
                                             </div>
                                             <div class="text-sm leading-6">
                                                 <label for="has_colors" class="font-medium text-gray-900">Έχει επιλογή χρώματος</label>

                                             </div>
                                         </div>
                                         <div class="relative flex gap-x-3">
                                             <div class="flex h-6 items-center">
                                                 <input  value="1" {{($field->has_images)?'checked':null}} id="has_images" name="has_images" type="checkbox"
                                                         class="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600">
                                             </div>
                                             <div class="text-sm leading-6">
                                                 <label for="has_images" class="font-medium text-gray-900">Έχει επιλογή εικόνας</label>
                                             </div>
                                         </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="mt-6 flex items-center justify-end gap-x-6">
                    <button type="submit"
                            class="rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">
                        Ενημέρωση
                    </button>
                </div>

            </form>
        </div>
    </div>
@endsection
