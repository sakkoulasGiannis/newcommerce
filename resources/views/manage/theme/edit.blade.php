@extends('layouts.themeEditor')
@section('content')
    <!-- Το πεδίο όπου θα εμφανιστεί ο GrapesJS Editor -->
    <div class="layers-container">
        <div id="container" class="flex flex-row">
            <div id="layers" class="column" style="flex-basis: 300px;">
                <div class="p-4">
                    <h2 class="text-white font-black text-lg border-b py-2">Pages</h2>
                    <select class="componentList" id="componentSelect"
                            class="mt-2 block w-full rounded-md border-0 py-1.5 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6">
                        @foreach($components as $c)
                            <option
                                {{($c->id == $component->id)?'selected':null}}  value="/manage/theme/{{$theme->id}}/component/{{$c->id}}"
                                class="text-white {{($c->id == $component->id)?'text-black':null }}">
                                {{$c->page_type}}
                            </option>
                        @endforeach
                    </select>
                    <script>
                        // Επιλογή του dropdown στοιχείου
                        document.getElementById('componentSelect').addEventListener('change', function () {
                            // Λήψη της επιλεγμένης τιμής (URL)
                            var selectedUrl = this.value;

                            // Ανακατεύθυνση στο επιλεγμένο URL
                            window.location.href = selectedUrl;
                        });
                    </script>

                </div>
                <button id="save-button" class="text-right">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                         stroke="currentColor" class="w-h h-6 text-white">
                        <path stroke-linecap="round" stroke-linejoin="round"
                              d="M20.25 6.375c0 2.278-3.694 4.125-8.25 4.125S3.75 8.653 3.75 6.375m16.5 0c0-2.278-3.694-4.125-8.25-4.125S3.75 4.097 3.75 6.375m16.5 0v11.25c0 2.278-3.694 4.125-8.25 4.125s-8.25-1.847-8.25-4.125V6.375m16.5 0v3.75m-16.5-3.75v3.75m16.5 0v3.75C20.25 16.153 16.556 18 12 18s-8.25-1.847-8.25-4.125v-3.75m16.5 0c0 2.278-3.694 4.125-8.25 4.125s-8.25-1.847-8.25-4.125"/>
                    </svg>
                    Αποθήκευση
                </button>
                <div class="panel__global-settings"></div>
                <div class=" " id="blocks"></div>
                <div id="layers-container"></div>
            </div>
            <div class=" " id="gjs"></div>
        </div>

        <!-- GrapesJS JavaScript -->
        <script>
            const escapeName = (name) => `${name}`.trim().replace(/([^a-z0-9\w-:/]+)/gi, '-');

            // Αρχικοποίηση του GrapesJS Editor
            const editor = grapesjs.init({

                    layerManager: {
                        appendTo: "#layers-container"
                    },
                    protectedCss: '',
                    container: '#gjs',
                    fromElement: true,
                    height: '100vh',
                    width: '100%',

                    selectorManager: {
                        componentFirst: true,
                    },
                    plugins: ['gjs-google-material-icons', 'grapesjs-code-editor', 'save-selected-component-plugin', 'grapesjs-rte-extensions', 'gjs-blocks-basic', 'html-block', 'grapesjs-style-bg'],
                    pluginsOpts: {
                        'grapesjs-code-editor': {
                            codeViewOptions: {
                                theme: 'material-darker',
                                lineNumbers: true,  // Εμφάνιση αριθμών γραμμών
                                mode: 'htmlmixed',  // Χρήση mixed mode για HTML και CSS
                            },
                        },
                        "gjs-blocks-basic": {
                            blocks: ['link-block', 'quote', 'text-basic', 'link', 'image', 'video', 'map', 'text'],
                        }

                    },

                    // Disable the storage manager for the moment
                    storageManager:
                        false,
                    assetManager:
                        {
                            // Upload endpoint, set `false` to disable upload, default `false`
                            upload: '/manage/' + {{$theme->id}} + '/theme-images',
                            // The name used in POST to pass uploaded files, default: `'files'`
                            uploadName: 'files',
                        },
                    // panels: { defaults: [] },
                    // Avoid any default paneld
                    canvas: {
                        styles: [
                            // 'https://unpkg.com/purecss@2.0.6/build/pure-min.css',
                            'https://cdn.jsdelivr.net/npm/tailwindcss@latest/dist/tailwind.min.css',
                            // 'https://cdn.jsdelivr.net/npm/bulma@1.0.2/css/bulma.min.css',
                            '/css/editor.css'
                            , '/css/main.css'
                        ], scripts:
                            [
                                {
                                    src: '/js/editor.js',
                                }
                            ]
                    },
                });

            const am = editor.AssetManager;
            const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
            const url = `/manage/{{$theme->id}}/theme-images`;
            fetch(url, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': csrfToken, // Προσθέτουμε το CSRF token στο header
                },
            })
                .then(response => {
                    if (!response.ok) {
                        throw new Error('Network response was not ok');
                    }
                    am.add(response);
                    return response.json(); // Ανάγνωση της απάντησης ως JSON
                })
                .then(data => {
                    data.forEach(item => {
                        am.add(item);
                    });
                })
                .catch(error => {
                    console.error('There has been a problem with your fetch operation:', error);
                });

            componentHtml = @json(($component->html != null)?$component->html:'[]');
            componentCss = @json($component->css);
            componentJs = @json($component->js);
            template = @json(($component->template != null)?$component->template:'{"components":[],"style":[]}');

            // Set the updated HTML to the editor
            let templateData = JSON.parse(template);
            editor.setComponents(templateData.components);
             // editor.setComponents(templateData.components);
            editor.setStyle(templateData.style);
            ///////
            ///////
            ///////
            ///////
            // compoentnt manager




            ///////
            ///////

            editor.on('component:selected', component => {
                const toolbar = component.get('toolbar') || [];

                // Έλεγχος αν το component είναι τύπου 'image'
                if (component.attributes.type === 'image') {
                    const ImageLinkCommand = {
                        icon: 'fa fa-link font-14',
                        title: 'Link',
                        commandName: 'image-link',
                    };

                    // Έλεγχος αν το component έχει γονικό στοιχείο link
                    const parentIsLink = (component.parent().tagName === 'a');
                    console.log(parentIsLink)

                    // Αν το image δεν έχει γονικό στοιχείο link, προσθέτουμε το 'Link' icon στο toolbar
                    if (!parentIsLink) {
                        const CommandExists = toolbar.some(
                            (item) => item.command === ImageLinkCommand.commandName,
                        );

                        if (!CommandExists) {
                            toolbar.push({
                                attributes: {
                                    class: ImageLinkCommand.icon,
                                    title: ImageLinkCommand.title,
                                },
                                command: ImageLinkCommand.commandName,
                            });

                            editor.Commands.add('image-link', {
                                run(editor, sender) {
                                    var url = window.prompt('Enter the URL to link to:');
                                    if (url) {
                                        const SelectedStyles = component.getStyle();

                                        const newElement = `<a href="${url}">
                                ${component.view.el.outerHTML}
                            </a>`;

                                        const parent = component.parent();
                                        const index = parent.components().indexOf(component);

                                        // Replace the component with the new element
                                        component.remove();
                                        parent.components().add(newElement, { at: index });

                                        // Restore the selected component's styles
                                        const newComponent = parent.components().at(index);
                                        newComponent.setStyle(SelectedStyles);

                                        // Trigger change events
                                        editor.trigger('change:canvas');
                                    }
                                },
                            });
                        }
                    }
                }

                // Έλεγχος αν το component είναι τύπου 'link'
                if (component.attributes.type === 'link') {
                    const UnlinkCommand = {
                        icon: 'fa fa-unlink font-14',
                        title: 'Unlink',
                        commandName: 'unlink',
                    };

                    const CommandExists = toolbar.some(
                        (item) => item.command === UnlinkCommand.commandName,
                    );

                    if (!CommandExists) {
                        toolbar.push({
                            attributes: {
                                class: UnlinkCommand.icon,
                                title: UnlinkCommand.title,
                            },
                            command: UnlinkCommand.commandName,
                        });

                        editor.Commands.add('unlink', {
                            run(editor, sender) {
                                const parent = component.parent();
                                const index = parent.components().indexOf(component);
                                const linkedContent = component.components().models.map(model => model.toHTML()).join('');

                                // Αφαίρεση του link και διατήρηση του περιεχομένου
                                component.remove();
                                parent.components().add(linkedContent, { at: index });

                                // Trigger change events
                                editor.trigger('change:canvas');
                            },
                        });
                    }

                    // Επιλογή για αλλαγή link
                    const ChangeLinkCommand = {
                        icon: 'fa fa-edit font-14',
                        title: 'Change Link',
                        commandName: 'change-link',
                    };

                    const ChangeLinkCommandExists = toolbar.some(
                        (item) => item.command === ChangeLinkCommand.commandName,
                    );

                    if (!ChangeLinkCommandExists) {
                        toolbar.push({
                            attributes: {
                                class: ChangeLinkCommand.icon,
                                title: ChangeLinkCommand.title,
                            },
                            command: ChangeLinkCommand.commandName,
                        });

                        editor.Commands.add('change-link', {
                            run(editor, sender) {
                                var url = window.prompt('Enter the new URL:');
                                if (url) {
                                    component.addAttributes({ href: url });
                                    editor.trigger('change:canvas');
                                }
                            },
                        });
                    }
                }

                component.set('toolbar', toolbar);
            });




            // Custom Component for Container

            editor.DomComponents.addType('container', {
                model: {
                    defaults: {

                        tagName: 'div',
                        classes: ['container'],

                        style: {
                            display: 'flex',
                            'max-width': 'var(--container-width)',
                            width: '100%',
                            "margin-bottom": '20px',
                            'gap': '20px',
                            "margin-left": 'auto',
                            "margin-right": 'auto',
                        },

                    },
                },
            });

            // Custom Component for Column
            editor.DomComponents.addType('column', {
                model: {
                    defaults: {
                        draggable: '.container',  // Περιορισμός του draggable μέσα στο container

                        tagName: 'div',
                        classes: ['column'],
                        style: {
                            display: 'flex',
                            'flex-direction': 'column',
                            'flex-grow': '1',
                            'flex-basis': '0',
                            'gap': '10px',
                        },


                    },
                },
            });

            // Custom Block for Container
            editor.BlockManager.add('just-div', {
                label: 'Div',
                category: 'Layout',
                attributes: {class: 'fa fa-square'},
                content: "<div></div>",
            });

            editor.BlockManager.add('container-block', {
                label: 'Container',
                category: 'Layout',
                attributes: {class: 'fa fa-square'},
                content: {type: 'container'},
            });

            // Custom Block for Column
            editor.BlockManager.add('column-block', {
                label: 'Column',
                category: 'Layout',
                attributes: {class: 'fa fa-columns'},
                content: {type: 'column'},
            });


            // menu nav
            editor.DomComponents.addType('mainMenu', {
                model: {
                    defaults: {
                        tagName: 'nav',
                        draggable: true,
                        droppable: true,
                        attributes: {
                            id: 'nav-menu',
                            class: 'main-navigation',
                        },
                        style: {
                            display: 'flex',
                            'justify-content': 'center',
                        },
                        components: [
                            {
                                tagName: 'button',
                                type: 'text',
                                attributes: {
                                    class: 'menu-toggle',
                                },
                                content: '☰',
                            },
                            {
                                tagName: 'button',
                                type: 'text',
                                attributes: {
                                    class: 'menu-close',
                                },
                                content: '✖',
                            },
                            {
                                tagName: 'ul',
                                style: {
                                    display: 'flex',
                                    'justify-content': 'center',
                                    'gap': '20px',
                                },
                                attributes: {
                                    class: 'nav-container',
                                },
                                droppable: true,
                                components: [
                                    // Θα προστεθούν τα nav items εδώ
                                ],
                            }
                        ],

                        script: function () {
                            const toggleButton = this.querySelector('.menu-toggle');
                            const closeButton = this.querySelector('.menu-close');
                            const navMenu = this;

                            toggleButton.addEventListener('click', () => {
                                navMenu.classList.toggle('open');
                            });

                            closeButton.addEventListener('click', () => {
                                navMenu.classList.remove('open');
                            });

                            document.addEventListener('click', (event) => {
                                if (!navMenu.contains(event.target) && !toggleButton.contains(event.target)) {
                                    navMenu.classList.remove('open');
                                }
                            });

                            let startX = 0;
                            let endX = 0;

                            document.addEventListener('touchstart', (event) => {
                                startX = event.touches[0].clientX;
                            });

                            document.addEventListener('touchmove', (event) => {
                                endX = event.touches[0].clientX;
                            });

                            document.addEventListener('touchend', () => {
                                const swipeDistance = endX - startX;

                                if (swipeDistance > 50) {
                                    navMenu.classList.add('open');
                                }

                                if (swipeDistance < -50) {
                                    navMenu.classList.remove('open');
                                }
                            });
                        }
                    }
                },
                view: {
                    tagName: 'nav', // Καθορίζουμε ρητά το tagName στο view για να διασφαλίσουμε τη διατήρησή του
                    onRender() {
                        const navMenu = this.el;
                        const toggleButton = navMenu.querySelector('.menu-toggle');
                        const closeButton = navMenu.querySelector('.menu-close');
                        const dropdownToggles = navMenu.querySelectorAll('.dropdown-toggle');

                        dropdownToggles.forEach(toggle => {
                            toggle.addEventListener('click', (e) => {
                                e.preventDefault();
                                const megaMenu = toggle.nextElementSibling;

                                document.querySelectorAll('.mega-menu.open').forEach(menu => {
                                    if (menu !== megaMenu) {
                                        menu.classList.remove('open');
                                    }
                                });

                                if (megaMenu) {
                                    megaMenu.classList.toggle('open');
                                }
                            });
                        });
                    }
                },


            });

            editor.BlockManager.add('mainMenu', {
                label: 'Main Menu',
                category: 'Navigation',
                tagName: 'nav',
                attributes: {class: 'fa fa-bars'},
                content: {type: 'mainMenu', tagName: 'nav'},
            });
            editor.DomComponents.addType('navItemMegaMenu', {
                model: {
                    defaults: {
                        tagName: 'li',
                        draggable: 'ul.nav-container',
                        droppable: true,
                        attributes: {
                            class: 'dropdown',
                        },
                        components: [
                            {
                                tagName: 'a',
                                type: 'link',
                                style: {
                                    padding: '20px 15px',
                                    color: '#222',
                                    'text-decoration': 'none',
                                },
                                attributes: {
                                    href: '#',
                                    class: 'dropdown-toggle',
                                },
                                content: 'MENU ITEM <i class="fa fa-angle-down"></i>',
                            },
                            {
                                tagName: 'div',
                                style: {
                                    display: "block",
                                    gap: "20px",
                                    background: '#fff',
                                    transition: 'visibility 0s, opacity 0.5s linear',
                                    position: 'absolute',
                                    left: '0',
                                    padding: "10px",
                                    width: '100%',
                                    "padding-bottom": 'inherit',
                                    top: '38px',
                                    margin: '0 auto',
                                },
                                attributes: {
                                    class: 'mega-menu',
                                },
                                components: [
                                    {
                                        tagName: 'div',
                                        label: 'Mega Menu Row',
                                        style: {
                                            display: 'flex',
                                            gap: '20px',
                                            "flex-wrap": 'wrap',
                                            margin: '0 auto',
                                            "max-width": "var(--container-width)",

                                        },
                                        attribute: {
                                            class: 'mega-menu-row',
                                        },
                                        components: [
                                            {
                                                tagName: 'div',
                                                attributes: {
                                                    class: 'nav-container',
                                                },
                                                style: {
                                                    padding: '10px',
                                                },
                                                components: [
                                                    {
                                                        tagName: 'div',
                                                        attributes: {
                                                            class: 'nav-item',
                                                        },
                                                        components: [
                                                            {
                                                                tagName: 'span',
                                                                type: 'text',
                                                                style: {
                                                                    color: "#222222",
                                                                },
                                                                content: 'Section Title',
                                                            },
                                                            {
                                                                tagName: 'ul',
                                                                components: [
                                                                    {
                                                                        tagName: 'li',
                                                                        components: [{
                                                                            tagName: 'a',
                                                                            type: 'link',
                                                                            style: {
                                                                                color: '#222',
                                                                                display: 'block',
                                                                                padding: '10px',
                                                                                "border-bottom": '3px solid transparent',
                                                                                transition: 'all .3s ease',
                                                                            },
                                                                            attributes: {href: '#', class: 'nav-link'},
                                                                            content: 'Link 1'
                                                                        }]
                                                                    },

                                                                ]
                                                            }
                                                        ]
                                                    }
                                                ]
                                            }
                                        ],
                                    }

                                ],
                            },
                        ],

                        script: function () {
                            //run script after 1 sec
                            setTimeout(() => {
                                const dropdown = this;
                                const megaMenu = dropdown.querySelector('.mega-menu');

                                dropdown.addEventListener('dblclick', function () {
                                    console.log('dblci')
                                    if (megaMenu) {
                                        console.log('sdf')
                                        megaMenu.classList.toggle('open');
                                        dropdown.classList.toggle('mega-menu-open');
                                    }
                                });
                            }, 2000);
                        },
                    }
                },
                isComponent(el) {
                    if (el.tagName === 'LI' && el.classList.contains('dropdown')) {
                        return { type: 'navItemMegaMenu' };
                    }
                },
                view: {
                    tagName: 'li', // Καθορίζουμε ρητά το tagName στο view για να διασφαλίσουμε τη διατήρησή του
                },

            });


            editor.BlockManager.add('navItemMegaMenu', {
                label: 'Nav Item with Mega Menu',
                category: 'Navigation',

                attributes: {class: 'fa fa-list'},
                content: {type: 'navItemMegaMenu', tagName: 'li'},
            });

            // end menu nav
            editor.BlockManager.add('Hero1', {
                label: 'Hero 1',
                category: 'heros',
                attributes: {class: 'fa fa-square'},
                content: `
                <div class="bg-white px-6 py-24 sm:py-32 lg:px-8">
  <div class="mx-auto max-w-2xl text-center">
    <h2 class="text-4xl font-bold tracking-tight text-gray-900 sm:text-6xl">Support center</h2>
    <p class="mt-6 text-lg leading-8 text-gray-600">Anim aute id magna aliqua ad ad non deserunt sunt. Qui irure qui lorem cupidatat commodo. Elit sunt amet fugiat veniam occaecat fugiat aliqua.</p>
  </div>
</div>`,
            });

            //////
            //////
            //////
            //////
            //////
            editor.BlockManager.add('paragraph', {
                label: 'Paragraph',
                attributes: {class:'fa fa-paragraph'},
                content: '<p>This is a paragraph.</p>',
                category: 'Basic',
            });


            editor.BlockManager.add('list', {
                label: 'List',
                attributes: {class: 'fa fa-list'},
                content: '<ul><li>List item 1</li><li>List item 2</li></ul>',
                category: 'Basic',
            });

            // open editor on selected
            editor.on('component:selected', () => {
                const openSmBtn = editor.Panels.getButton('views', 'open-sm');
                openSmBtn.set('active', 1);
            });

            function removeDuplicateCSS(css) {
                const cssRules = css.split('}');
                const uniqueRules = new Set();

                cssRules.forEach(rule => {
                    const trimmedRule = rule.trim();
                    if (trimmedRule) {
                        uniqueRules.add(trimmedRule + '}'); // Επαναφορά του '}'
                    }
                });

                return Array.from(uniqueRules).join('\n');
            }

            function replaceMenuContent(componentHtml, newContent) {
                // Create a DOMParser instance to parse the HTML string
                const parser = new DOMParser();
                const doc = parser.parseFromString(componentHtml, 'text/html');

                // Find the div with the data-renderedmenu attribute
                const targetDiv = doc.querySelector('div[data-renderedmenu]');

                if (targetDiv) {

                    // Replace the target div with the new content
                    targetDiv.outerHTML = replacement;

                    // Serialize the updated document back to an HTML string
                    const serializer = new XMLSerializer();

                    return serializer.serializeToString(doc);
                }


                // Return original HTML if target div is not found
                return componentHtml;
            }

            // save content
            function saveContent() {
                // Λήψη των components ως JSON
                const rawCss = editor.getCss();
                const cleanedCss = removeDuplicateCSS(rawCss);

                const jsonContent = editor.getHtml();
                const cssContent = cleanedCss;
                const jsContent = editor.getJs();
                let components = editor.getComponents();
                let style = editor.getStyle()

                let templateData = {
                    components: components,
                    style: style
                };

                const csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
                // Παράδειγμα αποστολής μέσω AJAX
                fetch('/manage/theme/{{$theme->id}}/component/{{$component->id}}/save-component', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                    },
                    body: JSON.stringify({
                        css: cssContent,
                        js: jsContent,
                        html: jsonContent,
                        template: JSON.stringify(templateData)

                    }),
                })
                    .then(response => response.json())
                    .then(data => console.log('Success:', data))
                    .catch((error) => console.error('Error:', error));
            }

            // Παράδειγμα κλήσης της συνάρτησης αποθήκευσης
            document.getElementById('save-button').addEventListener('click', saveContent);


        </script>
        <style>
            /*.panel__top {*/
            /*    padding: 0;*/
            /*    width: 100%;*/
            /*    display: block;*/
            /*    position: initial;*/
            /*    justify-content: center;*/
            /*    justify-content: space-between;*/
            /*}*/
            .gjs-pn-panel {
                padding: 0;
            }

            button#save-button svg {
                display: inline;
            }

            button#save-button {
                display: inline-block;
                background: #3e9c96;
                padding: 5px;
                margin-bottom: 10px;
                color: #fff;
                border-radius: 4px;
                box-shadow: 0 0 4px #131111;
                margin-left: 17px;
                width: 224px;
                text-align: center;
                border: solid 1px #317d78;
            }


            .change-theme-button:focus {
                /* background-color: yellow; */
                outline: none;
                box-shadow: 0 0 0 2pt #c5c5c575;
            }

            /*div#pages {*/
            /*    background: #444444;*/
            /*}*/
            div#layers {
                background: #1f2937;
            }

            .gjs-one-bg {
                background-color: #202937;
            }

            /* Fit icons properly */
            .gjs-block {
                /*width: 28%;*/
            }


            .gjs-radio-item input:checked + .gjs-radio-item-label {
                background-color: #000;
            }

            .gjs-sm-sector .gjs-sm-stack #gjs-sm-add, .gjs-clm-tags .gjs-sm-stack #gjs-sm-add {
                color: #000;
            }

            select#componentSelect {
                padding: 1px 5px;
                width: 100%;
                font-weight: 200;
            }


            .gjs-toolbar-items {
                display: flex;
                align-items: center;
            }

            .fa.fa-save.gjs-toolbar-item {
                font-size: 16px;
            }
        </style>

@endsection
