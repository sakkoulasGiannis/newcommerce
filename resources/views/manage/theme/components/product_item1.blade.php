<!--
title: Product item 1
type: product-list
-->
<div class="group relative editable">
    <div
        class="aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-md bg-gray-200 lg:aspect-none group-hover:opacity-75 lg:h-80">
        <!--productFirstImage-->
        <img data-gjs-locked="true"
             src="https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg"
             alt="Front of men&#039;s Basic Tee in black."
             class="h-full w-full object-cover object-center lg:h-full lg:w-full">
        <!--/productFirstImage-->
    </div>
    <div class="mt-4 flex justify-between">
        <div>
            <h3 class="text-sm text-gray-700" data-product-title>
                <!--productName-->
                <a data-product-name href="">
                    <span data-product-title>Product Title </span>
                </a>
            </h3>
            <p class="mt-1 text-sm text-gray-500" data-product-color>
                <span>Black</span>
            </p>
        </div>
        <p class="text-sm font-medium text-gray-900" data-product-price>

            <div data-product-price="">$25</div>

        </p>
    </div>
</div>
