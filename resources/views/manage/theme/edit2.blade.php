<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Theme Builder</title>
    <script src="https://cdn.tailwindcss.com"></script>
    <script defer src="https://cdn.jsdelivr.net/npm/alpinejs@3.x.x/dist/cdn.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css" rel="stylesheet">
    <link href="https://unpkg.com/grapesjs/dist/css/grapes.min.css" rel="stylesheet">
    <script src="https://unpkg.com/grapesjs"></script>
    <script src="https://unpkg.com/grapesjs-tailwind"></script>

    <style>
        .draggable {
            padding: 10px;
            border: 1px solid #ccc;
            margin: 10px;
            cursor: move;
        }

        .dropzone {
            padding: 20px;
            border: 2px dashed #ccc;
            min-height: 100px;
            margin-top: 20px;
        }

        .editable {
            border: 1px solid #ddd;
            padding: 10px;
            margin-top: 10px;
        }

        .hidden {
            display: none;
        }

        .editor-container {
            border: 1px solid #ddd;
            padding: 10px;
            margin-top: 10px;
            height: 500px;
            overflow: auto;
        }
    </style>
</head>
<body>


<!--
  This example requires updating your template:

  ```
  <html class="h-full bg-white">
  <body class="h-full">
  ```
-->
<div class="flex min-h-full w-full flex-col">
    <!-- 3 column wrapper -->
    <div class="  w-full   grow   xl:px-2">
        <!-- Left sidebar & main wrapper -->
        <div class="grid grid-cols-12">
            <div
                class="col-span-3 border-b border-gray-200 px-4 py-6 sm:px-6 lg:pl-8 xl:w-64 xl:shrink-0 xl:border-b-0 xl:border-r xl:pl-6">
                <ul>
                    <li><a href="/manage/theme/{{$theme->id}}/edit?type=header">header</a></li>
                    <li><a href="/manage/theme/{{$theme->id}}/edit?type=home-page">Home Page</a></li>
                    <li><a href="/manage/theme/{{$theme->id}}/edit?type=product-page">product page</a></li>
                    <li><a href="/manage/theme/{{$theme->id}}/edit?type=contact-page">contact page</a></li>
                    <li><a href="/manage/theme/{{$theme->id}}/edit?type=category-page">category page</a></li>
                    <li><a href="/manage/theme/{{$theme->id}}/edit?type=filters">filters</a></li>
                </ul>
            </div>

            <div class="col-span-9 px-4 py-6 sm:px-6 lg:pl-8 xl:flex-1 xl:pl-6">
                <div id="themeBuilder" class="grid grid-cols-2">
                    <div>


                        {{--    @dd(request()->get('type'))--}}

                        @switch(request()->get('type'))
                            @case('header')
                                @include('manage.theme.header')
                                @break
                            @case('home-page')
                                @include('manage.theme.home-page')
                                @break
                            @case('product-page')
                                @php
                                    $files = [
                                        ['title'=>'Item 1', 'name' => 'product_item1', 'directory' => 'components/product_items', 'content'=>null,'type'=>'product' , 'view'=>'product_list'],
                                        ['title'=>'Item 2','name' => 'product_item2', 'directory' => 'components/product_items', 'content'=>null,'type'=>'product', 'view'=>'product_list'],
                                        ['title'=>'Item 3','name' => 'product', 'directory' => 'components/product_page', 'content'=>null,'type'=>'product', 'view'=>'product_list'],
                                        ['title'=>'Grid 2', 'name'=>'grid_2', 'directory'=>'components/grids', 'content'=>null, 'type'=>'grid', 'view'=>'grid_view'],

                                    ];
                                @endphp
                                @break
                            @case('contact-page')
                                @include('manage.theme.contact-page')
                                @break
                            @case('category-page')
                                @include('manage.theme.category-page')
                                @break
                            @case('filters')
                                @include('manage.theme.filters')
                                @break
                            @default
                                <h2>No Blade File Selected</h2>
                                @php($files = [])
                        @endswitch

                        <div>
                            <h2>Available Blade Files</h2>
                            <div id="fileList">
                            </div>
                        </div>

                    </div>
                    <div>



                        <div>
                            <button id="prevItemButton">Previous Item</button>
                            <button id="nextItemButton">Next Item</button>
                        </div>

                        <div id="dropzone" class="dropzone">
                            <h2>Drop Blade Files Here</h2>
                            <button id="saveComponentButton">Save Component</button>

                            <div id="droppedFiles"></div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="editModal" class="editable hidden fixed top-0 w-full bg-white">

    <h3>Edit Content</h3>
    <div class="traits-container"></div>

    <div id="editor" class="editor-container" style="height: 600px;!important;"></div>
    <button id="openAssetManager">Open Asset Manager</button>

    <button id="saveButton">Save</button>

    <button id="cancelButton">Cancel</button>
</div>
############################
############################
############################
############################
<div>





    <!-- Edit Modal -->

</div>

<script>
    document.addEventListener('DOMContentLoaded', () => {
        const files = @json($files);
        const droppedFiles = [];
        let isEditing = false;
        let currentFile = null;
        let editorInstance = null;
        let currentIndex = 0;

        const fileList = document.getElementById('fileList');
        const dropzone = document.getElementById('dropzone');
        const droppedFilesContainer = document.getElementById('droppedFiles');
        const editModal = document.getElementById('editModal');
        const saveButton = document.getElementById('saveButton');
        const saveComponentButton = document.getElementById('saveComponentButton');
        const cancelButton = document.getElementById('cancelButton');
        const prevItemButton = document.getElementById('prevItemButton');
        const nextItemButton = document.getElementById('nextItemButton');

        function renderFileList() {
            fileList.innerHTML = files.map((file, index) => `
            <div class="draggable" draggable="true" data-file='${JSON.stringify(file)}'>
                <span>${file.title}</span>
            </div>
        `).join('');

            // Add event listeners for dragstart
            document.querySelectorAll('.draggable').forEach(item => {
                item.addEventListener('dragstart', handleDragStart);
            });
        }

        function renderDroppedFiles() {
            if (droppedFiles.length > 0) {
                droppedFilesContainer.innerHTML = `
                <div>
                    <button onclick="edit(0)">Edit</button>
                    <button onclick="removeDroppedFile(0)">Remove</button>
                    <div class="draggable">
                        <div id="custom-editor">
                            <h3>${droppedFiles[0].title}</h3>
                            <div>${droppedFiles[0].content}</div>
                        </div>
                    </div>
                </div>
            `;
            } else {
                droppedFilesContainer.innerHTML = '<p>No items dropped</p>';
            }
        }

        function initEditor() {
            if (editorInstance) {
                editorInstance.destroy();
            }

            editorInstance = grapesjs.init({
                container: '#editor',
                fromElement: true,
                height: '90vh',
                width: 'auto',
                selectorManager: {escapeName: (name) => `${name}`.trim().replace(/([^a-z0-9\w-:/]+)/gi, '-')},
                // plugins: ['grapesjs-tailwind'],
                canvas: {
                    styles: ['https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css', '/css/editor.css']
                },
                storageManager: {
                    type: 'remote',
                    stepsBeforeSave: 1,
                    urlStore: '/manage/save-content',
                    urlLoad: '/manage/load-content',
                    params: {},
                },
                traitManager: {
                    appendTo: '.traits-container',
                },
                assetManager: {
                    upload: {
                        url: '/upload',
                        headers: {
                            'Authorization': 'Bearer your-token',
                        },
                        uploadHandler: function (files) {
                            const formData = new FormData();
                            files.forEach(file => formData.append('files[]', file));
                            return fetch('/upload-image', {
                                method: 'POST',
                                body: formData,
                            }).then(response => response.json());
                        }
                    },
                    uploadText: 'Upload Images',
                    assets: []
                },

                styleManager: {
                    sectors: [
                        {
                            name: 'First sector',
                            properties: [
                                {
                                    // Default options
                                    // id: 'padding', // The id of the property, if missing, will be the same as `property` value
                                    type: 'number',
                                    label: 'Padding', // Label for the property
                                    property: 'padding', // CSS property to change
                                    default: '0', // Default value to display
                                    // Additonal `number` options
                                    units: ['px', '%'], // Units (available only for the 'number' type)
                                    min: 0, // Min value (available only for the 'number' type)
                                },
                                {
                                    type: 'select',
                                    label: 'Grid Columns',
                                    property: 'grid-cols',
                                    default: 'grid-cols-1',

                                    options: [
                                        {id: 'grid-cols-2', label: 'small'},
                                        {id: 'grid-cols-3', label: 'medium'},
                                        {id: 'grid-cols-4', label: 'large'},
                                    ], change: function (property, value, opt) {
                                        const selectedComponent = opt.target;
                                        const classes = selectedComponent.get('classes').models.map(model => model.id);

                                        const updatedClasses = classes.filter(cls => !cls.startsWith('grid-cols-'));
                                        updatedClasses.push(value);

                                        selectedComponent.setClass(updatedClasses.join(' '));

                                    },
                                }
                            ],
                        },
                    ]
                },
            });
            editorInstance.on('component:add', (model) => {
                console.log('Component added', model);
                if (model.get('type') === 'grid' && model.getClasses().includes('droppable-area')) {
                    console.log('Droppable area added');
                    const defaultClass = 'grid-cols-1';
                    const currentClasses = model.getClasses();
                    console.log(currentClasses);
                    if (!currentClasses.includes(defaultClass)) {
                        console.log('Adding default class');
                        model.addClass(defaultClass);
                    }
                }
            });
            // Προσθήκη custom component τύπου


            editorInstance.BlockManager.add('grid-2', {
                type: 'grid',
                label: 'Grid 2 columns',
                content: `<div class="grid grid-cols-1  gap-4 droppable-area p-2"></div>`,
                category: 'Grid',
                droppable: true,
                attributes: { // Default attributes
                    type: 'grid',
                    name: 'default-name',
                    placeholder: 'Insert text here',
                },
                traits: [
                    'name',
                    'placeholder',
                    {type: 'checkbox', name: 'required'},
                ],
            });


            editorInstance.BlockManager.add('bootstrap-button', {
                label: 'Button',
                content: '<button type="button" class="rounded bg-indigo-600 px-2 py-1 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">Button text</button>',
                category: 'Buttons',
            });

            editorInstance.BlockManager.add('container-1', {
                label: 'Full Container',
                content: '<div class="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8 droppable-area h-10 min-h-10 droppable-area p-2"></div>',
                category: 'Containers',
                droppable: true,
            });
            editorInstance.BlockManager.add('media-1', {
                label: 'Media Object 1',
                content: `<div class="flex">
                  <div class="mr-4 flex-shrink-0">
                    <svg class="h-full w-16 border border-gray-300 bg-white text-gray-300" preserveAspectRatio="none" stroke="currentColor" fill="none" viewBox="0 0 200 200" aria-hidden="true">
                      <path vector-effect="non-scaling-stroke" stroke-width="1" d="M0 0l200 200M0 200L200 0" />
                    </svg>
                  </div>
                  <div>
                    <h4 class="text-lg font-bold">Lorem ipsum</h4>
                    <p class="mt-1">Repudiandae sint consequuntur vel. Amet ut nobis explicabo numquam expedita quia omnis voluptatem. Minus quidem ipsam quia iusto.</p>
                  </div>
                </div>
                `,
                category: 'Medias',
                droppable: true,

            });


            document.getElementById('openAssetManager').addEventListener('click', () => {
                editorInstance.AssetManager.open();
            });

            if (currentFile && currentFile.content) {
                editorInstance.setComponents(currentFile.content);
            }
        }

        function saveComponent() {
            console.log('saveComponent');
            console.log(currentFile.content);

            fetch('/manage/theme/save-component', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    content: currentFile.content,
                    name: currentFile.name,
                    theme: '{{$theme->id}}',
                    type: currentFile.type,
                    styles: currentFile.styles,
                }),
            })
                .then(response => {
                    if (!response.ok) {
                        throw new Error('Network response was not ok');
                    }
                    return response.json();
                })
                .then(data => {
                    console.log('Success:', data);
                })
                .catch(error => {
                    console.error('Error:', error);
                });
        }

        function save() {
            isEditing = false;
            if (editorInstance) {
                const fullHtml = editorInstance.getHtml();
                const parser = new DOMParser();
                const doc = parser.parseFromString(fullHtml, 'text/html');
                const editableDiv = doc.querySelector('.editable');

                if (editableDiv) {
                    currentFile.content = editableDiv.outerHTML;
                } else {
                    console.warn('.editable div not found');
                    currentFile.content = '';
                }

                currentFile.styles = editorInstance.getCss();
                console.log(currentFile.styles);

                editorInstance.store();
                editorInstance.destroy();
                editorInstance = null;
            }
            editModal.classList.add('hidden');
            renderDroppedFiles();
        }

        function cancel() {
            isEditing = false;
            currentFile = null;
            if (editorInstance) {
                editorInstance.destroy();
                editorInstance = null;
            }
            editModal.classList.add('hidden');
        }

        window.edit = function (index) {
            currentFile = droppedFiles[index];
            isEditing = true;
            initEditor();
            editModal.classList.remove('hidden');
        }

        window.removeDroppedFile = function (index) {
            droppedFiles.splice(index, 1);
            renderDroppedFiles();
        }

        function handleDragStart(event) {
            event.dataTransfer.setData('text/json', event.target.dataset.file);
        }

        function handleDragStartDropped(event) {
            event.dataTransfer.setData('text/plain', event.target.dataset.index);
        }

        function handleDrop(event) {
            event.preventDefault();
            if (droppedFiles.length > 0) {
                return; // Only one item allowed in dropzone
            }
            const file = JSON.parse(event.dataTransfer.getData('text/json'));
            if (!droppedFiles.some(f => f.name === file.name)) {
                fetch(`/manage/theme/blade-content/${file.name}/?type=${file.type}`)
                    .then(response => {
                        if (!response.ok) {
                            throw new Error('Network response was not ok');
                        }
                        return response.json();
                    })
                    .then(data => {
                        file.content = data.content;
                        droppedFiles.push(file);
                        renderDroppedFiles();
                    })
                    .catch(error => console.error('Error fetching blade content:', error));
            }
        }

        function handleDragOver(event) {
            event.preventDefault();
        }

        function showPreviousItem() {
            if (currentIndex > 0) {
                currentIndex--;
                loadCurrentFile();
            }
        }

        function showNextItem() {
            if (currentIndex < files.length - 1) {
                currentIndex++;
                loadCurrentFile();
            }
        }

        function loadCurrentFile() {
            if (droppedFiles.length > 0) {
                droppedFiles.pop();
            }
            const file = files[currentIndex];
            fetch(`/manage/theme/blade-content/${file.name}/?type=${file.type}`)
                .then(response => {
                    if (!response.ok) {
                        throw new Error('Network response was not ok');
                    }
                    return response.json();
                })
                .then(data => {
                    file.content = data.content;
                    droppedFiles.push(file);
                    renderDroppedFiles();
                })
                .catch(error => console.error('Error fetching blade content:', error));
        }

        // Event Listeners
        dropzone.addEventListener('dragover', handleDragOver);
        dropzone.addEventListener('drop', handleDrop);

        saveButton.addEventListener('click', save);
        saveComponentButton.addEventListener('click', saveComponent);
        cancelButton.addEventListener('click', cancel);

        prevItemButton.addEventListener('click', showPreviousItem);
        nextItemButton.addEventListener('click', showNextItem);

        renderFileList();
        loadCurrentFile(); // Load the initial file
    });
</script>
</body>
</html>
