<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sortable JS with Dynamic Nesting and Actions</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Sortable/1.15.0/Sortable.min.js"></script>
    <script src="https://cdn.tailwindcss.com"></script>

</head>
<body>
<div class="grid grid-cols-12">
    <div class="col-span-3">
        <h3>List 1 (Source)</h3>
        <div id="list1" class="sortable-list"></div>
    </div>

    <div class="col-span-9">
        <h3>List 2 (Destination)</h3>
        <div id="list2" class="sortable-list"></div>
    </div>
</div>

<script>
    let fetchedData = [];

    // Function to create HTML elements recursively from JSON structure
    function renderJsonToHtml(jsonObject, isRoot = true) {
        const element = document.createElement(jsonObject.tag);

        // Set class, style, and other attributes
        if (jsonObject.class) element.className = jsonObject.class;
        if (jsonObject.style) element.style = jsonObject.style;
        if (jsonObject.attributes) {
            for (const attr in jsonObject.attributes) {
                element.setAttribute(attr, jsonObject.attributes[attr]);
            }
        }

        // Set the value if it's a text node
        if (jsonObject.value) {
            element.innerHTML = jsonObject.value;
        }

        // Add Edit and Delete buttons
        const actionContainer = document.createElement('div');
        actionContainer.className = 'action-container';
        const editButton = document.createElement('button');
        editButton.innerText = 'Edit';
        editButton.className = 'edit-btn';
        editButton.onclick = () => editElement(jsonObject, element);
        const deleteButton = document.createElement('button');
        deleteButton.innerText = 'Delete';
        deleteButton.className = 'delete-btn';
        deleteButton.onclick = () => deleteElement(element);
        actionContainer.appendChild(editButton);
        actionContainer.appendChild(deleteButton);

        // Add a drag handle
        if(!isRoot) {

            const dragHandle = document.createElement('div');
            dragHandle.className = 'drag-handle';
            dragHandle.innerText = '☰';  // Example drag handle icon
            element.appendChild(dragHandle);
        }
        // Append the action container and handle
        element.appendChild(actionContainer);

        // Recursively render children
        if (jsonObject.children && jsonObject.children.length > 0) {

            jsonObject.children.forEach(child => {

                const childElement = renderJsonToHtml(child, false);
                element.appendChild(childElement);
            });
        }

        return element;
    }

    // Function to create a simple list item for list1
    function createListFromData(data, parentElement) {
        data.forEach((item, index) => {
            const listItem = document.createElement('div');
            listItem.classList.add('draggable', 'list-item');
            listItem.setAttribute('data-id', index); // Use index to reference the item in the fetchedData array
            listItem.innerText = item.title || `Item ${index + 1}`; // Use title or a default name
            parentElement.appendChild(listItem);
        });
    }

    // Function to initialize Sortable.js on a given list
    function initializeSortable(listElement) {
        Sortable.create(listElement, {
            group: {
                name: 'shared',
                pull: 'clone',  // Allow copying to list2
                put: true       // Allow items to be put into this list
            },
            animation: 150,
            handle: '.draggable', // Only make .draggable items sortable
            fallbackOnBody: true,
            swapThreshold: 0.65,
            onAdd: function(evt) {
                const itemEl = evt.item;
                const itemId = itemEl.getAttribute('data-id'); // Get the ID (index) of the item

                // Find the corresponding JSON object in the fetchedData array
                const jsonData = fetchedData[itemId];

                // Render HTML from the JSON object when item is added to list2
                const renderedHtml = renderJsonToHtml(jsonData, true);

                // Replace the placeholder element with rendered HTML
                itemEl.replaceWith(renderedHtml);

                // Reinitialize nested sortables after new item is added
                initializeNestedSortables(document.getElementById('list2'));
            }
        });
    }

    // Function to initialize Sortable.js on all nested lists
    function initializeNestedSortables(parentElement) {
        const nestedLists = parentElement.querySelectorAll('.nested-list');
        nestedLists.forEach(nestedList => {
            Sortable.create(nestedList, {
                group: 'shared',
                animation: 150,
                fallbackOnBody: true,
                swapThreshold: 0.65,
                handle: '.draggable',
            });
        });
    }

    // Edit Element function
    function editElement(jsonObject, element) {
        const newValue = prompt('Edit Value:', jsonObject.value || '');
        if (newValue !== null) {
            jsonObject.value = newValue;
            const newElement = renderJsonToHtml(jsonObject, true);
            element.replaceWith(newElement);
        }
    }

    // Delete Element function
    function deleteElement(element) {
        element.remove();
    }

    // When the DOM is loaded, create the list from data and initialize Sortable.js
    document.addEventListener('DOMContentLoaded', function () {
        fetch('/manage/theme-components')
            .then(response => response.json())
            .then(data => {
                fetchedData = data; // Store the fetched data for reference when dragging items

                const list1Element = document.getElementById('list1');

                // Create the list from the fetched data
                createListFromData(fetchedData, list1Element);

                // Initialize Sortable for list1 and its nested lists
                initializeSortable(list1Element);
                initializeNestedSortables(list1Element);
            })
            .catch(error => {
                console.error('Error fetching data:', error);
            });

        // Initialize Sortable for list2 (Destination)
        Sortable.create(document.getElementById('list2'), {
            group: 'shared',
            animation: 150,
            fallbackOnBody: true,
            swapThreshold: 0.65,
            handle: '.drag-handle',
            onAdd: function(evt) {
                const itemEl = evt.item;
                const itemId = itemEl.getAttribute('data-id');

                // Find the corresponding JSON object in the fetchedData array
                const jsonData = fetchedData[itemId];

                // Render HTML from the JSON object when item is added to list2
                const renderedHtml = renderJsonToHtml(jsonData, true);

                // Replace the placeholder element with rendered HTML
                itemEl.replaceWith(renderedHtml);

                // Reinitialize nested sortables after new item is added
                initializeNestedSortables(document.getElementById('list2'));
            }
        });
    });
</script>

<style>
    .list-container {
        display: inline-block;
        vertical-align: top;
        margin-right: 20px;
    }
    .sortable-list {
        /*border: 1px solid #ccc;*/
        /*padding: 10px;*/
        /*width: 300px;*/
        /*min-height: 100px;*/
    }
    .draggable {
        padding: 8px;
        margin: 4px;
        background-color: #f4f4f4;
        border: 1px solid #ccc;
        cursor: move;
    }
    .nested-list {
        padding-left: 20px;
        margin-top: 10px;
        border-left: 1px solid #ccc;
    }
    .action-container {
        display: none;
        margin-top: 10px;
    }
    .edit-btn, .delete-btn {
        margin-right: 10px;
        padding: 5px;
    }
</style>
</body>
</html>
