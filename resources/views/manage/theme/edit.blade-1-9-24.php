@extends('layouts.themeEditor')
@section('content')
    <!-- Το πεδίο όπου θα εμφανιστεί ο GrapesJS Editor -->
    <div class="layers-container">

        <div id="container" class="flex flex-row">


            {{--       <div class="panel__top">--}}
            {{--           <div class="panel__basic-actions"></div>--}}
            {{--       </div>--}}

            <div id="layers" class="column" style="flex-basis: 300px;">
                <div class="p-4">
                    <h2 class="text-white font-black text-lg border-b py-2">Pages</h2>
                    <select class="componentList" id="componentSelect"
                            class="mt-2 block w-full rounded-md border-0 py-1.5 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6">
                        @foreach($components as $c)
                            <option
                                {{($c->id == $component->id)?'selected':null}}  value="/manage/theme/{{$theme->id}}/component/{{$c->id}}"
                                class="text-white {{($c->id == $component->id)?'text-black':null }}">
                                {{$c->page_type}}
                            </option>
                        @endforeach
                    </select>

                    <script>
                        // Επιλογή του dropdown στοιχείου
                        document.getElementById('componentSelect').addEventListener('change', function () {
                            // Λήψη της επιλεγμένης τιμής (URL)
                            var selectedUrl = this.value;

                            // Ανακατεύθυνση στο επιλεγμένο URL
                            window.location.href = selectedUrl;
                        });
                    </script>

                </div>
                <button id="save-button" class="text-right">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                         stroke="currentColor" class="w-h h-6 text-white">
                        <path stroke-linecap="round" stroke-linejoin="round"
                              d="M20.25 6.375c0 2.278-3.694 4.125-8.25 4.125S3.75 8.653 3.75 6.375m16.5 0c0-2.278-3.694-4.125-8.25-4.125S3.75 4.097 3.75 6.375m16.5 0v11.25c0 2.278-3.694 4.125-8.25 4.125s-8.25-1.847-8.25-4.125V6.375m16.5 0v3.75m-16.5-3.75v3.75m16.5 0v3.75C20.25 16.153 16.556 18 12 18s-8.25-1.847-8.25-4.125v-3.75m16.5 0c0 2.278-3.694 4.125-8.25 4.125s-8.25-1.847-8.25-4.125"/>
                    </svg>
Αποθήκευση
                </button>
<div class="panel__global-settings"> </div>
                <div class=" " id="blocks"></div>
                <div id="layers-container"></div>
            </div>
            <div class=" " id="gjs">


            </div>
            {{--            <div id="styles-container"></div>--}}


        </div>

        <!-- GrapesJS JavaScript -->
        <script>
            const escapeName = (name) => `${name}`.trim().replace(/([^a-z0-9\w-:/]+)/gi, '-');

            // Αρχικοποίηση του GrapesJS Editor
            const editor = grapesjs.init({
                    layerManager: {
                        appendTo: "#layers-container"
                    },


                    pluginsOpts: {
                        '@silexlabs/grapesjs-symbols':
                            {
                                appendTo: '.gjs-pn-views-container',
                            },
                    },
                    styleManager: {
                        sectors: [

                            {
                                name: 'Flexbox',
                                open: false,
                                buildProps: ['flex-direction', 'flex-wrap', 'justify-content', 'align-items', 'align-content', 'order', 'flex-basis', 'flex-grow', 'flex-shrink', 'align-self', 'gap', 'column'],
                                properties: [
                                    {
                                        name: 'Flex Direction',
                                        property: 'flex-direction',
                                        type: 'select',
                                        defaults: 'row',
                                        list: [
                                            {value: 'row', name: 'Row'},
                                            {value: 'row-reverse', name: 'Row Reverse'},
                                            {value: 'column', name: 'Column'},
                                            {value: 'column-reverse', name: 'Column Reverse'},
                                        ],
                                    },
                                    {
                                        name: 'Gap',
                                        property: 'gap',
                                        type: 'slider',
                                        units: ['px', 'em', '%'],
                                        defaults: '0',
                                        min: 0,
                                    },
                                ]
                            },
                            {
                                name: 'General',
                                buildProps: ['float', 'display', 'position', 'top', 'right', 'left', 'bottom'],
                                properties: [
                                    {
                                        name: 'Alignment',
                                        property: 'float',
                                        type: 'radio',
                                        defaults: 'none',
                                        list: [
                                            {value: 'none', className: 'fa fa-times'},
                                            {value: 'left', className: 'fa fa-align-left'},
                                            {value: 'right', className: 'fa fa-align-right'}
                                        ],
                                    },
                                    {property: 'position', type: 'select'}
                                ],
                            },
                            {
                                name: 'Dimension',
                                open: false,
                                buildProps: ['width', 'flex-width', 'height', 'max-width', 'min-height', 'margin', 'padding'],
                                properties: [
                                    {
                                        id: 'flex-width',
                                        type: 'integer',
                                        name: 'Width',
                                        units: ['px', '%'],
                                        property: 'flex-basis',
                                        toRequire: 1,
                                    },
                                    {
                                        property: 'margin',
                                        properties: [
                                            {name: 'Top', property: 'margin-top'},
                                            {name: 'Right', property: 'margin-right'},
                                            {name: 'Bottom', property: 'margin-bottom'},
                                            {name: 'Left', property: 'margin-left'}
                                        ],
                                    },
                                    {
                                        property: 'padding',
                                        properties: [
                                            {name: 'Top', property: 'padding-top'},
                                            {name: 'Right', property: 'padding-right'},
                                            {name: 'Bottom', property: 'padding-bottom'},
                                            {name: 'Left', property: 'padding-left'}
                                        ],
                                    }
                                ],
                            },
                            {
                                name: 'Typography',
                                open: false,
                                buildProps: ['font-family', 'font-size', 'font-weight', 'letter-spacing', 'color', 'line-height', 'text-align', 'text-decoration', 'text-shadow'],
                                properties: [
                                    {
                                        name: 'Font', property: 'font-family'
                                    },
                                    {
                                        name: 'Weight',
                                        property: 'font-weight'
                                    },
                                    {
                                        name: 'Font color',
                                        property: 'color'
                                    },
                                    {
                                        property: 'text-align',
                                        type: 'radio',
                                        defaults: 'left',
                                        list: [
                                            {value: 'left', name: 'Left', className: 'fa fa-align-left'},
                                            {value: 'center', name: 'Center', className: 'fa fa-align-center'},
                                            {value: 'right', name: 'Right', className: 'fa fa-align-right'},
                                            {value: 'justify', name: 'Justify', className: 'fa fa-align-justify'}
                                        ],
                                    },
                                    {
                                        property: 'text-decoration',
                                        type: 'radio',
                                        defaults: 'none',
                                        list: [
                                            {value: 'none', name: 'None', className: 'fa fa-times'},
                                            {value: 'underline', name: 'underline', className: 'fa fa-underline'},
                                            {value: 'line-through', name: 'Line-through', className: 'fa fa-strikethrough'}
                                        ],
                                    },
                                    {
                                        property: 'text-shadow',
                                        properties: [
                                            {name: 'X position', property: 'text-shadow-h'},
                                            {name: 'Y position', property: 'text-shadow-v'},
                                            {name: 'Blur', property: 'text-shadow-blur'},
                                            {name: 'Color', property: 'text-shadow-color'}
                                        ],
                                    }
                                ],
                            },
                            {
                                name: 'Decorations',
                                open: false,
                                buildProps: ['opacity', 'border-radius', 'border', 'box-shadow', 'background'],
                                properties: [
                                    {
                                        type: 'slider',
                                        property: 'opacity',
                                        defaults: 1,
                                        step: 0.01,
                                        max: 1,
                                        min: 0,
                                    },
                                    {
                                        property: 'border-radius',
                                        properties: [
                                            {name: 'Top', property: 'border-top-left-radius'},
                                            {name: 'Right', property: 'border-top-right-radius'},
                                            {name: 'Bottom', property: 'border-bottom-left-radius'},
                                            {name: 'Left', property: 'border-bottom-right-radius'}
                                        ],
                                    },
                                    {
                                        property: 'box-shadow',
                                        properties: [
                                            {name: 'X position', property: 'box-shadow-h'},
                                            {name: 'Y position', property: 'box-shadow-v'},
                                            {name: 'Blur', property: 'box-shadow-blur'},
                                            {name: 'Spread', property: 'box-shadow-spread'},
                                            {name: 'Color', property: 'box-shadow-color'},
                                            {name: 'Shadow type', property: 'box-shadow-type'}
                                        ],
                                    },
                                ],
                            },
                            {
                                name: 'Extra',
                                open: false,
                                buildProps: ['transition', 'perspective', 'transform'],
                                properties: [
                                    {
                                        property: 'transition',
                                        properties: [
                                            {name: 'Property', property: 'transition-property'},
                                            {name: 'Duration', property: 'transition-duration'},
                                            {name: 'Easing', property: 'transition-timing-function'}
                                        ],
                                    },
                                    {
                                        property: 'transform',
                                        properties: [
                                            {name: 'Rotate X', property: 'transform-rotate-x'},
                                            {name: 'Rotate Y', property: 'transform-rotate-y'},
                                            {name: 'Rotate Z', property: 'transform-rotate-z'},
                                            {name: 'Scale X', property: 'transform-scale-x'},
                                            {name: 'Scale Y', property: 'transform-scale-y'},
                                            {name: 'Scale Z', property: 'transform-scale-z'}
                                        ],
                                    }
                                ]
                            },
                            // {
                            //     name: 'Flex',
                            //     open: false,
                            //     properties: [
                            //         {
                            //             name: 'Flex Container',
                            //             property: 'display',
                            //             type: 'select',
                            //             defaults: 'block',
                            //             list: [
                            //                 {value: 'block', name: 'Disable'},
                            //                 {value: 'flex', name: 'Enable'}
                            //             ],
                            //         },
                            //         {
                            //             name: 'Flex Parent',
                            //             property: 'label-parent-flex',
                            //             type: 'integer',
                            //         },
                            //         {
                            //             name: 'Direction',
                            //             property: 'flex-direction',
                            //             type: 'radio',
                            //             defaults: 'row',
                            //             list: [
                            //                 {
                            //                     value: 'row',
                            //                     name: 'Row',
                            //                     className: 'icons-flex icon-dir-row',
                            //                     title: 'Row',
                            //                 },
                            //                 {
                            //                     value: 'row-reverse',
                            //                     name: 'Row reverse',
                            //                     className: 'icons-flex icon-dir-row-rev',
                            //                     title: 'Row reverse',
                            //                 },
                            //                 {
                            //                     value: 'column',
                            //                     name: 'Column',
                            //                     title: 'Column',
                            //                     className: 'icons-flex icon-dir-col',
                            //                 },
                            //                 {
                            //                     value: 'column-reverse',
                            //                     name: 'Column reverse',
                            //                     title: 'Column reverse',
                            //                     className: 'icons-flex icon-dir-col-rev',
                            //                 }
                            //             ],
                            //         },
                            //         {
                            //             name: 'Justify',
                            //             property: 'justify-content',
                            //             type: 'radio',
                            //             defaults: 'flex-start',
                            //             list: [
                            //                 {
                            //                     value: 'flex-start',
                            //                     className: 'icons-flex icon-just-start',
                            //                     title: 'Start',
                            //                 },
                            //                 {
                            //                     value: 'flex-end',
                            //                     title: 'End',
                            //                     className: 'icons-flex icon-just-end',
                            //                 },
                            //                 {
                            //                     value: 'space-between',
                            //                     title: 'Space between',
                            //                     className: 'icons-flex icon-just-sp-bet',
                            //                 },
                            //                 {
                            //                     value: 'space-around',
                            //                     title: 'Space around',
                            //                     className: 'icons-flex icon-just-sp-ar',
                            //                 },
                            //                 {
                            //                     value: 'center',
                            //                     title: 'Center',
                            //                     className: 'icons-flex icon-just-sp-cent',
                            //                 }
                            //             ],
                            //         },
                            //         {
                            //             name: 'Align',
                            //             property: 'align-items',
                            //             type: 'radio',
                            //             defaults: 'center',
                            //             list: [
                            //                 {
                            //                     value: 'flex-start',
                            //                     title: 'Start',
                            //                     className: 'icons-flex icon-al-start',
                            //                 },
                            //                 {
                            //                     value: 'flex-end',
                            //                     title: 'End',
                            //                     className: 'icons-flex icon-al-end',
                            //                 },
                            //                 {
                            //                     value: 'stretch',
                            //                     title: 'Stretch',
                            //                     className: 'icons-flex icon-al-str',
                            //                 },
                            //                 {
                            //                     value: 'center',
                            //                     title: 'Center',
                            //                     className: 'icons-flex icon-al-center',
                            //                 }
                            //             ],
                            //         },
                            //         {
                            //             name: 'Flex Children',
                            //             property: 'label-parent-flex',
                            //             type: 'integer',
                            //         },
                            //         {
                            //             name: 'Order',
                            //             property: 'order',
                            //             type: 'integer',
                            //             defaults: 0,
                            //             min: 0
                            //         },
                            //         {
                            //             name: 'Flex',
                            //             property: 'flex',
                            //             type: 'composite',
                            //             properties: [
                            //                 {
                            //                     name: 'Grow',
                            //                     property: 'flex-grow',
                            //                     type: 'integer',
                            //                     defaults: 0,
                            //                     min: 0
                            //                 },
                            //                 {
                            //                     name: 'Shrink',
                            //                     property: 'flex-shrink',
                            //                     type: 'integer',
                            //                     defaults: 0,
                            //                     min: 0
                            //                 },
                            //                 {
                            //                     name: 'Basis',
                            //                     property: 'flex-basis',
                            //                     type: 'integer',
                            //                     units: ['px', '%', ''],
                            //                     unit: '',
                            //                     defaults: 'auto',
                            //                 }
                            //             ],
                            //         },
                            //         {
                            //             name: 'Align',
                            //             property: 'align-self',
                            //             type: 'radio',
                            //             defaults: 'auto',
                            //             list: [
                            //                 {
                            //                     value: 'auto',
                            //                     name: 'Auto',
                            //                 },
                            //                 {
                            //                     value: 'flex-start',
                            //                     title: 'Start',
                            //                     className: 'icons-flex icon-al-start',
                            //                 },
                            //                 {
                            //                     value: 'flex-end',
                            //                     title: 'End',
                            //                     className: 'icons-flex icon-al-end',
                            //                 },
                            //                 {
                            //                     value: 'stretch',
                            //                     title: 'Stretch',
                            //                     className: 'icons-flex icon-al-str',
                            //                 },
                            //                 {
                            //                     value: 'center',
                            //                     title: 'Center',
                            //                     className: 'icons-flex icon-al-center',
                            //                 }
                            //             ],
                            //         }
                            //     ]
                            // }
                        ]
                    },
                    protectedCss: '',
                    container: '#gjs',
                    fromElement: true,
                    height: '100vh',
                    width: '100%',
                    commands:
                        {
                            defaults: [
                                window['@truenorthtechnology/grapesjs-code-editor'].codeCommandFactory(),
                            ],
                        },
                    selectorManager: {
                        componentFirst: false,
                    },
                    plugins: ["grapesjs-rte-extensions","global-settings-plugin","save-selected-component-plugin", "save-template", "gjs-blocks-basic", "html-block", "grapesjs-component-code-editor", "grapesjs-plugin-header", "gjs-google-material-icons"],
                    // Disable the storage manager for the moment
                    storageManager:
                        false,
                    assetManager:
                        {
                            // Upload endpoint, set `false` to disable upload, default `false`
                            upload: '/manage/' + {{$theme->id}} + '/theme-images',
                            // The name used in POST to pass uploaded files, default: `'files'`
                            uploadName: 'files',
                        },
                    // panels: { defaults: [] },
                    // Avoid any default paneld
                    canvas: {
                        styles: [
                            'https://unpkg.com/purecss@2.0.6/build/pure-min.css',
                            // 'https://cdn.jsdelivr.net/npm/tailwindcss@latest/dist/tailwind.min.css',
                            '/css/editor.css'
                            , '/css/main.css'
                        ], scripts:
                            [
                                {
                                    src: '/js/editor.js',
                                }
                            ]
                    },
                })
            ;
            editor.Panels.removeButton('options', 'device-select');


            editor.Panels.addPanel({
                id: 'devices-c',
                buttons: [
                    {
                        id: 'set-device-desktop',
                        command: 'set-device-desktop',
                        className: 'fa fa-desktop',
                        attributes: { title: 'Desktop' },
                        active: true,
                    },
                    {
                        id: 'set-device-tablet',
                        command: 'set-device-tablet',
                        className: 'fa fa-tablet',
                        attributes: { title: 'Tablet' },
                    },
                    {
                        id: 'set-device-mobile',
                        command: 'set-device-mobile2',
                        className: 'fa fa-mobile',
                        attributes: { title: 'Mobile' },
                    },

                ]
            });
            editor.Commands.add('set-device-desktop', {
                run(editor, sender) {
                    sender.set('active', true);
                    editor.setDevice('Desktop');
                }
            });

            editor.Commands.add('set-device-tablet', {
                run(editor, sender) {
                    sender.set('active', true);
                    editor.setDevice('Tablet');
                }
            });

            editor.Commands.add('set-device-mobile2', {
                run(editor, sender) {
                    sender.set('active', true);
                    editor.setDevice('Mobile');
                }
            });
            editor.render();
            //custom commands
                // Turn off default devices select and create new one


            componentHtml = @json(($component->html != null)?$component->html:'[]');
            componentCss = @json($component->css);
            componentJs = @json($component->js);
            template = @json(($component->template != null)?$component->template:'{"components":[],"style":[]}');


            // images
            const am = editor.AssetManager;
            const rte = editor.RichTextEditor;

            rte.add('placeholder', {
                name: 'Placeholder',
                order: 1,
                icon: `
                <select>
                    <option selected="true" disabled="disabled" id="defaultPlaceholder">Δυναμικές τιμές</option>
                    <option value="[[product_price]]">Τιμή</option>
                    <option value="[[product_sale_price]]">Εκπτωση</option>
                    <option value="[[lastname]]">Lastname</option>
                </select>
            `,
                event: 'change',
                result: (rte, action) => {
                    rte.insertHTML(action.btn.children[0].selectedOptions[0].value)
                    document.getElementById('defaultPlaceholder').selected = true;
                },
            });
            rte.add('fontAwesomeIconsd', {
                name: 'Font Awesome Icons',
                icon: 'αδσ', // Εικονίδιο που εμφανίζεται στο RTE
                event: 'click',
                result: (rte) => {
                    // Δημιουργία του popup container
                    const modal = editor.Modal;
                    const content = document.createElement('div');
                    const iconsList = [
                        'fa-smile', 'fa-heart', 'fa-star', 'fa-check',
                        'fa-times', 'fa-search', 'fa-user', 'fa-home',
                        'fa-envelope', 'fa-bell', 'fa-camera', 'fa-cog'
                    ];

                    // Προσθήκη των εικονιδίων δυναμικά στο popup
                    iconsList.forEach(iconClass => {
                        const iconElement = document.createElement('i');
                        iconElement.className = `fa ${iconClass}`;
                        iconElement.style.margin = '10px';
                        iconElement.style.cursor = 'pointer';
                        iconElement.onclick = () => {
                            rte.insertHTML(`<i class="fa ${iconClass}"></i>`);
                            modal.close(); // Κλείσιμο του popup μετά την επιλογή
                        };
                        content.appendChild(iconElement);
                    });

                    modal.setTitle('Select Font Awesome Icon');
                    modal.setContent(content);
                    modal.open();
                },
            });


            // custom device manager
            // const deviceManager = editor.Devices;
            // const device1 = deviceManager.add({
            //     // Without an explicit ID, the `name` will be taken. In case of missing `name`, a random ID will be created.
            //     id: 'tablet',
            //     name: 'Tablet',
            //     width: '900px', // This width will be applied on the canvas frame and for the CSS media
            // });


            // Panel should re render again otherwise
            // New button will not be shown on device panel

            // ajax call
            const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

            // Αλλαγή του URL με τη δυναμική τιμή του theme->id

            // URL του API endpoint
            const url = `/manage/{{$theme->id}}/theme-images`;

            // Κάνε το αίτημα GET χρησιμοποιώντας fetch με το CSRF token στον header
            fetch(url, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': csrfToken, // Προσθέτουμε το CSRF token στο header
                },
            })
                .then(response => {
                    if (!response.ok) {
                        throw new Error('Network response was not ok');
                    }
                    am.add(response);
                    return response.json(); // Ανάγνωση της απάντησης ως JSON
                })
                .then(data => {
                    // Διαχείριση των δεδομένων που επιστράφηκαν
                    data.forEach(item => {
                        // Προσθήκη των αντικειμένων στον asset manager
                        am.add(item);
                    });
                })
                .catch(error => {
                    console.error('There has been a problem with your fetch operation:', error);
                });


            // mobile pop up
            let mobileWindow;

            // Προσθήκη του custom κουμπιού στο toolbar
            editor.Panels.addButton('options', {
                id: 'open-mobile-preview',
                className: 'fa fa-mobile',
                label: '',
                command: 'open-mobile-preview',
                attributes: {title: 'Open Mobile Preview'}
            });

            // Προσθήκη της εντολής για το κουμπί
            editor.Commands.add('open-mobile-preview', {
                run(editor, sender) {
                    sender && sender.set('active', 0); // απενεργοποίηση του κουμπιού

                    // Άνοιγμα του παραθύρου μόνο αν δεν υπάρχει ήδη
                    if (!mobileWindow || mobileWindow.closed) {
                        mobileWindow = window.open('', '', 'width=375,height=667');
                    }

                    // Φόρτωση της αρχικής έκδοσης
                    updateMobilePreview();
                }
            });

            // Συνάρτηση για την ενημέρωση του mobile preview
            function updateMobilePreview() {
                const htmlContent = editor.getHtml();
                const cssContent = editor.getCss();

                // Έλεγχος αν το παράθυρο είναι ανοιχτό πριν ενημερώσουμε το περιεχόμενο
                if (mobileWindow && !mobileWindow.closed) {
                    mobileWindow.document.open();
                    mobileWindow.document.write(`
      <html>
        <head>
          <style>
            ${cssContent}
            body {
              margin: 0;
              padding: 0;
              font-family: Arial, sans-serif;
            }
            /* Προσθήκη responsive styling για mobile */
            @media (max-width: 768px) {
              body {
                width: 100%;
              }
            }
          </style>
        </head>
        <body>
          ${htmlContent}
        </body>
      </html>
    `);
                    mobileWindow.document.close();
                }
            }

            // Προσθήκη listener για να ανανεώνει το preview όταν υπάρχει αλλαγή
            editor.on('component:update', updateMobilePreview);

            // Προσθήκη listener και για άλλες αλλαγές (π.χ. drag & drop ή αλλαγές στα στυλ)
            editor.on('canvas:drop', updateMobilePreview);
            editor.on('style:change', updateMobilePreview);
            // Αναγνωρίζουμε τον editor


            editor.on('component:selected', function (component) {
                // Ελέγχουμε αν το component έχει την κλάση 'desktop-item'
                const classes = component.getClasses();
                const hasDesktopItemClass = classes.includes('mega-menu-nav-item');
                if (hasDesktopItemClass) {

                    // Βρίσκουμε το κοντινότερο mega-box
                    let megaBox = null;
                    let parent = component.parent();

                    // Αναζητούμε το mega-box από τα γονικά στοιχεία
                    while (parent) {
                        const megaBoxElements = parent.getEl().querySelectorAll('.mega-box');
                        console.log(megaBoxElements)
                        if (megaBoxElements.length > 0) {
                            megaBox = megaBoxElements[0];
                            console.log('break')
                            break;
                        }
                        parent = parent.parent();
                    }

                    if (megaBox) {
                        console.log('mega-box found', megaBox);
                        // Κλείνουμε όλα τα άλλα mega-box
                        document.querySelectorAll('.mega-box.open-mega').forEach(box => {
                            if (box !== megaBox) {  // Σύγκριση με το DOM στοιχείο
                                box.classList.remove('open-mega');
                            }
                        });

                        // Εναλλάσσουμε την κλάση 'open-mega' στο mega-box που βρήκαμε
                        megaBox.classList.toggle('open-mega');
                    }
                }
            });

            // Κλείσιμο του mega-menu όταν κάνετε κλικ έξω από αυτό


            // end mobilePopup

            // Ορισμός των components
            const replacement = '<div data-gjs-type="dynamic-menu"> </div>';
            // const updatedHtml = replaceMenuContent(componentHtml, replacement);
            //
            // componentHtml = replaceMenuContent(componentHtml);

            // Set the updated HTML to the editor
            let templateData = JSON.parse(template);
            editor.setComponents(templateData.components);
            editor.addStyle(componentCss);
            // editor.setComponents(templateData.components);
            editor.setStyle(templateData.style);
            const blockManager = editor.BlockManager;
            addCustomBlocks(editor);
            // blockManager.remove('c45')
            // blockManager.remove('c47')
            // blockManager.remove('c48')
            // blockManager.remove('c49')
            editor.Panels.removeButton("views", "open-layers");


            //     editor.runCommand('open-code');
            // }, 1000);
            const script = function () {
                console.log('Script initialized');

                // Function to close all mega-menus
                function closeAllMegaMenus() {
                    document.querySelectorAll('.dropdown-content').forEach(menu => {
                        menu.style.display = 'none';
                    });
                }

                // Function to toggle the mega-menu
                function toggleMegaMenu(event) {
                    event.preventDefault();

                    const span = event.currentTarget;
                    let megaMenu = span.nextElementSibling;

                    // If the next sibling is not a mega-menu, find the closest .nav-item and get the .mega-menu
                    if (!megaMenu || !megaMenu.classList.contains('dropdown-content')) {
                        const navItem = span.closest('.dropdown');
                        if (navItem) {
                            megaMenu = navItem.querySelector('.dropdown-content');
                        }
                    }

                    // Check if the megaMenu is found and is of the right class
                    if (!megaMenu || !megaMenu.classList.contains('dropdown-content')) {
                        console.log('No corresponding dropdown-content found');
                        return;
                    }

                    // Get the current display state of the mega-menu
                    const isCurrentlyVisible = megaMenu.style.display === 'block' || megaMenu.style.display === '';

                    // Close all other mega-menus
                    closeAllMegaMenus();

                    // Toggle the display of the selected mega-menu
                    megaMenu.style.display = isCurrentlyVisible ? 'none' : 'block';
                    console.log('Toggled mega-menu visibility:', megaMenu.style.display);
                }

                // Add event listener to each span.open
                const openItems = document.querySelectorAll('.open');

                openItems.forEach(item => {
                    console.log('clicked span')
                    item.addEventListener('click', toggleMegaMenu);

                });

                // Ensure that clicking outside of the mega-menu closes it
                document.addEventListener('click', (event) => {
                    console.log('click event')
                    if (!event.target.closest('.dropdown') && !event.target.closest('.dropdown-content') && !event.target.closest('.open')) {
                        console.log('Clicked outside, closing all mega-menus');
                        closeAllMegaMenus();
                    }
                });
            };


            editor.BlockManager.add('mega-menu-item', {
                label: 'Mega Menu Item',
                type: 'nav-item',
                content: {type: 'mega-menu-nav-item'},
                category: 'Basic',

            })

            editor.BlockManager.add('div', {
                label: 'Div',

                content: {
                    type: 'div',
                    content: '',
                    style: {padding: '10px'},
                    activeOnRender: true
                },
                category: 'Basic',
            })


            editor.BlockManager.add('mobile-button-menu', {
                label: 'Mobile menu button',
                type: 'nav-item',
                content: `<label for="menu-btn" class="btn menu-btn"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="size-6">
  <path stroke-linecap="round" stroke-linejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
</svg>
</label>`,
                category: 'Basic',

            })
            editor.BlockManager.add('simple-nav-item', {
                label: 'Simple Nav Item',
                type: 'nav-item',
                content: `<li class='mega-menu-nav-item'><a class='desktop-item' href="#">Τίτλος</a></li>`,
                category: 'Basic',
            });

            editor.BlockManager.add('paragraph', {
                label: 'Παράγραφος',
                content: {
                    type: 'text',
                    content: 'Με τον όρο Lorem ipsum αναφέρονται τα κείμενα εκείνα τα οποία είναι ακατάληπτα, δεν μπορεί δηλαδή κάποιος να βγάλει κάποιο λογικό νόημα από αυτά, και έχουν δημιουργηθεί με σκοπό να παρουσιάσουν στον αναγνώστη μόνο τα γραφιστικά χαρακτηριστικά...',
                    style: {padding: '10px'},
                    activeOnRender: true
                },
                tagName: 'p',
                category: 'Basic',
            });


            editor.BlockManager.add('main-navigation', {
                label: 'Μενού',
                content: {type: 'mainNavigation'},

                category: 'Basic',
            });

            editor.DomComponents.addType('mainNavigation', {
                model: {
                    defaults: {
                        tagName: 'nav',
                        draggable: true,
                        droppable: true,
                        attributes: {
                            id: 'nav-menu',
                            class: 'main-navigation',
                        },
                        components: [
                            {
                                tagName: 'button',
                                type: 'text',
                                attributes: {
                                    class: 'menu-toggle',
                                },
                                content: '☰',
                            },
                            {
                                tagName: 'ul',
                                attributes: {
                                    class: 'container',
                                },
                                components: [
                                    {
                                        tagName: 'li',
                                        attributes: {
                                            class: 'dropdown',
                                        },
                                        components: [
                                            {
                                                tagName: 'a',
                                                type: 'link',
                                                attributes: {
                                                    href: '#',
                                                    class: 'dropdown-toggle',
                                                },
                                                content: 'Pages',
                                                style: {
                                                    color: '#222222',
                                                    display: 'block',
                                                    padding: '20px 0',
                                                    borderBottom: '3px solid transparent',
                                                    transition: 'all .3s ease',
                                                },
                                            },
                                            {
                                                tagName: 'div',
                                                attributes: {
                                                    class: 'mega-menu',
                                                },
                                                components: [
                                                    {
                                                        tagName: 'div',
                                                        attributes: {
                                                            class: 'container',
                                                        },
                                                        components: [
                                                            {
                                                                tagName: 'div',
                                                                attributes: {
                                                                    class: 'item',
                                                                },
                                                                components: [
                                                                    {
                                                                        tagName: 'h3',
                                                                        content: 'Home Page',
                                                                    },
                                                                    {
                                                                        tagName: 'ul',
                                                                        components: [
                                                                            {
                                                                                tagName: 'li',
                                                                                components: [
                                                                                    {
                                                                                        tagName: 'a',
                                                                                        type: 'link',
                                                                                        attributes: {
                                                                                            href: '#',
                                                                                        },
                                                                                        content: 'Home Page No #1',
                                                                                    },
                                                                                ],
                                                                            },
                                                                            // Add more list items as needed
                                                                        ],
                                                                    },
                                                                ],
                                                            },
                                                            // Add more `.item` divs as needed
                                                        ],
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                    // Add more list items as needed
                                ],
                            },
                        ],
                        traits: [
                            'id',
                            'class',
                        ],
                    },
                },
            });



            // mega menu command


            editor.DomComponents.addType('mainNavigation', {
                model: {
                    defaults: {
                        tagName: 'nav',
                        draggable: true,
                        droppable: false,
                        attributes: {
                            id: 'nav-menu',
                            class: 'main-navigation',
                        },
                        components: [
                            {
                                tagName: 'button',
                                type: 'text',
                                attributes: {
                                    class: 'menu-toggle',
                                },
                                content: '☰',
                                style: {
                                    display: 'none',
                                    fontSize: '24px',
                                    color: 'white',
                                    backgroundColor: '#0ca0d6',
                                    border: 'none',
                                    padding: '15px',
                                    cursor: 'pointer',
                                },
                            },
                            {
                                tagName: 'ul',
                                attributes: {
                                    class: 'container',
                                },
                                style: {
                                    paddingLeft: '0',
                                    marginTop: '0',
                                    marginBottom: '0',
                                    listStyle: 'none',
                                    display: 'flex',
                                    justifyContent: 'space-between',
                                },
                                components: [
                                    {
                                        tagName: 'li',
                                        attributes: {
                                            class: 'dropdown',
                                        },
                                        style: {
                                            display: 'inline-block',
                                            fontSize: '14px',
                                            padding: '0 15px',
                                            position: 'relative',
                                        },
                                        components: [
                                            {
                                                tagName: 'a',
                                                type: 'link',
                                                attributes: {
                                                    href: '#',
                                                    class: 'dropdown-toggle',
                                                },
                                                content: 'Pages',
                                                style: {
                                                    color: '#fff',
                                                    display: 'block',
                                                    padding: '20px 0',
                                                    borderBottom: '3px solid transparent',
                                                    transition: 'all .3s ease',
                                                },
                                            },
                                            {
                                                tagName: 'div',
                                                attributes: {
                                                    class: 'mega-menu',
                                                },
                                                style: {
                                                    background: '#eee',
                                                    visibility: 'hidden',
                                                    opacity: '0',
                                                    transition: 'visibility 0s, opacity 0.5s linear',
                                                    position: 'absolute',
                                                    left: '0',
                                                    width: '100%',
                                                    paddingBottom: '20px',
                                                },
                                                components: [
                                                    {
                                                        tagName: 'div',
                                                        attributes: {
                                                            class: 'container',
                                                        },
                                                        style: {
                                                            display: 'flex',
                                                            flexWrap: 'wrap',
                                                        },
                                                        components: [
                                                            {
                                                                tagName: 'div',
                                                                attributes: {
                                                                    class: 'item',
                                                                },
                                                                style: {
                                                                    flexGrow: '1',
                                                                    margin: '0 10px',
                                                                    boxSizing: 'border-box',
                                                                },
                                                                components: [
                                                                    {
                                                                        tagName: 'h3',
                                                                        content: 'Home Page',
                                                                        style: {
                                                                            color: '#444',
                                                                        },
                                                                    },
                                                                    {
                                                                        tagName: 'ul',
                                                                        components: [
                                                                            {
                                                                                tagName: 'li',
                                                                                components: [
                                                                                    {
                                                                                        tagName: 'a',
                                                                                        type: 'link',
                                                                                        attributes: {
                                                                                            href: '#',
                                                                                        },
                                                                                        content: 'Home Page No #1',
                                                                                        style: {
                                                                                            borderBottom: '1px solid #ddd',
                                                                                            color: '#4ea3d8',
                                                                                            display: 'block',
                                                                                            padding: '10px 0',
                                                                                        },
                                                                                    },
                                                                                ],
                                                                            },
                                                                            // Add more list items as needed
                                                                        ],
                                                                    },
                                                                ],
                                                            },
                                                            // Add more `.item` divs as needed
                                                        ],
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                    // Add more list items as needed
                                ],
                            },
                        ],
                        traits: [
                            'id',
                            'class',
                            {
                                type: 'color',
                                label: 'Background',
                                name: 'background-color',
                                changeProp: 1,
                            },
                            {
                                type: 'text',
                                label: 'Font Size',
                                name: 'font-size',
                                changeProp: 1,
                            },
                            {
                                type: 'text',
                                label: 'Padding',
                                name: 'padding',
                                changeProp: 1,
                            },
                            {
                                type: 'color',
                                label: 'Text Color',
                                name: 'color',
                                changeProp: 1,
                            },
                        ],
                        styles: {
                            backgroundColor: '#0ca0d6',
                            fontSize: '14px',
                            padding: '0',
                            color: '#fff',
                        },
                    },
                    init() {
                        this.on('change:background-color', this.updateStyle);
                        this.on('change:font-size', this.updateStyle);
                        this.on('change:padding', this.updateStyle);
                        this.on('change:color', this.updateStyle);
                    },
                    updateStyle() {
                        const bgColor = this.get('background-color');
                        const fontSize = this.get('font-size');
                        const padding = this.get('padding');
                        const color = this.get('color');

                        this.addStyle({
                            backgroundColor: bgColor,
                            fontSize: fontSize,
                            padding: padding,
                            color: color,
                        });
                    },
                },
                view: {
                    onRender() {
                        this.model.updateStyle();
                    },
                },
            });


            editor.on('component:selected', () => {
                const openSmBtn = editor.Panels.getButton('views', 'open-sm');
                openSmBtn.set('active', 1);
            });


            // addCustomBlocks(blockManager);
            //
            // blockManager.add('bootstrap-button', {
            //     label: 'Bootstrap Button',
            //     content: `<button class="btn btn-primary btn-full-width">Click me</button>`,
            //     category: 'Basic',
            //     attributes: { class: 'gjs-fonts gjs-f-button' },
            // });

            // test  block


            // blockManager.add('test-block', {
            //     label: 'Test block',
            //     category: 'Basic',
            //
            //     attributes: { class: 'fa fa-text' },
            //     content: { type: 'comp-with-js', render: ({ myprop1, myprop2 }) => `<div class="cmp-css">My component with props: ${myprop1} ${myprop2}</div>` },
            // });


            // end test block


            // Automatically apply a width of 100% and add the predefined class to buttons
            editor.on('component:add', (component) => {
                if (component.is('button')) {
                    component.addClass('btn-full-width'); // Adds the predefined class
                    component.setStyle({width: '100%'}); // Ensures the button width is always 100%
                }
            });

            //////// custom code editor
            // custom code manager panel
            const panelViews = editor.Panels.addPanel({
                id: 'views'
            });
            panelViews.get('buttons').add([{
                attributes: {
                    title: 'Open Code'
                },
                className: 'fa fa-file-code-o',
                command: 'open-code',
                togglable: false, //do not close when button is clicked again
                id: 'open-code'
            }]);

            // custom blocks
            function addCustomBlocks(editor) {


// Ενεργοποίηση του Rich Text Editor όταν κάνεις διπλό κλικ στο block
//                 editor.on('component:selected', (component) => {
//                     if (component.is('text')) {
//                         editor.runCommand('tlb-text');
//                     }
//                 });

                // add this as editor.BlockManger  <div class="menu-toggle" id="menu-toggle"><span>☰ Menu</span></div>
                editor.BlockManager.add('menu-toggle', {
                    id: 'menu-toggle',
                    title: 'Menu Toggle',
                    label: 'Menu Toggle',
                    category: 'Basic',
                    content: {
                        type: 'button',
                        attributes: {
                            class: 'menu-toggle',
                            id: 'mobile-menu-toggle'
                        },
                        components: [{
                            type: 'text',
                            content: '☰ Menu'
                        }]
                    }
                });

                // editor.BlockManager.add('container', {
                //     // Your block properties...
                //     label: 'Container',
                //     category: 'Layout',
                //     content: {
                //         type: 'div',
                //         attributes: {"data-div": "true", "data-flex-container": "true"},
                //         style: {
                //             display: 'flex',
                //             'flex-wrap': 'wrap',
                //             padding: '10px 0',
                //
                //         },
                //     }
                // });
                editor.BlockManager.add('centered-container', {
                    label: 'Container',
                    category: 'Layout',

                    content: {
                        attributes: {
                            class: 'container',
                        },
                        tagName: 'div',
                        style: {
                            'max-width': 'var(--container-width)',
                            width: '100%',
                            margin: '0 auto',

                        }
                    },
                });

// Προσθήκη CSS αν χρειάζεται
//                 editor.CssComposer.addRule('.centered-container', {
//                     'max-width': '1280px',
//                     'margin': '0 auto',
//                     'text-align': 'center',
//                 });

                editor.BlockManager.add('OneColumn', {
                    // Your block properties...
                    label: 'One Column',
                    category: 'Layout',
                    content: [
                        {
                            type: 'div',
                            style: {
                                display: 'block',
                                'flex-grow': 1,
                            },
                        }
                    ]
                });

                editor.BlockManager.add('TwoColumns', {
                    // Your block properties...
                    label: 'Two Columns',
                    category: 'Layout',
                    content: [
                        {
                            type: 'div',
                            style: {
                                display: 'block',
                                'flex-grow': 1,
                            },
                        },
                        {
                            type: 'div',
                            style: {
                                display: 'block',
                                'flex-grow': 1,
                            },
                        }
                    ]
                });


                // custom edit for style manager
                // const sm = editor.StyleManager;
                const sm = editor.StyleManager;
                //  sm.addSector('column-gap 2', {
                //     name: 'Column Gap',
                //     open: false,
                //     type: 'number',
                //     default: '0',
                //     units: 'px',
                //     properties: ['gap'],
                // });

                blockManager.add('anchor', {
                    id: 'anchor',
                    label: 'Λίνκ',
                    category: 'Data',
                    droppable: true,
                    select: true,
                    draggable: true,
                    content: {
                        type: 'anchor',
                    }

                });


                editor.DomComponents.addType('anchor', {
                    model: {
                        defaults: {
                            tagName: 'a',
                            droppable: true,
                            style: {
                                display: 'flex',
                                padding: '5px',
                                color: '#222',
                                'text-decoration': 'none',
                            },
                            attributes: {
                                href: '#',
                                alt: '',
                            },
                            components: [{
                                style: {
                                    color: '#222',
                                    'text-decoration': 'none',
                                },
                                type: 'text',
                                content: 'Τίτλος'
                            }]
                        }
                    },
                    view: {
                        tagName: 'a',
                        attributes: {
                            href: '#',
                        },
                        content: 'Τίτλος'
                    }
                })



                blockManager.add('dynamicMenu', {
                    id: 'DynamicMenu',
                    label: 'DynamicMenu',
                    category: 'Data',
                    droppable: true,
                    select: true,
                    content: `

                <div data-dynamic-menu class="hidden sm:ml-6 sm:flex sm:space-x-8">
          <a href="#" class="inline-flex items-center border-b-2 border-indigo-500 px-1 pt-1 text-sm font-medium text-gray-900">ΑΡΧΙΚΗ</a>
          <a href="#" class="inline-flex items-center border-b-2 border-transparent px-1 pt-1 text-sm font-medium text-gray-500 hover:border-gray-300 hover:text-gray-700">ΠΡΟΙΟΝΤΑ</a>
          <a href="#" class="inline-flex items-center border-b-2 border-transparent px-1 pt-1 text-sm font-medium text-gray-500 hover:border-gray-300 hover:text-gray-700">ΕΠΙΚΟΙΝΩΝΙΑ</a>
        </div>
                 `,

                });
                editor.Components.addType('dynamic-categories', {
                    model: {
                        defaults: {
                            // Βασικές ρυθμίσεις
                            tagName: 'div',
                            droppable: false,
                            editable: false,
                            categories: [], // Θα αποθηκεύσουμε τις κατηγορίες εδώ
                            subcategories: [], // Θα αποθηκεύσουμε τις υποκατηγορίες εδώ

                            traits: [
                                {
                                    type: 'select',
                                    label: 'Category',
                                    name: 'category',
                                    options: [],
                                    changeProp: 1,
                                },
                            ],

                            script: function () {
                                // Αυτό το script θα εκτελείται στο frontend
                                var el = this.el;

                                function renderSubcategories(subcategories) {
                                    el.innerHTML = ''; // Καθαρίζει το εσωτερικό του element
                                    if (subcategories.length > 0) {
                                        var ul = document.createElement('ul');
                                        subcategories.forEach(function (subcategory) {
                                            var li = document.createElement('li');
                                            li.innerHTML = subcategory.name;
                                            ul.appendChild(li);
                                        });
                                        el.appendChild(ul);
                                    } else {
                                        el.innerHTML = 'No subcategories found.';
                                    }
                                }

                                // Όταν επιλέγεται μια κατηγορία, το backend πρέπει να μας στέλνει τις υποκατηγορίες
                                var category = this.get('category');
                                if (category) {
                                    fetch(`/get-subcategories?category=${category}`)
                                        .then((res) => res.json())
                                        .then((subcategories) => {
                                            renderSubcategories(subcategories);
                                        })
                                        .catch((err) => console.error('Error fetching subcategories', err));
                                }
                            },
                        },

                        init() {
                            // Όταν δημιουργείται το component, κάνουμε call στο /get-categories
                            fetch('/manage/categories_list_json', {
                                method: 'GET',
                                headers: {
                                    'Content-Type': 'application/json',
                                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                                }
                            })
                                .then((res) => res.json())
                                .then((categories) => {
                                    const traits = this.get('traits');
                                    const categoryTrait = traits.find((t) => t.get('name') === 'category');
                                    const options = categories.map((cat) => ({
                                        id: cat.id,
                                        name: cat.title.el,
                                        value: cat.id,
                                    }));
                                    categoryTrait.set('options', options);

                                    this.set('categories', categories); // Αποθηκεύουμε τις κατηγορίες στο μοντέλο
                                })
                                .catch((err) => console.error('Error fetching categories', err));

                            // Παρακολουθούμε τις αλλαγές στο category trait
                            this.on('change:category', this.fetchSubcategories);
                        },

                        fetchSubcategories() {
                            const categoryId = this.get('category');
                            if (categoryId) {
                                fetch(`/manage/categories_list_json?parent_id=${categoryId}`, {
                                    method: 'GET',
                                    headers: {
                                        'Content-Type': 'application/json',
                                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                                    }
                                })
                                    .then((res) => res.json())
                                    .then((subcategories) => {
                                        this.set('subcategories', subcategories);
                                        this.view.render(); // Επανασχεδιάζουμε το component με τις νέες υποκατηγορίες
                                    })
                                    .catch((err) => console.error('Error fetching subcategories', err));
                            }
                        },
                    },

                    view: {
                        onRender() {
                            const subcategories = this.model.get('subcategories');
                            if (subcategories.length > 0) {
                                const ul = document.createElement('ul');
                                subcategories.forEach((subcategory) => {
                                    const li = document.createElement('li');
                                    // add class to li
                                    li.classList.add('p-2', 'another-class');

                                    li.innerHTML = subcategory.title.el;
                                    li.addEventListener('focusout', function () {
                                        const updatedText = li.innerHTML;
                                        console.log('Updated content:', updatedText);
                                        // Εδώ μπορείς να προσθέσεις την λογική αποθήκευσης ή ενημέρωσης
                                    });


                                    ul.appendChild(li);
                                });
                                this.el.innerHTML = ''; // Καθαρίζουμε το εσωτερικό του element
                                this.el.appendChild(ul); // Προσθέτουμε τη λίστα υποκατηγοριών
                            } else {
                                this.el.innerHTML = 'No subcategories selected';
                            }
                        },
                    },
                });
                editor.BlockManager.add('dynamic-categories-block', {
                    label: 'Dynamic Categories',
                    category: 'Data',
                    content: {
                        type: 'dynamic-categories',
                    },
                });

                const staticMenuScripts = function () {
                    // `this` is bound to the component element
                    console.log('the element', this);
                    document.querySelectorAll('.product-btn').forEach(function (button) {
                        button.addEventListener('click', function () {
                            const flyoutMenu = this.nextElementSibling;
                            flyoutMenu.classList.toggle('show-menu');
                        });
                    });

                    // Προαιρετικά, κλείσιμο όταν κάνεις κλικ εκτός του μενού
                    document.addEventListener('click', function (event) {
                        document.querySelectorAll('.product-flyout').forEach(function (flyoutMenu) {
                            const productButton = flyoutMenu.previousElementSibling;

                            if (!productButton.contains(event.target) && !flyoutMenu.contains(event.target)) {
                                flyoutMenu.classList.remove('show-menu');
                            }
                        });
                    });
                    document.querySelector('.hidden.lg\\:flex').classList.add('click-enabled');
                    document.querySelector('.hidden.lg\\:flex').classList.remove('hover-enabled');

                };
                editor.BlockManager.add('Static Menu', {
                    id: 'staticMenu',
                    label: 'Static Menu',
                    // Select the component once it's dropped
                    select: true,
                    category: 'Data',
                    editable: true,
                    // You can pass components as a JSON instead of a simple HTML string,
                    // in this case we also use a defined component type `image`
                    // This triggers `active` event on dropped components and the `image`
                    // reacts by opening the AssetManager
                    activate: true,
                    content: {
                        editable: true,
                        type: 'static-menu',
                        content: `<!-- hey--><div  data-gjs-removable="true" data-gjs-selectable="false"  class="relative hover-container">
                        <button data-gjs-editable="true" type="button" class="product-btn flex items-center gap-x-1 text-sm font-semibold leading-6 text-gray-900">
                            Product
                            <svg class="h-5 w-5 flex-none text-gray-400" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                <path fill-rule="evenodd" d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z" clip-rule="evenodd" />
                            </svg>
                        </button>
                        <div data-gjs-editable="true" class="product-flyout absolute -left-8 top-4 z-10 mt-2 w-screen max-w-md overflow-hidden rounded-3xl bg-white shadow-lg ring-1 ring-gray-900/5 opacity-0 translate-y-1 transition-all ease-in-out duration-200 pointer-events-none">
                            <div class="p-4">
                                <div data-gjs-contentEditable="true" class="group relative flex items-center gap-x-6 rounded-lg p-4 text-sm leading-6 hover:bg-gray-50">
                                    <div class="flex h-11 w-11 flex-none items-center justify-center rounded-lg bg-gray-50 group-hover:bg-white">
                                        <svg class="h-6 w-6 text-gray-600 group-hover:text-indigo-600" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M10.5 6a7.5 7.5 0 107.5 7.5h-7.5V6z" />
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 10.5H21A7.5 7.5 0 0013.5 3v7.5z" />
                                        </svg>
                                    </div>
                                    <div class="flex-auto">
                                        <a href="#" class="block font-semibold text-gray-900">
                                            Analytics
                                            <span class="absolute inset-0"></span>
                                        </a>
                                        <p class="mt-1 text-gray-600">Get a better understanding of your traffic</p>
                                    </div>
                                </div>
                                <div class="group relative flex items-center gap-x-6 rounded-lg p-4 text-sm leading-6 hover:bg-gray-50">
                                    <div class="flex h-11 w-11 flex-none items-center justify-center rounded-lg bg-gray-50 group-hover:bg-white">
                                        <svg class="h-6 w-6 text-gray-600 group-hover:text-indigo-600" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M15.042 21.672L13.684 16.6m0 0l-2.51 2.225.569-9.47 5.227 7.917-3.286-.672zM12 2.25V4.5m5.834.166l-1.591 1.591M20.25 10.5H18M7.757 14.743l-1.59 1.59M6 10.5H3.75m4.007-4.243l-1.59-1.59" />
                                        </svg>
                                    </div>
                                    <div class="flex-auto">
                                        <a href="#" class="block font-semibold text-gray-900">
                                            Engagement
                                            <span class="absolute inset-0"></span>
                                        </a>
                                        <p class="mt-1 text-gray-600">Speak directly to your customers</p>
                                    </div>
                                </div>
                                <div class="group relative flex items-center gap-x-6 rounded-lg p-4 text-sm leading-6 hover:bg-gray-50">
                                    <div class="flex h-11 w-11 flex-none items-center justify-center rounded-lg bg-gray-50 group-hover:bg-white">
                                        <svg class="h-6 w-6 text-gray-600 group-hover:text-indigo-600" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M7.864 4.243A7.5 7.5 0 0119.5 10.5c0 2.92-.556 5.709-1.568 8.268M5.742 6.364A7.465 7.465 0 004.5 10.5a7.464 7.464 0 01-1.15 3.993m1.989 3.559A11.209 11.209 0 008.25 10.5a3.75 3.75 0 117.5 0c0 .527-.021 1.049-.064 1.565M12 10.5a14.94 14.94 0 01-3.6 9.75m6.633-4.596a18.666 18.666 0 01-2.485 5.33" />
                                        </svg>
                                    </div>
                                    <div class="flex-auto">
                                        <a href="#" class="block font-semibold text-gray-900">
                                            Security
                                            <span class="absolute inset-0"></span>
                                        </a>
                                        <p class="mt-1 text-gray-600">Your customers’ data will be safe and secure</p>
                                    </div>
                                </div>
                                <div class="group relative flex items-center gap-x-6 rounded-lg p-4 text-sm leading-6 hover:bg-gray-50">
                                    <div class="flex h-11 w-11 flex-none items-center justify-center rounded-lg bg-gray-50 group-hover:bg-white">
                                        <svg class="h-6 w-6 text-gray-600 group-hover:text-indigo-600" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 16.875h3.375m0 0h3.375m-3.375 0V13.5m0 3.375v3.375M6 10.5h2.25a2.25 2.25 0 002.25-2.25V6a2.25 2.25 0 00-2.25-2.25H6A2.25 2.25 0 003.75 6v2.25A2.25 2.25 0 006 10.5zm0 9.75h2.25A2.25 2.25 0 0010.5 18v-2.25a2.25 2.25 0 00-2.25-2.25H6a2.25 2.25 0 00-2.25 2.25V18A2.25 2.25 0 006 20.25zm9.75-9.75H18a2.25 2.25 0 002.25-2.25V6A2.25 2.25 0 0018 3.75h-2.25A2.25 2.25 0 0013.5 6v2.25a2.25 2.25 0 002.25 2.25z" />
                                        </svg>
                                    </div>
                                    <div class="flex-auto">
                                        <a href="#" class="block font-semibold text-gray-900">
                                            Integrations
                                            <span class="absolute inset-0"></span>
                                        </a>
                                        <p class="mt-1 text-gray-600">Connect with third-party tools</p>
                                    </div>
                                </div>
                                <div class="group relative flex items-center gap-x-6 rounded-lg p-4 text-sm leading-6 hover:bg-gray-50">
                                    <div class="flex h-11 w-11 flex-none items-center justify-center rounded-lg bg-gray-50 group-hover:bg-white">
                                        <svg class="h-6 w-6 text-gray-600 group-hover:text-indigo-600" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M16.023 9.348h4.992v-.001M2.985 19.644v-4.992m0 0h4.992m-4.993 0l3.181 3.183a8.25 8.25 0 0013.803-3.7M4.031 9.865a8.25 8.25 0 0113.803-3.7l3.181 3.182m0-4.991v4.99" />
                                        </svg>
                                    </div>
                                    <div class="flex-auto">
                                        <a href="#" class="block font-semibold text-gray-900">
                                            Automations
                                            <span class="absolute inset-0"></span>
                                        </a>
                                        <p class="mt-1 text-gray-600">Build strategic funnels that will convert</p>
                                    </div>
                                </div>
                            </div>
                            <div class="grid grid-cols-2 divide-x divide-gray-900/5 bg-gray-50">
                                <a href="#" class="flex items-center justify-center gap-x-2.5 p-3 text-sm font-semibold leading-6 text-gray-900 hover:bg-gray-100">
                                    <svg class="h-5 w-5 flex-none text-gray-400" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path fill-rule="evenodd" d="M2 10a8 8 0 1116 0 8 8 0 01-16 0zm6.39-2.908a.75.75 0 01.766.027l3.5 2.25a.75.75 0 010 1.262l-3.5 2.25A.75.75 0 018 12.25v-4.5a.75.75 0 01.39-.658z" clip-rule="evenodd" />
                                    </svg>
                                    Watch demo
                                </a>
                                <a href="#" class="flex items-center justify-center gap-x-2.5 p-3 text-sm font-semibold leading-6 text-gray-900 hover:bg-gray-100">
                                    <svg class="h-5 w-5 flex-none text-gray-400" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path fill-rule="evenodd" d="M2 3.5A1.5 1.5 0 013.5 2h1.148a1.5 1.5 0 011.465 1.175l.716 3.223a1.5 1.5 0 01-1.052 1.767l-.933.267c-.41.117-.643.555-.48.95a11.542 11.542 0 006.254 6.254c.395.163.833-.07.95-.48l.267-.933a1.5 1.5 0 011.767-1.052l3.223.716A1.5 1.5 0 0118 15.352V16.5a1.5 1.5 0 01-1.5 1.5H15c-1.149 0-2.263-.15-3.326-.43A13.022 13.022 0 012.43 8.326 13.019 13.019 0 012 5V3.5z" clip-rule="evenodd" />
                                    </svg>
                                    Contact sales
                                </a>
                            </div>
                        </div>
                    </div>`,
                    },


                })
                editor.Components.addType('static-menu', {
                    model: {
                        defaults: {
                            components: `
                        <div   data-gjs-selectable="false"  class="relative hover-container ">
                        <a  href=""   class="product-btn flex items-center gap-x-1 text-sm font-semibold leading-6 text-gray-900">

                            Menu Title
                            <svg class="h-5 w-5 flex-none text-gray-400" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                <path fill-rule="evenodd" d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z" clip-rule="evenodd" />
                            </svg>

                        </a>
                        <div   class="product-flyout absolute p-2 -left-8 top-4 z-10 mt-2 w-screen max-w-md overflow-hidden rounded-3xl bg-white shadow-lg ring-1 ring-gray-900/5 opacity-0 translate-y-1 transition-all ease-in-out duration-200 pointer-events-none">
                            <div class="p-4">
                                <div data-gjs-contentEditable="true" class="group relative flex items-center gap-x-6 rounded-lg p-4 text-sm leading-6 hover:bg-gray-50">
                                    <div class="flex h-11 w-11 flex-none items-center justify-center rounded-lg bg-gray-50 group-hover:bg-white">
                                        <svg class="h-6 w-6 text-gray-600 group-hover:text-indigo-600" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M10.5 6a7.5 7.5 0 107.5 7.5h-7.5V6z" />
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 10.5H21A7.5 7.5 0 0013.5 3v7.5z" />
                                        </svg>
                                    </div>
                                    <div class="flex-auto">
                                        <a href="#" class="block font-semibold text-gray-900">
                                            Analytics

                                        </a>
                                        <p class="mt-1 text-gray-600">Get a better understanding of your traffic</p>
                                    </div>
                                </div>

                            </div>
                            <div class="grid grid-cols-2 divide-x divide-gray-900/5 bg-gray-50">
                                <a href="#" class="flex items-center justify-center gap-x-2.5 p-3 text-sm font-semibold leading-6 text-gray-900 hover:bg-gray-100">
                                    <svg class="h-5 w-5 flex-none text-gray-400" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path fill-rule="evenodd" d="M2 10a8 8 0 1116 0 8 8 0 01-16 0zm6.39-2.908a.75.75 0 01.766.027l3.5 2.25a.75.75 0 010 1.262l-3.5 2.25A.75.75 0 018 12.25v-4.5a.75.75 0 01.39-.658z" clip-rule="evenodd" />
                                    </svg>
                                    Watch demo
                                </a>
                                <a href="#" class="flex items-center justify-center gap-x-2.5 p-3 text-sm font-semibold leading-6 text-gray-900 hover:bg-gray-100">
                                    <svg class="h-5 w-5 flex-none text-gray-400" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path fill-rule="evenodd" d="M2 3.5A1.5 1.5 0 013.5 2h1.148a1.5 1.5 0 011.465 1.175l.716 3.223a1.5 1.5 0 01-1.052 1.767l-.933.267c-.41.117-.643.555-.48.95a11.542 11.542 0 006.254 6.254c.395.163.833-.07.95-.48l.267-.933a1.5 1.5 0 011.767-1.052l3.223.716A1.5 1.5 0 0118 15.352V16.5a1.5 1.5 0 01-1.5 1.5H15c-1.149 0-2.263-.15-3.326-.43A13.022 13.022 0 012.43 8.326 13.019 13.019 0 012 5V3.5z" clip-rule="evenodd" />
                                    </svg>
                                    Contact sales
                                </a>
                            </div>
                        </div>
                    </div>
      `,

                            removable: true,
                            draggable: true,
                            badgable: true,
                            stylable: true,
                            highlightable: false,
                            selectable: true,
                            copyable: true,
                            resizable: true,
                            editable: true,
                            hoverable: true,
                            type: 'div',
                            attributes: { // Default attributes
                                type: 'text',
                                name: 'default-name',
                                placeholder: 'Insert text here',
                            },
                            script: staticMenuScripts,
                            // Add some style, just to make the component visible
                            styles: `
                        .product-flyout {
                             opacity: 0;
                             transform: translateY(1rem);
                             pointer-events: none;
                             transition: opacity 0.2s ease-in-out, transform 0.2s ease-in-out;
                             transition-delay: 200ms;
                         }
                         .hover-enabled .hover-container:hover .product-flyout {
                             opacity: 1;
                             transform: translateY(0);
                             pointer-events: auto;
                             transition-delay: 200ms;
                         }

                         .click-enabled .show-menu {
                             opacity: 1;
                             transform: translateY(0);
                             pointer-events: auto;
                         }
                         .translate-x-0 {
                             transform: translateX(0);
                         }
                        `,
                            toHTML(opts) {
                                const editable = this.findType('editable')[0];
                                return editable ? editable.toHTML(opts) : '';
                            },
                        },
                        view: {
                            events: {
                                dblclick: 'onActive',
                                focusout: 'onDisable',
                            },
                            onActive() {
                                // Προσθέτουμε μια μικρή καθυστέρηση για να διασφαλίσουμε ότι τα στοιχεία είναι πλήρως προσβάσιμα
                                setTimeout(() => {
                                    const editableElements = this.el.querySelectorAll('[data-gjs-editable="true"]');

                                    if (editableElements.length === 0) {
                                        console.log('No editable elements found');
                                    } else {
                                        editableElements.forEach(el => {
                                            el.contentEditable = true;
                                            console.log('Found editable element: ', el);
                                        });
                                    }
                                }, 0); // Η καθυστέρηση μπορεί να ρυθμιστεί αν χρειαστεί
                            },
                            onDisable() {
                                // Απενεργοποίηση contentEditable όταν χάνεται η εστίαση
                                this.el.querySelectorAll('[data-gjs-editable="true"]').forEach(el => {
                                    el.contentEditable = false;
                                });

                                // Ενημέρωση του μοντέλου με το νέο περιεχόμενο
                                const {el, model} = this;
                                model.set('content', el.innerHTML);
                            },
                        },
                        init() {

                        },
                        handlePropChange() {
                            const {someprop} = this.props();
                            console.log('New value of someprop: ', someprop);
                        },
                        handleAttrChange() {
                            console.log('Attributes updated: ', this.getAttributes());
                        },
                        handleTitleChange() {
                            console.log('Attribute title updated: ', this.getAttributes().title);
                        },
                    }

                });

                editor.BlockManager.add('SearchComponent', {
                    id: 'search',
                    label: 'Search Field',
                    draggable: true,
                    droppable: true,
                    category: 'Data',
                    content: {
                        type: 'custom-component',
                        content: `
                     <div data-search-input class="w-full sm:max-w-xs">
          <label for="search" class="sr-only">Search</label>
          <div class="relative">
            <div class="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
              <svg class="h-5 w-5 text-gray-400" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M9 3.5a5.5 5.5 0 100 11 5.5 5.5 0 000-11zM2 9a7 7 0 1112.452 4.391l3.328 3.329a.75.75 0 11-1.06 1.06l-3.329-3.328A7 7 0 012 9z" clip-rule="evenodd" />
              </svg>
            </div>
            <input id="search" name="search" class="block w-full rounded-md border-0 bg-white py-1.5 pl-10 pr-3 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" placeholder="Search" type="search">
          </div>
        </div>
        `,
                    },
                });


            }

            function removeDuplicateCSS(css) {
                const cssRules = css.split('}');
                const uniqueRules = new Set();

                cssRules.forEach(rule => {
                    const trimmedRule = rule.trim();
                    if (trimmedRule) {
                        uniqueRules.add(trimmedRule + '}'); // Επαναφορά του '}'
                    }
                });

                return Array.from(uniqueRules).join('\n');
            }


            function replaceMenuContent(componentHtml, newContent) {
                // Create a DOMParser instance to parse the HTML string
                const parser = new DOMParser();
                const doc = parser.parseFromString(componentHtml, 'text/html');

                // Find the div with the data-renderedmenu attribute
                const targetDiv = doc.querySelector('div[data-renderedmenu]');

                if (targetDiv) {

                    // Replace the target div with the new content
                    targetDiv.outerHTML = replacement;

                    // Serialize the updated document back to an HTML string
                    const serializer = new XMLSerializer();

                    return serializer.serializeToString(doc);
                }


                // Return original HTML if target div is not found
                return componentHtml;
            }


            // save content
            function saveContent() {
                // Λήψη των components ως JSON
                const rawCss = editor.getCss();
                const cleanedCss = removeDuplicateCSS(rawCss);

                const jsonContent = editor.getHtml();
                const cssContent = cleanedCss;
                const jsContent = editor.getJs();
                let components = editor.getComponents();
                let style = editor.getStyle()

                let templateData = {
                    components: components,
                    style: style
                };

                const csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
                // Παράδειγμα αποστολής μέσω AJAX
                fetch('/manage/theme/{{$theme->id}}/component/{{$component->id}}/save-component', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                    },
                    body: JSON.stringify({
                        css: cssContent,
                        js: jsContent,
                        html: jsonContent,
                        template: JSON.stringify(templateData)

                    }),
                })
                    .then(response => response.json())
                    .then(data => console.log('Success:', data))
                    .catch((error) => console.error('Error:', error));
            }

            // Παράδειγμα κλήσης της συνάρτησης αποθήκευσης
            document.getElementById('save-button').addEventListener('click', saveContent);

            //     heading plugin


        </script>
        <style>
            /*.panel__top {*/
            /*    padding: 0;*/
            /*    width: 100%;*/
            /*    display: block;*/
            /*    position: initial;*/
            /*    justify-content: center;*/
            /*    justify-content: space-between;*/
            /*}*/
            .gjs-pn-panel{
                padding: 0;
            }
            button#save-button svg {
                display:inline;
            }
            button#save-button {
                display: inline-block;
                background: #3e9c96;
                padding: 5px;
                margin-bottom: 10px;
                color: #fff;
                border-radius: 4px;
                box-shadow: 0 0 4px #131111;
                margin-left: 17px;
                width: 224px;
                text-align: center;
                border: solid 1px #317d78;
            }
            .panel__basic-actions {
                position: initial;
            }

            .grid-container {
                display: grid;
                grid-template-columns: repeat(auto-fill, minmax(100px, 1fr));
                gap: 10px;
            }


            .change-theme-button {
                width: 40px;
                height: 40px;
                border-radius: 50%;
                margin: 5px;
            }

            .change-theme-button:focus {
                /* background-color: yellow; */
                outline: none;
                box-shadow: 0 0 0 2pt #c5c5c575;
            }

            /*div#pages {*/
            /*    background: #444444;*/
            /*}*/
            div#layers {
                background: #1f2937;
            }

            .gjs-one-bg {
                background-color: #202937;
            }

            /* Fit icons properly */
            .gjs-block {
                /*width: 28%;*/
            }


            .gjs-radio-item input:checked + .gjs-radio-item-label {
                background-color: #000;
            }

            .gjs-sm-sector .gjs-sm-stack #gjs-sm-add, .gjs-clm-tags .gjs-sm-stack #gjs-sm-add {
                color: #000;
            }

            select#componentSelect {
                padding: 1px 5px;
                width: 100%;
                font-weight: 200;
            }
            /*.gjs-toolbar-items {*/
            /*    display: flex;*/
            /*}*/
            /*.fa.fa-save.gjs-toolbar-item {*/
            /*    font-size: 16px;*/
            /*}*/
            .gjs-devices-c{
                display:none;
            }
        </style>

@endsection
