@extends('layouts.themeEditor')
@section('content')
    <!-- Το πεδίο όπου θα εμφανιστεί ο GrapesJS Editor -->

    <div id="container" class="flex flex-row">
        <div id="pages">

            <div class="p-4">
                <h2 class="text-white font-black text-lg border-b py-2">Pages</h2>
                <ul class="text-white pt-2">
                    @foreach($components as $c)
                        <li class="text-white {{($c->id == $component->id)?'text-black':null }}"><a
                                href="/manage/theme/{{$theme->id}}/component/{{$c->id}}">{{$c->page_type}}</a></li>
                    @endforeach
                </ul>
            </div>
            <button id="save-button" class="text-right  bg-black">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                     stroke="currentColor" class="w-h h-6 text-white">
                    <path stroke-linecap="round" stroke-linejoin="round"
                          d="M20.25 6.375c0 2.278-3.694 4.125-8.25 4.125S3.75 8.653 3.75 6.375m16.5 0c0-2.278-3.694-4.125-8.25-4.125S3.75 4.097 3.75 6.375m16.5 0v11.25c0 2.278-3.694 4.125-8.25 4.125s-8.25-1.847-8.25-4.125V6.375m16.5 0v3.75m-16.5-3.75v3.75m16.5 0v3.75C20.25 16.153 16.556 18 12 18s-8.25-1.847-8.25-4.125v-3.75m16.5 0c0 2.278-3.694 4.125-8.25 4.125s-8.25-1.847-8.25-4.125"/>
                </svg>

            </button>

            <div class=" " id="blocks"></div>


        </div>
        <div class="basis-9/12" id="gjs">


        </div>
        <div id="styles-container"></div>
        <div id="layers-container"></div>


    </div>

    <!-- GrapesJS JavaScript -->
    <script>
        const escapeName = (name) => `${name}`.trim().replace(/([^a-z0-9\w-:/]+)/gi, '-');

        // Αρχικοποίηση του GrapesJS Editor
        const editor = grapesjs.init({


            // blockManager: {
            //     appendTo: '#blocks',
            // },
            // styleManager: {
            //     appendTo: '#styles-container',
            // },
            // layerManager: {
            //     appendTo: '#layers-container',
            // },
            // traitManager: {
            //     appendTo: '#trait-container',
            // },
            // selectorManager: {
            //     appendTo: '#styles-container',
            // },
            deviceManager: {},
            pluginsOpts: {
                '@silexlabs/grapesjs-symbols': {
                    appendTo: '.gjs-pn-views-container',
                },
            },

            // Indicate where to init the editor. You can also pass an HTMLElement
            container: '#gjs',
            // Get the content for the canvas directly from the element
            // As an alternative we could use: `components: '<h1>Hello World Component!</h1>'`,
            fromElement: true,
            // Size of the editor
            height: '100vh',
            width: '100%',
            commands: {
                defaults: [
                    window['@truenorthtechnology/grapesjs-code-editor'].codeCommandFactory(),
                ],
            },

            selectorManager: {
                componentFirst: true,
            },


            plugins: ["gjs-blocks-basic", "html-block", 'grapesjs-parser-postcss', "grapesjs-component-code-editor"],
            // Disable the storage manager for the moment
            storageManager: false,

            // panels: { defaults: [] },
            // Avoid any default panel
            canvas: {
                styles: [
                    //         'https://cdn.jsdelivr.net/npm/tailwindcss@latest/dist/tailwind.min.css',
                    '/css/editor.css'
                ]
            },
        });

        componentHtml = @json($component->html);
        componentCss = @json($component->sectionCss);

        // mobile pop up
        let mobileWindow;

        // Προσθήκη του custom κουμπιού στο toolbar
        editor.Panels.addButton('options', {
            id: 'open-mobile-preview',
            className: 'fa fa-mobile',
            label: '',
            command: 'open-mobile-preview',
            attributes: { title: 'Open Mobile Preview' }
        });

        // Προσθήκη της εντολής για το κουμπί
        editor.Commands.add('open-mobile-preview', {
            run(editor, sender) {
                sender && sender.set('active', 0); // απενεργοποίηση του κουμπιού

                // Άνοιγμα του παραθύρου μόνο αν δεν υπάρχει ήδη
                if (!mobileWindow || mobileWindow.closed) {
                    mobileWindow = window.open('', '', 'width=375,height=667');
                }

                // Φόρτωση της αρχικής έκδοσης
                updateMobilePreview();
            }
        });
        // Συνάρτηση για την ενημέρωση του mobile preview
        function updateMobilePreview() {
            const htmlContent = editor.getHtml();
            const cssContent = editor.getCss();

            // Έλεγχος αν το παράθυρο είναι ανοιχτό πριν ενημερώσουμε το περιεχόμενο
            if (mobileWindow && !mobileWindow.closed) {
                mobileWindow.document.open();
                mobileWindow.document.write(`
      <html>
        <head>
          <style>
            ${cssContent}
            body {
              margin: 0;
              padding: 0;
              font-family: Arial, sans-serif;
            }
            /* Προσθήκη responsive styling για mobile */
            @media (max-width: 768px) {
              body {
                width: 100%;
              }
            }
          </style>
        </head>
        <body>
          ${htmlContent}
        </body>
      </html>
    `);
                mobileWindow.document.close();
            }
        }
        // Προσθήκη listener για να ανανεώνει το preview όταν υπάρχει αλλαγή
        editor.on('component:update', updateMobilePreview);

        // Προσθήκη listener και για άλλες αλλαγές (π.χ. drag & drop ή αλλαγές στα στυλ)
        editor.on('canvas:drop', updateMobilePreview);
        editor.on('style:change', updateMobilePreview);
        // end mobilePopup

        // Ορισμός των components
        const replacement = '<div data-gjs-type="dynamic-menu"> </div>';
        const updatedHtml = replaceMenuContent(componentHtml, replacement);

        componentHtml = replaceMenuContent(componentHtml);

        // Set the updated HTML to the editor
        editor.setComponents(componentHtml);
        editor.addStyle(componentCss);


        const blockManager = editor.BlockManager;
        addCustomBlocks(editor);
        // blockManager.remove('c45')
        // blockManager.remove('c47')
        // blockManager.remove('c48')
        // blockManager.remove('c49')


        // editor.on('component:selected', () => {
        //     const openSmBtn = editor.Panels.getButton('views', 'open-sm');
        //     openSmBtn.set('active', 1);
        // });


        // addCustomBlocks(blockManager);
        //
        // blockManager.add('bootstrap-button', {
        //     label: 'Bootstrap Button',
        //     content: `<button class="btn btn-primary btn-full-width">Click me</button>`,
        //     category: 'Basic',
        //     attributes: { class: 'gjs-fonts gjs-f-button' },
        // });

        // test  block


        // blockManager.add('test-block', {
        //     label: 'Test block',
        //     category: 'Basic',
        //
        //     attributes: { class: 'fa fa-text' },
        //     content: { type: 'comp-with-js', render: ({ myprop1, myprop2 }) => `<div class="cmp-css">My component with props: ${myprop1} ${myprop2}</div>` },
        // });


        const script = function (props) {
            this.innerHTML = 'inner content'
            const myLibOpts = {
                prop1: props.myprop1,
                prop2: props.myprop2,
            };

        };


        // end test block


        // Automatically apply a width of 100% and add the predefined class to buttons
        editor.on('component:add', (component) => {
            if (component.is('button')) {
                component.addClass('btn-full-width'); // Adds the predefined class
                component.setStyle({width: '100%'}); // Ensures the button width is always 100%
            }
        });

        // stustom types
        //     editor.DomComponents.addType('my-input-type', {
        //         // Make the editor understand when to bind `my-input-type`
        //         isComponent: el => el.tagName === 'INPUT',
        //
        //         // Model definition
        //         model: {
        //             init() {
        //                 this.on('change:someprop', this.handlePropChange);
        //                 // Listen to any attribute change
        //                 this.on('change:attributes', this.handleAttrChange);
        //                 // Listen to title attribute change
        //                 this.on('change:attributes:title', this.handleTitleChange);
        //             },
        //
        //             handlePropChange() {
        //                 const { someprop } = this.props();
        //                 console.log('New value of someprop: ', someprop);
        //             },
        //
        //             handleAttrChange() {
        //                 console.log('Attributes updated: ', this.getAttributes());
        //             },
        //
        //             handleTitleChange() {
        //                 console.log('Attribute title updated: ', this.getAttributes().title);
        //             },
        //             // Default properties
        //             defaults: {
        //                 tagName: 'input',
        //                 draggable: 'form, form *', // Can be dropped only inside `form` elements
        //                 droppable: false, // Can't drop other elements inside
        //                 attributes: { // Default attributes
        //                     type: 'text',
        //                     name: 'default-name',
        //                     placeholder: 'Insert text here',
        //                 },
        //                 traits: [
        //                     'name',
        //                     'placeholder',
        //                     { type: 'checkbox', name: 'required' },
        //                 ],
        //             }
        //         }
        //

        //////// custom code editor
        // custom code manager panel
        const panelViews = editor.Panels.addPanel({
            id: 'views'
        });
        panelViews.get('buttons').add([{
            attributes: {
                title: 'Open Code'
            },
            className: 'fa fa-file-code-o',
            command: 'open-code',
            togglable: false, //do not close when button is clicked again
            id: 'open-code'
        }]);
        /////// end custom code editor

        //     top panel
        // editor.Panels.addPanel({
        //     id: 'basic-actions',
        //     el: '.panel__basic-actions',
        //     buttons: [
        //         {
        //             id: 'visibility',
        //             active: true, // active by default
        //             className: 'btn-toggle-borders',
        //             label: '<u>B</u>',
        //             command: 'sw-visibility', // Built-in command
        //         }, {
        //             id: 'export',
        //             className: 'btn-open-export',
        //             label: 'Exp',
        //             command: 'export-template',
        //             context: 'export-template', // For grouping context of buttons from the same panel
        //         },
        //         {
        //             id: 'show-json',
        //             className: 'btn-show-json',
        //             label: 'JSON',
        //             context: 'show-json',
        //             command(editor) {
        //                 editor.Modal.setTitle('Components JSON')
        //                     .setContent(`<textarea style="width:100%; height: 250px;">
        //     ${JSON.stringify(editor.getComponents())}
        //   </textarea>`)
        //                     .open();
        //             },
        //         }
        //     ],
        // });


        // editor.on('run:export-template:before', opts => {
        //     console.log('Before the command run');
        //     if (0 /* some condition */) {
        //         opts.abort = 1;
        //     }
        // });
        // editor.on('run:export-template', () => console.log('After the command run'));
        // editor.on('abort:export-template', () => console.log('Command aborted'));

        // custom blocks
        function addCustomBlocks(editor) {
            // const block = editor.BlockManager.add('myblock', {
            //     // Your block properties...
            //     label: 'Flex Container',
            //     category: 'Basic',
            //
            //     content: {
            //         type: 'div',
            //         attributes: {"data-div": "true"},
            //         style: {
            //             display: 'flex',
            //             'flex-wrap': 'wrap',
            //             padding: '10px',
            //             "row-gap": "10px",
            //             "column-gap": "10px",
            //         },
            //     }
            // });
            const block = editor.BlockManager.add('myblock', {
                // Your block properties...
                label: 'Flex Container',
                category: 'Basic',

                content: {
                    type: 'flex-container',
                    attributes: {"data-div": "true"},

                }
            });
            editor.Components.addType('flex-container', {
                model: {
                    defaults: {
                        tagName: 'div',
                        attributes: {"data-div": "true"},
                        style: {
                            'display': 'flex',
                            'gap': 'var(--gap)', // Εφαρμογή του CSS variable
                            '--cols': '3', // Ορισμός CSS variable
                            '--gap': '30px', // Ορισμός CSS variable
                            'min-height': '100px',
                        },

                    },
                },
                // Μπορείς να προσθέσεις περισσότερες επιλογές για το view αν χρειάζεται
            });

// Προσθέτουμε το νέο type ως block
            editor.BlockManager.add('flex-block', {
                label: 'Flex Container',
                content: {type: 'flex-container'}, // Χρησιμοποιούμε τον custom τύπο
                category: 'Basic',
            });


            editor.BlockManager.add('TwoColumn', {
                // Your block properties...
                label: 'Two Column',
                category: 'Basic',
                content: [
                    {
                        type: 'custom-div',
                        attributes: {"data-div": "true"},
                    },
                    {
                        attributes: {"data-div": "true"},
                        type: 'custom-div',

                    }
                ]
            });
            editor.Components.addType('custom-div', {
                model: {
                    defaults: {
                        attributes: {"data-div": "true"},
                        type: 'div',
                        tagName: 'div',
                        style: {
                            "flex-basis": 'calc(100% / var(--cols) - var(--gap) / var(--cols) * (var(--cols) - 1))',
                            column: 1
                        },
                        // Προσθέτεις το περιεχόμενο του div αν χρειάζεται

                    }
                }
            });


            // custom edit for style manager
            // const sm = editor.StyleManager;
            const sm = editor.StyleManager;
            //  sm.addSector('column-gap 2', {
            //     name: 'Column Gap',
            //     open: false,
            //     type: 'number',
            //     default: '0',
            //     units: 'px',
            //     properties: ['gap'],
            // });
            sm.addSector('Flex Gap ',[
                {
                    name: 'Column Gap',
                    open: false,
                    type: 'number',
                    units: 'px',

                    properties: ['--gap'],
                    default: 2,
                    min: 0,
                    step: 1,
                    max: 12

                },
                {
                    name: 'Columns',
                    open: false,
                    type: 'number',
                    units: 'px',

                    properties: ['--columns'],
                    default: 2,
                    min: 0,
                    step: 1,
                    max: 12

                }
            ], { at: 0 });
            // sm.addSector('column-gap', {
            //     name: 'Column & Gap',
            //     open: true, // true για να είναι ανοιχτό εξ αρχής
            //     buildProps: ['--cols', '--gap'],
            //     properties: [
            //         {
            //             name: 'Columns',
            //             property: '--cols',
            //             type: 'number',
            //             defaults: 1,
            //             min: 1,
            //             max: 12,
            //             step: 1,
            //             units: '', // Δεν χρησιμοποιούμε μονάδες για το --cols
            //         },
            //         {
            //             name: 'Gap',
            //             property: '--gap',
            //             type: 'number',
            //             defaults: 30,
            //             units: 'px',
            //             min: 0,
            //         },
            //     ],
            // });

            editor.BlockManager.add('grid-container', {
                label: 'Grid Container',
                content: `<div class="grid grid-cols-3 gap-4">
                    <div class="bg-blue-500">Column 1</div>
                    <div class="bg-red-500">Column 2</div>
                    <div class="bg-green-500">Column 3</div>
                </div>`,
                category: 'Layout',
                attributes: {class: 'gjs-block-section'},
            });

            // Define the Grid Container type
            editor.DomComponents.addType('grid', {
                model: {
                    defaults: {
                        tagName: 'div',
                        classes: ['grid'],
                        traits: [
                            {
                                type: 'select',
                                label: 'Columns',
                                name: 'grid-cols',
                                options: [
                                    {id: 'grid-cols-1', name: '1', value: 'grid-cols-1'},
                                    {id: 'grid-cols-2', name: '2', value: 'grid-cols-2'},
                                    {id: 'grid-cols-3', name: '3', value: 'grid-cols-3'},
                                    {id: 'grid-cols-4', name: '4', value: 'grid-cols-4'},
                                ],
                            },
                            {
                                type: 'select',
                                label: 'Rows',
                                name: 'grid-rows',
                                options: [
                                    {id: 'grid-rows-1', name: '1', value: 'grid-rows-1'},
                                    {id: 'grid-rows-2', name: '2', value: 'grid-rows-2'},
                                    {id: 'grid-rows-3', name: '3', value: 'grid-rows-3'},
                                ],
                            },
                            {
                                type: 'select',
                                label: 'Gap',
                                name: 'gap',
                                options: [
                                    {id: 'gap-0', name: '0', value: 'gap-0'},
                                    {id: 'gap-1', name: '1', value: 'gap-1'},
                                    {id: 'gap-2', name: '2', value: 'gap-2'},
                                    {id: 'gap-4', name: '4', value: 'gap-4'},
                                    {id: 'gap-8', name: '8', value: 'gap-8'},
                                ],
                            },
                        ],
                    },
                    init() {
                        this.on('change:attributes:grid-cols', this.updateClasses);
                        this.on('change:attributes:grid-rows', this.updateClasses);
                        this.on('change:attributes:gap', this.updateClasses);
                    },
                    updateClasses() {
                        const model = this;
                        const gridCols = model.get('attributes')['grid-cols'];
                        const gridRows = model.get('attributes')['grid-rows'];
                        const gap = model.get('attributes')['gap'];
                        const classes = [gridCols, gridRows, gap].filter(Boolean).join(' ');
                        model.addClass(classes);
                    },
                },
            });

            // Define the Grid Column Block
            editor.BlockManager.add('grid-column', {
                label: 'Grid Column',
                content: `<div class="col-span-1 bg-yellow-500">
                    Column
                </div>`,
                category: 'Layout',
                attributes: {class: 'gjs-block-section'},
            });

            // Define the Grid Column type
            editor.DomComponents.addType('grid-column', {
                model: {
                    defaults: {
                        tagName: 'div',
                        classes: ['col-span-1'],
                        traits: [
                            {
                                type: 'select',
                                label: 'Column Span',
                                name: 'col-span',
                                options: [
                                    {id: 'col-span-1', name: '1', value: 'col-span-1'},
                                    {id: 'col-span-2', name: '2', value: 'col-span-2'},
                                    {id: 'col-span-3', name: '3', value: 'col-span-3'},
                                    {id: 'col-span-4', name: '4', value: 'col-span-4'},
                                ],
                            },
                        ],
                    },
                    init() {
                        this.on('change:attributes:col-span', this.updateClasses);
                    },
                    updateClasses() {
                        const model = this;
                        const colSpan = model.get('attributes')['col-span'];
                        model.addClass(colSpan);
                    },
                },
            });


            blockManager.add('dynamicMenu',
                {
                id: 'DynamicMenu',
                label: 'DynamicMenu',
                category: 'Data',
                droppable: true,
                select: true,
                content: `

                <div data-dynamic-menu class="hidden sm:ml-6 sm:flex sm:space-x-8">
          <a href="#" class="inline-flex items-center border-b-2 border-indigo-500 px-1 pt-1 text-sm font-medium text-gray-900">ΑΡΧΙΚΗ</a>
          <a href="#" class="inline-flex items-center border-b-2 border-transparent px-1 pt-1 text-sm font-medium text-gray-500 hover:border-gray-300 hover:text-gray-700">ΠΡΟΙΟΝΤΑ</a>
          <a href="#" class="inline-flex items-center border-b-2 border-transparent px-1 pt-1 text-sm font-medium text-gray-500 hover:border-gray-300 hover:text-gray-700">ΕΠΙΚΟΙΝΩΝΙΑ</a>
        </div>
                 `,

            });
            blockManager.add('footer1', {
                id: 'footer1',
                label: 'Footer 1 ',
                category: 'Footer',
                droppable: true,
                select: true,
                content: `
               <footer class="bg-white" aria-labelledby="footer-heading">
  <h2 id="footer-heading" class="sr-only">Footer</h2>
  <div class="mx-auto max-w-7xl px-6 pb-8 pt-16 sm:pt-24 lg:px-8 lg:pt-32">
    <div class="xl:grid xl:grid-cols-3 xl:gap-8">
      <div class="space-y-8">
        <img class="h-7" src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=600" alt="Company name">
        <p class="text-sm leading-6 text-gray-600">Making the world a better place through constructing elegant hierarchies.</p>
        <div class="flex space-x-6">
          <a href="#" class="text-gray-400 hover:text-gray-500">
            <span class="sr-only">Facebook</span>
            <svg class="h-6 w-6" fill="currentColor" viewBox="0 0 24 24" aria-hidden="true">
              <path fill-rule="evenodd" d="M22 12c0-5.523-4.477-10-10-10S2 6.477 2 12c0 4.991 3.657 9.128 8.438 9.878v-6.987h-2.54V12h2.54V9.797c0-2.506 1.492-3.89 3.777-3.89 1.094 0 2.238.195 2.238.195v2.46h-1.26c-1.243 0-1.63.771-1.63 1.562V12h2.773l-.443 2.89h-2.33v6.988C18.343 21.128 22 16.991 22 12z" clip-rule="evenodd" />
            </svg>
          </a>
          <a href="#" class="text-gray-400 hover:text-gray-500">
            <span class="sr-only">Instagram</span>
            <svg class="h-6 w-6" fill="currentColor" viewBox="0 0 24 24" aria-hidden="true">
              <path fill-rule="evenodd" d="M12.315 2c2.43 0 2.784.013 3.808.06 1.064.049 1.791.218 2.427.465a4.902 4.902 0 011.772 1.153 4.902 4.902 0 011.153 1.772c.247.636.416 1.363.465 2.427.048 1.067.06 1.407.06 4.123v.08c0 2.643-.012 2.987-.06 4.043-.049 1.064-.218 1.791-.465 2.427a4.902 4.902 0 01-1.153 1.772 4.902 4.902 0 01-1.772 1.153c-.636.247-1.363.416-2.427.465-1.067.048-1.407.06-4.123.06h-.08c-2.643 0-2.987-.012-4.043-.06-1.064-.049-1.791-.218-2.427-.465a4.902 4.902 0 01-1.772-1.153 4.902 4.902 0 01-1.153-1.772c-.247-.636-.416-1.363-.465-2.427-.047-1.024-.06-1.379-.06-3.808v-.63c0-2.43.013-2.784.06-3.808.049-1.064.218-1.791.465-2.427a4.902 4.902 0 011.153-1.772A4.902 4.902 0 015.45 2.525c.636-.247 1.363-.416 2.427-.465C8.901 2.013 9.256 2 11.685 2h.63zm-.081 1.802h-.468c-2.456 0-2.784.011-3.807.058-.975.045-1.504.207-1.857.344-.467.182-.8.398-1.15.748-.35.35-.566.683-.748 1.15-.137.353-.3.882-.344 1.857-.047 1.023-.058 1.351-.058 3.807v.468c0 2.456.011 2.784.058 3.807.045.975.207 1.504.344 1.857.182.466.399.8.748 1.15.35.35.683.566 1.15.748.353.137.882.3 1.857.344 1.054.048 1.37.058 4.041.058h.08c2.597 0 2.917-.01 3.96-.058.976-.045 1.505-.207 1.858-.344.466-.182.8-.398 1.15-.748.35-.35.566-.683.748-1.15.137-.353.3-.882.344-1.857.048-1.055.058-1.37.058-4.041v-.08c0-2.597-.01-2.917-.058-3.96-.045-.976-.207-1.505-.344-1.858a3.097 3.097 0 00-.748-1.15 3.098 3.098 0 00-1.15-.748c-.353-.137-.882-.3-1.857-.344-1.023-.047-1.351-.058-3.807-.058zM12 6.865a5.135 5.135 0 110 10.27 5.135 5.135 0 010-10.27zm0 1.802a3.333 3.333 0 100 6.666 3.333 3.333 0 000-6.666zm5.338-3.205a1.2 1.2 0 110 2.4 1.2 1.2 0 010-2.4z" clip-rule="evenodd" />
            </svg>
          </a>
          <a href="#" class="text-gray-400 hover:text-gray-500">
            <span class="sr-only">X</span>
            <svg class="h-6 w-6" fill="currentColor" viewBox="0 0 24 24" aria-hidden="true">
              <path d="M13.6823 10.6218L20.2391 3H18.6854L12.9921 9.61788L8.44486 3H3.2002L10.0765 13.0074L3.2002 21H4.75404L10.7663 14.0113L15.5685 21H20.8131L13.6819 10.6218H13.6823ZM11.5541 13.0956L10.8574 12.0991L5.31391 4.16971H7.70053L12.1742 10.5689L12.8709 11.5655L18.6861 19.8835H16.2995L11.5541 13.096V13.0956Z" />
            </svg>
          </a>
          <a href="#" class="text-gray-400 hover:text-gray-500">
            <span class="sr-only">GitHub</span>
            <svg class="h-6 w-6" fill="currentColor" viewBox="0 0 24 24" aria-hidden="true">
              <path fill-rule="evenodd" d="M12 2C6.477 2 2 6.484 2 12.017c0 4.425 2.865 8.18 6.839 9.504.5.092.682-.217.682-.483 0-.237-.008-.868-.013-1.703-2.782.605-3.369-1.343-3.369-1.343-.454-1.158-1.11-1.466-1.11-1.466-.908-.62.069-.608.069-.608 1.003.07 1.531 1.032 1.531 1.032.892 1.53 2.341 1.088 2.91.832.092-.647.35-1.088.636-1.338-2.22-.253-4.555-1.113-4.555-4.951 0-1.093.39-1.988 1.029-2.688-.103-.253-.446-1.272.098-2.65 0 0 .84-.27 2.75 1.026A9.564 9.564 0 0112 6.844c.85.004 1.705.115 2.504.337 1.909-1.296 2.747-1.027 2.747-1.027.546 1.379.202 2.398.1 2.651.64.7 1.028 1.595 1.028 2.688 0 3.848-2.339 4.695-4.566 4.943.359.309.678.92.678 1.855 0 1.338-.012 2.419-.012 2.747 0 .268.18.58.688.482A10.019 10.019 0 0022 12.017C22 6.484 17.522 2 12 2z" clip-rule="evenodd" />
            </svg>
          </a>
          <a href="#" class="text-gray-400 hover:text-gray-500">
            <span class="sr-only">YouTube</span>
            <svg class="h-6 w-6" fill="currentColor" viewBox="0 0 24 24" aria-hidden="true">
              <path fill-rule="evenodd" d="M19.812 5.418c.861.23 1.538.907 1.768 1.768C21.998 8.746 22 12 22 12s0 3.255-.418 4.814a2.504 2.504 0 0 1-1.768 1.768c-1.56.419-7.814.419-7.814.419s-6.255 0-7.814-.419a2.505 2.505 0 0 1-1.768-1.768C2 15.255 2 12 2 12s0-3.255.417-4.814a2.507 2.507 0 0 1 1.768-1.768C5.744 5 11.998 5 11.998 5s6.255 0 7.814.418ZM15.194 12 10 15V9l5.194 3Z" clip-rule="evenodd" />
            </svg>
          </a>
        </div>
      </div>
      <div class="mt-16 grid grid-cols-2 gap-8 xl:col-span-2 xl:mt-0">
        <div class="md:grid md:grid-cols-2 md:gap-8">
          <div>
            <h3 class="text-sm font-semibold leading-6 text-gray-900">Solutions</h3>
            <ul role="list" class="mt-6 space-y-4">
              <li>
                <a href="#" class="text-sm leading-6 text-gray-600 hover:text-gray-900">Marketing</a>
              </li>
              <li>
                <a href="#" class="text-sm leading-6 text-gray-600 hover:text-gray-900">Analytics</a>
              </li>
              <li>
                <a href="#" class="text-sm leading-6 text-gray-600 hover:text-gray-900">Commerce</a>
              </li>
              <li>
                <a href="#" class="text-sm leading-6 text-gray-600 hover:text-gray-900">Insights</a>
              </li>
            </ul>
          </div>
          <div class="mt-10 md:mt-0">
            <h3 class="text-sm font-semibold leading-6 text-gray-900">Support</h3>
            <ul role="list" class="mt-6 space-y-4">
              <li>
                <a href="#" class="text-sm leading-6 text-gray-600 hover:text-gray-900">Pricing</a>
              </li>
              <li>
                <a href="#" class="text-sm leading-6 text-gray-600 hover:text-gray-900">Documentation</a>
              </li>
              <li>
                <a href="#" class="text-sm leading-6 text-gray-600 hover:text-gray-900">Guides</a>
              </li>
              <li>
                <a href="#" class="text-sm leading-6 text-gray-600 hover:text-gray-900">API Status</a>
              </li>
            </ul>
          </div>
        </div>
        <div class="md:grid md:grid-cols-2 md:gap-8">
          <div>
            <h3 class="text-sm font-semibold leading-6 text-gray-900">Company</h3>
            <ul role="list" class="mt-6 space-y-4">
              <li>
                <a href="#" class="text-sm leading-6 text-gray-600 hover:text-gray-900">About</a>
              </li>
              <li>
                <a href="#" class="text-sm leading-6 text-gray-600 hover:text-gray-900">Blog</a>
              </li>
              <li>
                <a href="#" class="text-sm leading-6 text-gray-600 hover:text-gray-900">Jobs</a>
              </li>
              <li>
                <a href="#" class="text-sm leading-6 text-gray-600 hover:text-gray-900">Press</a>
              </li>
              <li>
                <a href="#" class="text-sm leading-6 text-gray-600 hover:text-gray-900">Partners</a>
              </li>
            </ul>
          </div>
          <div class="mt-10 md:mt-0">
            <h3 class="text-sm font-semibold leading-6 text-gray-900">Legal</h3>
            <ul role="list" class="mt-6 space-y-4">
              <li>
                <a href="#" class="text-sm leading-6 text-gray-600 hover:text-gray-900">Claim</a>
              </li>
              <li>
                <a href="#" class="text-sm leading-6 text-gray-600 hover:text-gray-900">Privacy</a>
              </li>
              <li>
                <a href="#" class="text-sm leading-6 text-gray-600 hover:text-gray-900">Terms</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="mt-16 border-t border-gray-900/10 pt-8 sm:mt-20 lg:mt-24">
      <p class="text-xs leading-5 text-gray-500">&copy; 2020 Your Company, Inc. All rights reserved.</p>
    </div>
  </div>
</footer>

                 `,

            });


            // dynamic cat
            editor.Components.addType('dynamic-categories', {
                model: {
                    defaults: {
                        // Βασικές ρυθμίσεις
                        tagName: 'div',
                        droppable: false,
                        editable: false,
                        categories: [], // Θα αποθηκεύσουμε τις κατηγορίες εδώ
                        subcategories: [], // Θα αποθηκεύσουμε τις υποκατηγορίες εδώ

                        traits: [
                            {
                                type: 'select',
                                label: 'Category',
                                name: 'category',
                                options: [],
                                changeProp: 1,
                            },
                        ],

                        script: function () {
                            // Αυτό το script θα εκτελείται στο frontend
                            var el = this.el;

                            function renderSubcategories(subcategories) {
                                el.innerHTML = ''; // Καθαρίζει το εσωτερικό του element
                                if (subcategories.length > 0) {
                                    var ul = document.createElement('ul');
                                    subcategories.forEach(function (subcategory) {
                                        var li = document.createElement('li');
                                        li.innerHTML = subcategory.name;
                                        ul.appendChild(li);
                                    });
                                    el.appendChild(ul);
                                } else {
                                    el.innerHTML = 'No subcategories found.';
                                }
                            }

                            // Όταν επιλέγεται μια κατηγορία, το backend πρέπει να μας στέλνει τις υποκατηγορίες
                            var category = this.get('category');
                            if (category) {
                                fetch(`/get-subcategories?category=${category}`)
                                    .then((res) => res.json())
                                    .then((subcategories) => {
                                        renderSubcategories(subcategories);
                                    })
                                    .catch((err) => console.error('Error fetching subcategories', err));
                            }
                        },
                    },

                    init() {
                        // Όταν δημιουργείται το component, κάνουμε call στο /get-categories
                        fetch('/manage/categories_list_json', {
                            method: 'GET',
                            headers: {
                                'Content-Type': 'application/json',
                                'X-CSRF-TOKEN': '{{ csrf_token() }}',
                            }
                        })
                            .then((res) => res.json())
                            .then((categories) => {
                                const traits = this.get('traits');
                                const categoryTrait = traits.find((t) => t.get('name') === 'category');
                                const options = categories.map((cat) => ({
                                    id: cat.id,
                                    name: cat.title.el,
                                    value: cat.id,
                                }));
                                categoryTrait.set('options', options);

                                this.set('categories', categories); // Αποθηκεύουμε τις κατηγορίες στο μοντέλο
                            })
                            .catch((err) => console.error('Error fetching categories', err));

                        // Παρακολουθούμε τις αλλαγές στο category trait
                        this.on('change:category', this.fetchSubcategories);
                    },

                    fetchSubcategories() {
                        const categoryId = this.get('category');
                        if (categoryId) {
                            fetch(`/manage/categories_list_json?parent_id=${categoryId}`, {
                                method: 'GET',
                                headers: {
                                    'Content-Type': 'application/json',
                                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                                }
                            })
                                .then((res) => res.json())
                                .then((subcategories) => {
                                    this.set('subcategories', subcategories);
                                    this.view.render(); // Επανασχεδιάζουμε το component με τις νέες υποκατηγορίες
                                })
                                .catch((err) => console.error('Error fetching subcategories', err));
                        }
                    },
                },

                view: {
                    onRender() {
                        const subcategories = this.model.get('subcategories');
                        if (subcategories.length > 0) {
                            const ul = document.createElement('ul');
                            subcategories.forEach((subcategory) => {
                                const li = document.createElement('li');
                                // add class to li
                                li.classList.add('p-2', 'another-class');

                                li.innerHTML = subcategory.title.el;
                                li.addEventListener('focusout', function () {
                                    const updatedText = li.innerHTML;
                                    console.log('Updated content:', updatedText);
                                    // Εδώ μπορείς να προσθέσεις την λογική αποθήκευσης ή ενημέρωσης
                                });


                                ul.appendChild(li);
                            });
                            this.el.innerHTML = ''; // Καθαρίζουμε το εσωτερικό του element
                            this.el.appendChild(ul); // Προσθέτουμε τη λίστα υποκατηγοριών
                        } else {
                            this.el.innerHTML = 'No subcategories selected';
                        }
                    },
                },
            });

// Προσθέτουμε το block στο Block Manager
            editor.BlockManager.add('dynamic-categories-block', {
                label: 'Dynamic Categories',
                category: 'Basic',
                content: {
                    type: 'dynamic-categories',
                },
            });
            // end dynamic cat


            blockManager.add('grid3', {
                id: 'grid3',
                label: 'Grid 3 columns',
                category: 'Basic',
                droppable: true,
                select: true,
                content: {
                    type: 'div',
                    content: '',
                    attributes: {class: 'grid grid-cols-3 gap-4  p-2'},

                },
            });
            blockManager.add('grid4', {
                id: 'grid4',
                label: 'Grid 4 columns',
                category: 'Basic',
                droppable: true,
                select: true,
                content: {
                    type: 'div',
                    content: '',
                    attributes: {class: 'grid grid-cols-4 gap-4  '},

                },
            });

            // just a div

            const staticMenuScripts = function () {
                // `this` is bound to the component element
                console.log('the element', this);
                document.querySelectorAll('.product-btn').forEach(function (button) {
                    button.addEventListener('click', function () {
                        const flyoutMenu = this.nextElementSibling;
                        flyoutMenu.classList.toggle('show-menu');
                    });
                });

                // Προαιρετικά, κλείσιμο όταν κάνεις κλικ εκτός του μενού
                document.addEventListener('click', function (event) {
                    document.querySelectorAll('.product-flyout').forEach(function (flyoutMenu) {
                        const productButton = flyoutMenu.previousElementSibling;

                        if (!productButton.contains(event.target) && !flyoutMenu.contains(event.target)) {
                            flyoutMenu.classList.remove('show-menu');
                        }
                    });
                });
                document.querySelector('.hidden.lg\\:flex').classList.add('click-enabled');
                document.querySelector('.hidden.lg\\:flex').classList.remove('hover-enabled');

            };
            editor.BlockManager.add('Static Menu', {
                id: 'staticMenu',
                label: 'Static Menu',
                // Select the component once it's dropped
                select: true,
                category: 'Data',
                editable: true,
                // You can pass components as a JSON instead of a simple HTML string,
                // in this case we also use a defined component type `image`
                // This triggers `active` event on dropped components and the `image`
                // reacts by opening the AssetManager
                activate: true,
                content: {
                    editable: true,
                    type: 'static-menu',
                    content: `<!-- hey--><div  data-gjs-removable="true" data-gjs-selectable="false"  class="relative hover-container">
                        <button data-gjs-editable="true" type="button" class="product-btn flex items-center gap-x-1 text-sm font-semibold leading-6 text-gray-900">
                            Product
                            <svg class="h-5 w-5 flex-none text-gray-400" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                <path fill-rule="evenodd" d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z" clip-rule="evenodd" />
                            </svg>
                        </button>
                        <div data-gjs-editable="true" class="product-flyout absolute -left-8 top-4 z-10 mt-2 w-screen max-w-md overflow-hidden rounded-3xl bg-white shadow-lg ring-1 ring-gray-900/5 opacity-0 translate-y-1 transition-all ease-in-out duration-200 pointer-events-none">
                            <div class="p-4">
                                <div data-gjs-contentEditable="true" class="group relative flex items-center gap-x-6 rounded-lg p-4 text-sm leading-6 hover:bg-gray-50">
                                    <div class="flex h-11 w-11 flex-none items-center justify-center rounded-lg bg-gray-50 group-hover:bg-white">
                                        <svg class="h-6 w-6 text-gray-600 group-hover:text-indigo-600" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M10.5 6a7.5 7.5 0 107.5 7.5h-7.5V6z" />
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 10.5H21A7.5 7.5 0 0013.5 3v7.5z" />
                                        </svg>
                                    </div>
                                    <div class="flex-auto">
                                        <a href="#" class="block font-semibold text-gray-900">
                                            Analytics
                                            <span class="absolute inset-0"></span>
                                        </a>
                                        <p class="mt-1 text-gray-600">Get a better understanding of your traffic</p>
                                    </div>
                                </div>
                                <div class="group relative flex items-center gap-x-6 rounded-lg p-4 text-sm leading-6 hover:bg-gray-50">
                                    <div class="flex h-11 w-11 flex-none items-center justify-center rounded-lg bg-gray-50 group-hover:bg-white">
                                        <svg class="h-6 w-6 text-gray-600 group-hover:text-indigo-600" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M15.042 21.672L13.684 16.6m0 0l-2.51 2.225.569-9.47 5.227 7.917-3.286-.672zM12 2.25V4.5m5.834.166l-1.591 1.591M20.25 10.5H18M7.757 14.743l-1.59 1.59M6 10.5H3.75m4.007-4.243l-1.59-1.59" />
                                        </svg>
                                    </div>
                                    <div class="flex-auto">
                                        <a href="#" class="block font-semibold text-gray-900">
                                            Engagement
                                            <span class="absolute inset-0"></span>
                                        </a>
                                        <p class="mt-1 text-gray-600">Speak directly to your customers</p>
                                    </div>
                                </div>
                                <div class="group relative flex items-center gap-x-6 rounded-lg p-4 text-sm leading-6 hover:bg-gray-50">
                                    <div class="flex h-11 w-11 flex-none items-center justify-center rounded-lg bg-gray-50 group-hover:bg-white">
                                        <svg class="h-6 w-6 text-gray-600 group-hover:text-indigo-600" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M7.864 4.243A7.5 7.5 0 0119.5 10.5c0 2.92-.556 5.709-1.568 8.268M5.742 6.364A7.465 7.465 0 004.5 10.5a7.464 7.464 0 01-1.15 3.993m1.989 3.559A11.209 11.209 0 008.25 10.5a3.75 3.75 0 117.5 0c0 .527-.021 1.049-.064 1.565M12 10.5a14.94 14.94 0 01-3.6 9.75m6.633-4.596a18.666 18.666 0 01-2.485 5.33" />
                                        </svg>
                                    </div>
                                    <div class="flex-auto">
                                        <a href="#" class="block font-semibold text-gray-900">
                                            Security
                                            <span class="absolute inset-0"></span>
                                        </a>
                                        <p class="mt-1 text-gray-600">Your customers’ data will be safe and secure</p>
                                    </div>
                                </div>
                                <div class="group relative flex items-center gap-x-6 rounded-lg p-4 text-sm leading-6 hover:bg-gray-50">
                                    <div class="flex h-11 w-11 flex-none items-center justify-center rounded-lg bg-gray-50 group-hover:bg-white">
                                        <svg class="h-6 w-6 text-gray-600 group-hover:text-indigo-600" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 16.875h3.375m0 0h3.375m-3.375 0V13.5m0 3.375v3.375M6 10.5h2.25a2.25 2.25 0 002.25-2.25V6a2.25 2.25 0 00-2.25-2.25H6A2.25 2.25 0 003.75 6v2.25A2.25 2.25 0 006 10.5zm0 9.75h2.25A2.25 2.25 0 0010.5 18v-2.25a2.25 2.25 0 00-2.25-2.25H6a2.25 2.25 0 00-2.25 2.25V18A2.25 2.25 0 006 20.25zm9.75-9.75H18a2.25 2.25 0 002.25-2.25V6A2.25 2.25 0 0018 3.75h-2.25A2.25 2.25 0 0013.5 6v2.25a2.25 2.25 0 002.25 2.25z" />
                                        </svg>
                                    </div>
                                    <div class="flex-auto">
                                        <a href="#" class="block font-semibold text-gray-900">
                                            Integrations
                                            <span class="absolute inset-0"></span>
                                        </a>
                                        <p class="mt-1 text-gray-600">Connect with third-party tools</p>
                                    </div>
                                </div>
                                <div class="group relative flex items-center gap-x-6 rounded-lg p-4 text-sm leading-6 hover:bg-gray-50">
                                    <div class="flex h-11 w-11 flex-none items-center justify-center rounded-lg bg-gray-50 group-hover:bg-white">
                                        <svg class="h-6 w-6 text-gray-600 group-hover:text-indigo-600" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M16.023 9.348h4.992v-.001M2.985 19.644v-4.992m0 0h4.992m-4.993 0l3.181 3.183a8.25 8.25 0 0013.803-3.7M4.031 9.865a8.25 8.25 0 0113.803-3.7l3.181 3.182m0-4.991v4.99" />
                                        </svg>
                                    </div>
                                    <div class="flex-auto">
                                        <a href="#" class="block font-semibold text-gray-900">
                                            Automations
                                            <span class="absolute inset-0"></span>
                                        </a>
                                        <p class="mt-1 text-gray-600">Build strategic funnels that will convert</p>
                                    </div>
                                </div>
                            </div>
                            <div class="grid grid-cols-2 divide-x divide-gray-900/5 bg-gray-50">
                                <a href="#" class="flex items-center justify-center gap-x-2.5 p-3 text-sm font-semibold leading-6 text-gray-900 hover:bg-gray-100">
                                    <svg class="h-5 w-5 flex-none text-gray-400" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path fill-rule="evenodd" d="M2 10a8 8 0 1116 0 8 8 0 01-16 0zm6.39-2.908a.75.75 0 01.766.027l3.5 2.25a.75.75 0 010 1.262l-3.5 2.25A.75.75 0 018 12.25v-4.5a.75.75 0 01.39-.658z" clip-rule="evenodd" />
                                    </svg>
                                    Watch demo
                                </a>
                                <a href="#" class="flex items-center justify-center gap-x-2.5 p-3 text-sm font-semibold leading-6 text-gray-900 hover:bg-gray-100">
                                    <svg class="h-5 w-5 flex-none text-gray-400" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path fill-rule="evenodd" d="M2 3.5A1.5 1.5 0 013.5 2h1.148a1.5 1.5 0 011.465 1.175l.716 3.223a1.5 1.5 0 01-1.052 1.767l-.933.267c-.41.117-.643.555-.48.95a11.542 11.542 0 006.254 6.254c.395.163.833-.07.95-.48l.267-.933a1.5 1.5 0 011.767-1.052l3.223.716A1.5 1.5 0 0118 15.352V16.5a1.5 1.5 0 01-1.5 1.5H15c-1.149 0-2.263-.15-3.326-.43A13.022 13.022 0 012.43 8.326 13.019 13.019 0 012 5V3.5z" clip-rule="evenodd" />
                                    </svg>
                                    Contact sales
                                </a>
                            </div>
                        </div>
                    </div>`,
                },


            })
            editor.Components.addType('static-menu', {
                model: {
                    defaults: {
                        components: `
                        <div   data-gjs-selectable="false"  class="relative hover-container ">
                        <a  href=""   class="product-btn flex items-center gap-x-1 text-sm font-semibold leading-6 text-gray-900">

                            Menu Title
                            <svg class="h-5 w-5 flex-none text-gray-400" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                <path fill-rule="evenodd" d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z" clip-rule="evenodd" />
                            </svg>

                        </a>
                        <div   class="product-flyout absolute p-2 -left-8 top-4 z-10 mt-2 w-screen max-w-md overflow-hidden rounded-3xl bg-white shadow-lg ring-1 ring-gray-900/5 opacity-0 translate-y-1 transition-all ease-in-out duration-200 pointer-events-none">
                            <div class="p-4">
                                <div data-gjs-contentEditable="true" class="group relative flex items-center gap-x-6 rounded-lg p-4 text-sm leading-6 hover:bg-gray-50">
                                    <div class="flex h-11 w-11 flex-none items-center justify-center rounded-lg bg-gray-50 group-hover:bg-white">
                                        <svg class="h-6 w-6 text-gray-600 group-hover:text-indigo-600" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M10.5 6a7.5 7.5 0 107.5 7.5h-7.5V6z" />
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 10.5H21A7.5 7.5 0 0013.5 3v7.5z" />
                                        </svg>
                                    </div>
                                    <div class="flex-auto">
                                        <a href="#" class="block font-semibold text-gray-900">
                                            Analytics

                                        </a>
                                        <p class="mt-1 text-gray-600">Get a better understanding of your traffic</p>
                                    </div>
                                </div>

                            </div>
                            <div class="grid grid-cols-2 divide-x divide-gray-900/5 bg-gray-50">
                                <a href="#" class="flex items-center justify-center gap-x-2.5 p-3 text-sm font-semibold leading-6 text-gray-900 hover:bg-gray-100">
                                    <svg class="h-5 w-5 flex-none text-gray-400" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path fill-rule="evenodd" d="M2 10a8 8 0 1116 0 8 8 0 01-16 0zm6.39-2.908a.75.75 0 01.766.027l3.5 2.25a.75.75 0 010 1.262l-3.5 2.25A.75.75 0 018 12.25v-4.5a.75.75 0 01.39-.658z" clip-rule="evenodd" />
                                    </svg>
                                    Watch demo
                                </a>
                                <a href="#" class="flex items-center justify-center gap-x-2.5 p-3 text-sm font-semibold leading-6 text-gray-900 hover:bg-gray-100">
                                    <svg class="h-5 w-5 flex-none text-gray-400" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path fill-rule="evenodd" d="M2 3.5A1.5 1.5 0 013.5 2h1.148a1.5 1.5 0 011.465 1.175l.716 3.223a1.5 1.5 0 01-1.052 1.767l-.933.267c-.41.117-.643.555-.48.95a11.542 11.542 0 006.254 6.254c.395.163.833-.07.95-.48l.267-.933a1.5 1.5 0 011.767-1.052l3.223.716A1.5 1.5 0 0118 15.352V16.5a1.5 1.5 0 01-1.5 1.5H15c-1.149 0-2.263-.15-3.326-.43A13.022 13.022 0 012.43 8.326 13.019 13.019 0 012 5V3.5z" clip-rule="evenodd" />
                                    </svg>
                                    Contact sales
                                </a>
                            </div>
                        </div>
                    </div>
      `,

                        removable: true,
                        draggable: true,
                        badgable: true,
                        stylable: true,
                        highlightable: false,
                        selectable: true,
                        copyable: true,
                        resizable: true,
                        editable: true,
                        hoverable: true,
                        type: 'div',
                        attributes: { // Default attributes
                            type: 'text',
                            name: 'default-name',
                            placeholder: 'Insert text here',
                        },
                        script: staticMenuScripts,
                        // Add some style, just to make the component visible
                        styles: `
                        .product-flyout {
                             opacity: 0;
                             transform: translateY(1rem);
                             pointer-events: none;
                             transition: opacity 0.2s ease-in-out, transform 0.2s ease-in-out;
                             transition-delay: 200ms;
                         }
                         .hover-enabled .hover-container:hover .product-flyout {
                             opacity: 1;
                             transform: translateY(0);
                             pointer-events: auto;
                             transition-delay: 200ms;
                         }

                         .click-enabled .show-menu {
                             opacity: 1;
                             transform: translateY(0);
                             pointer-events: auto;
                         }
                         .translate-x-0 {
                             transform: translateX(0);
                         }
                        `,
                        toHTML(opts) {
                            const editable = this.findType('editable')[0];
                            return editable ? editable.toHTML(opts) : '';
                        },
                    },
                    view: {
                        events: {
                            dblclick: 'onActive',
                            focusout: 'onDisable',
                        },
                        onActive() {
                            // Προσθέτουμε μια μικρή καθυστέρηση για να διασφαλίσουμε ότι τα στοιχεία είναι πλήρως προσβάσιμα
                            setTimeout(() => {
                                const editableElements = this.el.querySelectorAll('[data-gjs-editable="true"]');

                                if (editableElements.length === 0) {
                                    console.log('No editable elements found');
                                } else {
                                    editableElements.forEach(el => {
                                        el.contentEditable = true;
                                        console.log('Found editable element: ', el);
                                    });
                                }
                            }, 0); // Η καθυστέρηση μπορεί να ρυθμιστεί αν χρειαστεί
                        },
                        onDisable() {
                            // Απενεργοποίηση contentEditable όταν χάνεται η εστίαση
                            this.el.querySelectorAll('[data-gjs-editable="true"]').forEach(el => {
                                el.contentEditable = false;
                            });

                            // Ενημέρωση του μοντέλου με το νέο περιεχόμενο
                            const {el, model} = this;
                            model.set('content', el.innerHTML);
                        },
                    },
                    init() {

                    },
                    handlePropChange() {
                        const {someprop} = this.props();
                        console.log('New value of someprop: ', someprop);
                    },
                    handleAttrChange() {
                        console.log('Attributes updated: ', this.getAttributes());
                    },
                    handleTitleChange() {
                        console.log('Attribute title updated: ', this.getAttributes().title);
                    },
                }

            });

            const script = function () {

                // `this` is bound to the component element
                console.log('the element', this);
            };

// Define a new custom component
            editor.Components.addType('comp-with-js', {
                model: {
                    defaults: {
                        script,
                        // Add some style, just to make the component visible
                        style: {
                            width: '100px',
                            height: '100px',
                            background: 'red',
                        }
                    }
                }
            });

// Create a block for the component, so we can drop it easily
            editor.Blocks.add('test-block', {
                label: 'Test block',
                attributes: {class: 'fa fa-text'},
                content: {type: 'comp-with-js'},
            });

            // repaeted list
            editor.Components.addType('repeating-list', {
                model: {
                    defaults: {
                        tagName: 'span',
                        classes: ['repeating-list'],
                        content: '<span class="item">Item 1</span>',
                        traits: [
                            {
                                type: 'text',
                                label: 'List Items',
                                name: 'items',
                                changeProp: 1,
                                onChange({target}) {
                                    const value = target.value;
                                    const items = value.split('\n').map(item => `<div class="item">${item}</div>`).join('');
                                    this.set({content: items});
                                    this.trigger('change:items');
                                },
                            },
                        ],
                    },
                },
            });

            // Προσθήκη του custom block για τη λίστα
            editor.BlockManager.add('repeating-list-block', {
                label: 'Repeating List',
                content: {
                    type: 'repeating-list',
                    content: '<div class="item">Item 1</div>',
                },
                category: 'List',
            });

            // Προσθήκη CSS για τα items της λίστας
            editor.addComponents(`
            <style>
                .repeating-list .item {
                    padding: 10px;
                    border: 1px solid #ccc;
                    margin-bottom: 5px;
                }
            </style>
        `);

            editor.BlockManager.add('SearchComponent', {
                id: 'search',
                label: 'Search Field',
                draggable: true,
                droppable: true,
                category: 'Data',
                content: {
                    type: 'custom-component',
                    content: `
                     <div data-search-input class="w-full sm:max-w-xs">
          <label for="search" class="sr-only">Search</label>
          <div class="relative">
            <div class="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
              <svg class="h-5 w-5 text-gray-400" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M9 3.5a5.5 5.5 0 100 11 5.5 5.5 0 000-11zM2 9a7 7 0 1112.452 4.391l3.328 3.329a.75.75 0 11-1.06 1.06l-3.329-3.328A7 7 0 012 9z" clip-rule="evenodd" />
              </svg>
            </div>
            <input id="search" name="search" class="block w-full rounded-md border-0 bg-white py-1.5 pl-10 pr-3 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" placeholder="Search" type="search">
          </div>
        </div>
        `,
                },
            });
            editor.BlockManager.add('FeatureSection', {
                id: 'FeatureSection',
                label: 'Feature Section',
                category: 'Sections',
                media: '<svg xmlns="http://www.w3.org/2000/svg" width="42" height="42" fill="currentColor" class="bi bi-blockquote-left" viewBox="0 0 16 16"><path d="M2.5 3a.5.5 0 0 0 0 1h11a.5.5 0 0 0 0-1zm5 3a.5.5 0 0 0 0 1h6a.5.5 0 0 0 0-1zm0 3a.5.5 0 0 0 0 1h6a.5.5 0 0 0 0-1zm-5 3a.5.5 0 0 0 0 1h11a.5.5 0 0 0 0-1zm.79-5.373q.168-.117.444-.275L3.524 6q-.183.111-.452.287-.27.176-.51.428a2.4 2.4 0 0 0-.398.562Q2 7.587 2 7.969q0 .54.217.873.217.328.72.328.322 0 .504-.211a.7.7 0 0 0 .188-.463q0-.345-.211-.521-.205-.182-.568-.182h-.282q.036-.305.123-.498a1.4 1.4 0 0 1 .252-.37 2 2 0 0 1 .346-.298zm2.167 0q.17-.117.445-.275L5.692 6q-.183.111-.452.287-.27.176-.51.428a2.4 2.4 0 0 0-.398.562q-.165.31-.164.692 0 .54.217.873.217.328.72.328.322 0 .504-.211a.7.7 0 0 0 .188-.463q0-.345-.211-.521-.205-.182-.568-.182h-.282a1.8 1.8 0 0 1 .118-.492q.087-.194.257-.375a2 2 0 0 1 .346-.3z"/></svg>',
                draggable: true,
                droppable: true,
                attributes: {class: 'custom-grid'}, // Βεβαιώσου ότι το Grid είναι droppable
                content: `<div class="overflow-hidden bg-white py-24 sm:py-32">
  <div class="mx-auto max-w-7xl px-6 lg:px-8">
    <div class="mx-auto grid max-w-2xl grid-cols-1 gap-x-8 gap-y-16 sm:gap-y-20 lg:mx-0 lg:max-w-none lg:grid-cols-2">
      <div class="lg:pr-8 lg:pt-4">
        <div class="lg:max-w-lg">
          <h2 class="text-base font-semibold leading-7 text-indigo-600">Deploy faster</h2>
          <p class="mt-2 text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">A better workflow</p>
          <p class="mt-6 text-lg leading-8 text-gray-600">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Maiores impedit perferendis suscipit eaque, iste dolor cupiditate blanditiis ratione.</p>
          <dl class="mt-10 max-w-xl space-y-8 text-base leading-7 text-gray-600 lg:max-w-none">
            <div class="relative pl-9">
              <dt class="inline font-semibold text-gray-900">
                <svg class="absolute left-1 top-1 h-5 w-5 text-indigo-600" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M5.5 17a4.5 4.5 0 01-1.44-8.765 4.5 4.5 0 018.302-3.046 3.5 3.5 0 014.504 4.272A4 4 0 0115 17H5.5zm3.75-2.75a.75.75 0 001.5 0V9.66l1.95 2.1a.75.75 0 101.1-1.02l-3.25-3.5a.75.75 0 00-1.1 0l-3.25 3.5a.75.75 0 101.1 1.02l1.95-2.1v4.59z" clip-rule="evenodd" />
                </svg>
                Push to deploy.
              </dt>
              <dd class="inline">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Maiores impedit perferendis suscipit eaque, iste dolor cupiditate blanditiis ratione.</dd>
            </div>
            <div class="relative pl-9">
              <dt class="inline font-semibold text-gray-900">
                <svg class="absolute left-1 top-1 h-5 w-5 text-indigo-600" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M10 1a4.5 4.5 0 00-4.5 4.5V9H5a2 2 0 00-2 2v6a2 2 0 002 2h10a2 2 0 002-2v-6a2 2 0 00-2-2h-.5V5.5A4.5 4.5 0 0010 1zm3 8V5.5a3 3 0 10-6 0V9h6z" clip-rule="evenodd" />
                </svg>
                SSL certificates.
              </dt>
              <dd class="inline">Anim aute id magna aliqua ad ad non deserunt sunt. Qui irure qui lorem cupidatat commodo.</dd>
            </div>
            <div class="relative pl-9">
              <dt class="inline font-semibold text-gray-900">
                <svg class="absolute left-1 top-1 h-5 w-5 text-indigo-600" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path d="M4.632 3.533A2 2 0 016.577 2h6.846a2 2 0 011.945 1.533l1.976 8.234A3.489 3.489 0 0016 11.5H4c-.476 0-.93.095-1.344.267l1.976-8.234z" />
                  <path fill-rule="evenodd" d="M4 13a2 2 0 100 4h12a2 2 0 100-4H4zm11.24 2a.75.75 0 01.75-.75H16a.75.75 0 01.75.75v.01a.75.75 0 01-.75.75h-.01a.75.75 0 01-.75-.75V15zm-2.25-.75a.75.75 0 00-.75.75v.01c0 .414.336.75.75.75H13a.75.75 0 00.75-.75V15a.75.75 0 00-.75-.75h-.01z" clip-rule="evenodd" />
                </svg>
                Database backups.
              </dt>
              <dd class="inline">Ac tincidunt sapien vehicula erat auctor pellentesque rhoncus. Et magna sit morbi lobortis.</dd>
            </div>
          </dl>
        </div>
      </div>
      <img src="https://tailwindui.com/img/component-images/dark-project-app-screenshot.png" alt="Product screenshot" class="w-[48rem] max-w-none rounded-xl shadow-xl ring-1 ring-gray-400/10 sm:w-[57rem] md:-ml-4 lg:-ml-0" width="2432" height="1442">
    </div>
  </div>
</div>
`,
            });
            editor.BlockManager.add('FeatureSection2', {
                id: 'FeatureSection',
                label: 'Feature Section 2',
                media: '<svg xmlns="http://www.w3.org/2000/svg" width="46" height="46" fill="currentColor" class="bi bi-card-text" viewBox="0 0 16 16"> <path d="M14.5 3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2z"/> <path d="M3 5.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5M3 8a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9A.5.5 0 0 1 3 8m0 2.5a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5"/> </svg>',
                draggable: true,
                droppable: true,
                category: 'Sections',
                attributes: {class: 'custom-grid'}, // Βεβαιώσου ότι το Grid είναι droppable

                content: {
                    type: 'custom-component',
                    content: `<div class="bg-white py-24 sm:py-32">
  <div class="mx-auto max-w-7xl px-6 lg:px-8">
    <div class="mx-auto max-w-2xl sm:text-center">
      <h2 class="text-base font-semibold leading-7 text-indigo-600">Everything you need</h2>
      <p class="mt-2 text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">No server? No problem.</p>
      <p class="mt-6 text-lg leading-8 text-gray-600">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Maiores impedit perferendis suscipit eaque, iste dolor cupiditate blanditiis.</p>
    </div>
  </div>
  <div class="relative overflow-hidden pt-16">
    <div class="mx-auto max-w-7xl px-6 lg:px-8">
      <img src="https://tailwindui.com/img/component-images/project-app-screenshot.png" alt="App screenshot" class="mb-[-12%] rounded-xl shadow-2xl ring-1 ring-gray-900/10" width="2432" height="1442">
      <div class="relative" aria-hidden="true">
        <div class="absolute -inset-x-20 bottom-0 bg-gradient-to-t from-white pt-[7%]"></div>
      </div>
    </div>
  </div>
  <div class="mx-auto mt-16 max-w-7xl px-6 sm:mt-20 md:mt-24 lg:px-8">
    <dl class="mx-auto grid max-w-2xl grid-cols-1 gap-x-6 gap-y-10 text-base leading-7 text-gray-600 sm:grid-cols-2 lg:mx-0 lg:max-w-none lg:grid-cols-3 lg:gap-x-8 lg:gap-y-16">
      <div class="relative pl-9">
        <dt class="inline font-semibold text-gray-900">
          <svg class="absolute left-1 top-1 h-5 w-5 text-indigo-600" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
            <path fill-rule="evenodd" d="M5.5 17a4.5 4.5 0 01-1.44-8.765 4.5 4.5 0 018.302-3.046 3.5 3.5 0 014.504 4.272A4 4 0 0115 17H5.5zm3.75-2.75a.75.75 0 001.5 0V9.66l1.95 2.1a.75.75 0 101.1-1.02l-3.25-3.5a.75.75 0 00-1.1 0l-3.25 3.5a.75.75 0 101.1 1.02l1.95-2.1v4.59z" clip-rule="evenodd" />
          </svg>
          Push to deploy.
        </dt>
        <dd class="inline">Lorem ipsum, dolor sit amet consectetur adipisicing elit aute id magna.</dd>
      </div>
      <div class="relative pl-9">
        <dt class="inline font-semibold text-gray-900">
          <svg class="absolute left-1 top-1 h-5 w-5 text-indigo-600" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
            <path fill-rule="evenodd" d="M10 1a4.5 4.5 0 00-4.5 4.5V9H5a2 2 0 00-2 2v6a2 2 0 002 2h10a2 2 0 002-2v-6a2 2 0 00-2-2h-.5V5.5A4.5 4.5 0 0010 1zm3 8V5.5a3 3 0 10-6 0V9h6z" clip-rule="evenodd" />
          </svg>
          SSL certificates.
        </dt>
        <dd class="inline">Anim aute id magna aliqua ad ad non deserunt sunt. Qui irure qui lorem cupidatat commodo.</dd>
      </div>
      <div class="relative pl-9">
        <dt class="inline font-semibold text-gray-900">
          <svg class="absolute left-1 top-1 h-5 w-5 text-indigo-600" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
            <path fill-rule="evenodd" d="M15.312 11.424a5.5 5.5 0 01-9.201 2.466l-.312-.311h2.433a.75.75 0 000-1.5H3.989a.75.75 0 00-.75.75v4.242a.75.75 0 001.5 0v-2.43l.31.31a7 7 0 0011.712-3.138.75.75 0 00-1.449-.39zm1.23-3.723a.75.75 0 00.219-.53V2.929a.75.75 0 00-1.5 0V5.36l-.31-.31A7 7 0 003.239 8.188a.75.75 0 101.448.389A5.5 5.5 0 0113.89 6.11l.311.31h-2.432a.75.75 0 000 1.5h4.243a.75.75 0 00.53-.219z" clip-rule="evenodd" />
          </svg>
          Simple queues.
        </dt>
        <dd class="inline">Ac tincidunt sapien vehicula erat auctor pellentesque rhoncus.</dd>
      </div>
      <div class="relative pl-9">
        <dt class="inline font-semibold text-gray-900">
          <svg class="absolute left-1 top-1 h-5 w-5 text-indigo-600" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
            <path fill-rule="evenodd" d="M10 2.5c-1.31 0-2.526.386-3.546 1.051a.75.75 0 01-.82-1.256A8 8 0 0118 9a22.47 22.47 0 01-1.228 7.351.75.75 0 11-1.417-.49A20.97 20.97 0 0016.5 9 6.5 6.5 0 0010 2.5zM4.333 4.416a.75.75 0 01.218 1.038A6.466 6.466 0 003.5 9a7.966 7.966 0 01-1.293 4.362.75.75 0 01-1.257-.819A6.466 6.466 0 002 9c0-1.61.476-3.11 1.295-4.365a.75.75 0 011.038-.219zM10 6.12a3 3 0 00-3.001 3.041 11.455 11.455 0 01-2.697 7.24.75.75 0 01-1.148-.965A9.957 9.957 0 005.5 9c0-.028.002-.055.004-.082a4.5 4.5 0 018.996.084V9.15l-.005.297a.75.75 0 11-1.5-.034c.003-.11.004-.219.005-.328a3 3 0 00-3-2.965zm0 2.13a.75.75 0 01.75.75c0 3.51-1.187 6.745-3.181 9.323a.75.75 0 11-1.186-.918A13.687 13.687 0 009.25 9a.75.75 0 01.75-.75zm3.529 3.698a.75.75 0 01.584.885 18.883 18.883 0 01-2.257 5.84.75.75 0 11-1.29-.764 17.386 17.386 0 002.078-5.377.75.75 0 01.885-.584z" clip-rule="evenodd" />
          </svg>
          Advanced security.
        </dt>
        <dd class="inline">Lorem ipsum, dolor sit amet consectetur adipisicing elit aute id magna.</dd>
      </div>
      <div class="relative pl-9">
        <dt class="inline font-semibold text-gray-900">
          <svg class="absolute left-1 top-1 h-5 w-5 text-indigo-600" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
            <path fill-rule="evenodd" d="M7.84 1.804A1 1 0 018.82 1h2.36a1 1 0 01.98.804l.331 1.652a6.993 6.993 0 011.929 1.115l1.598-.54a1 1 0 011.186.447l1.18 2.044a1 1 0 01-.205 1.251l-1.267 1.113a7.047 7.047 0 010 2.228l1.267 1.113a1 1 0 01.206 1.25l-1.18 2.045a1 1 0 01-1.187.447l-1.598-.54a6.993 6.993 0 01-1.929 1.115l-.33 1.652a1 1 0 01-.98.804H8.82a1 1 0 01-.98-.804l-.331-1.652a6.993 6.993 0 01-1.929-1.115l-1.598.54a1 1 0 01-1.186-.447l-1.18-2.044a1 1 0 01.205-1.251l1.267-1.114a7.05 7.05 0 010-2.227L1.821 7.773a1 1 0 01-.206-1.25l1.18-2.045a1 1 0 011.187-.447l1.598.54A6.993 6.993 0 017.51 3.456l.33-1.652zM10 13a3 3 0 100-6 3 3 0 000 6z" clip-rule="evenodd" />
          </svg>
          Powerful API.
        </dt>
        <dd class="inline">Anim aute id magna aliqua ad ad non deserunt sunt. Qui irure qui lorem cupidatat commodo.</dd>
      </div>
      <div class="relative pl-9">
        <dt class="inline font-semibold text-gray-900">
          <svg class="absolute left-1 top-1 h-5 w-5 text-indigo-600" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
            <path d="M4.632 3.533A2 2 0 016.577 2h6.846a2 2 0 011.945 1.533l1.976 8.234A3.489 3.489 0 0016 11.5H4c-.476 0-.93.095-1.344.267l1.976-8.234z" />
            <path fill-rule="evenodd" d="M4 13a2 2 0 100 4h12a2 2 0 100-4H4zm11.24 2a.75.75 0 01.75-.75H16a.75.75 0 01.75.75v.01a.75.75 0 01-.75.75h-.01a.75.75 0 01-.75-.75V15zm-2.25-.75a.75.75 0 00-.75.75v.01c0 .414.336.75.75.75H13a.75.75 0 00.75-.75V15a.75.75 0 00-.75-.75h-.01z" clip-rule="evenodd" />
          </svg>
          Database backups.
        </dt>
        <dd class="inline">Ac tincidunt sapien vehicula erat auctor pellentesque rhoncus.</dd>
      </div>
    </dl>
  </div>
</div>

`,
                },
            });
            editor.BlockManager.add('LogoClouds1', {
                id: 'LogoClouds1',
                label: 'Logo Clouds 1',
                media: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-layout-three-columns" viewBox="0 0 16 16"> <path d="M0 1.5A1.5 1.5 0 0 1 1.5 0h13A1.5 1.5 0 0 1 16 1.5v13a1.5 1.5 0 0 1-1.5 1.5h-13A1.5 1.5 0 0 1 0 14.5zM1.5 1a.5.5 0 0 0-.5.5v13a.5.5 0 0 0 .5.5H5V1zM10 15V1H6v14zm1 0h3.5a.5.5 0 0 0 .5-.5v-13a.5.5 0 0 0-.5-.5H11z"/> </svg>',
                draggable: true,
                category: 'Sections',
                order: 1,
                droppable: true,
                attributes: {class: 'custom-grid'}, // Βεβαιώσου ότι το Grid είναι droppable
                content: `<div class="bg-white py-24 sm:py-32">
  <div class="mx-auto max-w-7xl px-6 lg:px-8">
    <h2 class="text-center text-lg font-semibold leading-8 text-gray-900">Trusted by the world’s most innovative teams</h2>
    <div class="mx-auto mt-10 grid max-w-lg grid-cols-4 items-center gap-x-8 gap-y-10 sm:max-w-xl sm:grid-cols-6 sm:gap-x-10 lg:mx-0 lg:max-w-none lg:grid-cols-5">
      <img class="col-span-2 max-h-12 w-full object-contain lg:col-span-1" src="https://tailwindui.com/img/logos/158x48/transistor-logo-gray-900.svg" alt="Transistor" width="158" height="48">
      <img class="col-span-2 max-h-12 w-full object-contain lg:col-span-1" src="https://tailwindui.com/img/logos/158x48/reform-logo-gray-900.svg" alt="Reform" width="158" height="48">
      <img class="col-span-2 max-h-12 w-full object-contain lg:col-span-1" src="https://tailwindui.com/img/logos/158x48/tuple-logo-gray-900.svg" alt="Tuple" width="158" height="48">
      <img class="col-span-2 max-h-12 w-full object-contain sm:col-start-2 lg:col-span-1" src="https://tailwindui.com/img/logos/158x48/savvycal-logo-gray-900.svg" alt="SavvyCal" width="158" height="48">
      <img class="col-span-2 col-start-2 max-h-12 w-full object-contain sm:col-start-auto lg:col-span-1" src="https://tailwindui.com/img/logos/158x48/statamic-logo-gray-900.svg" alt="Statamic" width="158" height="48">
    </div>
  </div>
</div>`,
            });
            editor.BlockManager.add('Navigation1', {
                id: 'navigation1',
                label: 'Navigation 1',
                media: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-layout-three-columns" viewBox="0 0 16 16"> <path d="M0 1.5A1.5 1.5 0 0 1 1.5 0h13A1.5 1.5 0 0 1 16 1.5v13a1.5 1.5 0 0 1-1.5 1.5h-13A1.5 1.5 0 0 1 0 14.5zM1.5 1a.5.5 0 0 0-.5.5v13a.5.5 0 0 0 .5.5H5V1zM10 15V1H6v14zm1 0h3.5a.5.5 0 0 0 .5-.5v-13a.5.5 0 0 0-.5-.5H11z"/> </svg>',
                draggable: true,

                group: 'Elements',
                category: 'Μενού',
                order: 1,
                droppable: true,
                attributes: {class: 'custom-grid'}, // Βεβαιώσου ότι το Grid είναι droppable
                content: `<nav class="mx-auto flex max-w-7xl items-center justify-between p-6 lg:px-8" aria-label="Global">
        <div class="flex lg:flex-1">
            <a href="#" class="-m-1.5 p-1.5">
                <span class="sr-only">Your Company</span>
                <img class="h-8 w-auto" src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=600" alt="">
            </a>
        </div>
        <div class="flex lg:hidden ">

            {{-- mobile button --}}
                <button   type="button" class="-m-2.5 inline-flex items-center justify-center rounded-md p-2.5 text-gray-700" id="open-mobile-menu" >
                    <span class="sr-only">Open main menu</span>
                    <svg class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
                    </svg>
                </button>
{{-- end mobile button --}}
                </div>
                        <div data-dynamic-menu class="menu-wrapper hidden lg:flex md:fled lg:gap-x-12 px-4">
                            Drop your static or dynamic menu here
                        </div>
                        <div class="hidden lg:flex lg:flex-1 lg:justify-end" >
                            <a href="#" class="text-sm font-semibold leading-6 text-gray-900">Log in <span aria-hidden="true">&rarr;</span></a>
                        </div>
                    </nav>
                    <!-- Mobile menu, show/hide based on menu open state. -->
                    <div class="lg:hidden" role="dialog" aria-modal="true">
                        <!-- Background backdrop -->
                        <div class="fixed inset-0 z-10 bg-black opacity-50 hidden" id="mobile-menu-backdrop"></div>

                        <!-- Slide-over menu -->
                        <div id="mobile-menu" class="fixed inset-y-0 right-0 z-20 w-full overflow-y-auto bg-white px-6 py-6 sm:max-w-sm sm:ring-1 sm:ring-gray-900/10 translate-x-full transition-transform duration-300 ease-in-out">
                            <div class="flex items-center justify-between">
                                <a href="#" class="-m-1.5 p-1.5">
                                    <span class="sr-only">Your Company</span>
                                    <img class="h-8 w-auto" src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=600" alt="">
                                </a>
                                <button type="button" class="-m-2.5 rounded-md p-2.5 text-gray-700" id="close-mobile-menu">
                                    <span class="sr-only">Close menu</span>
                                    <svg class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
                                    </svg>
                                </button>
                            </div>

                            <div class="mt-6 flow-root">
                                <div class="-my-6 divide-y divide-gray-500/10">
                                    <div class="space-y-2 py-6">
                                        <div class="-mx-3">
                                            <button type="button" class="flex w-full items-center justify-between rounded-lg py-2 pl-3 pr-3.5 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50" id="product-btn" aria-controls="product-submenu" aria-expanded="false">
                                                Product
                                                <svg class="h-5 w-5 flex-none transition-transform" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                                    <path fill-rule="evenodd" d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z" clip-rule="evenodd" />
                                                </svg>
                                            </button>

                                            <!-- Product sub-menu -->
                                            <div class="mt-2 space-y-2 hidden" id="product-submenu">

                                            <a href="#" class="block rounded-lg py-2 pl-6 pr-3 text-sm font-semibold leading-7 text-gray-900 hover:bg-gray-50">Analytics</a>
                                                <a href="#" class="block rounded-lg py-2 pl-6 pr-3 text-sm font-semibold leading-7 text-gray-900 hover:bg-gray-50">Engagement</a>
                                                <a href="#" class="block rounded-lg py-2 pl-6 pr-3 text-sm font-semibold leading-7 text-gray-900 hover:bg-gray-50">Security</a>
                                                <a href="#" class="block rounded-lg py-2 pl-6 pr-3 text-sm font-semibold leading-7 text-gray-900 hover:bg-gray-50">Integrations</a>
                                                <a href="#" class="block rounded-lg py-2 pl-6 pr-3 text-sm font-semibold leading-7 text-gray-900 hover:bg-gray-50">Automations</a>
                                                <a href="#" class="block rounded-lg py-2 pl-6 pr-3 text-sm font-semibold leading-7 text-gray-900 hover:bg-gray-50">Watch demo</a>
                                                <a href="#" class="block rounded-lg py-2 pl-6 pr-3 text-sm font-semibold leading-7 text-gray-900 hover:bg-gray-50">Contact sales</a>
                                            </div>
                                        </div>
                                        <a href="#" class="-mx-3 block rounded-lg px-3 py-2 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50">Features</a>
                                        <a href="#" class="-mx-3 block rounded-lg px-3 py-2 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50">Marketplace</a>
                                        <a href="#" class="-mx-3 block rounded-lg px-3 py-2 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50">Company</a>
                                    </div>
                                    <div class="py-6">
                                        <a href="#" class="-mx-3 block rounded-lg px-3 py-2.5 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50">Log in</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
`,

            });
            editor.BlockManager.add('Navigation2', {
                id: 'navigation2',
                label: 'Navigation 2',
                media: '<svg xmlns="http://www.w3.org/2000/svg" width="46" height="46" fill="currentColor" class="bi bi-layout-three-columns" viewBox="0 0 16 16"> <path d="M0 1.5A1.5 1.5 0 0 1 1.5 0h13A1.5 1.5 0 0 1 16 1.5v13a1.5 1.5 0 0 1-1.5 1.5h-13A1.5 1.5 0 0 1 0 14.5zM1.5 1a.5.5 0 0 0-.5.5v13a.5.5 0 0 0 .5.5H5V1zM10 15V1H6v14zm1 0h3.5a.5.5 0 0 0 .5-.5v-13a.5.5 0 0 0-.5-.5H11z"/> </svg>',
                draggable: true,
                group: 'Elements',
                order: 1,
                droppable: true,
                category: 'Μενού',
                attributes: {class: 'custom-grid'}, // Βεβαιώσου ότι το Grid είναι droppable
                content: `<nav class="bg-white shadow">
  <div class="mx-auto max-w-7xl px-2 sm:px-6 lg:px-8">
    <div class="relative flex h-16 justify-between">
      <div class="absolute inset-y-0 left-0 flex items-center sm:hidden">
        <!-- Mobile menu button -->
        <button type="button" class="relative inline-flex items-center justify-center rounded-md p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500" aria-controls="mobile-menu" aria-expanded="false">
          <span class="absolute -inset-0.5"></span>
          <span class="sr-only">Open main menu</span>

          <svg class="block h-6 w-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
            <path stroke-linecap="round" stroke-linejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
          </svg>

          <svg class="hidden h-6 w-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
            <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
          </svg>
        </button>
      </div>
      <div class="flex flex-1 items-center justify-center sm:items-stretch sm:justify-start">
        <div class="flex flex-shrink-0 items-center">
          <img class="h-8 w-auto" src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=600" alt="Your Company">
        </div>
        <div class="hidden sm:ml-6 sm:flex sm:space-x-8 justify-between">
          <!-- Current: "border-indigo-500 text-gray-900", Default: "border-transparent text-gray-500 hover:border-gray-300 hover:text-gray-700" -->
          <a href="#" class="inline-flex items-center border-b-2 border-indigo-500 px-1 pt-1 text-sm font-medium text-gray-900">Dashboard</a>
          <a href="#" class="inline-flex items-center border-b-2 border-transparent px-1 pt-1 text-sm font-medium text-gray-500 hover:border-gray-300 hover:text-gray-700">Team</a>
          <a href="#" class="inline-flex items-center border-b-2 border-transparent px-1 pt-1 text-sm font-medium text-gray-500 hover:border-gray-300 hover:text-gray-700">Projects</a>
          <a href="#" class="inline-flex items-center border-b-2 border-transparent px-1 pt-1 text-sm font-medium text-gray-500 hover:border-gray-300 hover:text-gray-700">Calendar</a>
        </div>
      </div>
      <div class="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
        <button type="button" class="relative rounded-full bg-white p-1 text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">
          <span class="absolute -inset-1.5"></span>
          <span class="sr-only">View notifications</span>
          <svg class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
            <path stroke-linecap="round" stroke-linejoin="round" d="M14.857 17.082a23.848 23.848 0 005.454-1.31A8.967 8.967 0 0118 9.75v-.7V9A6 6 0 006 9v.75a8.967 8.967 0 01-2.312 6.022c1.733.64 3.56 1.085 5.455 1.31m5.714 0a24.255 24.255 0 01-5.714 0m5.714 0a3 3 0 11-5.714 0" />
          </svg>
        </button>

        <!-- Profile dropdown -->
        <div class="relative ml-3">
          <div>
            <button type="button" class="relative flex rounded-full bg-white text-sm focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2" id="user-menu-button" aria-expanded="false" aria-haspopup="true">
              <span class="absolute -inset-1.5"></span>
              <span class="sr-only">Open user menu</span>
              <img class="h-8 w-8 rounded-full" src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="">
            </button>
          </div>

          <!--
            Dropdown menu, show/hide based on menu state.

            Entering: "transition ease-out duration-200"
              From: "transform opacity-0 scale-95"
              To: "transform opacity-100 scale-100"
            Leaving: "transition ease-in duration-75"
              From: "transform opacity-100 scale-100"
              To: "transform opacity-0 scale-95"
          -->
          <div class="absolute right-0 z-10 mt-2 w-48 origin-top-right rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none" role="menu" aria-orientation="vertical" aria-labelledby="user-menu-button" tabindex="-1">
            <!-- Active: "bg-gray-100", Not Active: "" -->
            <a href="#" class="block px-4 py-2 text-sm text-gray-700" role="menuitem" tabindex="-1" id="user-menu-item-0">Your Profile</a>
            <a href="#" class="block px-4 py-2 text-sm text-gray-700" role="menuitem" tabindex="-1" id="user-menu-item-1">Settings</a>
            <a href="#" class="block px-4 py-2 text-sm text-gray-700" role="menuitem" tabindex="-1" id="user-menu-item-2">Sign out</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Mobile menu, show/hide based on menu state. -->
  <div class="sm:hidden" id="mobile-menu">
    <div class="space-y-1 pb-4 pt-2">
      <!-- Current: "bg-indigo-50 border-indigo-500 text-indigo-700", Default: "border-transparent text-gray-500 hover:bg-gray-50 hover:border-gray-300 hover:text-gray-700" -->
      <a href="#" class="block border-l-4 border-indigo-500 bg-indigo-50 py-2 pl-3 pr-4 text-base font-medium text-indigo-700">Dashboard</a>
      <a href="#" class="block border-l-4 border-transparent py-2 pl-3 pr-4 text-base font-medium text-gray-500 hover:border-gray-300 hover:bg-gray-50 hover:text-gray-700">Team</a>
      <a href="#" class="block border-l-4 border-transparent py-2 pl-3 pr-4 text-base font-medium text-gray-500 hover:border-gray-300 hover:bg-gray-50 hover:text-gray-700">Projects</a>
      <a href="#" class="block border-l-4 border-transparent py-2 pl-3 pr-4 text-base font-medium text-gray-500 hover:border-gray-300 hover:bg-gray-50 hover:text-gray-700">Calendar</a>
    </div>
  </div>
</nav>

`,

            });


            editor.BlockManager.add('Navigation3', {
                id: 'navigation3',
                label: 'Navigation 3',
                media: '<svg xmlns="http://www.w3.org/2000/svg" width="46" height="46" fill="currentColor" class="bi bi-layout-three-columns" viewBox="0 0 16 16"> <path d="M0 1.5A1.5 1.5 0 0 1 1.5 0h13A1.5 1.5 0 0 1 16 1.5v13a1.5 1.5 0 0 1-1.5 1.5h-13A1.5 1.5 0 0 1 0 14.5zM1.5 1a.5.5 0 0 0-.5.5v13a.5.5 0 0 0 .5.5H5V1zM10 15V1H6v14zm1 0h3.5a.5.5 0 0 0 .5-.5v-13a.5.5 0 0 0-.5-.5H11z"/> </svg>',
                draggable: true,
                group: 'Elements',
                order: 1,
                droppable: true,
                category: 'Μενού',
                attributes: {class: 'custom-grid'}, // Βεβαιώσου ότι το Grid είναι droppable
                content: `<header class="bg-white shadow">
  <div class="mx-auto max-w-7xl px-2 sm:px-4 lg:divide-y lg:divide-gray-200 lg:px-8">
    <div class="relative flex h-16 justify-between">
      <div class="relative z-10 flex px-2 lg:px-0">
        <div class="flex flex-shrink-0 items-center">
          <img class="h-8 w-auto" src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=600" alt="Your Company">
        </div>
      </div>
      <div class="relative z-0 flex flex-1 items-center justify-center px-2 sm:absolute sm:inset-0">
        <div data-search-input class="w-full sm:max-w-xs">
          <label for="search" class="sr-only">Search</label>
          <div class="relative">
            <div class="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
              <svg class="h-5 w-5 text-gray-400" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M9 3.5a5.5 5.5 0 100 11 5.5 5.5 0 000-11zM2 9a7 7 0 1112.452 4.391l3.328 3.329a.75.75 0 11-1.06 1.06l-3.329-3.328A7 7 0 012 9z" clip-rule="evenodd" />
              </svg>
            </div>
            <input id="search" name="search" class="block w-full rounded-md border-0 bg-white py-1.5 pl-10 pr-3 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" placeholder="Search" type="search">
          </div>
        </div>
      </div>
      <div class="relative z-10 flex items-center lg:hidden">
        <!-- Mobile menu button -->
        <button type="button" class="relative inline-flex items-center justify-center rounded-md p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500" aria-controls="mobile-menu" aria-expanded="false">
          <span class="absolute -inset-0.5"></span>
          <span class="sr-only">Open menu</span>
          <!--
            Icon when menu is closed.

            Menu open: "hidden", Menu closed: "block"
          -->
          <svg class="block h-6 w-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
            <path stroke-linecap="round" stroke-linejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
          </svg>
          <!--
            Icon when menu is open.

            Menu open: "block", Menu closed: "hidden"
          -->
          <svg class="hidden h-6 w-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
            <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
          </svg>
        </button>
      </div>
      <div class="hidden lg:relative lg:z-10 lg:ml-4 lg:flex lg:items-center">
        <button type="button" class="relative flex-shrink-0 rounded-full bg-white p-1 text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">
          <span class="absolute -inset-1.5"></span>
          <span class="sr-only">View notifications</span>
          <svg class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
            <path stroke-linecap="round" stroke-linejoin="round" d="M14.857 17.082a23.848 23.848 0 005.454-1.31A8.967 8.967 0 0118 9.75v-.7V9A6 6 0 006 9v.75a8.967 8.967 0 01-2.312 6.022c1.733.64 3.56 1.085 5.455 1.31m5.714 0a24.255 24.255 0 01-5.714 0m5.714 0a3 3 0 11-5.714 0" />
          </svg>
        </button>

        <!-- Profile dropdown -->
        <div class="relative ml-4 flex-shrink-0">
          <div>
            <button type="button" class="relative flex rounded-full bg-white focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2" id="user-menu-button" aria-expanded="false" aria-haspopup="true">
              <span class="absolute -inset-1.5"></span>
              <span class="sr-only">Open user menu</span>
              <img class="h-8 w-8 rounded-full" src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="">
            </button>
          </div>

          <!--
            Dropdown menu, show/hide based on menu state.

            Entering: "transition ease-out duration-100"
              From: "transform opacity-0 scale-95"
              To: "transform opacity-100 scale-100"
            Leaving: "transition ease-in duration-75"
              From: "transform opacity-100 scale-100"
              To: "transform opacity-0 scale-95"
          -->
          <div class="absolute right-0 z-10 mt-2 w-48 origin-top-right rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none" role="menu" aria-orientation="vertical" aria-labelledby="user-menu-button" tabindex="-1">
            <!-- Active: "bg-gray-100", Not Active: "" -->
            <a href="#" class="block px-4 py-2 text-sm text-gray-700" role="menuitem" tabindex="-1" id="user-menu-item-0">Your Profile</a>
            <a href="#" class="block px-4 py-2 text-sm text-gray-700" role="menuitem" tabindex="-1" id="user-menu-item-1">Settings</a>
            <a href="#" class="block px-4 py-2 text-sm text-gray-700" role="menuitem" tabindex="-1" id="user-menu-item-2">Sign out</a>
          </div>
        </div>
      </div>
    </div>
    <nav class="hidden lg:flex lg:space-x-8 lg:py-2" aria-label="Global">
      <!-- Current: "bg-gray-100 text-gray-900", Default: "text-gray-900 hover:bg-gray-50 hover:text-gray-900" -->
      <a href="#" class="inline-flex items-center rounded-md bg-gray-100 px-3 py-2 text-sm font-medium text-gray-900" aria-current="page">Dashboard</a>
      <a href="#" class="inline-flex items-center rounded-md px-3 py-2 text-sm font-medium text-gray-900 hover:bg-gray-50 hover:text-gray-900">Team</a>
      <a href="#" class="inline-flex items-center rounded-md px-3 py-2 text-sm font-medium text-gray-900 hover:bg-gray-50 hover:text-gray-900">Projects</a>
      <a href="#" class="inline-flex items-center rounded-md px-3 py-2 text-sm font-medium text-gray-900 hover:bg-gray-50 hover:text-gray-900">Calendar</a>
    </nav>
  </div>

  <!-- Mobile menu, show/hide based on menu state. -->
  <nav class="lg:hidden" aria-label="Global" id="mobile-menu">
    <div class="space-y-1 px-2 pb-3 pt-2">
      <!-- Current: "bg-gray-100 text-gray-900", Default: "text-gray-900 hover:bg-gray-50 hover:text-gray-900" -->
      <a href="#" class="block rounded-md bg-gray-100 px-3 py-2 text-base font-medium text-gray-900" aria-current="page">Dashboard</a>
      <a href="#" class="block rounded-md px-3 py-2 text-base font-medium text-gray-900 hover:bg-gray-50 hover:text-gray-900">Team</a>
      <a href="#" class="block rounded-md px-3 py-2 text-base font-medium text-gray-900 hover:bg-gray-50 hover:text-gray-900">Projects</a>
      <a href="#" class="block rounded-md px-3 py-2 text-base font-medium text-gray-900 hover:bg-gray-50 hover:text-gray-900">Calendar</a>
    </div>
    <div class="border-t border-gray-200 pb-3 pt-4">
      <div class="flex items-center px-4">
        <div class="flex-shrink-0">
          <img class="h-10 w-10 rounded-full" src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="">
        </div>
        <div class="ml-3">
          <div class="text-base font-medium text-gray-800">Tom Cook</div>
          <div class="text-sm font-medium text-gray-500">tom@example.com</div>
        </div>
        <button type="button" class="relative ml-auto flex-shrink-0 rounded-full bg-white p-1 text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">
          <span class="absolute -inset-1.5"></span>
          <span class="sr-only">View notifications</span>
          <svg class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
            <path stroke-linecap="round" stroke-linejoin="round" d="M14.857 17.082a23.848 23.848 0 005.454-1.31A8.967 8.967 0 0118 9.75v-.7V9A6 6 0 006 9v.75a8.967 8.967 0 01-2.312 6.022c1.733.64 3.56 1.085 5.455 1.31m5.714 0a24.255 24.255 0 01-5.714 0m5.714 0a3 3 0 11-5.714 0" />
          </svg>
        </button>
      </div>
      <div class="mt-3 space-y-1 px-2">
        <a href="#" class="block rounded-md px-3 py-2 text-base font-medium text-gray-500 hover:bg-gray-50 hover:text-gray-900">Your Profile</a>
        <a href="#" class="block rounded-md px-3 py-2 text-base font-medium text-gray-500 hover:bg-gray-50 hover:text-gray-900">Settings</a>
        <a href="#" class="block rounded-md px-3 py-2 text-base font-medium text-gray-500 hover:bg-gray-50 hover:text-gray-900">Sign out</a>
      </div>
    </div>
  </nav>
</header>
`,

            });
            editor.Components.addType('custom-component', {
                model: {
                    defaults: {
                        tagName: 'div',
                        classes: ['p-4', 'text-white'],
                        content: 'Custom Component',
                        draggable: true,
                        droppable: true, // Επιτρέπει την τοποθέτηση άλλων στοιχείων

                        attributes: { // Default attributes
                            type: 'text',
                            name: 'default-name',
                            placeholder: 'Insert text here',
                        },
                        traits: [


                            {
                                type: 'select',
                                label: 'Column Span',
                                name: 'class',
                                options: [
                                    {id: '', name: 'None'},
                                    {id: 'grid grid-cols-1', name: '1 Column'},
                                    {id: 'grid grid-cols-2', name: '2 Columns'},
                                    {id: 'grid grid-cols-3', name: '3 Columns'},
                                    {id: 'grid grid-cols-4', name: '4 Columns'},
                                    // Add more options if neededr
                                ],
                            },
                        ],
                        init() {
                            this.on('change:traits', this.updateClasses, this);
                            this.updateClasses();
                        },

                        updateClasses() {
                            const colSpan = this.getTrait('colSpan').getValue();
                            const classes = ['p-4', 'bg-blue-500', 'text-white'];
                            if (colSpan) {
                                classes.push(colSpan);
                            }
                            this.set('classes', classes);
                        },

                    },

                },
            });


            //
            //   editor.BlockManager.add('section', {
            //       id: 'section', // id is mandatory
            //       label: '<b>Section</b>', // You can use HTML/SVG inside labels
            //       attributes: {class: 'gjs-block-section'},
            //       content: {type: 'test'},
            //
            //       // content: `<section>
            //       //   <h1>This is a simple title</h1>
            //       //   <div>This is just a Lorem text: Lorem ipsum dolor sit amet</div>
            //       // </section>`,
            //       styles: `
            //   .cmp-css { color: red }
            //   .cmp-css-a { color: green }
            //   .cmp-css-b { color: blue }
            //
            //   @media (max-width: 992px) {
            //     .cmp-css{ color: darkred; }
            //     .cmp-css-a { color: darkgreen }
            //     .cmp-css-b { color: darkblue }
            //   }
            // `,
            //   });

            editor.BlockManager.add('text', {
                id: 'text',
                label: 'Text',
                content: '<div data-gjs-type="text">Insert your text here</div>',
            });
            editor.BlockManager.add('image', {
                id: 'image',
                label: 'Image',
                // Select the component once it's dropped
                select: true,
                // You can pass components as a JSON instead of a simple HTML string,
                // in this case we also use a defined component type `image`
                content: {type: 'image'},
                // This triggers `active` event on dropped components and the `image`
                // reacts by opening the AssetManager
                activate: true,
            });


        }

        function replaceMenuContent(componentHtml, newContent) {
            // Create a DOMParser instance to parse the HTML string
            const parser = new DOMParser();
            const doc = parser.parseFromString(componentHtml, 'text/html');

            // Find the div with the data-renderedmenu attribute
            const targetDiv = doc.querySelector('div[data-renderedmenu]');

            if (targetDiv) {

                // Replace the target div with the new content
                targetDiv.outerHTML = replacement;

                // Serialize the updated document back to an HTML string
                const serializer = new XMLSerializer();

                return serializer.serializeToString(doc);
            }


            // Return original HTML if target div is not found
            return componentHtml;
        }


        // save content
        function saveContent() {
            // Λήψη των components ως JSON
            const jsonContent = editor.getHtml();
            const cssContent = editor.getCss();
            const jsContent = editor.getJs();

            // Εδώ μπορείς να αποθηκεύσεις το jsonContent ή να το στείλεις σε έναν server
            console.log('JSON Content:', JSON.stringify(jsonContent));
            const csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
            // Παράδειγμα αποστολής μέσω AJAX
            fetch('/manage/theme/{{$theme->id}}/component/{{$component->id}}/save-component', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                },
                body: JSON.stringify({
                    css: cssContent,
                    js: jsContent,
                    html: jsonContent,
                }),
            })
                .then(response => response.json())
                .then(data => console.log('Success:', data))
                .catch((error) => console.error('Error:', error));
        }

        // Παράδειγμα κλήσης της συνάρτησης αποθήκευσης
        document.getElementById('save-button').addEventListener('click', saveContent);

    </script>
    <style>
        /*.panel__top {*/
        /*    padding: 0;*/
        /*    width: 100%;*/
        /*    display: block;*/
        /*    position: initial;*/
        /*    justify-content: center;*/
        /*    justify-content: space-between;*/
        /*}*/

        .panel__basic-actions {
            position: initial;
        }

        .grid-container {
            display: grid;
            grid-template-columns: repeat(auto-fill, minmax(100px, 1fr));
            gap: 10px;
        }


        .change-theme-button {
            width: 40px;
            height: 40px;
            border-radius: 50%;
            margin: 5px;
        }

        .change-theme-button:focus {
            /* background-color: yellow; */
            outline: none;
            box-shadow: 0 0 0 2pt #c5c5c575;
        }

        div#pages {
            background: #444444;
        }

        /* Fit icons properly */
        .gjs-block {
            width: 28%;
        }

        /*.gjs-one-bg {*/
        /*    background-color: #ffffff !important*/
        /*}*/

        /*.gjs-one-color {*/
        /*    color: #222222;*/
        /*}*/

        /*.gjs-one-color-h:hover {*/
        /*    color: #463a3c*/
        /*}*/

        /*.gjs-two-bg {*/
        /*    background-color: #b9a5a6*/
        /*}*/

        /*.gjs-two-color {*/
        /*    color: #000 !important*/
        /*}*/

        /*.gjs-two-color-h:hover {*/
        /*    color: #b9a5a6*/
        /*}*/

        /*.gjs-three-bg {*/
        /*    background-color: #fff*/
        /*}*/

        /*.gjs-three-color {*/
        /*    color: #804f7b*/
        /*}*/

        /*.gjs-three-color-h:hover {*/
        /*    color: #804f7b*/
        /*}*/

        /*.gjs-four-bg {*/
        /*    background-color: #d97aa6*/
        /*}*/

        /*.gjs-four-color {*/
        /*    color: #d97aa6*/
        /*}*/

        /*.gjs-four-color-h:hover {*/
        /*    color: #d97aa6*/
        /*}*/

        /*!*override*!*/
        /*.gjs-title,*/
        /*.gjs-layer-name,*/
        /*.gjs-no-app,*/
        /*.gjs-sm-title{*/
        /*    font-weight: 500 !important;*/
        /*}*/


        /*.gjs-color-warn {*/
        /*    color: #000;*/
        /*}*/

        /*.gjs-clm-tags #gjs-clm-checkbox,*/
        /*.gjs-clm-tags #gjs-clm-close {*/
        /*    color: #000 !important;*/
        /*}*/


        /*.gjs-clm-tags #gjs-clm-new {*/
        /*    color: #000;*/
        /*    padding: 5px 6px;*/
        /*    display: none;*/
        /*    background-color: #eee;*/
        /*}*/

        /*.gjs-sm-sector .gjs-sm-field select, .gjs-clm-tags .gjs-sm-field select, .gjs-sm-sector .gjs-clm-field select, .gjs-clm-tags .gjs-clm-field select,*/
        /*.gjs-sm-sector .gjs-sm-field input, .gjs-clm-tags .gjs-sm-field input, .gjs-sm-sector .gjs-clm-field input, .gjs-clm-tags .gjs-clm-field input,*/
        /*.gjs-field input, .gjs-field select, .gjs-field textarea {*/
        /*    color: #fff;*/
        /*    background-color: #ddd;*/
        /*    font-size: 13px;*/
        /*}*/
        /*.gjs-category-title, .gjs-layer-title, .gjs-block-category .gjs-title, .gjs-sm-sector-title, .gjs-trait-category .gjs-title {*/
        /*    font-weight: lighter;*/
        /*    background-color: rgb(151 151 151 / 10%);*/
        /*    letter-spacing: 1px;*/
        /*    padding: 9px 10px 9px 20px;*/
        /*    border-bottom: 1px solid rgb(255 255 255 / 25%);*/
        /*    text-align: left;*/
        /*    position: relative;*/
        /*    cursor: pointer;*/
        /*}*/
        .gjs-field-color-picker {
            background-color: #aaa !important;
        }

        .gjs-radio-item input:checked + .gjs-radio-item-label {
            background-color: #000;
        }

        .gjs-sm-sector .gjs-sm-stack #gjs-sm-add, .gjs-clm-tags .gjs-sm-stack #gjs-sm-add {
            color: #000;
        }

    </style>

@endsection
