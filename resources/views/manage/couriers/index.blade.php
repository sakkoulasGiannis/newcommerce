@extends('layouts.admin')
@section('content')
    <div class="mt-8 flow-root overflow-hidden">
        <div class="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
            <fieldset>
                <legend class="text-sm font-semibold leading-6 text-gray-900">Διαθέσιμοι τρόποι πληρωμής</legend>
                <div class="mt-6 grid grid-cols-1 gap-y-6 sm:grid-cols-3 sm:gap-x-4">
                    <!-- Active: "border-indigo-600 ring-2 ring-indigo-600", Not Active: "border-gray-300" -->
                    <!-- Active: "border-indigo-600 ring-2 ring-indigo-600", Not Active: "border-gray-300" -->
                    @foreach($couriers as $p)
                        <label aria-label="{{$p->d}}" aria-description="{{$p->description}}"
                               class="relative flex cursor-pointer rounded-lg border bg-white p-4 shadow-sm focus:outline-none">
                            <input type="checkbox" name="{{$p->name}}" value="" class="sr-only">
                            <span class="flex flex-1">
                            <span class="flex flex-col">
                              <span class="block text-sm font-medium text-gray-900">{{$p->title}}</span>
                              <span class="mt-1 flex items-center text-sm text-gray-500">{{$p->description}}</span>
{{--                              <span class="mt-6 text-sm font-medium text-gray-900">Ρυθμίσεις</span>--}}
                            </span>
                          </span>
                            @if($p->active)
                                <!-- Not Checked: "invisible" -->
                                <svg class="h-5 w-5 text-indigo-600" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                    <path fill-rule="evenodd"
                                          d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z"
                                          clip-rule="evenodd"/>
                                </svg>
                            @endif
                            <!--
                          Active: "border", Not Active: "border-2"
                          Checked: "border-indigo-600", Not Checked: "border-transparent"
                        -->
                            <span class="pointer-events-none absolute -inset-px rounded-lg  {{ $p->active ? 'border border-indigo-600' : 'border-transparent border-2' }}"
                                  aria-hidden="true"></span>
                        </label>
                    @endforeach

                </div>
            </fieldset>

        </div>
    </div>
@endsection
@section('scripts')
    @parent

@endsection

