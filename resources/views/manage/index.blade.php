@extends('layouts.admin')
@section('content')

    <!-- Statistics Cards -->
    <div class="grid grid-cols-1 gap-4 p-4 sm:grid-cols-2 lg:grid-cols-4">
        <div
            class="group flex items-center justify-between rounded-md border-b-4 border-blue-600 bg-blue-500 p-3 font-medium text-white shadow-lg dark:border-gray-600 dark:bg-gray-800">
            <div
                class="flex h-14 w-14 transform items-center justify-center rounded-full bg-white transition-all duration-300 group-hover:rotate-12">
                <svg width="30" height="30" fill="none" viewBox="0 0 24 24" stroke="currentColor"
                    class="transform stroke-current text-blue-800 transition-transform duration-500 ease-in-out dark:text-gray-800">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z">
                    </path>
                </svg>
            </div>
            <div class="text-right">
                <p class="text-2xl">{{ $users }}</p>
                <p>Customers</p>
            </div>
        </div>
        <div
            class="group flex items-center justify-between rounded-md border-b-4 border-blue-600 bg-blue-500 p-3 font-medium text-white shadow-lg dark:border-gray-600 dark:bg-gray-800">
            <div
                class="flex h-14 w-14 transform items-center justify-center rounded-full bg-white transition-all duration-300 group-hover:rotate-12">
                <svg width="30" height="30" fill="none" viewBox="0 0 24 24" stroke="currentColor"
                    class="transform stroke-current text-blue-800 transition-transform duration-500 ease-in-out dark:text-gray-800">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M16 11V7a4 4 0 00-8 0v4M5 9h14l1 12H4L5 9z"></path>
                </svg>
            </div>

            <div class="text-right">
                <p class="text-2xl">{{ $ordersNumber }}</p>
                <p>Orders</p>
            </div>
        </div>
        <div
            class="group flex items-center justify-between rounded-md border-b-4 border-blue-600 bg-blue-500 p-3 font-medium text-white shadow-lg dark:border-gray-600 dark:bg-gray-800">
            <div
                class="flex h-14 w-14 transform items-center justify-center rounded-full bg-white transition-all duration-300 group-hover:rotate-12">
                <svg width="30" height="30" fill="none" viewBox="0 0 24 24" stroke="currentColor"
                    class="transform stroke-current text-blue-800 transition-transform duration-500 ease-in-out dark:text-gray-800">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M13 7h8m0 0v8m0-8l-8 8-4-4-6 6"></path>
                </svg>
            </div>
            <div class="text-right">
                <p class="text-2xl">{{ $sales }}€</p>
                <p>Sales</p>
            </div>
        </div>
        <div
            class="group flex items-center justify-between rounded-md border-b-4 border-blue-600 bg-blue-500 p-3 font-medium text-white shadow-lg dark:border-gray-600 dark:bg-gray-800">
            <div
                class="flex h-14 w-14 transform items-center justify-center rounded-full bg-white transition-all duration-300 group-hover:rotate-12">
                {{-- <svg width="30" height="30" fill="none" viewBox="0 0 24 24" stroke="currentColor" class="stroke-current text-blue-800 dark:text-gray-800 transform transition-transform duration-500 ease-in-out"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8c-1.657 0-3 .895-3 2s1.343 2 3 2 3 .895 3 2-1.343 2-3 2m0-8c1.11 0 2.08.402 2.599 1M12 8V7m0 1v8m0 0v1m0-1c-1.11 0-2.08-.402-2.599-1M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg> --}}
                <svg width="30" xmlns="http://www.w3.org/2000/svg"
                    class="transform stroke-current text-blue-800 transition-transform duration-500 ease-in-out dark:text-gray-800"
                    fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M12 8v13m0-13V6a2 2 0 112 2h-2zm0 0V5.5A2.5 2.5 0 109.5 8H12zm-7 4h14M5 12a2 2 0 110-4h14a2 2 0 110 4M5 12v7a2 2 0 002 2h10a2 2 0 002-2v-7" />
                </svg>
            </div>
            <div class="text-right">
                <p class="text-2xl">{{ $products }}</p>
                <p>Products</p>
            </div>
        </div>
    </div>
    <!-- ./Statistics Cards -->

    <div class="grid grid-cols-1 gap-4 p-4 lg:grid-cols-2">

        <!-- Social Traffic -->
        <div
            class="relative mb-4 flex w-full min-w-0 flex-col break-words rounded bg-gray-50 shadow-lg dark:bg-gray-800 lg:mb-0">
            <div class="mb-0 rounded-t border-0 px-0">
                <div class="flex flex-wrap items-center px-4 py-2">
                    <div class="relative w-full max-w-full flex-1 flex-grow">
                        <h3 class="text-base font-semibold text-gray-900 dark:text-gray-50">Τελευταίες Παραγγελίες</h3>
                    </div>
                    <div class="relative w-full max-w-full flex-1 flex-grow text-right">
                        <a href="/manage/orders"
                            class="mr-1 mb-1 rounded bg-blue-500 px-3 py-1 text-xs font-bold uppercase text-white outline-none transition-all duration-150 ease-linear focus:outline-none active:bg-blue-600 dark:bg-gray-100 dark:text-gray-800 dark:active:text-gray-700"
                            type="button">See all</a>
                    </div>
                </div>
                <div class="block w-full overflow-x-auto">

                    <table class="w-full border-collapse items-center bg-transparent">
                        <thead>
                            <tr>
                                <th
                                    class="whitespace-nowrap border border-l-0 border-r-0 border-solid border-gray-200 bg-gray-100 px-4 py-3 text-left align-middle text-xs font-semibold uppercase text-gray-500 dark:border-gray-500 dark:bg-gray-600 dark:text-gray-100">
                                    Number</th>
                                <th
                                    class="whitespace-nowrap border border-l-0 border-r-0 border-solid border-gray-200 bg-gray-100 px-4 py-3 text-left align-middle text-xs font-semibold uppercase text-gray-500 dark:border-gray-500 dark:bg-gray-600 dark:text-gray-100">
                                    Email</th>
                                <th
                                    class="min-w-140-px whitespace-nowrap border border-l-0 border-r-0 border-solid border-gray-200 bg-gray-100 px-4 py-3 text-left align-middle text-xs font-semibold uppercase text-gray-500 dark:border-gray-500 dark:bg-gray-600 dark:text-gray-100">
                                    Price</th>
                                <th
                                    class="min-w-140-px whitespace-nowrap border border-l-0 border-r-0 border-solid border-gray-200 bg-gray-100 px-4 py-3 text-left align-middle text-xs font-semibold uppercase text-gray-500 dark:border-gray-500 dark:bg-gray-600 dark:text-gray-100">
                                    Status</th>
                                <th
                                    class="min-w-140-px whitespace-nowrap border border-l-0 border-r-0 border-solid border-gray-200 bg-gray-100 px-4 py-3 text-left align-middle text-xs font-semibold uppercase text-gray-500 dark:border-gray-500 dark:bg-gray-600 dark:text-gray-100">
                                    Created At</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($lastOrders) > 0)
                                @foreach ($lastOrders as $o)
                                    <tr class="text-gray-700 dark:text-gray-100">
                                        <td
                                            class="whitespace-nowrap border-t-0 border-l-0 border-r-0 p-4 px-4 text-left align-middle text-xs">
                                            {{ $o->order_num }}</td>
                                        <td
                                            class="whitespace-nowrap border-t-0 border-l-0 border-r-0 p-4 px-4 align-middle text-xs">
                                            {{ $o->detail && $o->detail->billing_email != null ? $o->detail->billing_email : null }}
                                        </td>
                                        <td
                                            class="whitespace-nowrap border-t-0 border-l-0 border-r-0 p-4 px-4 align-middle text-xs">
                                            <div class="flex items-center">
                                                <span
                                                    class="mr-2">{{ $o->detail ? $o->detail->total : null }}€</span>

                                            </div>
                                        </td>
                                        <td
                                            class="whitespace-nowrap border-t-0 border-l-0 border-r-0 p-4 px-4 align-middle text-xs">
                                            {{ $o->detail ? $o->detail->status : null }}</td>
                                        <td
                                            class="whitespace-nowrap border-t-0 border-l-0 border-r-0 p-4 px-4 align-middle text-xs">
                                            {{ $o->detail ? $o->created_at : null }}</td>
                                    </tr>
                                @endforeach
                            @endif

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- ./Social Traffic -->

        <!-- Recent Activities -->
        <div class="relative flex w-full min-w-0 flex-col break-words rounded bg-gray-50 shadow-lg dark:bg-gray-800">
            <div class="mb-0 rounded-t border-0 px-0">
                <div class="flex flex-wrap items-center px-4 py-2">
                    <div class="relative w-full max-w-full flex-1 flex-grow">
                        <h3 class="text-base font-semibold text-gray-900 dark:text-gray-50">Νέοι Χρήστες</h3>
                    </div>
                    <div class="relative w-full max-w-full flex-1 flex-grow text-right">
                        <a href="/manage/users"
                            class="mr-1 mb-1 rounded bg-blue-500 px-3 py-1 text-xs font-bold uppercase text-white outline-none transition-all duration-150 ease-linear focus:outline-none active:bg-blue-600 dark:bg-gray-100 dark:text-gray-800 dark:active:text-gray-700"
                            type="button">See all</a>
                    </div>
                </div>
                <div class="block w-full">

                    <table class="w-full border-collapse items-center bg-transparent">
                        <thead>
                            <tr>
                                <th
                                    class="whitespace-nowrap border border-l-0 border-r-0 border-solid border-gray-200 bg-gray-100 px-4 py-3 text-left align-middle text-xs font-semibold uppercase text-gray-500 dark:border-gray-500 dark:bg-gray-600 dark:text-gray-100">
                                    Id</th>
                                <th
                                    class="whitespace-nowrap border border-l-0 border-r-0 border-solid border-gray-200 bg-gray-100 px-4 py-3 text-left align-middle text-xs font-semibold uppercase text-gray-500 dark:border-gray-500 dark:bg-gray-600 dark:text-gray-100">
                                    Όνομα</th>
                                <th
                                    class="min-w-140-px whitespace-nowrap border border-l-0 border-r-0 border-solid border-gray-200 bg-gray-100 px-4 py-3 text-left align-middle text-xs font-semibold uppercase text-gray-500 dark:border-gray-500 dark:bg-gray-600 dark:text-gray-100">
                                    Email</th>
                                <th
                                    class="min-w-140-px whitespace-nowrap border border-l-0 border-r-0 border-solid border-gray-200 bg-gray-100 px-4 py-3 text-left align-middle text-xs font-semibold uppercase text-gray-500 dark:border-gray-500 dark:bg-gray-600 dark:text-gray-100">
                                    Εγγραφή</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($lastUsers) > 0)
                                @foreach ($lastUsers as $u)
                                    <tr class="text-gray-700 dark:text-gray-100">
                                        <td
                                            class="whitespace-nowrap border-t-0 border-l-0 border-r-0 p-4 px-4 text-left align-middle text-xs">
                                            {{ $u->id }}</td>
                                        <td
                                            class="whitespace-nowrap border-t-0 border-l-0 border-r-0 p-4 px-4 text-left align-middle text-xs">
                                            {{ $u->name }} {{ $u->lastname }}</td>
                                        <td
                                            class="whitespace-nowrap border-t-0 border-l-0 border-r-0 p-4 px-4 align-middle text-xs">
                                            {{ $u->email }}</td>

                                        <td
                                            class="whitespace-nowrap border-t-0 border-l-0 border-r-0 p-4 px-4 align-middle text-xs">
                                            {{ $u->created_at }}</td>
                                    </tr>
                                @endforeach
                            @endif

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- ./Recent Activities -->
        </div>


    @endsection
    @section('scripts')
        @parent
    @endsection
