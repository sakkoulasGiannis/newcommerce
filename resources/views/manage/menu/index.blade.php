@extends('layouts.admin')
@section('content')

    <div class="py-4">


        <div class="  max-w-7xl px-4 sm:px-6 lg:px-8">
            <div class="border-b border-gray-200 pb-5 sm:flex sm:items-center sm:justify-between">
                <h3 class="text-base font-semibold leading-6 text-gray-900">ΜΕΝΟΥ</h3>
                <div class="mt-3 sm:ml-4 sm:mt-0">
                    <a href="/manage/menu/create" type="button" class="inline-flex items-center rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">Δημιουργία</a>
                </div>
            </div>

        </div>
        <div class="mt-8 flow-root overflow-hidden">
            <div class="  max-w-7xl px-4 sm:px-6 lg:px-8">
                <table class="w-full text-left">
                    <thead class="bg-white">
                    <tr>
                        <th scope="col" class="relative isolate py-3.5 pr-3 text-left text-sm font-semibold text-gray-900">
                            Όνομα
                            <div class="absolute inset-y-0 right-full -z-10 w-screen border-b border-b-gray-200"></div>
                            <div class="absolute inset-y-0 left-0 -z-10 w-screen border-b border-b-gray-200"></div>
                        </th>
                        <th scope="col" class="relative py-3.5 pl-3">
                            <span class=" ">Ενεργό</span>
                        </th>

                        <th scope="col" class="relative py-3.5 pl-3">
                            <span class=" ">Επεξεργασία</span>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($menus as $m)
                        <tr>
                            <td class="relative py-4 pr-3 text-sm font-medium text-gray-900">
                                {{$m->title}}
                                <div class="absolute bottom-0 right-full h-px w-screen bg-gray-100"></div>
                                <div class="absolute bottom-0 left-0 h-px w-screen bg-gray-100"></div>
                            </td>

                            <td class="relative py-4 pr-3 text-sm font-medium text-gray-900">

                                @if($m->active)
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="size-6">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M9 12.75 11.25 15 15 9.75M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z" />
                                    </svg>
                                @else
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="size-6">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="m9.75 9.75 4.5 4.5m0-4.5-4.5 4.5M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z" />
                                    </svg>

                                @endif
                            </td>


                            <td class="relative py-4 pl-3 text-right text-sm font-medium">
                                <a href="/manage/menu/{{$m->id}}/edit" class="text-indigo-600 hover:text-indigo-900">Επεξεργασία </a>
                            </td>
                        </tr>
                    @endforeach
                    <!-- More people... -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection
@section('scripts')
    @parent

@endsection

