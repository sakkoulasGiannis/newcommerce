@extends('layouts.admin')
@section('content')

    <div class="py-4">


        <div class="  max-w-7xl px-4 sm:px-6 lg:px-8">
            <div class="border-b border-gray-200 pb-5 sm:flex sm:items-center sm:justify-between">
                <h3 class="text-base font-semibold leading-6 text-gray-900">NEO ΜΕΝΟΥ</h3>

            </div>

        </div>
        <div class="mt-8 flow-root overflow-hidden">
            <div class="  max-w-7xl px-4 sm:px-6 lg:px-8">
                <form method="post" action="/manage/menu">
                    @csrf
                    <div class="space-y-12">
                        <div class="border-b border-gray-900/10 pb-12">


                            <div class="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                                <div class="sm:col-span-4">
                                    <label for="title" class="block text-sm font-medium leading-6 text-gray-900">Τίτλος Μενού</label>
                                    <div class="mt-2">
                                        <div class="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                                            <input required type="text" name="title" id="title" autocomplete="username" class="px-4 block flex-1 border-0 bg-transparent py-1.5  text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6" placeholder="Κεντρικό μενού">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                     </div>

                    <div class="mt-6 flex items-center justify-end gap-x-6">
                        <a href="/manage/menu"  class="text-sm font-semibold leading-6 text-gray-900">Ακύρωση</a>
                        <button type="submit" class="rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">Αποθήκευση</button>
                    </div>
                </form>

            </div>
        </div>
    </div>


@endsection
@section('scripts')
    @parent

@endsection

