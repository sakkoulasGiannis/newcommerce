@extends('layouts.admin')
@section('content')
    <div class="m-4 flex flex-col border-2 bg-white">
        <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="inline-block min-w-full py-2 sm:px-6 lg:px-8">
                <div class="text-right">
                    <a href="/manage/pages/create" class="rounded-sm bg-green-500 p-2 text-white">Create</a>
                </div>
                <div class="overflow-hidden">

                    <table class="min-w-full">
                        <thead class="border-b">
                            <tr>
                                <th scope="col" class="px-6 py-4 text-left text-sm font-medium text-gray-900">
                                    #
                                </th>
                                <th scope="col" class="px-6 py-4 text-left text-sm font-medium text-gray-900">
                                    Title
                                </th>
                                <th scope="col" class="px-6 py-4 text-left text-sm font-medium text-gray-900">
                                    Name
                                </th>
                                <th scope="col" class="px-6 py-4 text-left text-sm font-medium text-gray-900">
                                    Edit
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pages as $p)
                                <tr class="border-b">
                                    <td class="whitespace-nowrap px-6 py-4 text-sm font-medium text-gray-900">1</td>
                                    <td class="whitespace-nowrap px-6 py-4 text-sm font-light text-gray-900">
                                        {{ $p->id }}
                                    </td>
                                    <td class="whitespace-nowrap px-6 py-4 text-sm font-light text-gray-900">
                                        {{ $p->title }}
                                    </td>
                                    <td class="whitespace-nowrap px-6 py-4 text-sm font-light text-gray-900">
                                        {{ $p->name }}
                                    </td>
                                    <td class="whitespace-nowrap px-6 py-4 text-sm font-light text-gray-900">
                                        <a href="/manage/pages/{{ $p->id }}/edit">Edit</a>
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
@endsection
