@extends('layouts.admin')
@section('content')




    <div class="m-4 rounded bg-white p-4 shadow">


        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif



        <form method="post" action="{{ route('pages.store') }}">
            @csrf
            <div class="mb-6">
                <label for="title" class="mb-2 block text-sm font-medium text-gray-900 dark:text-gray-300">Title</label>
                <input type="text" id="title" name="title"
                    class="block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 text-sm text-gray-900 focus:border-blue-500 focus:ring-blue-500 dark:border-gray-600 dark:bg-gray-700 dark:text-white dark:placeholder-gray-400 dark:focus:border-blue-500 dark:focus:ring-blue-500"
                    required>
            </div>
            <div class="mb-6">
                <label for="slug" class="mb-2 block text-sm font-medium text-gray-900 dark:text-gray-300">slug</label>
                <input type="text" id="slug" name="slug"
                    class="block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 text-sm text-gray-900 focus:border-blue-500 focus:ring-blue-500 dark:border-gray-600 dark:bg-gray-700 dark:text-white dark:placeholder-gray-400 dark:focus:border-blue-500 dark:focus:ring-blue-500"
                    required>
            </div>

            <textarea id="editor" class="form-textarea mt-1 block w-full" name="body" id="editor" cols="30" rows="40"></textarea>

            <div class="mb-6 flex items-start">
                <div class="flex h-5 items-center">
                    <input id="remember" aria-describedby="remember" type="checkbox" name="is_published" checked
                        class="focus:ring-3 h-4 w-4 rounded border border-gray-300 bg-gray-50 focus:ring-blue-300 dark:border-gray-600 dark:bg-gray-700 dark:ring-offset-gray-800 dark:focus:ring-blue-600">
                </div>
                <div class="ml-3 text-sm">
                    <label for="remember" class="font-medium text-gray-900 dark:text-gray-300">Active</label>
                </div>
            </div>
            <button type="submit"
                class="w-full rounded-lg bg-blue-700 px-5 py-2.5 text-center text-sm font-medium text-white hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800 sm:w-auto">Create</button>
        </form>
    </div>
@endsection
@section('scripts')
    <script>
        document.addEventListener("DOMContentLoaded", function () {
            tinymce.init({
                selector: '#editor',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount code codesample',
                toolbar: 'code undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
                setup: function (editor) {
                    editor.on('init', function () {
                        // Φορτώνει ξανά το αρχικό περιεχόμενο από το textarea
                        editor.setContent(document.getElementById('editor').value);
                    });
                }
            });
        });


    </script>
@endsection
