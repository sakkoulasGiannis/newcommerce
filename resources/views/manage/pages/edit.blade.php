@extends('layouts.admin')
@section('content')
    <a href="/content/{{ $page->slug }}" class="m-4 px-2 py-3 bg-green-400 text-white float-right" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
  <path stroke-linecap="round" stroke-linejoin="round" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
  <path stroke-linecap="round" stroke-linejoin="round" d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z" />
</svg></a>
    <div class="m-4 rounded bg-white p-4 shadow">
        as
        <form method="post" action="{{ route('pages.update', [$page->id]) }}">
            @method('PUT')

            @csrf
            <div class="mb-6">
                <label for="title" class="mb-2 block text-sm font-medium text-gray-900 dark:text-gray-300">Title</label>
                <input type="text" id="title" name="title"
                    class="block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 text-sm text-gray-900 focus:border-blue-500 focus:ring-blue-500 dark:border-gray-600 dark:bg-gray-700 dark:text-white dark:placeholder-gray-400 dark:focus:border-blue-500 dark:focus:ring-blue-500"
                    required value="{{ $page->title }}">
            </div>
            <div class="mb-6">
                <label for="slug" class="mb-2 block text-sm font-medium text-gray-900 dark:text-gray-300">slug</label>
                <input type="text" id="slug" name="slug" value="{{ $page->slug }}"
                    class="block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 text-sm text-gray-900 focus:border-blue-500 focus:ring-blue-500 dark:border-gray-600 dark:bg-gray-700 dark:text-white dark:placeholder-gray-400 dark:focus:border-blue-500 dark:focus:ring-blue-500"
                    required>
            </div>



            <!-- Button to save content -->

            <textarea
                id="editor"
                 class="  mt-1 block w-full" name="body"    >{!! $page->body !!}</textarea>

            <div class="mb-6 flex items-start">
                <div class="flex h-5 items-center">
                    <input id="remember" aria-describedby="remember" type="checkbox" name="is_published"
                        {{ $page->is_published ? 'checked' : null }}
                        class="focus:ring-3 h-4 w-4 rounded border border-gray-300 bg-gray-50 focus:ring-blue-300 dark:border-gray-600 dark:bg-gray-700 dark:ring-offset-gray-800 dark:focus:ring-blue-600">
                </div>
                <div class="ml-3 text-sm">
                    <label for="remember" class="font-medium text-gray-900 dark:text-gray-300">Active</label>
                </div>
            </div>
            <button type="submit"
                class="w-full rounded-lg bg-blue-700 px-5 py-2.5 text-center text-sm font-medium text-white hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800 sm:w-auto">Update</button>
        </form>
    </div>
@endsection
@section('scripts')


    <script>
        document.addEventListener("DOMContentLoaded", function () {
            tinymce.init({
                selector: '#editor',
                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount code codesample',
                toolbar: 'code undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
                setup: function (editor) {
                    editor.on('init', function () {
                        // Φορτώνει ξανά το αρχικό περιεχόμενο από το textarea
                        editor.setContent(document.getElementById('editor').value);
                    });
                }
            });
        });


    </script>
@endsection
