@extends('layouts.admin')
@section('content')
    <div class="py-4">
        <div class="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
        <form method="post" action="/manage/fields">
            @csrf
            <div class="space-y-12">
                <div class="border-b border-gray-900/10 pb-12">
                    <h2 class="text-base font-semibold leading-7 text-gray-900">Δημιουργία νέου πεδίου</h2>
                    <p class="mt-1 text-sm leading-6 text-gray-600"></p>

                    <div class="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                        <div class="sm:col-span-4">
                            <label for="username"
                                   class="block text-sm font-medium leading-6 text-gray-900">Μοναδικός τίτλος ιδιότητας</label>
                            <div class="mt-2">
                                <p class="mb-2 bg-red-500 px-4 py-2 text-white">Χρησιμοποιήστε μόνο αγγλικούς χαρακτήρες και μικρά γράμματα.</p>
                                <div
                                    class="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                                    <input required
                                           type="text" name="name" id="name"
                                           class="block flex-1 border-0 bg-transparent py-1.5 pl-3 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                                           placeholder="size">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mt-6 flex items-center justify-end gap-x-6">
                <button type="submit" class="rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">Δημιουργία</button>
            </div>

        </form>
    </div>
</div>
@endsection
