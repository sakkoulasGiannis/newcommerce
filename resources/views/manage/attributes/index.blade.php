@extends('layouts.admin')
@section('content')

   <div class="py-4">
    <div class="  max-w-7xl px-4 sm:px-6 lg:px-8">
        <div class="sm:flex sm:items-center">
            <div class="sm:flex-auto">
                <h1 class="text-base font-semibold leading-6 text-gray-900">Ιδιότητες</h1>
{{--                <p class="mt-2 text-sm text-gray-700">A list of all the users in your account including their name, title, email and role.</p>--}}
            </div>
            <div class="mt-4 sm:ml-16 sm:mt-0 sm:flex-none">
                <a href="/manage/fields/create" type="button" class="block rounded-md bg-indigo-600 px-3 py-2 text-center text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">Νέα ιδιότητα</a>
            </div>
        </div>
    </div>
    <div class="mt-8 flow-root overflow-hidden">
        <div class="  max-w-7xl px-4 sm:px-6 lg:px-8">
            <table class="w-full text-left">
                <thead class="bg-white">
                <tr>
                    <th scope="col" class="relative isolate py-3.5 pr-3 text-left text-sm font-semibold text-gray-900">
                        Όνομα
                        <div class="absolute inset-y-0 right-full -z-10 w-screen border-b border-b-gray-200"></div>
                        <div class="absolute inset-y-0 left-0 -z-10 w-screen border-b border-b-gray-200"></div>
                    </th>
                    <th scope="col" class="hidden px-3 py-3.5 text-left text-sm font-semibold text-gray-900 sm:table-cell">Εμφάνιση στα φίλτρα</th>
                    <th scope="col" class="hidden px-3 py-3.5 text-left text-sm font-semibold text-gray-900 md:table-cell">Ενεργό</th>
                    <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">Πολλαπλή επιλογή</th>

                    <th scope="col" class="relative py-3.5 pl-3">
                        <span class="sr-only">Edit</span>
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($attributes as $f)
                <tr>
                    <td class="relative py-4 pr-3 text-sm font-medium text-gray-900">
                        {{$f->title}}
                        <div class="absolute bottom-0 right-full h-px w-screen bg-gray-100"></div>
                        <div class="absolute bottom-0 left-0 h-px w-screen bg-gray-100"></div>
                    </td>
                    <td class="hidden px-3 py-4 text-sm text-gray-500 sm:table-cell">
                        @if($f->visible_in_product_listing)
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="size-6">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M9 12.75 11.25 15 15 9.75M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z" />
                            </svg>

                        @else
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="size-6">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M18.364 18.364A9 9 0 0 0 5.636 5.636m12.728 12.728A9 9 0 0 1 5.636 5.636m12.728 12.728L5.636 5.636" />
                            </svg>

                        @endif
                    </td>
                    <td class="hidden px-3 py-4 text-sm text-gray-500 sm:table-cell">
                        @if($f->is_enabled)
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="size-6">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M9 12.75 11.25 15 15 9.75M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z" />
                            </svg>

                        @else
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="size-6">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M18.364 18.364A9 9 0 0 0 5.636 5.636m12.728 12.728A9 9 0 0 1 5.636 5.636m12.728 12.728L5.636 5.636" />
                            </svg>

                        @endif
                    </td>
                    <td class="hidden px-3 py-4 text-sm text-gray-500 sm:table-cell">
                        @if($f->can_select_multiple)
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="size-6">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M9 12.75 11.25 15 15 9.75M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z" />
                            </svg>

                        @else
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="size-6">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M18.364 18.364A9 9 0 0 0 5.636 5.636m12.728 12.728A9 9 0 0 1 5.636 5.636m12.728 12.728L5.636 5.636" />
                            </svg>

                        @endif
                    </td>
                    <td>
                        <a href="/manage/attributes/{{$f->id}}/terms" class="text-indigo-600 hover:text-indigo-900">Τιμές</a>
                    </td>
                     <td class="relative py-4 pl-3 text-right text-sm font-medium">
                        <a href="/manage/attributes/{{$f->id}}/edit" class="text-indigo-600 hover:text-indigo-900">Επεξεργασία </a>
                    </td>
                </tr>
                @endforeach
                <!-- More people... -->
                </tbody>
            </table>
        </div>
    </div>
    </div>


@endsection
@section('scripts')
    @parent

@endsection

