@extends('layouts.admin')

@section('content')

    <div class="row mt-10">
        <div class="col-md-12">
            <ul class="nav justify-content-end">

                <li class="nav-item">
                    <a class="nav-link " href="{{ route('sliders.create') }}"><i class="fa fa-plus fa-lg"
                            aria-hidden="true"></i></a>
                </li>

            </ul>
        </div>
    </div>
    <div class="flex flex-col">
  <div class="overflow-x-auto sm:mx-0.5 lg:mx-0.5">
    <div class="py-2 inline-block min-w-full sm:px-6 lg:px-8">
      <div class="overflow-hidden">
        <table class="min-w-full">
          <thead class="bg-gray-200 border-b">
            <tr>
              <th scope="col" class="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                #
              </th>
              <th scope="col" class="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                Title
              </th>
              <th scope="col" class="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                Created at
              </th>
              <th scope="col" class="text-sm font-medium text-gray-900 px-6 py-4 text-left">
Edit
              </th>
            </tr>
          </thead>
          <tbody>
             @foreach ($sliders as $slide)
            <tr class="bg-white border-b transition duration-300 ease-in-out hover:bg-gray-100">
              <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">{{ $slide->id }}</td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                {{ $slide->title }}
              </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                {{ $slide->created_at }}
              </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
               <a href="{{ route('sliders.edit', $slide->id) }}">Edit</a>
              </td>
            </tr>
             @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@endsection
