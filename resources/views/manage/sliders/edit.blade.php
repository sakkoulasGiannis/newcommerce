@extends('layouts.admin')

@section('content')

    <div class="container-fluid">
        <div class="grid grid-cols-4">
            <div class="p-2">
                <form action="{{ route('sliders.update', $slider->id) }}" method="POST">
                    @csrf
                    @method('PUT')

                    <div class="col-md-8 inline-block">
                        <input type="text" name="title" value="{{ old('title', $slider->title) }}" class="form-control">
                    </div>
                    <div class="col-md-4 inline-block">
                        <button type="submit" class="btn btn-success bg-green-200 px-2 py-2">Update</button>
                    </div>
                </form>

            </div>
        </div>


        <div class="flex flex-col">
            <div class="overflow-x-auto sm:mx-0.5 lg:mx-0.5">
                <div class="py-2 inline-block min-w-full sm:px-6 lg:px-8">
                    <div class="overflow-hidden">
                        <table class="min-w-full">
                            <thead class="bg-gray-200 border-b">
                            <tr>
                                <th scope="col" class="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                                    #
                                </th>
                                <th scope="col" class="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                                    Image
                                </th>
                                <th scope="col" class="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                                    Data
                                </th>
                                <th scope="col" class="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                                    Delete
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($slider->images as $img)
                                <tr class="bg-white border-b transition duration-300 ease-in-out hover:bg-gray-100">

                                    <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">{{ $img->id }}</td>

                                    <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                                        <img src="{{ Storage::url($img->url) }}" class="img-responsive" width="150">
                                    </td>


                                    <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                                        <form action="{{ route('slider.image.update', $img->id) }}" method="POST">
                                            @csrf
                                            @method('PUT')

                                            <ul>
                                                <li class="inline-block">
                                                    <input type="text" name="alt" value="{{ old('alt', $img->alt) }}" class="form-control" placeholder="Alt Text">
                                                </li>
                                                <li class="inline-block">
                                                    <input type="text" name="href" value="{{ old('href', $img->href) }}" class="form-control" placeholder="Link">
                                                </li>
                                                <li class="inline-block">
                                                    <input type="text" name="order_num" value="{{ old('order_num', $img->order_num) }}" class="form-control" placeholder="Order">
                                                </li>
                                                <li class="inline-block">
                                                    <button type="submit" class="btn btn-success form-control">Update</button>
                                                </li>
                                            </ul>
                                        </form>
                                    </td>

                                    <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">

                                        <form action="{{ route('slider.image.destroy', $img->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-alert">Delete</button>
                                        </form>

                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>


    <div class="row mt-4 bg-gray-100 p-4 w-3/4">


        <form action="{{ route('sliders.addPhoto', $slider->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="grid grid-cols-3 gap-4">
                <div class="col-md-5">
                    <input type="file" name="url" class="form-control">
                </div>
                <div class="col-md-5">
                    <div class="grid grid-cols-2 gap-2">
                        <div class="col-md-6">
                            <input type="text" name="alt" class="form-control" placeholder="Alt">
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="href" class="form-control" placeholder="Url">
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="submit" class="form-control bg-green-500 px-4 py-2 text-white">Submit</button>
                </div>
            </div>
        </form>


    </div>

    {{-- <form method="post" action="{{route('sliders.addPhoto', $slider->id)}}" class="dropzone"> --}}
    {{-- {{csrf_field()}} --}}
    {{-- </form> --}}
@endsection

@section('page_scripts')
    <script src="{{ asset('assets/manage/js/dropzone.js') }}" type="text/javascript"></script>

    <script>
        var myDropzone = new Dropzone(".dropzone", {});
    </script>
@endsection
