@extends('layouts.admin')
@section('content')
    <div class="mt-8 flow-root overflow-hidden">
        <table class="min-w-full divide-y divide-gray-200">
            <thead class="bg-gray-50">
            <tr>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Title
                </th>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Description
                </th>
{{--                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">--}}
{{--                    Actions--}}
{{--                </th>--}}
            </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">


            @foreach($payments as $p)
                <tr x-data="{toggleSettings:false}">
                    <td class="px-6 py-4 whitespace-nowrap">
                        <div class="text-sm font-medium text-gray-900">{{$p->title}}</div>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        <div class="text-sm text-gray-500">{{$p->description}}</div>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap text-sm font-medium">


{{--                        <button x-on:click="toggleSettings = !toggleSettings"--}}
{{--                                class="text-indigo-600 hover:text-indigo-900">--}}
{{--                            Edit--}}
{{--                        </button>--}}
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="px-6 py-4 whitespace-nowrap">
                        @switch($p->id)
                            @case(1)
                                @break
                            @case(2)

{{--                                <form action="/manage/settings/store" method="post">--}}
{{--                                    @csrf--}}
{{--                                    <input type="hidden" name="encode" value="true">--}}
{{--                                    <input type="hidden" name="group" value="payments">--}}
{{--                                    <input type="hidden" name="key" value="alphabank_credit_card">--}}
{{--                                    <div class="bg-gray-100 p-4 rounded-md">--}}
{{--                                        <h3 class="text-lg font-medium text-gray-900"> {{$p->title}}</h3>--}}
{{--                                        <p class="mt-2 text-sm text-gray-500">Here you can manage the settings for this--}}
{{--                                            payment method.</p>--}}

{{--                                        <div--}}
{{--                                            class="mt-10 space-y-8 border-b   pb-12 sm:space-y-0 sm:divide-y sm:divide-gray-900/10 sm:border-t sm:pb-0">--}}
{{--                                            <div class="sm:grid sm:grid-cols-3 sm:items-start sm:gap-4 sm:py-6">--}}
{{--                                                <label for="alphabank_active"--}}
{{--                                                       class="block text-sm font-medium leading-6 text-gray-900 sm:pt-1.5">Ενεργό</label>--}}
{{--                                                <div class="mt-2 sm:col-span-2 sm:mt-0">--}}
{{--                                                    <input type="hidden" name="alphabank_active" value="0">--}}

{{--                                                    <input id="alphabank_active"--}}
{{--                                                           value="{{(isset($settings[$p->name][0]['value']['alphabank_active']) && $settings[$p->name][0]['value']['alphabank_active'])?1:0}}"--}}
{{--                                                           aria-describedby="comments-description"--}}
{{--                                                           {{(isset($settings[$p->name][0]['value']['alphabank_active']) && $settings[$p->name][0]['value']['alphabank_active']) ?'checked':''}}--}}
{{--                                                           name="alphabank_active" type="checkbox"--}}
{{--                                                           class="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600">--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            <div class="sm:grid sm:grid-cols-3 sm:items-start sm:gap-4 sm:py-6">--}}
{{--                                                <label for="alphabank_test_env"--}}
{{--                                                       class="block text-sm font-medium leading-6 text-gray-900 sm:pt-1.5">Δοκιμαστικό--}}
{{--                                                    περιβάλλον</label>--}}
{{--                                                <div class="mt-2 sm:col-span-2 sm:mt-0">--}}
{{--                                                    <input type="hidden" name="alphabank_test_env" value="0">--}}

{{--                                                    <input id="alphabank_test_env"--}}
{{--                                                           value="{{($settings[$p->name][0]['value']['alphabank_test_env'])?1:0}}"--}}
{{--                                                           {{($settings[$p->name][0]['value']['alphabank_test_env']) ?'checked':''}}--}}
{{--                                                           name="alphabank_test_env" type="checkbox"--}}
{{--                                                           class="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600">--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            <div class="sm:grid sm:grid-cols-3 sm:items-start sm:gap-4 sm:py-6">--}}
{{--                                                <label for="alphabank_mid"--}}
{{--                                                       class="block text-sm font-medium leading-6 text-gray-900 sm:pt-1.5">Merchant--}}
{{--                                                    id (mid)</label>--}}
{{--                                                <div class="mt-2 sm:col-span-2 sm:mt-0">--}}
{{--                                                    <input type="text" name="alphabank_mid" id="alphabank_mid"--}}
{{--                                                           value="{{$settings[$p->name][0]['value']['alphabank_mid']}}"--}}
{{--                                                           class="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:max-w-xs sm:text-sm sm:leading-6">--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            <div class="sm:grid sm:grid-cols-3 sm:items-start sm:gap-4 sm:py-6">--}}
{{--                                                <label for="alphabank_secret"--}}
{{--                                                       class="block text-sm font-medium leading-6 text-gray-900 sm:pt-1.5">Shared--}}
{{--                                                    Secret</label>--}}
{{--                                                <div class="mt-2 sm:col-span-2 sm:mt-0">--}}
{{--                                                    <input type="text" name="alphabank_secret" id="alphabank_secret"--}}
{{--                                                           value="{{$settings[$p->name][0]['value']['alphabank_secret']}}"--}}
{{--                                                           class="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:max-w-xs sm:text-sm sm:leading-6">--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            <div class="sm:grid sm:grid-cols-3 sm:items-start sm:gap-4 sm:py-6">--}}
{{--                                                <label for="alphabank_pre_authorize"--}}
{{--                                                       class="block text-sm font-medium leading-6 text-gray-900 sm:pt-1.5">Pre-Authorize</label>--}}
{{--                                                <div class="mt-2 sm:col-span-2 sm:mt-0">--}}
{{--                                                    <input type="hidden" name="alphabank_pre_authorize" value="0">--}}

{{--                                                    <input id="alphabank_pre_authorize"--}}
{{--                                                           value="{{$settings[$p->name][0]['value']['alphabank_pre_authorize']}}"--}}
{{--                                                           aria-describedby="comments-description"--}}

{{--                                                           name="alphabank_pre_authorize" type="checkbox"--}}
{{--                                                           class="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600">--}}

{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            <div class="sm:grid sm:grid-cols-3 sm:items-start sm:gap-4 sm:py-6">--}}
{{--                                                <label for="alphabank_max_installments"--}}
{{--                                                       class="block text-sm font-medium leading-6 text-gray-900 sm:pt-1.5">Max--}}
{{--                                                    Installments</label>--}}
{{--                                                <div class="mt-2 sm:col-span-2 sm:mt-0">--}}
{{--                                                    <input type="text" name="alphabank_max_installments"--}}
{{--                                                           value="{{$settings[$p->name][0]['value']['alphabank_max_installments']}}"--}}
{{--                                                           id="alphabank_max_installments"--}}
{{--                                                           class="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:max-w-xs sm:text-sm sm:leading-6">--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            <div class="sm:grid sm:grid-cols-3 sm:items-start sm:gap-4 sm:py-6">--}}
{{--                                                <label for="alphabank_installments"--}}
{{--                                                       class="block text-sm font-medium leading-6 text-gray-900 sm:pt-1.5">Installments</label>--}}
{{--                                                <div class="mt-2 sm:col-span-2 sm:mt-0">--}}
{{--                                                <textarea type="text" name="alphabank_installments"--}}
{{--                                                          id="alphabank_installments"--}}
{{--                                                          class="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:max-w-xs sm:text-sm sm:leading-6">--}}
{{--{{$settings[$p->name][0]['value']['alphabank_installments']}}--}}
{{--                                                </textarea>--}}

{{--                                                    <p class="text-sm text-gray-500"><br><br>Set rules of installments.--}}
{{--                                                        Please put one rule per line.<br><strong>Rule--}}
{{--                                                            format</strong><br>"{amount}-{amount}":"{min_installments} ---}}
{{--                                                        {max_installments}",<br>"{amount}-{amount}":"{installments}"<br><br><strong>Example--}}
{{--                                                            (with range)</strong><br>"0-100":"2-4", (From 0 to 100 cart--}}
{{--                                                        total, 2 to 4 installments)<br>"101-300":"5-8" (From 101 to 300--}}
{{--                                                        cart total, 5 to 8 installments)<br><br><strong>Example (without--}}
{{--                                                            range)</strong><br>"0-100":"4", (From 0 to 100 cart total, 4--}}
{{--                                                        installments)<br>"101-300":"8" (From 101 to 300 cart total, 8--}}
{{--                                                        installments)<br><br><strong>Important:</strong> If cart total--}}
{{--                                                        is not in range, max installments will be used<br><strong>Important:</strong>--}}
{{--                                                        Please do not leave any extra spaces.</p>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <button>Submit</button>--}}
{{--                                </form>--}}
                                @break
                            @default

                                @break
                        @endswitch
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>


    </div>
@endsection
@section('scripts')
    @parent

@endsection

