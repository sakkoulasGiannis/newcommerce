@extends('layouts.admin')
@section('content')
    <!-- main content -->
    <form class="grid grid-cols-12 gap-2" action="/manage/users/{{ $user->id }}" method="post">
        @csrf
@method('PUT')

        <div class="col-span-12 md:col-span-4">
            <div class="my-2 rounded-md border bg-white px-2 py-4">
                <h3 class="p-2">Επεξεργασία {{ $user->name }}</h3>
                <div class="flex justify-between px-2">
                    <div class="w-full max-w-sm">
                        <div class="mb-6 md:flex md:items-center">
                            <div class="md:w-1/3">
                                <label class="mb-1 block pr-4 font-bold text-gray-500 md:mb-0 md:text-right"
                                    for="inline-full-name">
                                    Όνομα
                                </label>
                            </div>
                            <div class="md:w-2/3">
                                <input name="name"
                                    class="w-full appearance-none rounded border-2 border-gray-200 bg-gray-200 py-2 px-4 leading-tight text-gray-700 focus:border-purple-500 focus:bg-white focus:outline-none"
                                    id="inline-full-name" type="text" value="{{ $user->name }}">
                                @error('name')
                                    <div class="text-red-500">{{ $message }}</div>
                                @enderror

                            </div>
                        </div>
                        <div class="mb-6 md:flex md:items-center">
                            <div class="md:w-1/3">
                                <label class="mb-1 block pr-4 font-bold text-gray-500 md:mb-0 md:text-right" for="email">
                                    Email
                                </label>
                            </div>
                            <div class="md:w-2/3">
                                <input name="email" value="{{ $user->email }}"
                                    class="w-full appearance-none rounded border-2 border-gray-200 bg-gray-200 py-2 px-4 leading-tight text-gray-700 focus:border-purple-500 focus:bg-white focus:outline-none"
                                    id="email" type="email">
                                @error('email')
                                    <div class="text-red-500">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>



                        <div class="mb-6 md:flex md:items-center">

                            <div class="md:w-1/3">
                                <label class="mb-1 block pr-4 font-bold text-gray-500 md:mb-0 md:text-right" for="password">
                                    Κωδικός
                                </label>
                            </div>
                            <div class="md:w-2/3">

                                <input name="password"
                                    class="w-full appearance-none rounded border-2 border-gray-200 bg-gray-200 py-2 px-4 leading-tight text-gray-700 focus:border-purple-500 focus:bg-white focus:outline-none"
                                    id="password" type="password">
                                @error('password')
                                    <div class="text-red-500">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="mb-6 md:flex md:items-center">
                            <div class="md:w-1/3"></div>
                            <label class="block text-gray-500 md:w-2/3">
                                <input class="mr-2 leading-tight" type="checkbox" name="updatePassword">
                                {{ __('general.Update Password') }}
                            </label>
                        </div>
                        <div class="mb-6 md:flex md:items-center">
                            <div class="md:w-1/3"></div>
                            <label class="block text-gray-500 md:w-2/3">
                                <input class="mr-2 leading-tight" type="checkbox">
                                {{ __('general.Ιnform user by email') }}
                            </label>
                        </div>

                    </div>

                </div>
            </div>
            <div class="my-2 rounded-md border bg-white px-2 py-4">

                <h2>Ρόλος</h2>
                <select name="role" id="role" class="w-full">
                    @foreach ($roles as $r)
                        <option value="{{ $r->name }}" {{ $user->hasRole($r->name) ? 'selected' : null }}>
                            {{ $r->name }}</option>
                    @endforeach
                </select>

            </div>

            <div class="flex-0">
                <button
                    class="focus:shadow-outline rounded bg-purple-500 py-2 px-4 font-bold text-white shadow hover:bg-purple-400 focus:outline-none"
                    type="submit">
                    Ενημέρωση
                </button>
            </div>
        </div>



    </form>
@endsection
@section('scripts')
@endsection
