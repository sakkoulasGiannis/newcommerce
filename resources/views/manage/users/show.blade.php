@extends('layouts.admin')
@section('content')

    <show-model entity="users" :headers="{{json_encode([
        ['title'=>'Id', 'value'=>'id'],
        ['title'=>'Name', 'value'=>'name'],
        ['title'=>'Last Name', 'value'=>'lastname'],
        ['title'=>'Email', 'value'=>'email'],
        ['title'=>'Email Verified at', 'value'=>'email_verified_at'],
        ['title'=>'Phone', 'value'=>'phone'],
        ['title'=>'Mobile', 'value'=>'mobile'],
        ['title'=>'Is Subscribed', 'value'=>'subscribed'],
        ['title'=>'Newsletter', 'value'=>'newsletter'],

    ])}}" :data="{{$user->toJson()}}"></show-model>

@endsection
@section('scripts')
    @parent

@endsection

