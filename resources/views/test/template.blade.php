<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mega Menu with Pure.CSS</title>
    <link rel="stylesheet" href="https://unpkg.com/purecss@2.0.6/build/pure-min.css">
    <style>


        ul {
            padding-left: 0;
            margin-top: 0;
            margin-bottom: 0;
            list-style: none;
        }

        nav {
            background: #fff;
            font-size: 0;
            position: relative;
        }

        nav > ul {
            display: flex;
            justify-content: center;
        }

        nav > ul > li {
            display: inline-block;
            font-size: 14px;
            padding: 0 15px;
            position: relative;
        }

        nav > ul > li > a {
            color: #222;
            display: block;
            padding: 20px 0;
            border-bottom: 3px solid transparent;
            transition: all .3s ease;
        }
        nav > ul > li:hover > a {
            color: #444;
            border-bottom: 3px solid #444;
        }

        .mega-menu {
            background: #eee;
            visibility: hidden;
            opacity: 0;
            transition: visibility 0s, opacity 0.5s linear;
            position: absolute;
            left: 0;
            width: 100%;
            padding-bottom: 20px;
        }
        .mega-menu h3 {color: #444;}

        .mega-menu .nav-container {
            display: flex;
            flex-wrap: wrap;
        }
        .mega-menu .nav-item {
            flex-grow: 1;
            margin: 0 10px;
            box-sizing: border-box;
        }
        .mega-menu .nav-item img {
            width: 100%;
        }
        .mega-menu a {
            border-bottom: 1px solid #ddd;
            color: #4ea3d8;
            display: block;
            padding: 10px 0;
        }
        .mega-menu a:hover {color: #2d6a91;}

        .dropdown {position: static;}

        .dropdown:hover .mega-menu {
            visibility: visible;
            opacity: 1;
        }

        /* Mobile Toggle Button */
        .menu-toggle, .menu-close {
            display: none;
            font-size: 24px;
            color: white;
            background-color: #0ca0d6;
            border: none;
            padding: 15px;
            cursor: pointer;
        }

        .menu-close {
            text-align: right;
            padding-right: 20px;
            font-size: 18px;
            color: white;
        }
        .menu-toggle, .menu-close {
            display: none; /* Κρυφό σε μεγαλύτερες οθόνες */
        }



        /* Responsive Styling */
        @media (max-width: 768px) {
            .menu-toggle, .menu-close {
                display: block; /* Ορατό μόνο σε οθόνες μικρότερες των 768px */
            }
            nav {
                position: absolute;
                left: -80%;
                width: 80%;
                top: 0;
                height: 100%;
                background: #0ca0d6;
                transition: left 0.3s ease;
            }

            nav.open {
                left: 0;
            }

            nav > ul {
                display: block;
                padding: 0;
            }

            nav > ul > li {
                display: block;
                text-align: left;
                padding: 0;
            }

            nav > ul > li > a {
                padding: 15px;
                border-bottom: 1px solid #fff;
            }

            .mega-menu {
                position: static;
                opacity: 1;
                visibility: hidden;
                padding: 0;
                max-height: 0;
                overflow: hidden;
                transition: max-height 0.3s ease;
            }

            .mega-menu.open {
                visibility: visible;
                max-height: 500px; /* Αρκετό για να χωρέσει το περιεχόμενο */
                padding: 20px 0;
            }

            .mega-menu .nav-container {
                display: block;
            }

            .mega-menu .nav-item {
                margin-bottom: 20px;
            }

            .mega-menu a {
                padding: 10px 0;
            }

            .menu-toggle {
                display: block;
            }
        }
    </style>
</head>
<body>

<button class="menu-toggle">☰</button>

<nav id="nav-menu">
    <button class="menu-close">✖</button>
    <ul class="nav-container">
        <li><a href='#'>Home</a></li>
        <li class='dropdown'>
            <a href='#' class="dropdown-toggle">Pages <i class="fa fa-angle-down"></i></a>
            <div class='mega-menu'>
                <div class="nav-container">
                    <div class="nav-item">
                        <h3>Home Page</h3>
                        <ul>
                            <li><a href='#'>Home Page No #1</a></li>
                            <li><a href='#'>Home Page No #2</a></li>
                            <li><a href='#'>Home Page No #3</a></li>
                            <li><a href='#'>Home Page No #4</a></li>

                        </ul>
                    </div> <!-- /.nav-item -->
                    <div class="nav-item">
                        <h3>Contact</h3>
                        <ul>
                            <li><a href='#'>Contact Page No #1</a></li>
                            <li><a href='#'>Contact Page No #2</a></li>
                            <li><a href='#'>Contact Page No #3</a></li>
                            <li><a href='#'>Contact Page No #4</a></li>
                            <li><a href='#'>Contact Page No #5</a></li>
                        </ul>
                    </div> <!-- /.nav-item -->
                    <div class="nav-item">
                        <h3>Portfolio</h3>
                        <ul>
                            <li><a href='#'>Portfolio Page No #1</a></li>
                            <li><a href='#'>Portfolio Page No #2</a></li>
                            <li><a href='#'>Portfolio Page No #3</a></li>
                        </ul>
                    </div> <!-- /.nav-item -->
                    <div class="nav-item">
                        <h3>Custom</h3>
                        <img src="http://placehold.it/250x200">
                    </div> <!-- /.nav-item -->
                </div><!-- /.nav-container -->
            </div><!-- /.mega-menu -->
        </li><!-- /.dropdown -->
        <li class='dropdown'>
            <a href='#' class="dropdown-toggle">Images <i class="fa fa-angle-down"></i> </a>
            <div class='mega-menu'>
                <div class="nav-container">
                    <div class="nav-item">
                        <h3>Image One</h3>
                        <img src="http://placehold.it/250x200">
                    </div> <!-- /.nav-item -->

                    <div class="nav-item">
                        <h3>Image One</h3>
                        <img src="http://placehold.it/250x200">
                    </div> <!-- /.nav-item -->

                    <div class="nav-item">
                        <h3>Image One</h3>
                        <img src="http://placehold.it/250x200">
                    </div> <!-- /.nav-item -->

                    <div class="nav-item">
                        <h3>Image One</h3>
                        <img src="http://placehold.it/250x200">
                    </div> <!-- /.nav-item -->

                    <div class="nav-item">
                        <h3>Image One</h3>
                        <img src="http://placehold.it/250x200">
                    </div> <!-- /.nav-item -->

                    <div class="nav-item">
                        <h3>Image One</h3>
                        <img src="http://placehold.it/250x200">
                    </div> <!-- /.nav-item -->

                </div><!-- .nav-container -->
            </div><!-- .mega-menu-->
        </li><!-- .dropdown -->
        <li><a href='#'>About</a></li>
        <li><a href='#'>Contact</a></li>
    </ul><!-- .nav-container -->
</nav>

<script>
    const toggleButton = document.querySelector('.menu-toggle');
    const closeButton = document.querySelector('.menu-close');
    const navMenu = document.getElementById('nav-menu');
    const dropdownToggles = document.querySelectorAll('.dropdown-toggle');

    // Toggle the nav menu
    toggleButton.addEventListener('click', () => {
        navMenu.classList.toggle('open');
    });

    // Close the nav menu
    closeButton.addEventListener('click', () => {
        navMenu.classList.remove('open');
    });

    // Toggle the mega menus on click
    dropdownToggles.forEach(toggle => {
        toggle.addEventListener('click', (e) => {
            e.preventDefault(); // Αποφυγή της προεπιλεγμένης ενέργειας του συνδέσμου

            const megaMenu = toggle.nextElementSibling;

            // Κλείσε όλα τα άλλα ανοιχτά μενού
            document.querySelectorAll('.mega-menu.open').forEach(menu => {
                if (menu !== megaMenu) {
                    menu.classList.remove('open');
                }
            });

            // Εναλλαγή της προβολής του επιλεγμένου μενού
            megaMenu.classList.toggle('open');
        });
    });

//     close mobile menu on click out

    // Close the nav menu if clicking outside of it
    document.addEventListener('click', (event) => {
        if (!navMenu.contains(event.target) && !toggleButton.contains(event.target)) {
            navMenu.classList.remove('open');
        }
    });

    // toggle mobile menu ον gestures
    let startX = 0;
    let endX = 0;

    // Αναγνωριστικό Gesture για touchstart
    document.addEventListener('touchstart', (event) => {
        startX = event.touches[0].clientX;
    });

    // Αναγνωριστικό Gesture για touchmove
    document.addEventListener('touchmove', (event) => {
        endX = event.touches[0].clientX;
    });

    // Αναγνωριστικό Gesture για touchend
    document.addEventListener('touchend', () => {
        const swipeDistance = endX - startX;

        // Αν το swipe είναι από αριστερά προς τα δεξιά (swipe δεξιά)
        if (swipeDistance > 50) {
            // Άνοιγμα μενού εάν το swipe είναι δεξιά
            navMenu.classList.add('open');
        }

        // Αν το swipe είναι από δεξιά προς τα αριστερά (swipe αριστερά)
        if (swipeDistance < -50) {
            // Κλείσιμο μενού εάν το swipe είναι αριστερά
            navMenu.classList.remove('open');
        }
    });

</script>

</body>
</html>
