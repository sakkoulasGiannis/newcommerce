import { defineStore } from 'pinia';

export const useSelectedItemIdStore = defineStore('selectedItemId', {
    state: () => ({
        selectedItemId: null
    }),
    actions: {
        setSelectedItemId(id) {
            this.selectedItemId = id;
        },
        getSelectedItemId() {
            return this.selectedItemId;
        }
    }
});
