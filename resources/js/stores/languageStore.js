import { defineStore } from 'pinia';
import axios from 'axios';

export const useLanguageStore = defineStore('language', {
    state: () => ({
        languages: [],
    }),
    actions: {
        async fetchLanguages() {
            try {
                const response = await axios.get('/manage/get-languages-list');
                this.languages = response.data.languages;
            } catch (error) {
                console.error('Error loading languages:', error);
            }
        },
    },
});
