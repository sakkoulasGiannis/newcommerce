/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import {createApp} from 'vue';
import { createPinia } from 'pinia';
import axios from 'axios';
import { useLanguageStore } from './stores/languageStore';
import vClickOutside from "click-outside-vue3"
require('process');
import 'process';
window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';



let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

const pinia = createPinia();
const app = createApp({})
app.use(pinia);

app.component('product-index', require('./components/products/index.vue').default);
app.component('product-create', require('./components/products/create.vue').default);
app.component('product-edit', require('./components/products/edit.vue').default);
app.component('product-filter', require('./components/products/filter.vue').default);
app.component('rules-index', require('./components/rules/index.vue').default);
app.component('rules-create', require('./components/rules/create.vue').default);
app.component('courier-create', require('./components/couriers/create.vue').default);
app.component('vouchers-index', require('./components/vouchers/index.vue').default);
app.component('vouchers-create', require('./components/vouchers/create.vue').default);
// app.component('treeview', require('./components/elements/treeView').default);
app.component('inventory', require('./components/elements/inventory').default);
app.component('tree', require('./components/elements/Tree').default);
app.component('categoryTree', require('./components/elements/categoryTree').default);
app.component('Table', require('./components/elements/datatable').default);
app.component('nodeTree', require('./components/elements/NodeTree').default);
app.component('ordersEdit', require('./components/orders/edit').default);
app.component('orders', require('./components/orders/index').default);
app.component('ordersCreate', require('./components/orders/create').default);
app.component('ordersShow', require('./components/orders/show').default);
app.component('orders-filter', require('./components/orders/filter').default);
app.component('fields', require('./components/fields/index').default);
app.component('terms', require('./components/terms/terms').default);
app.component('myQueryBuilder', require('./components/elements/myQueryBuilder').default);
app.component('OrangeQueryBuilder', require('./components/elements/OrangeQueryBuilder').default);
app.component('panel', require('./components/elements/panel').default);
app.component('unOpenedOrders', require('./components/elements/unOpenedOrders').default);
//users
app.component('users-index', require('./components/users/index.vue').default);
app.component('users-create', require('./components/users/create.vue').default);
app.component('users-edit', require('./components/users/edit.vue').default);
app.component('modules', require('./components/modules/index.vue').default);
// end users
// redirects
app.component('redirects-index', require('./components/redirects/index.vue').default);
//end redirects
app.component('shipping-payment-rule', require('./components/elements/shipping-payment-rule').default);
//categories
app.component('categories-index', require('./components/categories/index.vue').default);
app.component('menu-builder', require('./components/categories/menu.vue').default);
app.component('categories-edit', require('./components/categories/edit.vue').default);
//end categories
app.component('inputText', require('./components/elements/fields/textfield').default);
app.component('inputSelect', require('./components/elements/fields/selectfield').default);
// app.component('multipleSelect', require('./components/elements/fields/multiselectfield').default);
app.component('searchField', require('./components/elements/fields/selectfieldswithearch').default)
app.component('show-model', require('./components/elements/showModel').default);
//general
app.component('admin-search', require('./components/elements/adminSearch').default);
// app.component('shippingPaymentRule', require('./components/elements/shipping_payment_rule').default);
app.component('builder', require('./components/builder/index').default)
app.component('builderItem', require('./components/builder/item').default)

app.component('theme-index', require('./components/theme/index').default)


app.config.globalProperties.$languages = ["el", "en"];
app.use(vClickOutside)

const languageStore = useLanguageStore();

languageStore.fetchLanguages().then(() => {
    app.mount("#app");
}).catch(error => {
    app.mount("#app"); // Στην περίπτωση που αποτύχει η φόρτωση, συνεχίζουμε με το mount της εφαρμογής
    console.error('Error loading languages:', error);
});

// app.mount("#app");


// require('./bootstrap');
