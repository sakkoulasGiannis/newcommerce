import {
    createApp
} from 'vue';
import {
    axios
} from 'axios'

import mitt from 'mitt'
import vClickOutside from "click-outside-vue3"

window.axios = require('axios');
const eventBus = mitt()

let frontApp = createApp({})


frontApp.component('searchSimple', require('./Pages/searchSimple').default);
frontApp.component('search', require('./Pages/search').default);
frontApp.component('searchResults', require('./Pages/searchResults').default);
frontApp.component('checkout', require('./Pages/checkout').default);
frontApp.component('filters', require('./Pages/filters').default);

frontApp.config.globalProperties.eventBus = eventBus

frontApp.use(vClickOutside)

frontApp.mount("#frontapp");
